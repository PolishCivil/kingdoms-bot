package pl.polishcivil.fourkingdoms.auth

import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit

data class AuthData(val usernames: List<String>, val validUntil: Long) {
    fun isValid() = System.currentTimeMillis() < validUntil

    fun validUntilDate() = Date(validUntil)
}

object Authentication {
    val key = intArrayOf(535325, 535435, 5345767, 3123123)

    enum class AuthResult {
        EXPIRED, VALID, NOT_AVAILABLE, ERROR
    }

    fun generateHash(data: AuthData): String {
        val text = "users = ${data.usernames.joinToString(separator = ",")}\nvalid = ${data.validUntil}"
        val textData = text.toByteArray()
        val textDataEncrypted = ByteBuffer.wrap(textData)
        Xtea.encipher(textDataEncrypted, 0, textData.size, key)
        return Base64.getEncoder().encodeToString(textDataEncrypted.array())
    }

    fun encodeHash(hash: String): AuthData {
        if (hash == "") {
            return AuthData(emptyList(), 0)
        }
        try {
            val decodedBase64 = Base64.getDecoder().decode(hash)
            val encryptedHash = ByteBuffer.wrap(decodedBase64)
            Xtea.decipher(encryptedHash, 0, decodedBase64.size, key)
            val text = encryptedHash.array().toString(Charset.forName("UTF-8"))
            val lines = text.lines()

            val usernames = lines[0].split("users = ")[1].split(",")
            val validUntil = lines[1].split("valid = ")[1].toLong()

            return AuthData(usernames, validUntil)
        } catch (e: Exception) {
            return AuthData(emptyList(), 0)
        }
    }

}

fun main(args: Array<String>) {
    val threeDaysFromNow = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(3)

    val accounts = listOf(listOf("Ania34"),listOf("player1525817819198583"),listOf("Holigans"), listOf("yobbo"), listOf("polishcivil"), listOf("leszek34"), listOf("Hamer"), listOf("Greatseal"), listOf("Asdfikqw"), listOf("Łukaszszszszsz"), listOf("Kermit st200"), listOf("lady julia9"))
    for (account in accounts) {
        val data = AuthData(account, threeDaysFromNow)
        val hash = Authentication.generateHash(data)
        val valid = Authentication.encodeHash(hash) == data
        if (!valid) throw RuntimeException("Not valid!")
        println("account = $account hash = $hash")
    }

}