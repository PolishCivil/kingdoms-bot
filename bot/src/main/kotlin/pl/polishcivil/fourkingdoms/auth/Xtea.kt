package pl.polishcivil.fourkingdoms.auth

import java.nio.ByteBuffer

/**
 * An implementation of the XTEA block cipher.
 * @author Graham
 * *
 * @author `Discardedx2
 */
object Xtea {

    /**
     * The golden ratio.
     */
    val GOLDEN_RATIO = 0x9E3779B9.toInt()

    /**
     * The number of rounds.
     */
    val ROUNDS = 32

    /**
     * Deciphers the specified [java.nio.ByteBuffer] with the given key.
     * @param buffer The buffer.
     * *
     * @param key The key.
     * *
     * @throws IllegalArgumentException if the key is not exactly 4 elements
     * * long.
     */
    fun decipher(buffer: ByteBuffer, key: IntArray) {
        if (key.size != 4)
            throw IllegalArgumentException()

        var i = 0
        while (i < buffer.limit()) {
            var sum = GOLDEN_RATIO * ROUNDS
            var v0 = buffer.getInt(i * 4)
            var v1 = buffer.getInt(i * 4 + 4)
            for (j in 0..ROUNDS - 1) {
                v1 = (v0 shl 4 xor (v0 shr 5)) + v0 xor sum + key[sum shr 11 and 3]
                sum -= GOLDEN_RATIO
                v0 = (v1 shl 4 xor (v1 shr 5)) + v1 xor sum + key[sum and 3]
            }
            buffer.putInt(i * 4, v0)
            buffer.putInt(i * 4 + 4, v1)
            i += 8
        }
    }

    /**
     * Enciphers the specified [java.nio.ByteBuffer] with the given key.
     * @param buffer The buffer.
     * *
     * @param key The key.
     * *
     * @throws IllegalArgumentException if the key is not exactly 4 elements
     * * long.
     */
    fun encipher(buffer: ByteBuffer, key: IntArray) {
        if (key.size != 4)
            throw IllegalArgumentException()

        var i = 0
        while (i < buffer.limit()) {
            var sum = 0
            var v0 = buffer.getInt(i * 4)
            var v1 = buffer.getInt(i * 4 + 4)
            for (j in 0..ROUNDS - 1) {
                v0 = (v1 shl 4 xor (v1 shr 5)) + v1 xor sum + key[sum and 3]
                sum += GOLDEN_RATIO
                v1 = (v0 shl 4 xor (v0 shr 5)) + v0 xor sum + key[sum shr 11 and 3]
            }
            buffer.putInt(i * 4, v0)
            buffer.putInt(i * 4 + 4, v1)
            i += 8
        }
    }

    /**
     * Deciphers the xtea encryption.
     * @param key The key
     * *
     * @param data The data to decipher
     * *
     * @param offset The offset.
     * *
     * @param length The length of the data to dicpher.
     * *
     * @return The deciphered data.
     */
    @JvmOverloads fun decipher(key: IntArray, data: ByteArray, offset: Int = 0, length: Int = data.size): ByteArray {
        val numBlocks = (length - offset) / 8
        val buffer = ByteBuffer.wrap(data)
        buffer.position(offset)
        for (i in 0..numBlocks - 1) {
            var y = buffer.int
            var z = buffer.int
            var sum = 0
            val delta = -1640531527

            var numRounds = 32
            while (numRounds-- > 0) {
                y += (z shl 4 xor z.ushr(5)) + z xor key[sum and 3] + sum
                sum += delta
                z += (y shl 4 xor y.ushr(5)) + y xor key[sum.ushr(11) and 3] + sum
            }
            buffer.position(buffer.position() - 8)
            buffer.putInt(y)
            buffer.putInt(z)
        }
        return buffer.array()
    }

    fun decipher(buffer: ByteBuffer, start: Int, end: Int, key: IntArray) {
        if (key.size != 4)
            throw IllegalArgumentException()

        val numQuads = (end - start) / 8
        for (i in 0..numQuads - 1) {
            var sum = GOLDEN_RATIO * ROUNDS
            var v0 = buffer.getInt(start + i * 8)
            var v1 = buffer.getInt(start + i * 8 + 4)
            for (j in 0..ROUNDS - 1) {
                v1 -= (v0 shl 4 xor v0.ushr(5)) + v0 xor sum + key[sum.ushr(11) and 3]
                sum -= GOLDEN_RATIO
                v0 -= (v1 shl 4 xor v1.ushr(5)) + v1 xor sum + key[sum and 3]
            }
            buffer.putInt(start + i * 8, v0)
            buffer.putInt(start + i * 8 + 4, v1)
        }
    }

    fun encipher(buffer: ByteBuffer, start: Int, end: Int, key: IntArray) {
        if (key.size != 4)
            throw IllegalArgumentException()

        val numQuads = (end - start) / 8
        for (i in 0..numQuads - 1) {
            var sum = 0
            var v0 = buffer.getInt(start + i * 8)
            var v1 = buffer.getInt(start + i * 8 + 4)
            for (j in 0..ROUNDS - 1) {
                v0 += (v1 shl 4 xor v1.ushr(5)) + v1 xor sum + key[sum and 3]
                sum += GOLDEN_RATIO
                v1 += (v0 shl 4 xor v0.ushr(5)) + v0 xor sum + key[sum.ushr(11) and 3]
            }
            buffer.putInt(start + i * 8, v0)
            buffer.putInt(start + i * 8 + 4, v1)
        }
    }

}
