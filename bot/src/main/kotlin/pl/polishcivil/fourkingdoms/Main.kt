package pl.polishcivil.fourkingdoms

import pl.polishcivil.fourkingdoms.universe.BotServer
import pl.polishcivil.fourkingdoms.universe.BotUniverse

/**
 * Created by polish on 4/19/2017.
 */

fun main(args: Array<String>) {
    val universe = BotUniverse()
    val server = BotServer(universe)
    server.start(8090)
}