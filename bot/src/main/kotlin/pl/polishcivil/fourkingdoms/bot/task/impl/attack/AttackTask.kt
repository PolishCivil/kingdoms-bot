package pl.polishcivil.fourkingdoms.bot.task.impl.attack

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.exec
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackWaveConfig
import pl.polishcivil.fourkingdoms.bot.model.game.MinuteSkipVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.AttackSetupUnitVO
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.impl.attack.*
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.impl.skip.MinuteSkipDialogScript
import pl.polishcivil.fourkingdoms.bot.task.GameTask
import pl.polishcivil.fourkingdoms.bot.task.impl.worldmap.LocationNavigationTask
import pl.polishcivil.fourkingdoms.bot.task.utils.waitFor
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall
import java.util.concurrent.TimeUnit

/**
 * Created by Polish Civil on 11.05.2017.
 */
class AttackTask(configuration: AttackConfig) : GameTask<AttackConfig>(configuration) {
    enum class State {
        UNKNOWN,
        WAIT,
        NAVIGATE_TARGET,
        HANDLE_COOL_DOWN,
        RING_MENU,
        SELECT_SOURCE,
        SETUP_ARMY,
        SEND_ATTACK
    }

    private val navigationTask = LocationNavigationTask(configuration.target)
    private var setupArmyTask = SetupArmyTask(configuration.waveConfigs, configuration.leaderOptionNr)
    private var lastSentDate = -1L
    private var currentState = State.UNKNOWN

    var errorOccurred = false
    var resolvedCooldownEndForTarget = -1L

    override fun checkFinished(bot: Bot): Boolean {
        return false//always running
    }

    override fun execute(bot: Bot) {
        val leaderAvailableForMovement = bot.internalDataPerception.leaders().filter { it.isAvailableForMovement }.any { it.consecutiveLordID == configuration.leaderOptionNr }
        if (!leaderAvailableForMovement) {
            this.currentState = State.WAIT
            return
        }
        try {
            this.currentState = resolveState(bot)
            when (currentState) {
                State.HANDLE_COOL_DOWN -> {
                    bot.exec(CooldownDialogScript.remainingTimeLocationPair())?.let {
                        (remainingSeconds, location) ->
                        if (location != configuration.target) {
                            LogManager.getLogger().warn("Wrong target on skip time!")
                            bot.exec(LayoutManagerScript.closeDialog("MinuteSkipDialog"))
                            bot.exec(LayoutManagerScript.closeDialog("CooldownDialog"))
                        } else {
                            resolvedCooldownEndForTarget = System.currentTimeMillis() + remainingSeconds * 1000
                            LogManager.getLogger().debug("Resolved cooldown end for target ${configuration.target} remaining seconds = $remainingSeconds")
                            if (!configuration.skipCoolDown) {
                                bot.exec(LayoutManagerScript.closeDialog("CooldownDialog"))
                                return
                            }
                            if (bot.displayPerception.dialogVisible("MinuteSkipDialog")) {
                                val available = bot.exec(MinuteSkipDialogScript.minuteSkipList())!!
                                val best = getBestMinuteSkip(available, remainingSeconds)
                                best?.let { best ->
                                    LogManager.getLogger().debug("Using minute skip $best")
                                    bot.exec(MinuteSkipDialogScript.scrollToMenuSkipAt(best.first))?.let {
                                        bot.exec(MinuteSkipDialogScript.menuSkipButtonAt(best.first))?.let {
                                            bot.mouse.click(it)
                                            waitFor(3, TimeUnit.SECONDS, {
                                                bot.exec(CooldownDialogScript.remainingTimeLocationPair())?.first?.let {
                                                    val newTime = remainingSeconds - it
                                                    resolvedCooldownEndForTarget = System.currentTimeMillis() + newTime * 1000
                                                    startAttackDialogVisible(bot)
                                                } ?: false
                                            })
                                        }
                                    }
                                }
                            } else {
                                //open the minute skip dialog
                                bot.exec(CooldownDialogScript.showSkipsButton())?.let {
                                    bot.mouse.click(it)
                                    waitFor(5, TimeUnit.SECONDS, { bot.displayPerception.dialogVisible("MinuteSkipDialog") })
                                    Thread.sleep(2000)
                                }
                            }
                        }
                    }
                }
                State.NAVIGATE_TARGET -> {
                    LogManager.getLogger().info("Centering the location to ${configuration.target}")
                    if (!navigationTask.checkFinished(bot)) {
                        navigationTask.execute(bot)
                    } else {
                        LogManager.getLogger().info("Location centered to ${configuration.target}")
                    }
                }
                State.RING_MENU -> {
                    LogManager.getLogger().info("Pressing attack on ring menu")
                    //ring menu visible, everything fine, attacking
                    findAttackButton(bot)?.let {
                        bot.mouse.click(it)
                        waitFor(10, TimeUnit.SECONDS, { startAttackDialogVisible(bot) || bot.displayPerception.dialogVisible("CooldownDialog") })
                    }
                }
                State.SELECT_SOURCE -> {
                    setupArmyTask.resetLogic()
                    //checking the selection
                    val model = bot.exec(StartAttackDialogScript.sourceTargetPair())!!
                    if (model.second != configuration.target) {
                        LogManager.getLogger().warn("Target mismatch needed ${configuration.target} found ${model.second}")
                        closeStartAttackDialog(bot)
                    } else {
                        if (selectSourceCastle(bot)) {
                            //opening the attack dialog
                            LogManager.getLogger().info("Opening attack dialog...")
                            bot.exec(StartAttackDialogScript.okButton())?.let {
                                bot.mouse.click(it)
                                waitFor(10, TimeUnit.SECONDS, { attackDialogVisible(bot) })
                            }
                        } else {
                            LogManager.getLogger().warn("Couldn't select source castle")
                        }
                    }
                }
                State.SETUP_ARMY -> {
                    val sourceTarget = bot.exec(AttackSetupManagerScript.sourceTargetPair())!!
                    if (sourceTarget.first == configuration.source && sourceTarget.second == configuration.target) {
                        if (!setupArmyTask.checkFinished(bot)) {
                            setupArmyTask.execute(bot)
                        } else {
                            bot.exec(AttackDialogScript.attackButton())?.let { bot.mouse.click(it) }?.let {
                                waitFor(3, TimeUnit.SECONDS, { postAttackDialogVisible(bot) })
                            }
                        }
                    } else {
                        LogManager.getLogger().warn("Something wrong with source | target got $sourceTarget config source = ${configuration.source} target = ${configuration.target} , going back")
                        closeAttackDialog(bot)
                    }
                }
                State.SEND_ATTACK -> {
                    val selected = toWaveConfigs(bot.exec(AttackSetupManagerScript.selectedUnits())!!)
                    val matches = matches(selected, configuration.waveConfigs)
                    if (!matches) {
                        LogManager.getLogger().warn("Something wrong with selected attack configuration, closing dialog..")
                        bot.exec(LayoutManagerScript.closeDialog("AttackDialog"))
                        bot.exec(LayoutManagerScript.closeDialog("PostAttackPopUpDialog"))
                    } else {
                        val selectedHorse = bot.exec(PostAttackPopupDialogScript.selectedHorse())!!
                        if (selectedHorse != configuration.horse) {
                            LogManager.getLogger().debug("Selecting horse ${configuration.horse}")
                            bot.exec(PostAttackPopupDialogScript.selectHorse(configuration.horse))
                        } else {
                            LogManager.getLogger().info("Horse selected, army settled up, sending the attack!")
                            bot.exec(PostAttackPopupDialogScript.yesButton())?.let { bot.mouse.click(it) }
                            if (waitFor(15, TimeUnit.SECONDS, {
                                bot.internalDataPerception.armyData?.moves?.any { it.target == configuration.target } ?: false
                            })) {
                                lastSentDate = System.currentTimeMillis()
                                LogManager.getLogger().info("Attack sent successfully from ${configuration.source} to  ${configuration.target}")
                            }
                        }
                    }
                }
                else -> {
                    LogManager.getLogger().warn("Unknown state ;(")
                }
            }
        } catch(e: AttackTaskException) {
            LogManager.getLogger().catching(e)
            errorOccurred = true
        }
    }

    private fun matches(selected: Map<AttackWall, List<AttackWaveConfig>>, config: Map<AttackWall, List<AttackWaveConfig>>): Boolean {
        for ((wall, waves) in selected) {
            var waveNr = 0
            for (attackWaveConfig in waves) {
                val selectedList = attackWaveConfig.units
                val configList = config[wall]?.getOrNull(waveNr)?.units ?: emptyList()

                for (unit in configList) {
                    selectedList.find { it.id() == unit.id() } ?: return false
                }

                for (unit in selectedList) {
                    val find = configList.find { it.id() == unit.id() } ?: return false
                }
                waveNr++
            }
        }

        return true
    }

    private fun toWaveConfigs(units: List<AttackSetupUnitVO>): Map<AttackWall, List<AttackWaveConfig>> {
        return units.groupBy { it.flank }
                .mapValues {
                    it.value.groupBy { it.waveId }.mapValues {
                        AttackWaveConfig(it.value.map {
                            AttackUnitConfig.absoluteID(it.id, it.amount, it.slotId.contains("soldier", true))
                        })
                    }.values.toList()
                }
    }

    private fun selectSourceCastle(bot: Bot): Boolean {
        val selected = bot.exec(StartAttackDialogScript.sourceTargetPair())!!.first
        if (selected == configuration.source) {
            return true
        }
        val size = bot.exec(StartAttackDialogScript.availableLocationsSize())!!
        for (index in 0..size - 1) {
            val location = bot.exec(StartAttackDialogScript.availableLocationAt(index))
            if (location == configuration.source) {
                LogManager.getLogger().debug("Selecting castle at index $index")
                bot.exec(StartAttackDialogScript.selectCastleIndex(index))
                return waitFor(3, TimeUnit.SECONDS, { bot.exec(StartAttackDialogScript.sourceTargetPair())!!.first == configuration.source })
            }
        }
        LogManager.getLogger().error("Couldn't find source castle at ${configuration.source}")
        return false
    }

    private fun closeStartAttackDialog(bot: Bot): Boolean {
        if (!startAttackDialogVisible(bot)) {
            return true
        }
        bot.exec(LayoutManagerScript.closeDialog("StartAttackDialog"))
        return waitFor(3, TimeUnit.SECONDS, { !startAttackDialogVisible(bot) })

    }

    private fun closeAttackDialog(bot: Bot): Boolean {
        if (!attackDialogVisible(bot)) {
            return true
        }
        if (bot.displayPerception.attackSetup != null) {
            bot.exec(LayoutManagerScript.closeDialog("CastleAttackSetupDialog"))
        }

        bot.exec(LayoutManagerScript.closeDialog("AttackDialog"))
        return waitFor(3, TimeUnit.SECONDS) { !attackDialogVisible(bot) }

    }

    private fun startAttackDialogVisible(bot: Bot) = bot.displayPerception.dialogVisible("StartAttackDialog")

    private fun postAttackDialogVisible(bot: Bot) = bot.displayPerception.dialogVisible("PostAttackPopUpDialog")

    private fun attackDialogVisible(bot: Bot) = bot.displayPerception.attack != null

    private fun findAttackButton(bot: Bot): DisplayRectangle? {
        val size = bot.exec(LayoutManagerScript.getRingMenuButtonSize())!!
        return (0..size - 1)
                .map {
                    bot.exec(LayoutManagerScript.getRingMenuButtonVO(it))!!
                }
                .firstOrNull { it.headingText == "Attack" }
                ?.rectangle
    }


    fun shouldExecute(bot: Bot): Boolean {
        if (errorOccurred) {
            return false
        }
        val availableLeaders = bot.internalDataPerception.leaders().filter { it.isAvailableForMovement }
        val leaderAvailableForMovement = availableLeaders.any { it.consecutiveLordID == configuration.leaderOptionNr }
        if (!leaderAvailableForMovement) {
            currentState = State.WAIT
            return false
        }
        val remainingCooldown = resolvedCooldownEndForTarget - System.currentTimeMillis()

        var coolingDown = remainingCooldown >= 5000
        if (coolingDown) {
            return configuration.skipCoolDown
        }

        return true
    }


    fun resolveState(bot: Bot): State {
        if (errorOccurred) {
            return State.UNKNOWN
        }
        if (bot.displayPerception.dialogVisible("CooldownDialog") || bot.displayPerception.dialogVisible("MinuteSkipDialog")) {
            return State.HANDLE_COOL_DOWN
        }
        if (postAttackDialogVisible(bot)) {
            return State.SEND_ATTACK
        }
        if (startAttackDialogVisible(bot)) {
            return State.SELECT_SOURCE
        }
        if (attackDialogVisible(bot)) {
            return State.SETUP_ARMY
        }
        if (bot.displayPerception.worldMap?.ringMenuVisible ?: false) {
            if (bot.displayPerception.worldMap!!.selectedMapLocation == configuration.target) {
                return State.RING_MENU

            }
        }
        return State.NAVIGATE_TARGET
    }

    fun lastSentDate() = lastSentDate

    fun resolvedState() = currentState


    fun getBestMinuteSkip(from: List<Pair<Int, MinuteSkipVO>>, remainingTime: Int): Pair<Int, MinuteSkipVO>? {
        val sortedByDescending = from.sortedByDescending {
            val skipVal = (it.second.skipTimeInMinutes * 60)
            val loss =
                    if (remainingTime > skipVal) {
                        remainingTime - skipVal
                    } else {
                        skipVal - remainingTime
                    }
            loss
        }
        return sortedByDescending.lastOrNull()
    }
}