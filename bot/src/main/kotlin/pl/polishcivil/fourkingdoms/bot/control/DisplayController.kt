package pl.polishcivil.fourkingdoms.bot.control

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.game.Widget
import pl.polishcivil.fourkingdoms.bot.model.game.Widget.Companion.ROOT_WIDGET
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by polish on 4/19/2017.
 */

fun Bot.setDebugRect(rectangle: DisplayRectangle) {
    connection.invoke("PaintController", "setRectangle", rectangle.x, rectangle.y, rectangle.width, rectangle.height, 0, 1)
}

