package pl.polishcivil.fourkingdoms.bot.model.game.attack

/**
 * Created by polish on 5/3/2017.
 */
data class SelectedUnitsModel(val selectedUnits: List<AttackSetupUnitVO>)