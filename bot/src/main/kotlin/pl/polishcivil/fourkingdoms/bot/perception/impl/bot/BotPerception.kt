package pl.polishcivil.fourkingdoms.bot.perception.impl.bot

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration
import pl.polishcivil.fourkingdoms.bot.perception.IPerception
import pl.polishcivil.fourkingdoms.bot.task.TaskServiceStatus
import pl.polishcivil.fourkingdoms.bot.task.impl.attack.AttackTask
import pl.polishcivil.fourkingdoms.bot.task.impl.attack.schedule.ScheduleTask

/**
 * Created by Polish Civil on 13.05.2017.
 */
class BotPerception : IPerception {
    var currentSchedule: ScheduleTask? = null
    var taskServiceStatus = TaskServiceStatus.RESOLVING_BOT
    var configuration: BotConfiguration? = null

    override fun update(bot: Bot) {
        this.taskServiceStatus = bot.taskService.status
        this.configuration = bot.taskService.resolvedConfiguration
        this.currentSchedule = bot.taskService.mainTask?.attackingController?.currentTask()
    }
}