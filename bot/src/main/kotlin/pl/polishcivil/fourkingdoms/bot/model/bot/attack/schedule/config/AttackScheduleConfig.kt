package pl.polishcivil.fourkingdoms.bot.model.bot.attack.schedule.config

import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig
import java.util.*

/**
 * Created by Polish Civil on 24.05.2017.
 */
class AttackScheduleConfig(var attacks: List<AttackConfig> = listOf(), var enabled: Boolean = false, var name: String = "no-name") : Cloneable