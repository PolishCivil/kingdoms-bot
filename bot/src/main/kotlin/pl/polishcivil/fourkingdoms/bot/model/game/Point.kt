package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by polish on 4/21/2017.
 */
data class Point(val x: Number, val y: Number) {
    companion object {
        val NIL = pl.polishcivil.fourkingdoms.bot.model.game.Point(-1, -1)
    }
}