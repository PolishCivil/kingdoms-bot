package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by polish on 6/1/17.
 */
data class AccountInfoVO(val loginName: String, val password: String)
