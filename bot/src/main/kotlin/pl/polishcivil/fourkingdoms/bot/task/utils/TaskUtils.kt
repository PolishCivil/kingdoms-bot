package pl.polishcivil.fourkingdoms.bot.task.utils

import org.apache.logging.log4j.LogManager
import java.util.concurrent.TimeUnit

/**
 * Created by polish on 4/19/2017.
 */


fun waitFor(time: Int, unit: TimeUnit, check: () -> Boolean): Boolean {
    val start = System.currentTimeMillis()
    while (System.currentTimeMillis() - start < unit.toMillis(time.toLong())) {
        try {
            if (check.invoke()) {
                break
            }
            Thread.sleep(250)
        } catch (e: Exception) {
            LogManager.getLogger().catching(e)
            return false
        }

    }
    try {
        return check.invoke()
    } catch (e: Exception) {
        LogManager.getLogger().catching(e)
        return false
    }

}