package pl.polishcivil.fourkingdoms.bot.task

import pl.polishcivil.fourkingdoms.bot.Bot

/**
 * Created by polish on 4/19/2017.
 */
abstract class GameTask<out CFG>(val configuration: CFG) {

    abstract fun checkFinished(bot: Bot): Boolean

    abstract fun execute(bot: Bot)

    override fun toString(): String {
        return "Task ${this.javaClass.simpleName} config = $configuration"
    }
}