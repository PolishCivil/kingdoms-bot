package pl.polishcivil.fourkingdoms.bot.model.game.layout.panels

import pl.polishcivil.fourkingdoms.bot.model.game.Point

/**
 * Created by polish on 4/21/2017.
 */
data class WorldMapNavigationPanel(var searchContainerVisible: Boolean, var targetLocation: Point)