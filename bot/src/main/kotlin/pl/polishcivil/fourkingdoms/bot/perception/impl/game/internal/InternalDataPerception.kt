package pl.polishcivil.fourkingdoms.bot.perception.impl.game.internal

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.model.game.GameState
import pl.polishcivil.fourkingdoms.bot.model.game.KingdomData
import pl.polishcivil.fourkingdoms.bot.model.game.army.ArmyData
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleData
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleLordData
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleUserData
import pl.polishcivil.fourkingdoms.bot.model.game.player.PlayerInfoModel

/**
 * Created by polish on 4/19/2017.
 */
data class InternalDataPerception(
        val currentKingdomData: KingdomData? = null,
        val gameState: String? = null,
        val armyData: ArmyData? = null,
        val player: PlayerInfoModel? = null,
        val castleData: CastleUserData? = null,
        val currentCastleData: CastleData? = null,
        val lordData: CastleLordData? = null

) {
    fun gameState(): GameState {
        val state: GameState = when (gameState) {
        //@formatter:off
            "UNKNOWN" -> GameState.UNKNOWN
            "other" -> GameState.UNKNOWN
            "iso_map" -> GameState.ISO_MAP
            "world_map" -> GameState.WORLD_MAP
            else -> {
                LogManager.getLogger().warn("Unknown game state: $gameState")
                GameState.UNKNOWN
            }
        //@formatter:on
        }
        return state
    }

    fun leaders() = lordData?.leaders ?: emptyList()

}