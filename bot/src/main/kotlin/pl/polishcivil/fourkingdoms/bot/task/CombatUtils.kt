package pl.polishcivil.fourkingdoms.bot.task

import java.util.Arrays
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom



/**
 * Created by Polish Civil on 29.06.2017.
 */
object CombatUtils {
    val ITEMS_LEFTWALL_TOOLS = intArrayOf(2, 2)

    val LEVELS_LEFTWALL_TOOLS = intArrayOf(0, 37)

    val ITEMS_LEFTWALL_UNITS = intArrayOf(0, 0)

    val LEVELS_LEFTWALL_UNITS = intArrayOf(0, 13)

    val ITEMS_MIDDLEWALL_TOOLS = intArrayOf(1, 1, 1)

    val LEVELS_MIDDLEWALL_TOOLS = intArrayOf(0, 11, 37)

    val ITEMS_MIDDLEWALL_UNITS = intArrayOf(0, 0, 0)

    val LEVELS_MIDDLEWALL_UNITS = intArrayOf(0, 0, 26)

    val ITEMS_RIGHTWALL_TOOLS = intArrayOf(2, 2)

    val LEVELS_RIGHTWALL_TOOLS = intArrayOf(0, 37)

    val ITEMS_RIGHTWALL_UNITS = intArrayOf(0, 0)

    val LEVELS_RIGHTWALL_UNITS = intArrayOf(0, 13)

    fun getAmountSoldiersFlank(effectiveDefenseLevel: Int, bonus: Double): Int {
        return Math.ceil(getMaxAttackers(effectiveDefenseLevel).toDouble() * 0.2 * (1.0 + bonus / 100)).toInt()
    }

    fun getAmountSoldiersMiddle(effectiveDefenseLevel: Int, bonus: Double): Int {
        return Math.ceil((getMaxAttackers(effectiveDefenseLevel) - getAmountSoldiersFlankWithoutBonus(effectiveDefenseLevel) * 2) * (1 + bonus / 100)).toInt()
    }

    fun getAmountSoldiersFlankWithoutBonus(effectiveDefenseLevel: Int): Int {
        return Math.ceil(getMaxAttackers(effectiveDefenseLevel) * 0.2).toInt()
    }

    fun getTotalAmountToolsFlank(defenseLevel: Int, bonus: Int): Int {
        return getTotalAmountTools(false, defenseLevel, bonus)
    }

    fun getTotalAmountToolsMiddle(defenseLevel: Int): Int {
        return getTotalAmountTools(true, defenseLevel, 0)
    }

    fun getTotalAmountTools(front: Boolean, defenseLevel: Int, toolBonus: Int): Int {
        if (front) {
            if (defenseLevel < 11) {
                return 10
            }
            if (defenseLevel < 37) {
                return 20
            }
            if (defenseLevel < 50) {
                return 30
            }
            if (defenseLevel < 69) {
                return 40
            }
            return 50
        }
        if (defenseLevel < 37) {
            return 10
        }
        if (defenseLevel < 50) {
            return 20
        }
        if (defenseLevel < 69) {
            return 30
        }
        return Math.ceil((40 + toolBonus).toDouble()).toInt()
    }

    fun getMaxAttackers(effectiveDefenseLevel: Int): Int {
        var _loc2_ = 320
        if (effectiveDefenseLevel <= 69) {
            _loc2_ = Math.min(260, 5 * effectiveDefenseLevel + 8)
        }
        return _loc2_
    }

    fun effectiveDefenseLevel(targetOwnerLevel: Int, areaType: Kingdom): Int {
        var targetOwnerLevel = targetOwnerLevel
        if (areaType == Kingdom.FIERY_HEIGTHS) {
            targetOwnerLevel = Math.max(70, targetOwnerLevel)
        } else if (areaType == Kingdom.SWORD_COAST) {
            targetOwnerLevel = Math.max(70, targetOwnerLevel)
        } else if (areaType == Kingdom.UNDERGROUND) {
            targetOwnerLevel = Math.max(70, targetOwnerLevel)
        }
        return targetOwnerLevel
    }

    fun getAmountOfUnlockedSlots(wall: AttackWall, soldiers: Boolean, effectiveDefenseLevel: Int): Int {
        var levels: IntArray? = null
        when (wall) {
            AttackWall.LEFT -> levels = if (soldiers) LEVELS_LEFTWALL_UNITS else LEVELS_LEFTWALL_TOOLS
            AttackWall.RIGHT -> levels = if (soldiers) LEVELS_RIGHTWALL_UNITS else LEVELS_RIGHTWALL_TOOLS
            AttackWall.MIDDLE -> levels = if (soldiers) LEVELS_MIDDLEWALL_UNITS else LEVELS_MIDDLEWALL_TOOLS
        }
        return Arrays.stream(levels!!).filter { level -> level <= effectiveDefenseLevel }.count().toInt()
    }

}