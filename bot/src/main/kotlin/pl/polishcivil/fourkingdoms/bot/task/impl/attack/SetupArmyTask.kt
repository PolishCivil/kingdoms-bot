package pl.polishcivil.fourkingdoms.bot.task.impl.attack

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.exec
import pl.polishcivil.fourkingdoms.bot.control.execute
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.AvailableArmyInfo
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackWaveConfig
import pl.polishcivil.fourkingdoms.bot.model.game.attack.AttackSetupUnitVO
import pl.polishcivil.fourkingdoms.bot.script.impl.attack.AttackDialogScript
import pl.polishcivil.fourkingdoms.bot.script.impl.attack.AttackUnitSetupDialogScript
import pl.polishcivil.fourkingdoms.bot.script.impl.equipment.LordOverviewDialogScript
import pl.polishcivil.fourkingdoms.bot.task.CombatUtils
import pl.polishcivil.fourkingdoms.bot.task.GameTask
import pl.polishcivil.fourkingdoms.bot.task.main.DevTask
import pl.polishcivil.fourkingdoms.bot.task.utils.waitFor
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by polish on 4/27/2017.
 */
class SetupArmyTask(configuration: Map<AttackWall, List<AttackWaveConfig>>, val leaderID: Int) : GameTask<Map<AttackWall, List<AttackWaveConfig>>>(configuration) {
    private var finished = false

    fun resetLogic() {
        finished = false
    }

    override fun execute(bot: Bot) {
        if (selectedLeader(bot) != leaderID) {
            if (!bot.displayPerception.dialogVisible("LordOverviewDialog")) {
                bot.exec(AttackDialogScript.lordOverviewButton())?.let {
                    bot.mouse.click(it)
                    waitFor(5, TimeUnit.SECONDS) { bot.displayPerception.dialogVisible("LordOverviewDialog") }
                }
            } else {
                val selectedLord = bot.exec(LordOverviewDialogScript.selectedLordID())!!
                if (selectedLord.consecutiveLordID != leaderID) {
                    LogManager.getLogger().info("Selecting leader ${leaderID}")
                    val availableLords = bot.exec(LordOverviewDialogScript.availableLordIDs())
                    availableLords!!.find { it.consecutiveLordID == leaderID }?.let {
                        (id) ->
                        bot.exec(LordOverviewDialogScript.selectLord(id))
                    }
                } else {
                    LogManager.getLogger().info("Applying lord selection")
                    bot.exec(LordOverviewDialogScript.applyButton())?.let {
                        bot.mouse.click(it)
                        waitFor(5, TimeUnit.SECONDS) { selectedLeader(bot) == leaderID }
                    }
                }
            }

        } else {
            val available = JSONUtils.deserialize(bot.execute(DevTask::class.java.getResource("/available_army_info.js").readText()).first, AvailableArmyInfo::class.java)
            val units = distributeUnits(available, configuration)
            if (!units.any { it.amount > 0 }) {
                throw RuntimeException("NO UNITS!")
            }
            LogManager.getLogger().debug("Distributed units: {}", units.filter { it.amount != 0 })
            val distributonCode = createDistributionCode(units)
            val script = DevTask::class.java.getResource("/setup_army.js").readText().replace("//GENERATED_CODE", distributonCode)
            val execute = bot.execute(script)
            if (execute.first == "OK") {
                finished = true
            } else {
                LogManager.getLogger().warn("Something went wrong when setting up the army: {}", execute)
            }
        }
    }

    fun createDistributionCode(units: List<AttackSetupUnitVO>): String {
        val builder = StringBuilder()

        var index = 0
        for (unit in units) {
            builder.append("var unit_$index = script_ctx.new_instance_6(cls);").append("\n")
            builder.append("unit_$index['flankId'] = ${unit.flank.ordinal};").append("\n")
            builder.append("unit_$index['waveId'] = ${unit.waveId};").append("\n")
            builder.append("unit_$index['slotId'] = '${unit.slotId}';").append("\n")
            if (unit.id > 0) {
                builder.append("unit_$index['unitVO'] = WOD_DATA.createUnitVO(${unit.id});").append("\n")
            }
            builder.append("unit_$index['amount'] = ${unit.amount};").append("\n")
            builder.append("ATTACK_SETUP_MANAGER.setUnitAsSelected(unit_$index);").append("\n")
            index++
        }
        return builder.toString()
    }

    fun distributeUnits(availableArmyInfo: AvailableArmyInfo, attackConfig: Map<pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall, List<AttackWaveConfig>>): List<AttackSetupUnitVO> {
        data class MutableUnitVO(var wodID: Int, var amount: Int) {
            fun grab(amount: Int): Boolean {
                if (this.amount < amount) {
                    return false
                }
                this.amount -= amount
                return true
            }
        }

        data class MutableUnitInv(val units: List<MutableUnitVO>) {
            fun grab(wodId: Int, amount: Int): Boolean {
                return units.firstOrNull { it.wodID == wodId }?.grab(amount) ?: false
            }

            fun available(wodId: Int) = units.firstOrNull { it.wodID == wodId }?.amount ?: 0
        }


        val mutableInv = MutableUnitInv(availableArmyInfo.availableUnits.map { MutableUnitVO(it.wodId, it.amount) })
        LogManager.getLogger().debug("Distributing the units available army info = {}", availableArmyInfo)

        val units = mutableListOf<AttackSetupUnitVO>()
        val effectiveDefenseLevel = availableArmyInfo.effectiveDefenseLevel
        for (wall in AttackWall.values()) {
            if (wall == AttackWall.UNKNOWN) {
                continue
            }
            val wallConfig = attackConfig[wall]
            for (waveNr in 0..availableArmyInfo.maxAllowedWaves - 1) {
                if (wallConfig == null || waveNr >= wallConfig.size) {
                    (0..CombatUtils.getAmountOfUnlockedSlots(wall, false, effectiveDefenseLevel)).mapTo(units) { AttackSetupUnitVO(wall.ordinal, waveNr, "unitCategoryTools$it", 0, 0, 0) }
                    (0..CombatUtils.getAmountOfUnlockedSlots(wall, true, effectiveDefenseLevel)).mapTo(units) { AttackSetupUnitVO(wall.ordinal, waveNr, "unitCategorySoldiers$it", 0, 0, 0) }
                } else {

                    val maxSoldier = if (wall == AttackWall.MIDDLE) availableArmyInfo.maxTroopAmountFront else availableArmyInfo.maxTroopAmountFlank
                    val maxTool = if (wall == AttackWall.MIDDLE) availableArmyInfo.maxToolAmountFront else availableArmyInfo.maxToolAmountFlank

                    val waveConfig = wallConfig[waveNr]

                    val soldiers = waveConfig.soldiers()
                    val tools = waveConfig.tools()

                    val toolSlots = CombatUtils.getAmountOfUnlockedSlots(wall, false, effectiveDefenseLevel)
                    val soldierSlots = CombatUtils.getAmountOfUnlockedSlots(wall, true, effectiveDefenseLevel)
                    for (soldierSlotNr in 0..soldierSlots - 1) {
                        val soldierConfigForSlot = soldiers.getOrNull(soldierSlotNr)
                        if (soldierConfigForSlot == null) {
                            AttackSetupUnitVO(wall.ordinal, waveNr, "unitCategorySoldiers$soldierSlotNr", 0, 0, -1)
                        } else {
                            val absoluteAmount = if (soldierConfigForSlot.percentage) {
                                (maxSoldier * (soldierConfigForSlot.unitAmount / 100.0)).toInt()
                            } else {
                                soldierConfigForSlot.unitAmount
                            }
                            if (maxSoldier < absoluteAmount || !mutableInv.grab(soldierConfigForSlot.id(), absoluteAmount)) {
                                throw AttackTaskException("Soldier amount exceeded! wall = $wall wave = $waveNr slot = $soldierSlotNr tried to put amount = $absoluteAmount of unit ${soldierConfigForSlot.id()}")
                            }
                            units.add(AttackSetupUnitVO(wall.ordinal, waveNr, "unitCategorySoldiers$soldierSlotNr", absoluteAmount, 0, soldierConfigForSlot.id()))
                        }
                    }

                    for (toolSlotNr in 0..toolSlots - 1) {
                        val toolConfigForSlot = tools.getOrNull(toolSlotNr)
                        if (toolConfigForSlot == null) {
                            AttackSetupUnitVO(wall.ordinal, waveNr, "unitCategoryTools$toolSlotNr", 0, 0, -1)
                        } else {
                            val absoluteAmount = if (toolConfigForSlot.percentage) {
                                (maxTool * (toolConfigForSlot.unitAmount / 100.0)).toInt()
                            } else {
                                toolConfigForSlot.unitAmount
                            }
                            if (maxTool < absoluteAmount || !mutableInv.grab(toolConfigForSlot.id(), absoluteAmount)) {
                                throw AttackTaskException("Tool amount exceeded! wall = $wall wave = $waveNr slot = $toolSlotNr tried to put amount = $absoluteAmount of unit ${toolConfigForSlot.identifier} max = $maxTool available = ${mutableInv.available(toolConfigForSlot.id())}")
                            }
                            units.add(AttackSetupUnitVO(wall.ordinal, waveNr, "unitCategoryTools$toolSlotNr", absoluteAmount, 0, toolConfigForSlot.id()))
                        }
                    }
                }
            }
        }
        return units
    }

    override fun checkFinished(bot: Bot): Boolean {
        return finished
    }


    private fun selectedLeader(bot: Bot): Int {
        return bot.exec(AttackDialogScript.selectedLordID()) ?: -1
    }
}
