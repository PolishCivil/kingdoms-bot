package pl.polishcivil.fourkingdoms.bot.script.impl.skip

import pl.polishcivil.fourkingdoms.bot.model.game.MinuteSkipVO
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangleUIElement
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by Polish Civil on 15.05.2017.
 */
class MinuteSkipDialogScript : LayoutManagerScript() {
    init {
        selectDialog("MinuteSkipDialog")
    }

    companion object {
        fun minuteSkipList(): RemoteScriptResultMapper<List<Pair<Int, MinuteSkipVO>>> {
            val script = MinuteSkipDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "consumablesList", "consumablesList")
            script.getField("consumablesList", "listItems", "listItems")
            script.getField("consumablesList", "dataProvider", "dataProvider")

            for (i in 0..8) {
                script.getField("listItems", "item_$i", "$i")
                script.getAndPushFieldJSON("item_$i", "skip_item_$i", "minuteSkipVO")
                script.invoke("dataProvider", "item_index_$i", "getItemIndex", "skip_item_$i")
                script.pushVar("item_index_$i")
            }

            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                val skips = mutableListOf<Pair<Int, MinuteSkipVO>>()
                for (i in 0..8) {
                    resultSet.getVar("skip_item_$i")?.let {
                        val skipVO = JSONUtils.deserialize(it, MinuteSkipVO::class.java)
                        skips.add(Pair(i, skipVO))
                    }
                }
                skips
            })

        }

        fun scrollToMenuSkipAt(index: Int): RemoteScriptResultMapper<String?> {
            val script = MinuteSkipDialogScript()
            script.putInt("index", index)
            script.getField(SELECTED_DIALOG_VAR, "consumablesList", "consumablesList")
            script.getField("consumablesList", "listItems", "listItems")
            script.getField("consumablesList", "dataProvider", "dataProvider")
            script.getField("listItems", "item_$index", "$index")
            script.getAndPushFieldJSON("item_$index", "skip_item_$index", "minuteSkipVO")
            script.invoke(SELECTED_DIALOG_VAR, "scrollResult", "scrollToMinuteSkip", "skip_item_$index")
            return RemoteScriptResultMapper.justStringMapper("skip_item_$index", script)
        }

        fun menuSkipButtonAt(index: Int): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = MinuteSkipDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "consumablesList", "consumablesList")
            script.getField("consumablesList", "listItems", "listItems")
            script.putInt("index", index)
            script.getField("listItems", "item_$index", "$index")
            script.getField("item_$index", "skipButton", "skipButton")
            script.getDisplayRectangleUIElement("skipButton", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }
    }
}