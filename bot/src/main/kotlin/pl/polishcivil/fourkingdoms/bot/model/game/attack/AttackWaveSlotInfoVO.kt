package pl.polishcivil.fourkingdoms.bot.model.game.attack

/**
 * Created by polish on 5/3/2017.
 */
data class AttackWaveSlotInfoVO(val flankId: Int, val slotIndex: Int, val waveId: Int, val category: String) {

    fun flank(): pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall = pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall.values()[flankId]


    fun isTools() = category.endsWith("Tools")

    fun isSoldiers() = !isTools()
}