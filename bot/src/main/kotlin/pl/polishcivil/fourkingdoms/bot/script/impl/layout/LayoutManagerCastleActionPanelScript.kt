package pl.polishcivil.fourkingdoms.bot.script.impl.layout

import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.skip.MinuteSkipDialogScript

/**
 * Created by polish on 4/20/2017.
 */
class LayoutManagerCastleActionPanelScript(availablePanels: List<String>) : LayoutManagerScript() {
    init {
        //todo refactor
        if (availablePanels.contains("ActionPanelFactionMap")) {
            getField("panels_component_dict", "panel_instance", "ActionPanelFactionMap")
        }else{
            getField("panels_component_dict", "panel_instance", "CastleActionPanel")
        }
        getField("panel_instance", "menuButtonTravel", "menuButtonTravel")
        getField("menuButtonTravel", "isMenuTravelSelected", "isSelected")
    }

    companion object {

        fun travelButtonSelected(availablePanels: List<String>): RemoteScriptResultMapper<Boolean?> {
            val script = LayoutManagerCastleActionPanelScript(availablePanels)
            script.pushJSON("isMenuTravelSelected")
            return RemoteScriptResultMapper.jsonMapper("isMenuTravelSelected", script)
        }

        fun travelButtonRect(availablePanels: List<String>): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerCastleActionPanelScript(availablePanels)
            script.getField("menuButtonTravel", "disp", "disp")
            script.invokeStatic("controller.impl.DisplayController", "rectangle", "getRectangle", "disp")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun goToWorldMapRect(availablePanels: List<String>): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerCastleActionPanelScript(availablePanels)
            script.getField("menuButtonTravel", "btnGoToWorldMap", "btnGoToWorldMap")
            script.getField("btnGoToWorldMap", "disp", "disp")
            script.invokeStatic("controller.impl.DisplayController", "rectangle", "getRectangle", "disp")
            script.pushJSON("rectangle")
            script.pushVar("btnGoToWorldMap")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun goToCastleRect(availablePanels: List<String>): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerCastleActionPanelScript(availablePanels)
            script.getField("menuButtonTravel", "btnDropUpLocations", "btnDropUpLocations")
            script.getField("menuButtonTravel", "btnGoToWorldMap", "btnGoToWorldMap")
            script.getField("btnDropUpLocations", "disp", "disp")
            script.invokeStatic("controller.impl.DisplayController", "rectangle", "getRectangle", "disp")
            script.pushVar("menuButtonTravel")
            script.pushVar("menuButtonTravel")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

    }
}