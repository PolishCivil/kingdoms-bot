package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by polish on 5/18/17.
 */
data class LordVO(val id: Int, val consecutiveLordID: Int, val isAvailableForMovement: Boolean, val type: String)