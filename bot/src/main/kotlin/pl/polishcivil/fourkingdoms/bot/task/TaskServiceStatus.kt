package pl.polishcivil.fourkingdoms.bot.task

/**
 * Created by Polish Civil on 13.05.2017.
 */
enum class TaskServiceStatus {
    RESOLVING_BOT, RUNNING, PAUSED
}