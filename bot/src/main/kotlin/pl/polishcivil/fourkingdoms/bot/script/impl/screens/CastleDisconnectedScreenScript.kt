package pl.polishcivil.fourkingdoms.bot.script.impl.screens

import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.GameModelScript
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangle

/**
 * Created by polish on 6/1/17.
 */
class CastleDisconnectedScreenScript : GameModelScript("com.goodgamestudios.mobileBasic.net.connection.model.AutoReconnectionModel") {
    init {
    }

    companion object {
        fun reconnect(): RemoteScriptResultMapper<String?> {
            val script = CastleDisconnectedScreenScript()
            script.pushVar(MODEL_INSTANCE_VAR_NAME)
            script.putStr("SIGNAL_CLS_NAME", "com.goodgamestudios.mobileBasic.net.socketserver.signals.TryConnectingToServerSignal")
            script.invokeStatic("controller.impl.ClassController", "SIGNAL_CLS", "getDefinition", "SIGNAL_CLS_NAME")
            script.invoke("APP_CORE_INSTANCE","sendNotify","sendNotification","SIGNAL_CLS")
            script.pushVar("SIGNAL_CLS_NAME")
            return RemoteScriptResultMapper.justStringMapper("SIGNAL_CLS_NAME", script)
        }
    }
}