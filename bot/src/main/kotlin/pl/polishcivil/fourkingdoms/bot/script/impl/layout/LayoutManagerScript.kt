package pl.polishcivil.fourkingdoms.bot.script.impl.layout

import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.model.game.layout.RingMenuButtonVO
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.GameModelScript
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 4/20/2017.
 */
open class LayoutManagerScript : GameModelScript("com.goodgamestudios.castle.basicView.layout.layoutManager.BasicLayoutManager") {


    init {
        getModelField("ringMenuComponents", "ringMenuComponents")
        getModelField("screens", "screens")
        getModelField("layers_dict", "_layers")
        getModelField("dialogs", "_dialogs")
        getModelField("inGameUIComponents", "inGameUIComponents")
        getModelField("panels", "panels")
        getField("panels", "panels_component_dict", "componentDict")
        getField("ringMenuComponents", "ring_component_dict", "componentDict")
        getField("ring_component_dict", RING_MENU_VAR, "RingMenu")
        getField("inGameUIComponents", "ingame_component_dict", "componentDict")
        getField("screens", "screens_component_dict", "componentDict")
        getField(RING_MENU_VAR, "ring_menu_properties", "properties")
        getField("ring_menu_properties", "ringMenuVO", "ringMenuVO")
        getField("ringMenuVO", RING_MENU_OBJECT_VAR, "ringMenuObject")
    }

    protected fun selectScreen(screenName: String) {
        getField("screens_component_dict", SELECTED_SCREEN_VAR, screenName)
    }

    protected fun selectPanel(panelName: String) {
        getField("panels_component_dict", SELECTED_PANEL_VAR, panelName)
    }

    fun selectDialog(name: String) {
        putStr("dialog_name", name)
        invoke("dialogs", "dialogs_vec", "getShownDialogsInCollectionForName", "dialog_name")
        getField("dialogs_vec", SELECTED_DIALOG_VAR, "0")
    }

    companion object {
        val SELECTED_SCREEN_VAR = "selected_screen"
        val SELECTED_PANEL_VAR = "selected_panel"
        val SELECTED_DIALOG_VAR = "selected_dialog"
        val RING_MENU_VAR = "ringMenuVar"
        val RING_MENU_OBJECT_VAR = "ringMenuObject"

        fun layers(): RemoteScriptResultMapper<List<String>?> {
            val script = LayoutManagerScript()
            script.invokeStatic("org.as3commons.lang.DictionaryUtils", "result", "getKeys", "layers_dict")
            script.pushJSON("result")
            return RemoteScriptResultMapper.jsonMapper("result", script)

        }

        fun closeDialog(name: String): RemoteScriptResultMapper<String?> {
            val script = LayoutManagerScript()
            script.selectDialog(name)
            script.getField(SELECTED_DIALOG_VAR, "closeSignal", "closeSignal")
            script.pushVar("closeSignal")
            script.invoke("closeSignal", "dispatch", "dispatch")
            return RemoteScriptResultMapper.justStringMapper("closeSignal", script)
        }

        fun test(): RemoteScriptResultMapper<String?> {
            val script = LayoutManagerScript()
            script.pushVar("dialogs")
            return RemoteScriptResultMapper.justStringMapper("dialogs", script)
        }

        fun dialogs(): RemoteScriptResultMapper<List<String>?> {
            val script = LayoutManagerScript()
            script.invoke("dialogs", "result", "getShownDialogNames")
            script.pushJSON("result")
            return RemoteScriptResultMapper.jsonMapper("result", script)
        }

        fun ringMenuComponents(): RemoteScriptResultMapper<List<String>?> {
            val script = LayoutManagerScript()
            script.invokeStatic("org.as3commons.lang.DictionaryUtils", "result", "getKeys", "ring_component_dict")
            script.pushJSON("result")
            return RemoteScriptResultMapper.jsonMapper("result", script)
        }

        fun panelsComponents(): RemoteScriptResultMapper<List<String>?> {
            val script = LayoutManagerScript()
            script.invokeStatic("org.as3commons.lang.DictionaryUtils", "result", "getKeys", "panels_component_dict")
            script.pushJSON("result")
            return RemoteScriptResultMapper.jsonMapper("result", script)
        }

        fun screensComponents(): RemoteScriptResultMapper<List<String>?> {
            val script = LayoutManagerScript()
            script.invokeStatic("org.as3commons.lang.DictionaryUtils", "result", "getKeys", "screens_component_dict")
            script.pushJSON("result")
            return RemoteScriptResultMapper.jsonMapper("result", script)
        }

        fun ingameComponents(): RemoteScriptResultMapper<List<String>?> {
            val script = LayoutManagerScript()
            script.invokeStatic("org.as3commons.lang.DictionaryUtils", "result", "getKeys", "ingame_component_dict")
            script.pushJSON("result")
            return RemoteScriptResultMapper.jsonMapper("result", script)
        }

        fun getRingMenuButtonSize(): RemoteScriptResultMapper<Int?> {
            val script = LayoutManagerScript()
            script.getField(RING_MENU_VAR, "buttons", "ringMenuButtons")
            script.getField("buttons", "size", "length")
            script.pushJSON("size")
            return RemoteScriptResultMapper.jsonMapper("size", script)
        }

        fun getRingMenuButtonVO(buttonIndex: Int): RemoteScriptResultMapper<RingMenuButtonVO?> {
            val script = LayoutManagerScript()
            script.getField(RING_MENU_VAR, "buttons", "ringMenuButtons")
            script.getField("buttons", "button", "$buttonIndex")
            script.getField("button", "iconHolder", "iconHolder")
            script.invokeStatic("controller.impl.DisplayController", "rectangle", "getRectangle", "iconHolder")
            script.getField("button", "params", "params")
            script.getField("params", "param", "0")
            script.getField("param", "headingText", "headingText")
            script.pushVar("headingText")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                resultSet.getVar("headingText")?.let { headingText ->
                    RingMenuButtonVO(headingText, JSONUtils.deserialize(resultSet.getVar("rectangle")!!, DisplayRectangle::class.java))
                }
            })
        }
    }
}