package pl.polishcivil.fourkingdoms.bot.script.impl

import pl.polishcivil.fourkingdoms.bot.script.RemoteScript
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.mediator.GGSMediatorScript

/**
 * Created by polish on 4/19/2017.
 */
open class GameModelScript(modelClsName: String) : RemoteScript() {

    init {
        putStr("MODEL_CLS_NAME", modelClsName)
        invokeStatic("controller.impl.ClassController", "MODEL_CLS", "getDefinition", "MODEL_CLS_NAME")
        invokeStatic("com.goodgamestudios.mobileBasic.application.AppCore", "APP_CORE_INSTANCE", "getInstance")
        invoke("APP_CORE_INSTANCE", MODEL_INSTANCE_VAR_NAME, "getModel", "MODEL_CLS")
    }

    protected fun getModelField(varName: String, fieldName: String) {
        getField(GameModelScript.MODEL_INSTANCE_VAR_NAME, varName, fieldName)
    }

    companion object {
        val MODEL_INSTANCE_VAR_NAME = "MODEL_INSTANCE"


        fun test(modelClsName: String): RemoteScriptResultMapper<String?> {
            val script = GameModelScript(modelClsName)
            script.pushVar(MODEL_INSTANCE_VAR_NAME)
            return RemoteScriptResultMapper.justStringMapper(MODEL_INSTANCE_VAR_NAME, script)
        }

    }
}