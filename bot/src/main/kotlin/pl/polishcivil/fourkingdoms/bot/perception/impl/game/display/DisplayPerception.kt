package pl.polishcivil.fourkingdoms.bot.perception.impl.game.display

import pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.attack.AttackDialogPerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.attack.CastleAttackSetupDialogPerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.worldmap.WorldMapPerception

/**
 * Created by polish on 4/19/2017.
 */
data class DisplayPerception(
        val worldMap: WorldMapPerception? = null,
        val attack: AttackDialogPerception? = null,
        val attackSetup: CastleAttackSetupDialogPerception? = null,
        val shownDialogNames: List<String> = emptyList(),
        val layers: List<String> = emptyList(),
        val shownPanels: List<String> = emptyList(),
        val ringMenus: List<String> = emptyList(),
        val shownScreens: List<String> = emptyList()
) {
    fun dialogVisible(name: String) = shownDialogNames.contains(name)

    fun screenVisible(name: String) = shownScreens.contains(name)
}