package pl.polishcivil.fourkingdoms.bot.model.game.layout

/**
 * Created by polish on 4/20/2017.
 */
data class RingMenuButtonVO(val headingText: String, val rectangle: DisplayRectangle)
