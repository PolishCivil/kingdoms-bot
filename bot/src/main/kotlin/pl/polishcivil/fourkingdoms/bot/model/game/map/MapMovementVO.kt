package pl.polishcivil.fourkingdoms.bot.model.game.map

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig

/**
 * Created by Polish Civil on 13.05.2017.
 */
data class MapMovementVO(val source: LocationConfig, val target: LocationConfig, val type: Int, val myMovement: Boolean, val timeReachTargetMillis: Long)