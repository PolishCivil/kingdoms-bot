package pl.polishcivil.fourkingdoms.bot.task.main

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.execute
import pl.polishcivil.fourkingdoms.bot.script.getScript

/**
 * Created by polish on 6/10/17.
 */
class DevTask {
    fun execute(bot: Bot) {
        if (bot.gamePerception.internal.player?.userName == "Luis2189") {
            println("bot.gamePerception.display.shownPanels = ${bot.gamePerception.display.shownPanels}")
            val execute = bot.execute(getScript("/script/actions/panels/ActionPanel_OpenLocationsDialog.js"))
            println("execute = ${execute}")
        }
    }


}
