package pl.polishcivil.fourkingdoms.bot.model.game.attack

/**
 * Created by polish on 5/7/2017.
 */
data class AvailableUnitVO(val wodID: Int, val inventoryAmount: Int, val currentAvailableAmount: Int, val initialAvailableAmount: Int)