package pl.polishcivil.fourkingdoms.bot.script.impl.screens

import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript

/**
 * Created by polish on 6/1/17.
 */
class CastleKingdomOverviewScreenScript : LayoutManagerScript() {
    init {
        selectScreen("CastleKingdomOverviewScreen")
    }

    companion object {
        fun hide(): RemoteScriptResultMapper<String?> {
            val script = CastleKingdomOverviewScreenScript()

            script.putStr("SIGNAL_CLS_NAME", "com.goodgamestudios.mobileBasic.application.signals.keyboard.BackButtonPressedSignal")
            script.invokeStatic("controller.impl.ClassController", "SIGNAL_CLS", "getDefinition", "SIGNAL_CLS_NAME")
            script.invoke("APP_CORE_INSTANCE", "SIGNAL_INSTANCE", "getSignal", "SIGNAL_CLS")
            script.invoke("SIGNAL_INSTANCE", "ret", "dispatch")
            script.pushVar("SIGNAL_CLS")
            return RemoteScriptResultMapper.justStringMapper("SIGNAL_CLS", script)
        }
    }
}