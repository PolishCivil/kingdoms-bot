package pl.polishcivil.fourkingdoms.bot.task.impl.attack.schedule

import lombok.Lombok
import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.schedule.config.AttackScheduleConfig
import pl.polishcivil.fourkingdoms.bot.task.GameTask
import pl.polishcivil.fourkingdoms.bot.task.impl.attack.AttackTask

/**
 * Created by Polish Civil on 29.05.2017.
 */
class ScheduleTask(config: AttackScheduleConfig) : GameTask<AttackScheduleConfig>(config) {
    var currentAttackIndex = 0
    val attackTasks = config.attacks.map { AttackTask(it) }.toList()


    override fun checkFinished(bot: Bot): Boolean {
        return false
    }

    override fun execute(bot: Bot) {
        val task = currentAttack()
        LogManager.getLogger().debug("Current attack = {}", "${task.configuration.source} -> ${task.configuration.target}")
        LogManager.getLogger().debug("Leader {} available == {}", task.configuration.leaderOptionNr, bot.internalDataPerception.leaders())
        val shouldExecute = task.shouldExecute(bot)
        LogManager.getLogger().debug("Should exec? {}", shouldExecute)
        if (shouldExecute) {
            task.execute(bot)
        } else {
            currentAttackIndex = nextAttackIndex()
        }
    }

    fun shouldExecute(bot: Bot): Boolean {
        if (configuration.attacks.isEmpty()) {
            return false
        }
        if (!configuration.enabled) {
            return false
        }
        if (currentAttack().shouldExecute(bot)) {
            return true
        }
        if (nextAttack().shouldExecute(bot)) {
            return true
        }
        return false
    }

    fun nextAttackIndex(): Int {
        if (currentAttackIndex + 1 >= attackTasks.size) {
            return 0
        }
        return currentAttackIndex + 1
    }

    fun currentAttack(): AttackTask = attackTasks[currentAttackIndex]

    fun nextAttack(): AttackTask = attackTasks[nextAttackIndex()]
}