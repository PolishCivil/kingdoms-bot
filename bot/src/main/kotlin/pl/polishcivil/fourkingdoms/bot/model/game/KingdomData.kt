package pl.polishcivil.fourkingdoms.bot.model.game

import com.google.gson.annotations.SerializedName
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom

/**
 * Created by polish on 4/19/2017.
 */
data class KingdomData(
        @com.google.gson.annotations.SerializedName("activeKingdomID")
        val activeKingdomID: Int
) {
    fun getActiveKingdom() = pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom.getByRawType(activeKingdomID)!!
}