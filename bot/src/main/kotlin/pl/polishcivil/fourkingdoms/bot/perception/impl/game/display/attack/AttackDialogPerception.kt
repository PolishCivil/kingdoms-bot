package pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.attack

import pl.polishcivil.fourkingdoms.bot.model.game.attack.AttackWaveSlotInfoVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.SelectedUnitsModel
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall

/**
 * Created by polish on 5/3/2017.
 */
data class AttackDialogPerception(
        //attack dialog things
        val selectedFlank: Int = -1,
        val availableWaves: Int = -1
        //selection dialog things
//        val slotInfo: AttackWaveSlotInfoVO? = null,
//        val unitSetupSelectedSlot: Int = -1,
//        val unitSetupSelectedWave: Int = -1,
//        val unitSetupSelectedUnits: SelectedUnitsModel? = null

        //    override fun update(bot: Bot) {
//        val displayPerception = bot.displayPerception
//
//        this.attackSetupVisible = displayPerception.shownDialogNames.contains("AttackDialog")
//        if (attackSetupVisible) {
//            updateAttackDialogStuff(bot)
//            this.unitSetupVisible = displayPerception.shownDialogNames.contains("CastleAttackSetupDialog")
//            if (unitSetupVisible) {
//                updateSelectionDialogStuff(bot)
//            }
//        } else {
//            this.unitSetupVisible = false
//        }
//    }

//                private fun updateAttackDialogStuff(bot: Bot) {
//        this.selectedFlank = bot.exec(AttackDialogScript.getSelectedFlank()) ?: AttackWall.UNKNOWN

//                private fun updateSelectionDialogStuff(bot: Bot) {
//        this.slotInfo = bot.exec(AttackUnitSetupDialogScript.slotInfo())
//        slotInfo?.let {
//            this.unitSetupSelectedSlot = bot.exec(AttackUnitSetupDialogScript.selectedSlotIndex()) ?: -1
//            this.unitSetupSelectedWave = it.waveId
//            this.unitSetupSelectedUnits = bot.exec(AttackUnitSetupDialogScript.selectedUnits())
//        }
//        }
) {
    fun selectedFlank(): AttackWall = AttackWall.values()[this.selectedFlank]
}