package pl.polishcivil.fourkingdoms.bot.perception

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis

/**
 * Created by polish on 4/19/2017.
 */
class PerceptionService(val bot: Bot) {
    val executor = Executors.newSingleThreadScheduledExecutor({ r -> Thread(r, "Perception service") })!!

    fun start() {
        executor.scheduleAtFixedRate({
            try {
                val measure = measureTimeMillis {
                    bot.updatePerception()
                }
                LogManager.getLogger().debug("Perception update took {} for bot {}", measure, bot.gamePerception.internal.player?.userName)
            } catch(e: Exception) {
                LogManager.getLogger().catching(e)
            }
        }, 0, 500, TimeUnit.MILLISECONDS)
    }

    fun stop() {
        executor.shutdownNow()
        LogManager.getLogger().info("Perception service for ${this.bot} stopped!")
    }
}