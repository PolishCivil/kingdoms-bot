package pl.polishcivil.fourkingdoms.bot.script.impl.attack

import pl.polishcivil.fourkingdoms.bot.model.game.attack.AttackWaveSlotInfoVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.AvailableUnitListItemVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.AvailableUnitVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.SelectedUnitsModel
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.impl.mediator.GGSMediatorScript
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangleUIElement
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 5/3/2017.
 */
class AttackUnitSetupDialogScript : LayoutManagerScript() {
    init {
        selectDialog("CastleAttackSetupDialog")


        //grab the mediator map
        putStr("v0", "robotlegs.bender.extensions.mediatorMap.api.IMediatorMap")
        invokeStatic("controller.impl.ClassController", "mediatorMapInstanceDef", "getDefinition", "v0")
        invokeStatic("com.goodgamestudios.mobileBasic.application.AppCore", "appCore", "getInstance")
        invoke("appCore", "mediatorMapInstance", "getModel", "mediatorMapInstanceDef")
        //grab the view handler
        getField("mediatorMapInstance", GGSMediatorScript.MEDIATOR_FACTORY_VAR, "_ggsFactory")
        getField("mediatorMapInstance", GGSMediatorScript.VIEW_HANDLER_VAR, "_viewHandler")
        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_factory", "_factory")
        //grab the mediator dict
        getField("_factory", "_mediators", "_mediators")


        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_knownMappings", "_knownMappings")
        putStr("dialogClsName", "com.goodgamestudios.castle.gameplay.attack.views.attackDialog.unitSelection.UnitSelectionDialog")
        invokeStatic("controller.impl.ClassController", "dialogCls", "getDefinition", "dialogClsName")
        getDict("_knownMappings", "mappings", "dialogCls")
        getField("mappings", "result1", "2")
        invoke(GGSMediatorScript.MEDIATOR_FACTORY_VAR, MEDIATOR_VAR, "getMediator", LayoutManagerScript.SELECTED_DIALOG_VAR, "result1")

        getField(SELECTED_DIALOG_VAR, "propertiesVar", "properties")
        getField(SELECTED_DIALOG_VAR, "slotIndexSelectedVar", "slotIndexSelected")
        getField("propertiesVar", "slotInfoVar", "attackWaveSlotInfoVO")
    }

    companion object {
        val MEDIATOR_VAR = "UnitSelectionMediatorVar"

        fun selectedUnitCount(): RemoteScriptResultMapper<Int?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_optionsList", "_optionsList")
            script.getField(SELECTED_DIALOG_VAR, "progressBar", "progressBar")
            script.getField("progressBar", "selectedUnitsCount", "selectedValue")
            script.getField("_optionsList", "selectedOption", "selectedOption")
            script.pushVar("selectedUnitsCount")
            return RemoteScriptResultMapper.jsonMapper("selectedUnitsCount", script)
        }

        fun maxUnitsAmount(): RemoteScriptResultMapper<Int?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "progressBar", "progressBar")
            script.getField("progressBar", "selectedUnitsCount", "maxValue")
            script.pushVar("selectedUnitsCount")
            return RemoteScriptResultMapper.jsonMapper("selectedUnitsCount", script)
        }

        fun availableSlots(): RemoteScriptResultMapper<Int?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_optionsList", "_optionsList")
            script.getField("_optionsList", "totalOptions", "totalOptions")
            script.pushVar("totalOptions")
            return RemoteScriptResultMapper.jsonMapper("totalOptions", script)
        }

        fun slotRectangle(slotNr: Int): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_optionsList", "_optionsList")
            script.getField("_optionsList", "listOptions", "listOptions")
            script.getField("listOptions", "option", "$slotNr")
            script.getField("option", "dispClip", "dispClip")
            script.getDisplayRectangle("dispClip", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun clearSlotRect(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_availableUnitListComponent", "_availableUnitListComponent")
            script.getField("_availableUnitListComponent", "btnSlotsReset", "btnSlotsReset")
            script.getDisplayRectangleUIElement("btnSlotsReset", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun selectedUnits(): RemoteScriptResultMapper<SelectedUnitsModel?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(MEDIATOR_VAR, "_temporarySelectedUnitsService", "_temporarySelectedUnitsService")
            script.getField("_temporarySelectedUnitsService", "model", "model")
            script.getField("model", "selectedUnits", "selectedUnits")
            script.pushJSON("model")
            return RemoteScriptResultMapper.jsonMapper<SelectedUnitsModel>("model", script)
        }

        fun isMeleeUnits(): RemoteScriptResultMapper<Boolean?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(MEDIATOR_VAR, "_isMeleeUnitsSelected", "_isMeleeUnitsSelected")
            script.pushJSON("_isMeleeUnitsSelected")
            return RemoteScriptResultMapper.jsonMapper("_isMeleeUnitsSelected", script)
        }


        fun isSoldiers(): RemoteScriptResultMapper<Boolean?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_availableUnitListComponent", "_availableUnitListComponent")
            script.pushVar("_availableUnitListComponent")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                val component = resultSet.getVar("_availableUnitListComponent")!!

                !component.contains("Tools")
            })
        }

        fun slotInfo(): RemoteScriptResultMapper<AttackWaveSlotInfoVO?> {
            val script = AttackUnitSetupDialogScript()
            script.pushJSON("slotInfoVar")
            return RemoteScriptResultMapper.jsonMapper("slotInfoVar", script)
        }

        fun selectedSlotIndex(): RemoteScriptResultMapper<Int?> {
            val script = AttackUnitSetupDialogScript()
            script.pushJSON("slotIndexSelectedVar")
            return RemoteScriptResultMapper.jsonMapper("slotIndexSelectedVar", script)
        }

        fun scrollToUnit(unitID: Int): RemoteScriptResultMapper<String?> {
            val script = AttackUnitSetupDialogScript()
            script.putInt("unitID", unitID)
            script.invoke(SELECTED_DIALOG_VAR, "dummy", "scrollToEntry", "unitID")
            script.pushVar("dummy")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                resultSet.getVar("dummy")
            })
        }

        fun meleeRangedTabButtons(): RemoteScriptResultMapper<Pair<DisplayRectangle, DisplayRectangle>?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_availableUnitListComponent", "_availableUnitListComponent")
            script.getField("_availableUnitListComponent", "dispClip", "dispClip")
            script.getField("dispClip", "btn_melee", "btn_melee")
            script.getField("dispClip", "btn_range", "btn_range")
            script.getDisplayRectangle("btn_melee", "melee_rect")
            script.getDisplayRectangle("btn_range", "range_rect")
            script.pushJSON("melee_rect")
            script.pushJSON("range_rect")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                val meleeRect = JSONUtils.deserialize(resultSet.getVar("melee_rect")!!, DisplayRectangle::class.java)
                val rangeRect = JSONUtils.deserialize(resultSet.getVar("range_rect")!!, DisplayRectangle::class.java)
                Pair(meleeRect, rangeRect)
            })
        }

        fun applyRect(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "dispClip", "dispClip")
            script.getField("dispClip", "btn_yes", "btn_yes")
            script.getDisplayRectangle("btn_yes", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun closeRect(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "dispClip", "dispClip")
            script.getField("dispClip", "btn_no", "btn_no")
            script.getDisplayRectangle("btn_no", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun unitListItem(index: Int): RemoteScriptResultMapper<AvailableUnitListItemVO?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_availableUnitListComponent", "_availableUnitListComponent")
            script.getField("_availableUnitListComponent", "itemsList", "itemsList")
            script.getField("itemsList", "dataViewPort", "dataViewPort")
            script.getField("dataViewPort", "_activeRenderers", "_activeRenderers")
            script.getField("_activeRenderers", "wrapper", "$index")
            script.getField("wrapper", "scrollButton", "scrollButton")
            script.getField("scrollButton", "_availableUnitListItemVO", "_availableUnitListItemVO")
            script.getField("scrollButton", "sliderComponent", "sliderComponent")
            script.getField("_availableUnitListItemVO", "availableUnitVO", "availableUnitVO")

            script.getField("availableUnitVO", "inventoryAmount", "inventoryAmount")
            script.getField("availableUnitVO", "id", "id")
            script.getField("availableUnitVO", "initialAvailableAmount", "initialAvailableAmount")
            script.getField("availableUnitVO", "currentAvailableAmount", "currentAvailableAmount")


            //grab the slider values
            script.getField("sliderComponent", "selectedValue", "selectedValue")
            script.getField("sliderComponent", "maxValue", "maxValue")
            script.pushVar("sliderComponent")
            script.pushVar("selectedValue")
            script.pushVar("maxValue")

            script.pushVar("inventoryAmount")
            script.pushVar("id")
            script.pushVar("initialAvailableAmount")
            script.pushVar("currentAvailableAmount")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                resultSet.getVar("inventoryAmount")?.let { inventoryAmount ->
                    resultSet.getVar("id")?.let { id ->
                        resultSet.getVar("initialAvailableAmount")?.let { initialAvailableAmount ->
                            resultSet.getVar("currentAvailableAmount")?.let { currentAvailableAmount ->
                                if (resultSet.getVar("selectedValue") == "undefined") {
                                    return@customMapper null
                                }
                                val selected = resultSet.getVar("selectedValue")!!.toInt()
                                val max = resultSet.getVar("maxValue")!!.toInt()
                                val unitVO = AvailableUnitVO(id.toInt(), inventoryAmount.toInt(), currentAvailableAmount.toInt(), initialAvailableAmount.toInt())
                                AvailableUnitListItemVO(unitVO, selected, max)
                            }
                        }
                    }
                }
            })
        }

        fun setSelectedAmountListItem(index: Int, value: Int): RemoteScriptResultMapper<String?> {
            val script = AttackUnitSetupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "_availableUnitListComponent", "_availableUnitListComponent")
            script.getField("_availableUnitListComponent", "itemsList", "itemsList")
            script.getField("itemsList", "dataViewPort", "dataViewPort")
            script.getField("dataViewPort", "_activeRenderers", "_activeRenderers")
            script.getField("_activeRenderers", "wrapper", "$index")
            script.getField("wrapper", "scrollButton", "scrollButton")
            script.getField("scrollButton", "_availableUnitListItemVO", "_availableUnitListItemVO")
            script.getField("scrollButton", "sliderComponent", "sliderComponent")
            script.getField("_availableUnitListItemVO", "availableUnitVO", "availableUnitVO")

            script.getField("availableUnitVO", "inventoryAmount", "inventoryAmount")
            script.getField("availableUnitVO", "id", "id")
            script.getField("availableUnitVO", "initialAvailableAmount", "initialAvailableAmount")
            script.getField("availableUnitVO", "currentAvailableAmount", "currentAvailableAmount")

            script.putInt("value", value)
            script.setField("sliderComponent", "returnVar", "selectedValue", "value")
            script.pushVar("returnVar")
            return RemoteScriptResultMapper.justStringMapper("returnVar", script)
        }
    }
}