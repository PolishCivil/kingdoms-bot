package pl.polishcivil.fourkingdoms.bot.model.json

import com.google.gson.*
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackWaveConfig
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils
import java.lang.reflect.Type

/**
 * Created by Polish Civil on 30.05.2017.
 */
class WaveConfigsAdapter : JsonDeserializer<Map<AttackWall, List<AttackWaveConfig>>>, JsonSerializer<Map<AttackWall, List<AttackWaveConfig>>> {
    override fun serialize(src: Map<AttackWall, List<AttackWaveConfig>>, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val jsonObject = JsonObject()
        for (value in AttackWall.values()) {
            val array = JsonArray()
            for (config in src[value] ?: emptyList()) {
                array.add(Gson().fromJson(JSONUtils.serialize(config), JsonElement::class.java))
            }
            jsonObject.add(value.name, array)
        }
        return jsonObject
    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Map<AttackWall, List<AttackWaveConfig>> {
        val mutableMapOf = mutableMapOf<AttackWall, List<AttackWaveConfig>>()
        for (value in AttackWall.values()) {
            json.asJsonObject.get(value.name)?.let {
                val waves = (it as JsonArray).map { Gson().fromJson(it, AttackWaveConfig::class.java) }.toList()
                mutableMapOf.put(value, waves)
            }
        }
        return mutableMapOf
    }

}