package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by Polish Civil on 13.05.2017.
 */
enum class HorseOption(val index: Int) {
    NO_HORSE(-1), FIRST(0), SECOND(1), THIRD(2), FOURTH(3);

    companion object {
        fun forIndex(index: Int): pl.polishcivil.fourkingdoms.bot.model.game.HorseOption? = values().firstOrNull { it.index == index }
    }
}