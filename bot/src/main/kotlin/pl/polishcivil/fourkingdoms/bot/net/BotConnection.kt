package pl.polishcivil.fourkingdoms.bot.net

import org.apache.logging.log4j.LogManager
import org.xsocket.connection.HandlerChain
import org.xsocket.connection.IDataHandler
import org.xsocket.connection.IDisconnectHandler
import org.xsocket.connection.INonBlockingConnection
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by polish on 4/19/2017.
 */
class BotConnection
(val connection: INonBlockingConnection)
    : IDataHandler, IDisconnectHandler {

    private val responseLock = Object()
    private var responseMessage = "EMPTY"
    private var internalMeasure = 0L

    init {
        this.connection.handler = HandlerChain(listOf(this, this.connection.handler))
    }

    fun get(controller: String, call: String, vararg params: Any = emptyArray()): Pair<String,Long> {
        synchronized(this) {
            synchronized(responseLock) {
                if (!connection.isOpen) {
                    return Pair("null", 0)

                }
                connection.write("G $controller:$call")
                if (params.isNotEmpty()) {
                    connection.write(Arrays.toString(params))
                }
                connection.write("${0.toChar()}")
                responseLock.wait()
                return Pair(responseMessage, internalMeasure)
            }
        }
    }

    fun invoke(controller: String, call: String, vararg params: Any = emptyArray()) {
        synchronized(this) {
            synchronized(responseLock) {
                if (!connection.isOpen) {
                    return
                }
                responseMessage = "EMPTY"
                internalMeasure = -1L
                connection.write("I $controller:$call")
                if (params.isNotEmpty()) {
                    connection.write(Arrays.toString(params))
                }
                connection.write("${0.toChar()}")
                responseLock.wait()
            }
        }
    }


    override fun onData(connection: INonBlockingConnection): Boolean {
        val message = connection.readStringByDelimiter("${0.toChar()}")
        if (message == "<policy-file-request/>") {
            connection.write("<?xml version=\"1.0\"?><cross-domain-policy><allow-access-from domain='*' to-ports='8090' /></cross-domain-policy>")
            connection.write("${0.toChar()}")
            connection.close()
            println("POLICY SENT BITCH")
            return true
        }
        if (message.startsWith("R ")) {
            val end = message.lastIndexOf(":")
            synchronized(responseLock, {
                responseMessage = message.substring(2, end)
                internalMeasure = message.substring(end + 1, message.length - 1).toLong()
                responseLock.notifyAll()
            })
        }
        return false
    }

    override fun onDisconnect(connection: INonBlockingConnection): Boolean {
        synchronized(responseLock, {
            responseMessage = "EMPTY"
            internalMeasure = -1L
            responseLock.notifyAll()
        })
        return false
    }

}