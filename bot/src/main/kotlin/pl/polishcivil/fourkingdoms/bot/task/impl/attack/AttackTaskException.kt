package pl.polishcivil.fourkingdoms.bot.task.impl.attack

/**
 * Created by Polish Civil on 15.05.2017.
 */
class AttackTaskException(message: String) : RuntimeException(message)