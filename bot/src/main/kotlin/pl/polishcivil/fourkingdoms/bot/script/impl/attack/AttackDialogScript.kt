package pl.polishcivil.fourkingdoms.bot.script.impl.attack

import pl.polishcivil.fourkingdoms.bot.model.game.InventoryItemVO
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.impl.mediator.GGSMediatorScript
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangleUIElement
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 4/25/2017.
 */
class AttackDialogScript : LayoutManagerScript() {
    init {
        selectDialog("AttackDialog")
        //grab appcore instance
        invokeStatic("com.goodgamestudios.mobileBasic.application.AppCore", "appCore", "getInstance")

        //grab performing attack data
        putStr("performingAttackDataClsName", "com.goodgamestudios.castle.gameplay.attack.models.PerformingAttackData")
        invokeStatic("controller.impl.ClassController", "performingAttackDataDef", "getDefinition", "performingAttackDataClsName")
        invoke("appCore", "performingAttackDataInstance", "getModel", "performingAttackDataDef")


        //grab the mediator map
        putStr("v0", "robotlegs.bender.extensions.mediatorMap.api.IMediatorMap")
        invokeStatic("controller.impl.ClassController", "mediatorMapInstanceDef", "getDefinition", "v0")
        invoke("appCore", "mediatorMapInstance", "getModel", "mediatorMapInstanceDef")
        //grab the view handler
        getField("mediatorMapInstance", GGSMediatorScript.MEDIATOR_FACTORY_VAR, "_ggsFactory")
        getField("mediatorMapInstance", GGSMediatorScript.VIEW_HANDLER_VAR, "_viewHandler")
        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_factory", "_factory")
        //grab the mediator dict
        getField("_factory", "_mediators", "_mediators")


        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_knownMappings", "_knownMappings")
        putStr("dialogClsName", "com.goodgamestudios.castle.gameplay.attack.views.attackDialog.AttackDialog")
        invokeStatic("controller.impl.ClassController", "dialogCls", "getDefinition", "dialogClsName")
        getDict("_knownMappings", "mappings", "dialogCls")
        getField("mappings", "result1", "2")
        invoke(GGSMediatorScript.MEDIATOR_FACTORY_VAR, MEDIATOR_VAR, "getMediator", LayoutManagerScript.SELECTED_DIALOG_VAR, "result1")
    }

    companion object {
        const val MEDIATOR_VAR = "AttackMediatorVar"


        fun unitInventory(): RemoteScriptResultMapper<List<InventoryItemVO>?> {
            val script = AttackDialogScript()
            script.getField("performingAttackDataInstance", "attackInfo", "attackInfo")
            script.getField("attackInfo", "unitInventory", "unitInventory")
            script.getField("unitInventory", "getAll", "getAll")
            script.pushJSON("getAll")
            return RemoteScriptResultMapper.jsonListMapper("getAll", script)
        }

        fun changeFlank(flank: AttackWall): RemoteScriptResultMapper<String?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.putInt("flank_id", flank.ordinal)
            script.invoke(LayoutManagerScript.SELECTED_DIALOG_VAR, "dummy", "changeFlank", "flank_id")
            script.pushVar("dummy")
            return RemoteScriptResultMapper.justStringMapper("dummy", script)
        }

        fun getSelectedFlank(): RemoteScriptResultMapper<AttackWall?> {
            val script = AttackDialogScript()
            script.getField(MEDIATOR_VAR, "result", "_currentFlankId")
            script.pushVar("result")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                resultSet.getVar("result")?.toIntOrNull()?.let {
                    AttackWall.values()[it]
                }
            })
        }

        fun waveScrollClip(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getAndPushField(LayoutManagerScript.SELECTED_DIALOG_VAR, "waveList", "waveList")
            script.getAndPushField("waveList", "viewPort", "viewPort")
            script.getAndPushField("viewPort", "visibleWidth", "visibleWidth")
            script.getAndPushField("viewPort", "visibleHeight", "visibleHeight")
            script.getAndPushField("viewPort", "contentX", "contentX")
            script.getAndPushField("viewPort", "contentY", "contentY")
            script.getAndPushField("viewPort", "clipRect", "clipRect")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                resultSet.getVar("visibleWidth")?.let {
                    val x = resultSet.getVar("contentX")!!.toDouble()
                    val y = resultSet.getVar("contentY")!!.toDouble()
                    val width = resultSet.getVar("visibleWidth")!!.toDouble()
                    val height = resultSet.getVar("visibleHeight")!!.toDouble()
                    DisplayRectangle(x, y, width, height)
                }
            })
        }

        fun scrollToWave(waveNr: Int): RemoteScriptResultMapper<String?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(LayoutManagerScript.SELECTED_DIALOG_VAR, "waveList", "waveList")
            script.putInt("waveNr", waveNr)
            script.getField("waveList", "viewPort", "viewPort")
            script.getField("waveList", "_items", "_items")
            script.getField("_items", "waveItem", "$waveNr")
            script.getField("waveItem", "waveItemDisplay", "disp")

            script.invoke("waveList", "dummy", "scrollToDisplayIndex", "waveNr")
            script.getDisplayRectangle("waveItemDisplay", "waveItemDisplayBounds")
            script.pushVar("waveItemDisplayBounds")
            return RemoteScriptResultMapper.justStringMapper("waveItemDisplayBounds", script)
        }


        fun getWaveRectangle(waveNr: Int): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(LayoutManagerScript.SELECTED_DIALOG_VAR, "waveList", "waveList")
            script.putInt("waveNr", waveNr)
            script.getField("waveList", "viewPort", "viewPort")
            script.getField("waveList", "_items", "_items")
            script.getField("_items", "waveItem", "$waveNr")
            script.getField("waveItem", "waveItemDisplay", "disp")
            script.getDisplayRectangle("waveItemDisplay", "waveItemDisplayBounds")
            script.pushJSON("waveItemDisplayBounds")
            return RemoteScriptResultMapper.jsonMapper("waveItemDisplayBounds", script)
        }

        fun slotButtonsSize(waveNr: Int): RemoteScriptResultMapper<Int?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(LayoutManagerScript.SELECTED_DIALOG_VAR, "waveList", "waveList")
            script.putInt("waveNr", waveNr)
            script.getField("waveList", "viewPort", "viewPort")
            script.getField("waveList", "_items", "_items")
            script.getField("_items", "waveItem", "$waveNr")
            script.getField("waveItem", "_slotButtons", "_slotButtons")
            script.getAndPushField("_slotButtons", "length", "length")
            return RemoteScriptResultMapper.jsonMapper("length", script)
        }

        fun slotButton(waveNr: Int, index: Int): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(LayoutManagerScript.SELECTED_DIALOG_VAR, "waveList", "waveList")
            script.putInt("waveNr", waveNr)
            script.getField("waveList", "viewPort", "viewPort")
            script.getField("waveList", "_items", "_items")
            script.getField("_items", "waveItem", "$waveNr")
            script.getField("waveItem", "_slotButtons", "_slotButtons")
            script.getField("_slotButtons", "button", "$index")
            script.getField("button", "dispClip", "dispClip")
            script.getDisplayRectangle("dispClip", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun attackButton(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(LayoutManagerScript.SELECTED_DIALOG_VAR, "dispClip", "dispClip")
            script.getField("dispClip", "btn_attack", "btn_attack")
            script.getDisplayRectangle("btn_attack", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun selectedLordID(): RemoteScriptResultMapper<Int?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(SELECTED_DIALOG_VAR, "lordOverviewButton", "lordOverviewButton")
            script.getField("lordOverviewButton", "_lordVO", "_lordVO")
            script.getAndPushFieldJSON("_lordVO", "consecutiveLordID", "consecutiveLordID")
            return RemoteScriptResultMapper.jsonMapper("consecutiveLordID", script)
        }

        fun lordOverviewButton(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LayoutManagerScript()
            script.selectDialog("AttackDialog")
            script.getField(SELECTED_DIALOG_VAR, "lordOverviewButton", "lordOverviewButton")
            script.getDisplayRectangleUIElement("lordOverviewButton", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }
    }
}