package pl.polishcivil.fourkingdoms.bot.script

/**
 * Created by polish on 4/21/2017.
 */
class RemoteScriptResultSet {
    val resultSet: Map<String, String>

    constructor() {
        resultSet = emptyMap()
    }

    constructor(clientResult: String) {
        this.resultSet = clientResult.split(":::").map { it.split("--->") }
                .filter { it.size == 2 }
                .map { Pair(it[0], it[1]) }.toMap()
    }


    fun getVar(name: String): String? = when (resultSet[name]) {
        "null" -> null
        else -> {
            resultSet[name]
        }
    }

    override fun toString(): String {
        return "ResultSet {$resultSet}"
    }
}

