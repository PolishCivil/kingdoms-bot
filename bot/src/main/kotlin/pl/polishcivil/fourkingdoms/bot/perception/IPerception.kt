package pl.polishcivil.fourkingdoms.bot.perception

import pl.polishcivil.fourkingdoms.bot.Bot

/**
 * Created by polish on 4/19/2017.
 */
interface IPerception {
    fun update(bot: Bot)
}