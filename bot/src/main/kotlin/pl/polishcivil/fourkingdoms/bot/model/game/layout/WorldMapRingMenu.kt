package pl.polishcivil.fourkingdoms.bot.model.game.layout

/**
 * Created by Polish Civil on 11.05.2017.
 */
data class WorldMapRingMenu(val buttons: List<RingMenuButtonVO>)