package pl.polishcivil.fourkingdoms.bot.model.bot.attack.config

/**
 * Created by Polish Civil on 30.05.2017.
 */
data class TestConfig(var name: String)
