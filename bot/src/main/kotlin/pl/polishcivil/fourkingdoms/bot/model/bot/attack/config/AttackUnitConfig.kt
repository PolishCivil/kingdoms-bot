package pl.polishcivil.fourkingdoms.bot.model.bot.attack.config

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitDefinition

/**
 * Created by polish on 4/27/2017.
 */
class AttackUnitConfig(var identifier: String = "Scaling ladder", var unitAmount: Int = 1, var percentage: Boolean = false, var tool: Boolean = true) : Cloneable {
    enum class Type {
        RANGED, MELEE, TOOL
    }

    fun isBestRangeUnit() = identifier == "RANGE"

    fun isBestMeleeUnit() = identifier == "MELEE"

    private fun isIDUnit() = isNumeric(identifier)

    fun id(): Int {
        if (isBestMeleeUnit() || isBestRangeUnit()) {
            throw RuntimeException("Cannot get id for best units!")
        }
        if (!isIDUnit()) {
            val find = pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud.getINSTANCE().itemsFile.units.all.find {
                val localizedName = it.getLocalizedName(pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud.getINSTANCE(), pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag.EN)
                localizedName.equals(identifier, true)
            } ?: throw RuntimeException("Couldn't find unit with name $identifier")
            return find.id
        }
        return identifier.toInt()
    }

    fun type(): AttackUnitConfig.Type {
        if (isBestMeleeUnit()) {
            return AttackUnitConfig.Type.MELEE
        } else if (isBestRangeUnit()) {
            return AttackUnitConfig.Type.RANGED
        }
        val definition = pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud.getINSTANCE().itemsFile.units.findByID(id())
        if (definition is pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitDefinition) {
            return AttackUnitConfig.Type.TOOL
        } else {
            definition as pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.soldier.SoldierUnitDefinition
            if (definition.meleeAttack > definition.rangeAttack) {
                return AttackUnitConfig.Type.MELEE
            } else {
                return AttackUnitConfig.Type.RANGED
            }
        }

    }

    companion object {
        fun forName(enName: String, amount: Int, percentage: Boolean): AttackUnitConfig {
            if (enName == "RANGE" || enName == "MELEE") {
                return AttackUnitConfig(enName, amount, percentage, false)
            }
            val find = pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud.getINSTANCE().itemsFile.units.all.find {
                it.getLocalizedName(pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud.getINSTANCE(), pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag.EN).equals(enName, true)
            }!!
            return AttackUnitConfig("${find.id}", amount, percentage, find is ToolUnitDefinition)
        }

        fun forNamePercentage(enName: String, amount: Int): AttackUnitConfig {
            return Companion.forName(enName, amount, true)
        }


        fun absoluteID(id: Int, amount: Int, tool: Boolean): AttackUnitConfig {
            return AttackUnitConfig("$id", amount, false, tool)
        }

        fun id(id: Int, amount: Int, percentage: Boolean, tool: Boolean): AttackUnitConfig {
            return AttackUnitConfig("$id", amount, percentage, tool)
        }
    }
}

fun isNumeric(input: String): Boolean =
        try {
            input.toDouble()
            true
        } catch(e: NumberFormatException) {
            false
        }
