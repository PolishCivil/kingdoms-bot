package pl.polishcivil.fourkingdoms.bot.widget

import pl.polishcivil.fourkingdoms.bot.model.game.Widget
import java.util.*

/**
 * Created by polish on 4/19/2017.
 */
class WidgetQuery {
    private val results: LinkedList<Widget>

    private constructor(results: LinkedList<Widget>) {
        this.results = results
    }

    constructor(from: Widget) : this(from.children)


    fun selectDesc(pattern: String, vararg options: RegexOption): WidgetQuery {
        return WidgetQuery(results.filterTo(LinkedList<Widget>())
        { it.desc.matches(Regex(pattern, options.toSet())) })
    }

    fun selectText(pattern: String, vararg options: RegexOption): WidgetQuery {
        return WidgetQuery(results.filterTo(LinkedList<Widget>())
        { it.text.matches(Regex(pattern, options.toSet())) })
    }

    private fun selectAll(from: Widget, into: LinkedList<Widget>) {
        into.add(from)
        for (child in from.children) {
            selectAll(child, into)
        }
    }

    fun selectAll(): WidgetQuery {
        val result = LinkedList<Widget>()
        for (widget in results) {
            selectAll(widget, result)
        }
        return WidgetQuery(result)
    }

    fun selectChildren(): WidgetQuery {
        return WidgetQuery(results.flatMapTo(LinkedList<Widget>(), { it.children }))
    }

    fun selectIndex(index: Int): WidgetQuery {
        if (results.size < index) {
            throw RuntimeException("Results size lower than expected index!")
        }
        return WidgetQuery(results[index])
    }

    fun selectFirst(): WidgetQuery {
        return WidgetQuery(results.first)
    }

    fun selectLast(): WidgetQuery {
        return WidgetQuery(results.last)
    }

    fun first(): Widget? {
        return results.firstOrNull()
    }

    fun last(): Widget? {
        return results.lastOrNull()
    }

    fun all() = results
}