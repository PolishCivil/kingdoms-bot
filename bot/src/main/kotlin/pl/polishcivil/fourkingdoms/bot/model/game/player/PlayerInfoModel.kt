package pl.polishcivil.fourkingdoms.bot.model.game.player

/**
 * Created by Polish Civil on 14.05.2017.
 */
data class PlayerInfoModel(val email: String, val userName: String)