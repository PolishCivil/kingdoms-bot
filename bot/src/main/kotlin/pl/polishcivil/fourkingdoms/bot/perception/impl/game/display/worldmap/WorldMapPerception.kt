package pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.worldmap

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.game.Point
import pl.polishcivil.fourkingdoms.bot.model.game.layout.panels.WorldMapNavigationPanel

/**
 * Created by polish on 4/21/2017.
 */
data class WorldMapPerception(
        val ringMenuVisible: Boolean = false,
        val selectedMapLocation: LocationConfig?,
        val navigationPanel: WorldMapNavigationPanel? = null
)