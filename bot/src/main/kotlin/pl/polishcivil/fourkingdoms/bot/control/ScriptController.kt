package pl.polishcivil.fourkingdoms.bot.control

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.graph.alg.TopologicalSort
import pl.polishcivil.fourkingdoms.bot.graph.impl.DirectedGraph
import pl.polishcivil.fourkingdoms.bot.script.RemoteScript
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultSet
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import java.util.*

/**
 * Created by polish on 4/19/2017.
 */
object ScriptController {

}

fun Bot.executeScript(script: RemoteScript): RemoteScriptResultSet {
    val response = connection.get("ScriptController", "execute", script.getCode())
    return RemoteScriptResultSet(response.first)
}

fun Bot.execute(code: String): Pair<String, Long> {
    return connection.get("ScriptController", "executeScript", fixCode(code))
}

data class ScriptEntry(val file: String, val code: String)

private fun buildImports(entry: ScriptEntry): Set<String> {
    if (entry.code.contains("importScripts(")) {
        val substringAfter = entry.code.substringAfter("importScripts(").substringBefore(")").replace("\n", "").replace("\"", "")
        return substringAfter.split(",").map { it.replace(" ", "") }.toSet()
    }
    return emptySet()
}


private fun buildImportsRec(parent: ScriptEntry, into: DirectedGraph<ScriptEntry> = DirectedGraph<ScriptEntry>()): DirectedGraph<ScriptEntry> {
    if (!into.nodes.contains(parent)) {
        into.addNode(parent)
    }
    for (import in buildImports(parent)) {
        val resource = ScriptController::class.java.getResource("/$import") ?: throw RuntimeException("Couldn't find imported script /$import")
        var code = resource.readText().lines()
                .filterNot { it.contains("//") }
                .joinToString(separator = "\n") { it }

        val child = into.nodes.find { it.file == import } ?: ScriptEntry(import, code)
        if (!into.nodes.contains(child)) {
            into.addNode(child)
            buildImportsRec(child, into)
        }
        into.addEdge(parent, child)
    }
    return into
}

private fun fixCode(_code: String): String {


    val buildImportsRec = buildImportsRec(ScriptEntry("root", _code))
    val sort = TopologicalSort.sortDependency(buildImportsRec)
    val code = sort.map { it.code }
            .joinToString(separator = "\n") { it }
            .lines()
            .filterNot {  it.replace(" ", "").startsWith("//") }
            .filterNot { it.replace(" ", "").startsWith("importScripts")  }
            .joinToString(separator = "") { it }
    return "$code\nscript_ctx.result = main();"


}

fun <T> Bot.exec(script: RemoteScriptResultMapper<T>): T? {
    var result = executeScript(script.script)
    try {
        return script.mapper.invoke(result)
    } catch(e: Exception) {
        LogManager.getLogger().error("RESULT: $result")
        throw e
    }

}