package pl.polishcivil.fourkingdoms.bot.model.game.attack

/**
 * Created by polish on 5/7/2017.
 */
data class AvailableUnitListItemVO(val availableUnitVO: AvailableUnitVO, val selectedAmount: Int, val maxAmount: Int)