package pl.polishcivil.fourkingdoms.bot.script.names

/**
 * Created by polish on 4/21/2017.
 */
object BasicUIElementNames {
    val DISPLAY_CLIP = "dispClip"
    val DISPLAY_SPRITE = "disp"
    val HIDDEN = "_isHidden"
    val LOCKED = "isLocked"
}