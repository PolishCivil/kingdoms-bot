package pl.polishcivil.fourkingdoms.bot.script

import pl.polishcivil.fourkingdoms.bot.task.main.DevTask

/**
 * Created by Polish Civil on 03.07.2017.
 */


fun getScript(path: String) = DevTask::class.java.getResource(path).readText()