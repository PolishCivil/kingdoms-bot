package pl.polishcivil.fourkingdoms.bot.script.impl.attack

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.impl.mediator.GGSMediatorScript
import pl.polishcivil.fourkingdoms.bot.script.utils.deserializeLocation
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangle

/**
 * Created by Polish Civil on 11.05.2017.
 */
class StartAttackDialogScript : LayoutManagerScript() {
    init {
        selectDialog("StartAttackDialog")
        invokeStatic("com.goodgamestudios.mobileBasic.application.AppCore", "appCore", "getInstance")

        //grab the mediator map
        putStr("v0", "robotlegs.bender.extensions.mediatorMap.api.IMediatorMap")
        invokeStatic("controller.impl.ClassController", "mediatorMapInstanceDef", "getDefinition", "v0")
        invoke("appCore", "mediatorMapInstance", "getModel", "mediatorMapInstanceDef")
        //grab the view handler
        getField("mediatorMapInstance", GGSMediatorScript.MEDIATOR_FACTORY_VAR, "_ggsFactory")
        getField("mediatorMapInstance", GGSMediatorScript.VIEW_HANDLER_VAR, "_viewHandler")
        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_factory", "_factory")
        //grab the mediator dict
        getField("_factory", "_mediators", "_mediators")


        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_knownMappings", "_knownMappings")
        putStr("dialogClsName", "com.goodgamestudios.castle.gameplay.attack.views.startAttackDialog.StartAttackDialog")
        invokeStatic("controller.impl.ClassController", "dialogCls", "getDefinition", "dialogClsName")
        getDict("_knownMappings", "mappings", "dialogCls")
        getField("mappings", "result1", "1")
        invoke(GGSMediatorScript.MEDIATOR_FACTORY_VAR, MEDIATOR_VAR, "getMediator", LayoutManagerScript.SELECTED_DIALOG_VAR, "result1")
    }

    companion object {
        val MEDIATOR_VAR = "StartMediator"

        fun sourceTargetPair(): RemoteScriptResultMapper<Pair<LocationConfig, LocationConfig>?> {
            val script = StartAttackDialogScript()
            script.getField(MEDIATOR_VAR, "targetCastle", "targetCastle")
            script.getField(SELECTED_DIALOG_VAR, "sourceCastleList", "sourceCastleList")
            script.invoke("sourceCastleList", "sourceCastle", "getSelectedCastle")
            val source = script.deserializeLocation("sourceCastle", "source")
            val target = script.deserializeLocation("targetCastle", "target")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                val sourceLocation = source.toLocation(resultSet)!!
                val targetLocation = target.toLocation(resultSet)!!

                Pair(sourceLocation, targetLocation)
            })
        }

        fun availableLocationsSize(): RemoteScriptResultMapper<Int?> {
            val script = StartAttackDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "sourceCastleList", "sourceCastleList")
            script.getField("sourceCastleList", "castleList", "castleList")
            script.getField("castleList", "size", "length")
            script.pushVar("size")
            return RemoteScriptResultMapper.jsonMapper("size", script)
        }

        fun okButton(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = StartAttackDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "dispClip", "dispClip")
            script.getField("dispClip", "btn_okay", "btn_okay")
            script.getDisplayRectangle("btn_okay", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun availableLocationAt(index: Int): RemoteScriptResultMapper<LocationConfig> {
            val script = StartAttackDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "sourceCastleList", "sourceCastleList")
            script.getField("sourceCastleList", "castleList", "castleList")
            script.getField("castleList", "object", "$index")
            script.pushVar("object")
            val deserializeLocation = script.deserializeLocation("object", "mapObject")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                deserializeLocation.toLocation(resultSet)!!
            })
        }

        fun selectCastleIndex(index: Int): RemoteScriptResultMapper<String?> {
            val script = StartAttackDialogScript()
            script.putInt("index", index)
            script.getField(SELECTED_DIALOG_VAR, "sourceCastleList", "sourceCastleList")
            script.getField("sourceCastleList", "castleChanged", "castleChanged")
            script.invoke("sourceCastleList", "dummy", "setSelectedIndexCastle", "index")
            script.invoke("castleChanged", "signal", "dispatch")
            return RemoteScriptResultMapper.justStringMapper("dummy", script)
        }
    }
}