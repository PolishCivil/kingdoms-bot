package pl.polishcivil.fourkingdoms.bot.script

import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 4/21/2017.
 */
class RemoteScriptResultMapper<out T>(val script: RemoteScript, val mapper: (resultSet: RemoteScriptResultSet) -> T) {


    companion object {
        inline fun <reified T> jsonMapper(ofVar: String, script: RemoteScript): RemoteScriptResultMapper<T?> =
                customMapper(script, {
                    it.getVar(ofVar)?.let {
                        JSONUtils.deserialize(it, T::class.java)
                    }
                })

        inline fun <reified T> jsonListMapper(ofVar: String, script: RemoteScript): RemoteScriptResultMapper<List<T>?> =
                customMapper(script) {
                    JSONUtils.deserializeList(it.getVar(ofVar), T::class.java)
                }


        fun justStringMapper(ofVar: String, script: RemoteScript): RemoteScriptResultMapper<String?> =
                customMapper(script, {
                    it.getVar(ofVar)
                })

        inline fun <reified T> customMapper(script: RemoteScript, noinline mapper: (resultSet: RemoteScriptResultSet) -> T):
                RemoteScriptResultMapper<T> =
                RemoteScriptResultMapper(script, mapper)
    }
}