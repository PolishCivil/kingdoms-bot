package pl.polishcivil.fourkingdoms.bot.model.bot.attack.config

/**
 * Created by polish on 4/27/2017.
 */
data class AttackWaveConfig(var units: List<AttackUnitConfig>) {

    fun soldiers() = units.filter { !it.tool }

    fun tools() = units.filter { it.tool }
}