package pl.polishcivil.fourkingdoms.bot.task.impl.worldmap

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.exec
import pl.polishcivil.fourkingdoms.bot.control.execute
import pl.polishcivil.fourkingdoms.bot.model.game.GameState
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.game.layout.panels.WorldMapNavigationPanel
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.worldmap.WorldMapPerception
import pl.polishcivil.fourkingdoms.bot.script.getScript
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerCastleActionPanelScript
import pl.polishcivil.fourkingdoms.bot.task.GameTask
import pl.polishcivil.fourkingdoms.bot.task.utils.waitFor
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom
import java.util.concurrent.TimeUnit

/**
 * Created by polish on 4/19/2017.
 */
class LocationNavigationTask(configuration: LocationConfig) : GameTask<LocationConfig>(configuration) {

    override fun execute(bot: Bot) {
        val activeKingdom = bot.internalDataPerception.currentKingdomData!!.getActiveKingdom()
        when (bot.internalDataPerception.gameState()) {
            GameState.ISO_MAP -> goToWorldMap(bot)
            GameState.WORLD_MAP -> {
                visitLocation(activeKingdom, bot)
            }
            GameState.UNKNOWN -> {
                if (bot.displayPerception.screenVisible("CastleKingdomOverviewScreen")) {
                    visitLocation(activeKingdom, bot)
                }
            }
        }
    }

    private fun visitLocation(activeKingdom: Kingdom, bot: Bot) {
        if (activeKingdom == configuration.kingdom && bot.internalDataPerception.gameState() == GameState.WORLD_MAP) {
            val worldMapPerception = bot.displayPerception.worldMap
            if (!checkCurrentLocation(worldMapPerception!!, configuration)) {
                centerMap(bot, configuration)
            }
        } else {
            pickKingdomLocation(bot)
        }
    }

    override fun checkFinished(bot: Bot): Boolean {
        return bot.displayPerception.worldMap?.let {
            checkCurrentLocation(bot.displayPerception.worldMap!!, configuration)
        } ?: false
    }

    private fun checkCurrentLocation(worldMapPerception: WorldMapPerception, location: LocationConfig): Boolean {
        return worldMapPerception.selectedMapLocation == location
    }

    private fun pickKingdomLocation(bot: Bot) {
        LogManager.getLogger().info("Picking kingdom location")
        goToWorldMap(bot)
    }

    private fun centerMap(bot: Bot, location: LocationConfig) {
        //opening the menu
        if (bot.displayPerception.worldMap == null) {
            return
        }
        if (bot.displayPerception.worldMap!!.navigationPanel == null) {
            waitFor(1, TimeUnit.SECONDS) { bot.displayPerception.worldMap?.navigationPanel != null }
            return
        }
        bot.execute(getScript("/script/actions/panels/WorldMapNavigationPanel.js").replace("//GENERATED_CODE", "x = ${location.x}; y = ${location.y};"))
        waitFor(3, TimeUnit.SECONDS) { bot.displayPerception.worldMap?.ringMenuVisible ?: false }
    }


    private fun goToWorldMap(bot: Bot) {
        if (bot.displayPerception.dialogVisible("LocationsDialog")) {
            val kingdom = configuration.kingdom
            val scriptCode = getScript("/script/actions/dialogs/locations/GoToKingdom.js")
                    .replace("//GENERATED_CODE", "kingdomID = ${kingdom.kid};")
            val result = bot.execute(scriptCode)
            LogManager.getLogger().debug("Switching to $kingdom result = $result")
            waitFor(2, TimeUnit.SECONDS) { !bot.displayPerception.dialogVisible("LocationsDialog") }
        } else {
            LogManager.getLogger().debug("Opening locations dialog")
            bot.execute(getScript("/script/actions/panels/ActionPanel_OpenLocationsDialog.js"))
            waitFor(5, TimeUnit.SECONDS) { bot.displayPerception.dialogVisible("LocationsDialog") }
        }
    }
}