package pl.polishcivil.fourkingdoms.bot.model.bot.attack.config

import com.google.gson.annotations.JsonAdapter
import pl.polishcivil.fourkingdoms.bot.model.game.HorseOption
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.json.WaveConfigsAdapter

/**
 * Created by polish on 4/27/2017.
 */
class AttackConfig(
        @JsonAdapter(WaveConfigsAdapter::class)
        var waveConfigs: Map<pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall, List<AttackWaveConfig>> = HashMap(),
        var source: LocationConfig = LocationConfig.NIL, var target: LocationConfig = LocationConfig.NIL, var horse: HorseOption = HorseOption.NO_HORSE, var leaderOptionNr: Int = 0, var skipCoolDown: Boolean = false) : Cloneable
