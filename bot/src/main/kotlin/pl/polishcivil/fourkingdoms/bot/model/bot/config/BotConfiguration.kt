package pl.polishcivil.fourkingdoms.bot.model.bot.config

import pl.polishcivil.fourkingdoms.bot.model.bot.attack.schedule.config.AttackScheduleConfig

/**
 * Created by Polish Civil on 13.05.2017.
 */
data class BotConfiguration(val scheduleConfigs: List<AttackScheduleConfig> = emptyList(), val authKey: String = "", val username: String, val reconnectTimeoutMinutes: Int = 1)

fun botEmptyConfig(username: String) = BotConfiguration(emptyList(), "", username)