package pl.polishcivil.fourkingdoms.bot.script.impl.mediator

import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.GameModelScript
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript

/**
 * Created by polish on 4/25/2017.
 */
class GGSMediatorScript : GameModelScript("robotlegs.bender.extensions.mediatorMap.api.IMediatorMap") {
    init {
        getField(GameModelScript.MODEL_INSTANCE_VAR_NAME, MEDIATOR_FACTORY_VAR, "_ggsFactory")
        getField(GameModelScript.MODEL_INSTANCE_VAR_NAME, VIEW_HANDLER_VAR, "_viewHandler")
    }

    companion object {
        val MEDIATOR_FACTORY_VAR = "mediatorFactory"
        val VIEW_HANDLER_VAR = "viewHandler"

        fun test(script: LayoutManagerScript): RemoteScriptResultMapper<String?> {
            //grab the mediator map
            script.putStr("v0", "robotlegs.bender.extensions.mediatorMap.api.IMediatorMap")
            script.invokeStatic("controller.impl.ClassController", "mediatorMapInstanceDef", "getDefinition", "v0")
            script.invokeStatic("com.goodgamestudios.mobileBasic.application.AppCore", "appCore", "getInstance")
            script.invoke("appCore", "mediatorMapInstance", "getModel", "mediatorMapInstanceDef")
            //grab the view handler
            script.getField("mediatorMapInstance", MEDIATOR_FACTORY_VAR, "_ggsFactory")
            script.getField("mediatorMapInstance", VIEW_HANDLER_VAR, "_viewHandler")
            script.getField(VIEW_HANDLER_VAR, "_factory", "_factory")
            //grab the mediator dict
            script.getField("_factory", "_mediators", "_mediators")

            //grab the mediator for dialog instance
//            script.getDict("_mediators", "result0", LayoutManagerScript.SELECTED_DIALOG_VAR)

            script.getField(VIEW_HANDLER_VAR, "_knownMappings", "_knownMappings")
            script.putStr("dialogClsName", "com.goodgamestudios.castle.gameplay.attack.views.attack.AttackDialog")
            script.invokeStatic("controller.impl.ClassController", "dialogCls", "getDefinition", "dialogClsName")
            script.getDict("_knownMappings", "mappings", "dialogCls")
            script.getField("mappings", "result1", "2")
            script.invoke(MEDIATOR_FACTORY_VAR, "result2", "getMediator", LayoutManagerScript.SELECTED_DIALOG_VAR, "result1")
            script.getField("result2", "result3", "_currentFlankId")
//            script.invokeStatic("org.as3commons.lang.DictionaryUtils", "result1", "getKeys", "result0")
            script.pushVar("result3")
            return RemoteScriptResultMapper.justStringMapper("result3", script)
        }


    }
}