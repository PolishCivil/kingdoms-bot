package pl.polishcivil.fourkingdoms.bot

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.control.execute
import pl.polishcivil.fourkingdoms.bot.input.Keyboard
import pl.polishcivil.fourkingdoms.bot.input.Mouse
import pl.polishcivil.fourkingdoms.bot.net.BotConnection
import pl.polishcivil.fourkingdoms.bot.perception.PerceptionService
import pl.polishcivil.fourkingdoms.bot.perception.impl.bot.BotPerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.GamePerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.DisplayPerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.internal.InternalDataPerception
import pl.polishcivil.fourkingdoms.bot.task.TaskService
import pl.polishcivil.fourkingdoms.bot.task.main.DevTask
import pl.polishcivil.fourkingdoms.universe.BotUniverse
import pl.polishcivil.fourkingdoms.universe.event.impl.BotPerceptionUpdateEvent
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 4/19/2017.
 */
class Bot(val connection: BotConnection, private val universe: BotUniverse) {
    private val perceptionService = PerceptionService(this)

    val mouse = Mouse(this)
    val keyboard = Keyboard(this)

    val gamePerception = GamePerception()

    var displayPerception = gamePerception.display
        get() = gamePerception.display

    var internalDataPerception = gamePerception.internal
        get() = gamePerception.internal

    val perception = BotPerception()

    val taskService = TaskService(this, universe)

    private var lastGamePerceptionUpdate = 0L
    var perceptionAge: Long = Long.MAX_VALUE
        get() = System.currentTimeMillis() - lastGamePerceptionUpdate

    init {
        taskService.start()
        perceptionService.start()
    }

    fun destroy() {
        taskService.stop()
        perceptionService.stop()
    }


    fun updatePerception() {
        try {
            gamePerception.update(this)
            perception.update(this)
        } catch(e: Exception) {
            LogManager.getLogger().catching(e)
        }

        universe.eventBus.submit(BotPerceptionUpdateEvent(this))
        this.lastGamePerceptionUpdate = System.currentTimeMillis()
    }

    override fun toString(): String {
        return "Bot ${connection.connection}"
    }

}