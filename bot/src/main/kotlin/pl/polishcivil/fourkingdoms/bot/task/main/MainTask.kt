package pl.polishcivil.fourkingdoms.bot.task.main

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.auth.AuthData
import pl.polishcivil.fourkingdoms.auth.Authentication
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.exec
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.impl.screens.CastleDisconnectedScreenScript
import pl.polishcivil.fourkingdoms.bot.task.GameTask
import pl.polishcivil.fourkingdoms.bot.task.utils.waitFor
import java.util.concurrent.TimeUnit

/**
 * Created by Polish Civil on 13.05.2017.
 */
class MainTask(configuration: BotConfiguration) : GameTask<BotConfiguration>(configuration) {
    val dev: DevTask? = null
    val attackingController = AttackingController(configuration)
    val auth: AuthData = Authentication.encodeHash(configuration.authKey)
    var invalid = false
    var reconnectedTime = -1L
    override fun checkFinished(bot: Bot): Boolean {
        return false//main task never finishes
    }

    override fun execute(bot: Bot) {
        if (invalid) return

        dev?.let {
            it.execute(bot)
            return
        }

        val player = bot.gamePerception.internal.player ?: return
        val validUsername = auth.usernames.any { it.equals(player.userName, true) }
        if (!validUsername) {
            System.err.println("User not found in auth data ${auth}")
            invalid = true
            return
        }
        val validDate = auth.isValid()
        if (!validDate) {
            System.err.println("${auth} expired")
            invalid = true
            return
        }


        if (bot.displayPerception.screenVisible("CastleDisconnectedScreen") && bot.displayPerception.shownScreens.size == 1) {
            if (reconnectedTime == -1L) {
                LogManager.getLogger().debug("Disconnected..")
                reconnectedTime = System.currentTimeMillis()
            }
            if (!bot.displayPerception.screenVisible("CastleIsoScreen") && !bot.displayPerception.shownPanels.contains("CastleAllianceChatPanel")) {
                val timeReconnected = System.currentTimeMillis() - reconnectedTime
                if (timeReconnected >= configuration.reconnectTimeoutMinutes * 60000) {
                    LogManager.getLogger().debug("Reconnecting..")
                    bot.exec(CastleDisconnectedScreenScript.reconnect())?.let {
                        val waitFor = waitFor(30, TimeUnit.SECONDS) { bot.displayPerception.screenVisible("CastleIsoScreen") && bot.displayPerception.shownPanels.contains("CastleAllianceChatPanel") }
                        LogManager.getLogger().debug("Reconnecting... $it $waitFor")
                    }
                } else {
                    LogManager.getLogger().debug("Disconnected... for ${timeReconnected / 60000} minutes...")
                }
            }
            return
        }
        this.reconnectedTime = -1
        firstDummyDialog(bot)?.let { dialogName ->
            LogManager.getLogger().debug("Closing dummy dialog $dialogName")
            bot.exec(LayoutManagerScript.closeDialog(dialogName))
            waitFor(1, TimeUnit.SECONDS, { !bot.displayPerception.dialogVisible(dialogName) })
            return
        }

        if (attackingController.execute(bot)) {
            return
        }
    }

    fun firstDummyDialog(bot: Bot): String? {
        val visibleDialogNames = bot.displayPerception.shownDialogNames
        val dummyDialogNames = listOf(
                "SpecialEventsPrimeTimeAllianceDialog",
                "ShoppingCartPrimeDayDialog",
                "RedBannerRewardDialog",
                "Dialog_WhatsNew",
                "LoginBonusDialog",
                "SpecialEventsPrivatePrimeTimeDialog",
                "CombinedRewardsDialog",
                "WhaleChestOfferDialog",
                "DidYouKnowRepairDialog",
                "GenericNoRewardEventFinisherDialog",
                "PaymentRewardOfferDialog",
                "QuestCompletedDialog",
                "AnnouncementsDialog",
                "ArtifactMainDialog",
                "ArtifactStarterDialog",
                "ArtifactStarterDialog",
                "MultiRewardChestOfferDialog",
                "TieredPrimeDayDialog"
        )
        return dummyDialogNames.lastOrNull { visibleDialogNames.contains(it) }

    }
}