package pl.polishcivil.fourkingdoms.bot.model.game

import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom

/**
 * Created by polish on 4/19/2017.
 */
data class LocationConfig(val kingdom: pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom, val x: Int, val y: Int) {
    constructor(point: Point, kingdom: pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom) : this(kingdom, point.x.toInt(), point.y.toInt())

    companion object {
        val NIL = pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig(Kingdom.UNKNOWN, -1, -1)
    }
}