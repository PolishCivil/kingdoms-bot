package pl.polishcivil.fourkingdoms.bot.script.impl.attack

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.game.attack.AttackSetupUnitVO
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.GameModelScript
import pl.polishcivil.fourkingdoms.bot.script.utils.deserializeLocation

/**
 * Created by polish on 5/6/2017.
 */
class AttackSetupManagerScript : GameModelScript("com.goodgamestudios.castle.gameplay.attack.services.attackSetup.AttackSetupManager") {

    companion object {
        fun selectedUnits(): RemoteScriptResultMapper<List<AttackSetupUnitVO>?> {
            val script = AttackSetupManagerScript()
            script.getField(MODEL_INSTANCE_VAR_NAME, "selectedUnits", "selectedUnits")
            script.invoke("selectedUnits", "all", "getAll")
            script.pushJSON("all")
            return RemoteScriptResultMapper.jsonListMapper<AttackSetupUnitVO>("all", script)
        }

        fun sourceTargetPair(): RemoteScriptResultMapper<Pair<LocationConfig, LocationConfig>?> {
            val script = AttackSetupManagerScript()
            script.getField(MODEL_INSTANCE_VAR_NAME, "performingAttackData", "performingAttackData")
            script.getField("performingAttackData", "attackInfo", "attackInfo")
            script.getField("attackInfo", "sourceArea", "sourceArea")
            script.getField("attackInfo", "targetArea", "targetArea")

            val source = script.deserializeLocation("sourceArea", "source")
            val target = script.deserializeLocation("targetArea", "target")

            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                Pair(source.toLocation(resultSet)!!, target.toLocation(resultSet)!!)
            })
        }
    }
}