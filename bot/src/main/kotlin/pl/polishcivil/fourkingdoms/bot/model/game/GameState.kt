package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by polish on 4/19/2017.
 */
enum class GameState {
    ISO_MAP,
    WORLD_MAP,
    UNKNOWN
}