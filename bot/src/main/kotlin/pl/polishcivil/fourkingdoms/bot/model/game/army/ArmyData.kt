package pl.polishcivil.fourkingdoms.bot.model.game.army

import pl.polishcivil.fourkingdoms.bot.model.game.map.MapMovementVO

/**
 * Created by Polish Civil on 13.05.2017.
 */
data class ArmyData(val moves: List<MapMovementVO> = emptyList())