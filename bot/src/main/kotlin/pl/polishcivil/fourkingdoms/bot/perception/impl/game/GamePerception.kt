package pl.polishcivil.fourkingdoms.bot.perception.impl.game

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.execute
import pl.polishcivil.fourkingdoms.bot.perception.IPerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.DisplayPerception
import pl.polishcivil.fourkingdoms.bot.perception.impl.game.internal.InternalDataPerception
import pl.polishcivil.fourkingdoms.bot.task.main.DevTask
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 6/11/17.
 */
class GamePerception : IPerception {
    var display = DisplayPerception()
    var internal = InternalDataPerception()

    override fun update(bot: Bot) {
        val script = DevTask::class.java.getResource("/update_perception.js").readText()
        val response = bot.execute(script)
        try {
            val first = response.first
            val deserialize = JSONUtils.deserialize(first, Wrapper::class.java)
            this.display = deserialize.display
            this.internal = deserialize.internal
        } catch(e: Exception) {
            LogManager.getLogger().error("Something went wrong while updating the game perception!")
            LogManager.getLogger().error("Response = $response")
            LogManager.getLogger().catching(e)
        }
        LogManager.getLogger().trace("Game perception build took internally ${response.second} ms.")
    }


    data class Wrapper(val display: DisplayPerception, val internal: InternalDataPerception)
}