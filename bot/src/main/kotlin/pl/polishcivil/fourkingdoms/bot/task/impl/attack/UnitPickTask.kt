package pl.polishcivil.fourkingdoms.bot.task.impl.attack

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.exec
import pl.polishcivil.fourkingdoms.bot.model.game.InventoryItemVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.AvailableUnitListItemVO
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig
import pl.polishcivil.fourkingdoms.bot.script.impl.attack.AttackDialogScript
import pl.polishcivil.fourkingdoms.bot.script.impl.attack.AttackUnitSetupDialogScript
import pl.polishcivil.fourkingdoms.bot.task.GameTask
import pl.polishcivil.fourkingdoms.bot.task.utils.waitFor
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitDefinition
import java.util.concurrent.TimeUnit

/**
 * Created by polish on 5/6/2017.
 */
class UnitPickTask(configuration: List<AttackUnitConfig>) : GameTask<List<AttackUnitConfig>>(configuration) {

    private var discoveredAbsoluteAmounts = HashMap<AttackUnitConfig, Int>()
    private var settingsApplied = false

    override fun checkFinished(bot: Bot): Boolean {

        if (settingsApplied) {
            return true
        }
        if (!checkSelectedUnits(bot)) {
            return false
        }
        return false
    }

    override fun execute(bot: Bot) {
        //sanity check
        sanityCheck(bot)

        if (configuration.isEmpty()) {
            bot.exec(AttackUnitSetupDialogScript.applyRect())?.let {
                bot.mouse.click(it)
                settingsApplied = waitFor(5, TimeUnit.SECONDS, { bot.displayPerception.attackSetup == null })
                LogManager.getLogger().debug("Applied = $settingsApplied")
            }
            return
        }

        val selectedUnitsCount = bot.exec(AttackUnitSetupDialogScript.selectedUnitCount())!!
        val availableUnits = bot.exec(AttackDialogScript.unitInventory())!!
        val checkSelectedUnits = checkSelectedUnits(bot)

        if (!checkSelectedUnits && selectedUnitsCount > 0) {
            val slotCount = bot.exec(AttackUnitSetupDialogScript.availableSlots())!!
            for (i in 0..slotCount - 1) {
                if (bot.exec(AttackUnitSetupDialogScript.selectedUnitCount())!! == 0) {
                    break
                }
                bot.exec(AttackUnitSetupDialogScript.slotRectangle(i))?.let {
                    bot.mouse.click(it)
                    bot.exec(AttackUnitSetupDialogScript.clearSlotRect())
                }?.let {
                    bot.mouse.click(it)
                }
            }
        } else {
            if (!checkSelectedUnits) {
                LogManager.getLogger().info("Selecting units... ${checkSelectedUnits(bot)}")
                val slotCount = bot.exec(AttackUnitSetupDialogScript.availableSlots())!!
                for (slot in 0..slotCount - 1) {
                    val configuration = configuration.getOrNull(slot)
                    configuration?.let { config ->
                        bot.exec(AttackUnitSetupDialogScript.slotRectangle(slot))?.let {
                            bot.mouse.click(it)
                            val unitID = findUnit(availableUnits, config)
                            val maxUnitsAmount = bot.exec(AttackUnitSetupDialogScript.maxUnitsAmount())!!
                            val type = config.type()
                            if (ensureVisibleTypes(bot, type)) {
                                bot.exec(AttackUnitSetupDialogScript.scrollToUnit(unitID))?.let {
                                    LogManager.getLogger().debug("Scrolled to unit $unitID")
                                    findListItemIndex(bot, unitID)?.let {
                                        (index, listItem) ->
                                        val absoluteAmount = if (config.percentage) {
                                            (maxUnitsAmount * (config.unitAmount / 100.0)).toInt()
                                        } else {
                                            config.unitAmount
                                        }
                                        if (listItem.maxAmount < absoluteAmount) {
                                            throw AttackTaskException("Amount exceeded! $listItem $absoluteAmount")
                                        }
                                        if (listItem.selectedAmount != absoluteAmount) {
                                            LogManager.getLogger().info("Setting up the unit amount at slot $slot item index $index item $listItem to $absoluteAmount")
                                            bot.exec(AttackUnitSetupDialogScript.setSelectedAmountListItem(index, absoluteAmount))?.let {
                                                discoveredAbsoluteAmounts.put(config, absoluteAmount)
                                            }
                                        }
                                    }
                                } ?: throw RuntimeException("Couldn't scroll to $configuration")
                            }
                        }
                    }
                }
            }

            val selectedProperly = checkSelectedUnits(bot)
            if (selectedProperly) {
                LogManager.getLogger().info("Units selected successfully!, applying changes")
                bot.exec(AttackUnitSetupDialogScript.applyRect())?.let {
                    bot.mouse.click(it)
                    settingsApplied = waitFor(5, TimeUnit.SECONDS, { bot.displayPerception.attackSetup == null })
                    LogManager.getLogger().debug("Applied = $settingsApplied")
                }
            } else {
                LogManager.getLogger().warn("Couldn't select units properly.")
            }
        }
    }

    private fun findListItemIndex(bot: Bot, unitID: Int): Pair<Int, AvailableUnitListItemVO>? {
        for (index in 0..3) {
            bot.exec(AttackUnitSetupDialogScript.unitListItem(index))?.let {
                if (it.availableUnitVO.wodID == unitID) {
                    return Pair(index, it)
                }
            }
        }
        return null
    }

    private fun checkSelectedUnits(bot: Bot): Boolean {
        val selectedUnits = bot.exec(AttackUnitSetupDialogScript.selectedUnits())!!
        for (config in configuration) {
            val selected = selectedUnits.selectedUnits.filter { it.id == config.id() }
            if (selected.isEmpty()) {
                return false
            }
            for (unitVO in selected) {
                if (unitVO.amount == discoveredAbsoluteAmounts[config]) {
                    return true
                }
            }
        }
        return true
    }

    private fun sanityCheck(bot: Bot) {
        val soldiers = bot.exec(AttackUnitSetupDialogScript.isSoldiers())!!
        for (attackUnitConfig in configuration) {
            val soldier = attackUnitConfig.isBestMeleeUnit() || attackUnitConfig.isBestRangeUnit()
            if (soldier && !soldiers) {
                throw RuntimeException("WRONG DIALOG!")
            }
            val byID = EmpireCloud.getINSTANCE().itemsFile.units.findByID(attackUnitConfig.id())
            if (byID is ToolUnitDefinition && soldiers) {
                throw RuntimeException("WRONG DIALOG!")
            }
            if (byID !is ToolUnitDefinition && !soldiers) {
                throw RuntimeException("WRONG DIALOG!")
            }
        }
    }

    private fun findUnit(availableUnits: List<InventoryItemVO>, config: AttackUnitConfig): Int {
        if (config.isBestRangeUnit()) {
            //find best ranged
            TODO("Not available")
        } else if (config.isBestMeleeUnit()) {
            //find best melee
            TODO("Not available")
        }
        val id = config.id()
        availableUnits.find { it.wodId == id } ?: throw RuntimeException("Unit id $id not found! available = ${availableUnits}")
        return id
    }

    private fun ensureVisibleTypes(bot: Bot, type: AttackUnitConfig.Type): Boolean {
        if (type == AttackUnitConfig.Type.TOOL) {
            //its tool so sanity check does the job
            return true
        } else {
            if (selectedUnitType(bot) == type) {
                return true
            }
            bot.exec(AttackUnitSetupDialogScript.meleeRangedTabButtons())?.let {
                if (type == AttackUnitConfig.Type.MELEE) {
                    bot.mouse.click(it.first)
                } else {
                    bot.mouse.click(it.second)
                }
            }
            return waitFor(5, TimeUnit.SECONDS, { selectedUnitType(bot) == type })
        }
    }


    private fun selectedUnitType(bot: Bot): AttackUnitConfig.Type {
        val soldiers = bot.exec(AttackUnitSetupDialogScript.isSoldiers())!!
        if (!soldiers) {
            return AttackUnitConfig.Type.TOOL
        }
        val melee = bot.exec(AttackUnitSetupDialogScript.isMeleeUnits())!!
        if (melee) {
            return AttackUnitConfig.Type.MELEE
        }
        return AttackUnitConfig.Type.RANGED
    }
}