package pl.polishcivil.fourkingdoms.bot.model.game.player

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig

/**
 * Created by Polish Civil on 14.05.2017.
 */
data class CastleUserData(val gold: Int, val rubies: Int, val ownedLocations: List<LocationConfig>)