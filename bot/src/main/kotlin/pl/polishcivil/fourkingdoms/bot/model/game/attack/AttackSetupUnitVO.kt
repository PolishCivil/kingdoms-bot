package pl.polishcivil.fourkingdoms.bot.model.game.attack

import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall

/**
 * Created by polish on 5/3/2017.
 */
data class AttackSetupUnitVO
(
        private val flankId: Int,
        val waveId: Int,
        val slotId: String,
        val amount: Int,
        val maxAmount: Int,
        val id: Int
) {
    val flank: pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall
        get() = pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall.values()[flankId]
}
