package pl.polishcivil.fourkingdoms.bot.script.impl.attack

import pl.polishcivil.fourkingdoms.bot.model.game.HorseOption
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangle

/**
 * Created by Polish Civil on 13.05.2017.
 */
class PostAttackPopupDialogScript : LayoutManagerScript() {
    init {
        selectDialog("PostAttackPopUpDialog")
    }

    companion object {
        fun selectedHorse(): RemoteScriptResultMapper<HorseOption?> {
            val script = PostAttackPopupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "optionListHorses", "optionListHorses")
            script.getField("optionListHorses", "selectedIndex", "selectedIndex")
            script.pushJSON("selectedIndex")
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                HorseOption.forIndex(resultSet.getVar("selectedIndex")!!.toInt())
            })
        }

        fun selectHorse(option: HorseOption): RemoteScriptResultMapper<String?> {
            val script = PostAttackPopupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "optionListHorses", "optionListHorses")
            script.putInt("index", option.index)
            script.invoke("optionListHorses", "result", "selectItemByIndex", "index")
            script.pushVar("result")
            return RemoteScriptResultMapper.justStringMapper("result", script)
        }

        fun yesButton(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = PostAttackPopupDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "dispClip", "dispClip")
            script.getField("dispClip", "btn_yes", "btn_yes")
            script.getDisplayRectangle("btn_yes", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }
    }
}