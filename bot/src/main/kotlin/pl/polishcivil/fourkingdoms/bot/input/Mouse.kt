package pl.polishcivil.fourkingdoms.bot.input

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.game.Point
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle

/**
 * Created by polish on 4/19/2017.
 */
class Mouse(private val bot: Bot) {

    val point: Point
        get() {
            val mouseX = java.lang.Double.parseDouble(bot.connection.get(CONTROLLER, "mouseX").first).toInt()
            val mouseY = java.lang.Double.parseDouble(bot.connection.get(CONTROLLER, "mouseY").first).toInt()
            return Point(mouseX, mouseY)
        }

    fun setMouseTo(point: Point) = bot.connection.invoke(CONTROLLER, "setLocation", point.x, point.y)

    fun setMouseTo(x: Number, y: Number) = setMouseTo(Point(x, y))

    fun click(point: Point) {
        setMouseTo(point)
        bot.connection.invoke(CONTROLLER, "touchDown")
        bot.connection.invoke(CONTROLLER, "touchUp")
    }

    fun click(x: Number, y: Number) = click(Point(x, y))

    fun click(displayRectangle: DisplayRectangle) {
        click(displayRectangle.x.toInt() + displayRectangle.width.toInt() / 2, displayRectangle.y.toInt() + displayRectangle.height.toInt() / 2)
    }

    companion object {
        val CONTROLLER = "MouseController"
    }
}