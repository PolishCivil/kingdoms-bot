package pl.polishcivil.fourkingdoms.bot.script.impl.attack

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.utils.deserializeLocation
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangleUIElement

/**
 * Created by Polish Civil on 15.05.2017.
 */
class CooldownDialogScript : LayoutManagerScript() {
    init {
        selectDialog("CooldownDialog")
    }

    companion object {
        fun remainingTimeLocationPair(): RemoteScriptResultMapper<Pair<Int, LocationConfig>?> {
            val script = CooldownDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "properties", "properties")
            script.getField("properties", "mapObjectVO", "mapObjectVO")
            script.getAndPushField("properties", "remainingAttackCooldownTimeInSeconds", "remainingAttackCooldownTimeInSeconds")
            val locationVarData = script.deserializeLocation("mapObjectVO", "location")

            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                locationVarData.toLocation(resultSet)?.let { location ->
                    val seconds = resultSet.getVar("remainingAttackCooldownTimeInSeconds")!!.toInt()
                    Pair(seconds, location)
                }
            })
        }

        fun showSkipsButton(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = CooldownDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "payCostsBtn", "payCostsBtn")
            script.getDisplayRectangleUIElement("payCostsBtn", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }
    }
}