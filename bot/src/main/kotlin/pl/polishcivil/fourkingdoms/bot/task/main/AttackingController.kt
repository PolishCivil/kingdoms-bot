package pl.polishcivil.fourkingdoms.bot.task.main

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration
import pl.polishcivil.fourkingdoms.bot.task.impl.attack.schedule.ScheduleTask

/**
 * Created by Polish Civil on 15.05.2017.
 */
class AttackingController(configuration: BotConfiguration) {
    private val attackTasks = configuration.scheduleConfigs.map { ScheduleTask(it) }
    private var currentSchedule: ScheduleTask? = null

    fun execute(bot: Bot): Boolean {
        if (currentSchedule == null) {
            currentSchedule = nextScheduleTask(bot) ?: return false
            return true
        } else {
            val task = currentSchedule!!
            if (task.shouldExecute(bot)) {
                task.execute(bot)
                return true
            } else {
                currentSchedule = null
                return false
            }
        }
    }

    private fun nextScheduleTask(bot: Bot) = attackTasks.firstOrNull { it.shouldExecute(bot) }

    fun currentTask() = currentSchedule

    fun tasks() = attackTasks.toList()
}
