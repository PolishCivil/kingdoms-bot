package pl.polishcivil.fourkingdoms.bot.script.impl.equipment

import pl.polishcivil.fourkingdoms.bot.model.game.LordVO
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultMapper
import pl.polishcivil.fourkingdoms.bot.script.impl.attack.AttackDialogScript
import pl.polishcivil.fourkingdoms.bot.script.impl.layout.LayoutManagerScript
import pl.polishcivil.fourkingdoms.bot.script.impl.mediator.GGSMediatorScript
import pl.polishcivil.fourkingdoms.bot.script.utils.getDisplayRectangleUIElement

/**
 * Created by polish on 5/18/17.
 */
class LordOverviewDialogScript : LayoutManagerScript() {
    init {
        selectDialog("LordOverviewDialog")
        //grab appcore instance
        invokeStatic("com.goodgamestudios.mobileBasic.application.AppCore", "appCore", "getInstance")

        //grab the mediator map
        putStr("v0", "robotlegs.bender.extensions.mediatorMap.api.IMediatorMap")
        invokeStatic("controller.impl.ClassController", "mediatorMapInstanceDef", "getDefinition", "v0")
        invoke("appCore", "mediatorMapInstance", "getModel", "mediatorMapInstanceDef")
        //grab the view handler
        getField("mediatorMapInstance", GGSMediatorScript.MEDIATOR_FACTORY_VAR, "_ggsFactory")
        getField("mediatorMapInstance", GGSMediatorScript.VIEW_HANDLER_VAR, "_viewHandler")
        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_factory", "_factory")
        //grab the mediator dict
        getField("_factory", "_mediators", "_mediators")


        getField(GGSMediatorScript.VIEW_HANDLER_VAR, "_knownMappings", "_knownMappings")
        putStr("dialogClsName", "com.goodgamestudios.castle.gameplay.equipment.views.LordOverviewDialog")
        invokeStatic("controller.impl.ClassController", "dialogCls", "getDefinition", "dialogClsName")
        getDict("_knownMappings", "mappings", "dialogCls")
        getField("mappings", "result1", "1")
        invoke(GGSMediatorScript.MEDIATOR_FACTORY_VAR, MEDIATOR_VAR, "getMediator", LayoutManagerScript.SELECTED_DIALOG_VAR, "result1")
    }

    companion object {
        const val MEDIATOR_VAR = "LordOverviewMediatorVar"

        fun selectedLordID(): RemoteScriptResultMapper<LordVO?> {
            val script = LordOverviewDialogScript()
            script.getField(MEDIATOR_VAR, "currentLord", "currentLord")
            script.getAndPushFieldJSON("currentLord", "consecutiveLordID", "consecutiveLordID")
            script.getAndPushFieldJSON("currentLord", "id", "id")
            script.getAndPushFieldJSON("currentLord", "isAvailableForMovement", "isAvailableForMovement")
            script.getAndPushFieldJSON("currentLord", "type", "type")

            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                resultSet.getVar("consecutiveLordID")?.let {
                    LordVO(resultSet.getVar("id")!!.toInt(),
                            resultSet.getVar("consecutiveLordID")!!.toInt(),
                            resultSet.getVar("isAvailableForMovement")!!.toBoolean(),
                            resultSet.getVar("type")!!
                    )
                }
            })
        }

        fun selectLord(lordID: Int): RemoteScriptResultMapper<String?> {
            val script = LordOverviewDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "lordSwitcher", "lordSwitcher")
            script.putInt("id", lordID)
            script.invoke("lordSwitcher", "result", "setSelectionByID", "id")
            return RemoteScriptResultMapper.justStringMapper("result", script)
        }

        fun applyButton(): RemoteScriptResultMapper<DisplayRectangle?> {
            val script = LordOverviewDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "okButton", "okButton")
            script.getDisplayRectangleUIElement("okButton", "rectangle")
            script.pushJSON("rectangle")
            return RemoteScriptResultMapper.jsonMapper("rectangle", script)
        }

        fun availableLordIDs(): RemoteScriptResultMapper<List<LordVO>?> {
            val script = LordOverviewDialogScript()
            script.getField(SELECTED_DIALOG_VAR, "lordSwitcher", "lordSwitcher")
            script.getField("lordSwitcher", "lords", "lords")

            for (i in 0..20) {
                script.getField("lords", "lord_$i", "$i")
                script.getAndPushField("lord_$i", "lord_${i}_id_0", "consecutiveLordID")
                script.getAndPushField("lord_$i", "lord_${i}_id_1", "id")
                script.getAndPushField("lord_$i", "lord_${i}_isAvailableForMovement", "isAvailableForMovement")
                script.getAndPushField("lord_$i", "lord_${i}_type", "type")
            }
            return RemoteScriptResultMapper.customMapper(script, { resultSet ->
                val list = mutableListOf<LordVO>()
                for (i in 0..20) {
                    resultSet.getVar("lord_${i}_id_0")?.let {
                        consecutive ->
                        if (consecutive != "undefined") {
                            val consecutiveLordID = resultSet.getVar("lord_${i}_id_0")!!.toInt()
                            val rawLordID = resultSet.getVar("lord_${i}_id_1")!!.toInt()
                            val isAvailableForMovement = resultSet.getVar("lord_${i}_isAvailableForMovement")!!.toBoolean()
                            val type = resultSet.getVar("lord_${i}_type")!!
                            list.add(LordVO(rawLordID, consecutiveLordID, isAvailableForMovement, type))
                        }
                    }
                }
                list
            })
        }
    }
}