package pl.polishcivil.fourkingdoms.bot.model.game.layout

import java.awt.Point
import java.awt.Rectangle

/**
 * Created by polish on 4/20/2017.
 */
data class DisplayRectangle(val x: Number, val y: Number, val width: Number, val height: Number) {

    fun contains(point: java.awt.Point) = toRect().contains(point)

    fun contains(other: java.awt.Rectangle?): Boolean {
        if (other == null) {
            return false
        }
        return toRect().contains(other)
    }

    fun contains(other: DisplayRectangle?): Boolean {
        if (other == null) {
            return false
        }
        return toRect().contains(other.toRect())
    }

    fun toRect() = java.awt.Rectangle(x.toInt(), y.toInt(), width.toInt(), height.toInt())

    companion object {
        val NIL = DisplayRectangle(-1, -1, -1, -1)
    }
}