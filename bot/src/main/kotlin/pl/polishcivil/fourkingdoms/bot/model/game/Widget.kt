package pl.polishcivil.fourkingdoms.bot.model.game

import java.awt.Rectangle
import java.util.*

/**
 * Created by polish on 4/19/2017.
 */

class Widget(val parent: Widget?, val x: Int, val y: Int, val width: Int, val height: Int, val desc: String, val text: String, val children: LinkedList<Widget>) {
    companion object {
        val ROOT_WIDGET = Widget(null, -1, -1, -1, -1, "", "", LinkedList())
    }

    val rectangle = Rectangle(x, y, width, height)

    fun isRoot(): Boolean {
        return parent == ROOT_WIDGET
    }

    override fun toString(): String {
        return "Widget [${parent?.children?.indexOf(this) ?: -1}] desc = '$desc' text = '$text' children_cnt = ${children.size}"
    }
}