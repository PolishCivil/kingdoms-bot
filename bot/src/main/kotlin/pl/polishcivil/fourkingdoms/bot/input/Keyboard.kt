package pl.polishcivil.fourkingdoms.bot.input

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.game.layout.DisplayRectangle

/**
 * Created by polish on 4/19/2017.
 */

class Keyboard(private val bot: Bot) {
    companion object {
        val CONTROLLER = "KeyboardController"
    }


    fun setText(text: String, displayRectangle: DisplayRectangle) {
        bot.mouse.click(displayRectangle)
        Thread.sleep(250)
        bot.connection.invoke(CONTROLLER, "setText", text, displayRectangle.x.toInt(), displayRectangle.y.toInt())
    }
}

