package pl.polishcivil.fourkingdoms.bot.task

import org.apache.logging.log4j.LogManager
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration
import pl.polishcivil.fourkingdoms.bot.model.bot.config.botEmptyConfig
import pl.polishcivil.fourkingdoms.bot.task.main.MainTask
import pl.polishcivil.fourkingdoms.universe.BotUniverse
import pl.polishcivil.fourkingdoms.universe.event.impl.BotConfigurationChangedEvent
import pl.polishcivil.fourkingdoms.universe.event.impl.BotTaskUpdateEvent
import pl.polishcivil.projects.jkingdoms.core.BotConstants
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Created by polish on 4/19/2017.
 */
class TaskService(val bot: Bot, val universe: BotUniverse) {
    val executor = Executors.newSingleThreadScheduledExecutor({ r -> Thread(r, "TaskService $bot") })!!
    var status = TaskServiceStatus.RESOLVING_BOT

    var resolvedConfiguration: BotConfiguration? = null
    private var appliedConfiguration: BotConfiguration? = null
    var mainTask: MainTask? = null

    fun start() {
        LogManager.getLogger().info("Task executor for bot $bot started!")
        executor.scheduleAtFixedRate({
            try {
                loop()
                universe.eventBus.submit(BotTaskUpdateEvent(bot))
            } catch(e: Exception) {
                LogManager.getLogger().catching(e)
            }
        }, 0, 500, TimeUnit.MILLISECONDS)
    }

    private fun loop() {
        if (resolvedConfiguration == null) {
            status = TaskServiceStatus.RESOLVING_BOT
        }

        when (status) {
            TaskServiceStatus.RESOLVING_BOT -> {
                bot.internalDataPerception.player?.userName?.let { username ->
                    val file = BotConstants.getBotConfigurationFile(username)
                    var configuration = if (file.exists()) {
                        JSONUtils.deserialize(file, BotConfiguration::class.java)
                    } else {
                        botEmptyConfig(username)
                    }
                    resolvedConfiguration = configuration
                    appliedConfiguration = configuration.copy()
                    status = TaskServiceStatus.RUNNING
                }
            }
            TaskServiceStatus.RUNNING -> {
                if (mainTask == null) {
                    mainTask = MainTask(appliedConfiguration!!)
                    universe.eventBus.submit(BotConfigurationChangedEvent(bot))
                } else {
                    bot.gamePerception.internal.player?.let {
                        player ->
                        val appliedUsername = appliedConfiguration!!.username
                        if (appliedUsername.equals(player.userName, true)) {
                            mainTask!!.execute(bot)
                        } else {
                            LogManager.getLogger().info("Player switched users saving current config for $appliedUsername!")
                            val botConfigurationFile = BotConstants.getBotConfigurationFile(appliedUsername)
                            JSONUtils.serialize(appliedConfiguration!!, botConfigurationFile)
                            resolvedConfiguration = null
                        }
                    }

                }
            }
            TaskServiceStatus.PAUSED -> {

            }
        }
    }

    fun reconfigure(configuration: BotConfiguration) {
        bot.internalDataPerception.player?.userName?.let { username ->
            var config = configuration
            if (configuration.username != username) {
                config = configuration.copy(username = username)
            }
            JSONUtils.serialize(config, BotConstants.getBotConfigurationFile(username))
        }
        resolvedConfiguration = null
        appliedConfiguration = null
        mainTask = null
    }

    fun pause() {
        status = TaskServiceStatus.PAUSED
    }

    fun resume() {
        status = TaskServiceStatus.RESOLVING_BOT
    }

    fun stop() {
        LogManager.getLogger().info("Task service for bot ${this.bot} stopped!")
        executor.shutdownNow()
    }
}