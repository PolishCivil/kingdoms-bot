package pl.polishcivil.fourkingdoms.bot.model.game.attack

/**
 * Created by Polish Civil on 29.06.2017.
 */
data class UnitVO(val wodId: Int, val amount: Int)