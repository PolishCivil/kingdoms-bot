package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by Polish Civil on 15.05.2017.
 */
data class MinuteSkipVO(val id: Int, val inventoryAmount: Int, val inventoryCap: Int, val skipTimeInMinutes: Int)