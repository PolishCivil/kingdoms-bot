package pl.polishcivil.fourkingdoms.bot.perception.impl.game.display.attack

import pl.polishcivil.fourkingdoms.bot.model.game.attack.AttackWaveSlotInfoVO
import pl.polishcivil.fourkingdoms.bot.model.game.attack.SelectedUnitsModel

/**
 * Created by polish on 6/11/17.
 */
data class CastleAttackSetupDialogPerception(
        val slotInfo: AttackWaveSlotInfoVO? = null,
        val unitSetupSelectedSlot: Int = -1,
        val unitSetupSelectedWave: Int = -1,
        val unitSetupSelectedUnits: SelectedUnitsModel? = null
) {
}