package pl.polishcivil.fourkingdoms.bot.model.game.player

import pl.polishcivil.fourkingdoms.bot.model.game.LordVO

/**
 * Created by polish on 6/11/17.
 */
data class CastleLordData(val leaders:List<LordVO> = emptyList())