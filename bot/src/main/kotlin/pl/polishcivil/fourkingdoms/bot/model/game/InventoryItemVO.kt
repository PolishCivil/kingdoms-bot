package pl.polishcivil.fourkingdoms.bot.model.game

/**
 * Created by polish on 5/6/2017.
 */
data class InventoryItemVO(val wodId: Int, val amount: Int)