package pl.polishcivil.fourkingdoms.bot.model.bot.attack

import pl.polishcivil.fourkingdoms.bot.model.game.attack.UnitVO

/**
 * Created by Polish Civil on 29.06.2017.
 */
data class AvailableArmyInfo(val availableUnits: List<UnitVO>, val effectiveDefenseLevel: Int, val maxTroopAmountFlank: Int, val maxTroopAmountFront: Int, val maxToolAmountFlank: Int, val maxToolAmountFront: Int, val maxAllowedWaves: Int)