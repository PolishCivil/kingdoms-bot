package pl.polishcivil.fourkingdoms.bot.script

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.control.executeScript
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 4/19/2017.
 */
open class RemoteScript {
    private val codeBuilder = StringBuilder()
    private val varNames = HashSet<String>()

    private fun invokeMethod(static: Boolean, fromVariableName: String, toVariableName: String, methodName: String, paramVarNames: List<String>) {
        if (!static) {
            checkVarUsed(fromVariableName)
        }
        checkVarUnused(toVariableName)
        varNames.add(toVariableName)
        val opcode = if (static) "INVOKE_STATIC" else "INVOKE"
        codeBuilder.append(opcode).append("\t").append(fromVariableName).append("\t").append(toVariableName).append("\t").append(methodName)
        for (paramVarName in paramVarNames) {
            checkVarUsed(paramVarName)
            codeBuilder.append("\t").append(paramVarName)
        }
        codeBuilder.append("\n")
    }

    fun pushVar(varName: String) {
        checkVarUsed(varName)
        codeBuilder.append("PUSH").append("\t").append(varName).append("\n")
    }

    fun pushJSON(fromAndTo: String) {
        checkVarUsed(fromAndTo)
        codeBuilder.append("PUSH_JSON").append("\t").append(fromAndTo).append("\n")
    }

    fun json(from: String, to: String) {
        checkVarUsed(from)
        checkVarUnused(to)
        varNames.add(to)
        codeBuilder.append("JSON").append("\t").append(from).append("\t").append(to).append("\n")
    }


    fun putVar(opcode: String, varName: String, value: Any) {
        checkVarUnused(varName)
        varNames.add(varName)
        codeBuilder.append(opcode).append("\t").append(varName).append("\t").append(value).append("\n")
    }


    fun getField(fromVariableName: String, toVariableName: String, fieldName: String) {
        checkVarUnused(toVariableName)
        checkVarUsed(fromVariableName)
        varNames.add(toVariableName)
        codeBuilder.append("GET").append("\t").append(fromVariableName).append("\t").append(toVariableName).append("\t").append(fieldName).append("\n")
    }

    fun getAndPushField(fromVariableName: String, toVariableName: String, fieldName: String) {
        getField(fromVariableName, toVariableName, fieldName)
        pushVar(toVariableName)
    }

    fun getAndPushFieldJSON(fromVariableName: String, toVariableName: String, fieldName: String) {
        getField(fromVariableName, toVariableName, fieldName)
        pushJSON(toVariableName)
    }

    fun setField(fromVariableName: String, toVariableName: String, fieldName: String, valueVarName: String) {
        checkVarUnused(toVariableName)
        checkVarUsed(fromVariableName)
        checkVarUsed(valueVarName)
        varNames.add(toVariableName)
        codeBuilder.append("SET").append("\t").append(fromVariableName).append("\t").append(valueVarName).append("\t").append(toVariableName).append("\t").append(fieldName).append("\n")
    }

    fun getDict(fromVariableName: String, toVariableName: String, keyVarName: String) {
        checkVarUnused(toVariableName)
        checkVarUsed(fromVariableName)
        checkVarUsed(keyVarName)
        varNames.add(toVariableName)
        codeBuilder.append("GET_DICT").append("\t").append(fromVariableName).append("\t").append(toVariableName).append("\t").append(keyVarName).append("\n")
    }

    fun putInt(varName: String, value: Int) = putVar("PUT_INT", varName, value)

    fun putStr(varName: String, value: String) = putVar("PUT_STR", varName, value)

    fun invokeStatic(className: String, toVariableName: String, methodName: String, vararg paramVarNames: String) {
        invokeMethod(true, className, toVariableName, methodName, paramVarNames.asList())
    }

    fun invoke(fromVariableName: String, toVariableName: String, methodName: String, vararg paramVarNames: String) {
        invokeMethod(false, fromVariableName, toVariableName, methodName, paramVarNames.asList())
    }

    private fun checkVarUsed(name: String) {
        if (!varNames.contains(name)) {
            throw RuntimeException("Script's variable $name isn't used, code = ${getCode()}")
        }
    }

    private fun checkVarUnused(name: String) {
        if (varNames.contains(name)) {
            throw RuntimeException("Script's variable $name is already used, code = ${getCode()}")
        }
    }

    fun getCode() = codeBuilder.toString()
}
