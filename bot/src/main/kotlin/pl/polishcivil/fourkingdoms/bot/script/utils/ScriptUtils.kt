package pl.polishcivil.fourkingdoms.bot.script.utils

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig
import pl.polishcivil.fourkingdoms.bot.model.game.Point
import pl.polishcivil.fourkingdoms.bot.script.RemoteScript
import pl.polishcivil.fourkingdoms.bot.script.RemoteScriptResultSet
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils

/**
 * Created by polish on 4/21/2017.
 */

fun RemoteScript.getDisplayRectangle(fromVar: String, toVar: String) {
    invokeStatic("controller.impl.DisplayController", toVar, "getRectangle", fromVar)
}

fun RemoteScript.getDisplayRectangleUIElement(fromVar: String, toVar: String) {
    getField(fromVar, "_displaySprite", "disp")
    invokeStatic("controller.impl.DisplayController", toVar, "getRectangle", "_displaySprite")
}

fun RemoteScript.deserializeLocation(fromWorldMapObjectVOVar: String, to: String): LocationVarData {
    getField(fromWorldMapObjectVOVar, "${to}_absAreaPos", "absAreaPos")
    getField(fromWorldMapObjectVOVar, "${to}_kingdomId", "kingdomId")
    pushJSON("${to}_absAreaPos")
    pushVar("${to}_kingdomId")
    return LocationVarData("${to}_absAreaPos", "${to}_kingdomId")
}

data class LocationVarData(val pointVar: String, val kingdomVar: String) {
    fun toLocation(resultSet: RemoteScriptResultSet): LocationConfig? {
        return resultSet.getVar(pointVar)?.let { point ->
            resultSet.getVar(kingdomVar)?.let { kingdom ->
                val point = JSONUtils.deserialize(point, Point::class.java)
                val kingdom = Kingdom.getByRawType(kingdom.toInt())
                LocationConfig(point, kingdom)
            }
        }
    }
}