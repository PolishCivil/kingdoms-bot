package pl.polishcivil.fourkingdoms.universe

import org.xsocket.connection.IConnection
import org.xsocket.connection.Server

/**
 * Created by polish on 4/19/2017.
 */
class BotServer(val universe: BotUniverse) {
    private var server: Server? = null

    fun start(port: Int) {
        server = Server(port, universe)
        server!!.flushmode = IConnection.FlushMode.ASYNC
        server!!.idleTimeoutMillis = 30_000
        server!!.start()
    }

    fun stop() {
        server?.close()
    }
}