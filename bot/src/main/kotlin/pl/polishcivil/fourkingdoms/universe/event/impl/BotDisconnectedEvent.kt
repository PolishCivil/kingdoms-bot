package pl.polishcivil.fourkingdoms.universe.event.impl

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.universe.event.BotEvent

/**
 * Created by polish on 4/19/2017.
 */
class BotDisconnectedEvent(bot: Bot) : BotEvent(bot)