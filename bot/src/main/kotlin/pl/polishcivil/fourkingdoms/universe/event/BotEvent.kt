package pl.polishcivil.fourkingdoms.universe.event

import pl.polishcivil.fourkingdoms.bot.Bot

/**
 * Created by polish on 4/19/2017.
 */
open class BotEvent(val bot: Bot)