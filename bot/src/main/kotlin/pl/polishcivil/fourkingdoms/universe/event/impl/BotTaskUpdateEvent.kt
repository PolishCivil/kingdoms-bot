package pl.polishcivil.fourkingdoms.universe.event.impl

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.universe.event.BotEvent

/**
 * Created by polish on 5/6/2017.
 */
class BotTaskUpdateEvent(bot: Bot) : BotEvent(bot)
