package pl.polishcivil.fourkingdoms.universe

import org.apache.logging.log4j.LogManager
import org.xsocket.connection.IConnectHandler
import org.xsocket.connection.IDisconnectHandler
import org.xsocket.connection.INonBlockingConnection
import org.xsocket.connection.NonBlockingConnection
import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.bot.net.BotConnection
import pl.polishcivil.fourkingdoms.universe.event.EventBus
import pl.polishcivil.fourkingdoms.universe.event.impl.BotConnectedEvent
import pl.polishcivil.fourkingdoms.universe.event.impl.BotDisconnectedEvent
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

/**
 * Created by polish on 4/19/2017.
 */
class BotUniverse : IConnectHandler, IDisconnectHandler {
    val eventBus = EventBus()

    private val connectedBots = CopyOnWriteArrayList<Bot>()

    fun add(connection: INonBlockingConnection) {
        synchronized(connectedBots) {
            val c = connection as NonBlockingConnection
            LogManager.getLogger().debug("Adding bot connection = $c")
            val botConnection = BotConnection(connection)
            val bot = Bot(botConnection, this)
            connectedBots.add(bot)
            eventBus.submit(BotConnectedEvent(bot))
        }
    }

    fun remove(connection: INonBlockingConnection) {
        synchronized(connectedBots) {
            LogManager.getLogger().debug("Removing bot connection = $connection")
            connectedBots.find { it.connection.connection == connection }?.let {
                it.destroy()
                connectedBots.remove(it)
                eventBus.submit(BotDisconnectedEvent(it))
            }
        }
    }

    override fun onConnect(connection: INonBlockingConnection): Boolean {
        try {
            add(connection)
        } catch(e: Exception) {
            LogManager.getLogger().catching(e)
        }
        return true
    }

    override fun onDisconnect(connection: INonBlockingConnection): Boolean {
        try {
            remove(connection)
        } catch(e: Exception) {
            LogManager.getLogger().catching(e)
        }
        return true
    }

    fun getConnectedBots() = Collections.unmodifiableList(connectedBots)!!
}