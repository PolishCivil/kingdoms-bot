package pl.polishcivil.fourkingdoms.universe.event.impl

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.universe.event.BotEvent

/**
 * Created by polish on 4/21/2017.
 */
class BotPerceptionUpdateEvent(bot: Bot) : BotEvent(bot)