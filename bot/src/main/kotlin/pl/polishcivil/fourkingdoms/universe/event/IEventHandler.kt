package pl.polishcivil.fourkingdoms.universe.event

/**
 * Created by polish on 4/19/2017.
 */
interface IEventHandler<in E : BotEvent> {
    fun handle(event: E)
}