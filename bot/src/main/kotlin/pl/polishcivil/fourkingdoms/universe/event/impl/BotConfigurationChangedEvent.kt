package pl.polishcivil.fourkingdoms.universe.event.impl

import pl.polishcivil.fourkingdoms.bot.Bot
import pl.polishcivil.fourkingdoms.universe.event.BotEvent

/**
 * Created by Polish Civil on 24.05.2017.
 */
class BotConfigurationChangedEvent(bot: Bot) : BotEvent(bot)