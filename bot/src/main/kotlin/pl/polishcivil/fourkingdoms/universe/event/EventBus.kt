package pl.polishcivil.fourkingdoms.universe.event

import org.apache.logging.log4j.LogManager
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors

@Suppress("UNCHECKED_CAST")
/**
 * Created by polish on 4/19/2017.
 */
class EventBus {
    val executor = Executors.newSingleThreadExecutor()
    val handlers = ConcurrentHashMap<Class<*>, IEventHandler<*>>()

    fun <T : BotEvent> registerHandler(cls: Class<T>, handler: IEventHandler<T>) {
        handlers.put(cls, handler)
    }

    fun <T : IEventHandler<*>> deregister(handler: T) {
        for ((key, value) in handlers) {
            if (value == handler) {
                if (handlers.remove(key) == null) {
                    LogManager.getLogger().warn("Couldn't deregister handler {}", handler)
                }
            }
        }
    }

    fun submit(event: BotEvent) {
        executor.submit({
            val handler: IEventHandler<BotEvent>? = handlers[event.javaClass] as IEventHandler<BotEvent>?
            handler?.handle(event)
        })
    }
}