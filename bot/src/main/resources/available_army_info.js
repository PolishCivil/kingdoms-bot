importScripts("script/services/AttackSetupManager.js", "script/models/WodData.js", "script/models/PerformingAttackData.js", "script/services/PerformingAttackService.js");


function main() {
    var availableUnits = unitInventory();
    var object = {};
    object.availableUnits = availableUnits;
    object.maxTroopAmountFlank = PERFORMING_ATTACK_SERVICE.getMaxTroopAmountForFlank();
    object.maxTroopAmountFront = PERFORMING_ATTACK_SERVICE.getMaxTroopAmountForFront();

    object.maxToolAmountFlank = PERFORMING_ATTACK_SERVICE.getMaxToolAmountForFlank();
    object.maxToolAmountFront = PERFORMING_ATTACK_SERVICE.getMaxToolAmountForFront();

    var level = PERFORMING_ATTACK_DATA["attackInfo"]["sourceOwner"]["playerLevel"];
    var defLevel = PERFORMING_ATTACK_DATA["attackInfo"]["effectiveDefenseLevel"];
    object.maxAllowedWaves = PERFORMING_ATTACK_SERVICE.getAmountOfAllowedWaves(level);
    object.effectiveDefenseLevel = defLevel;
    return stringify(object);
}
