importScripts("script/services/AttackSetupManager.js", "script/models/WodData.js", "script/dialogs/AttackDialog.js");


function main() {
    var cls = getClass("com.goodgamestudios.castle.gameplay.attack.vos.AttackSetupUnitVO");


    //GENERATED_CODE


    var attackDialog = getAttackDialog();
    var currentFlankID = attackDialog.mediator["_currentFlankId"];
    var view = attackDialog.mediator["view"]["flankChangedSignal"].dispatch(currentFlankID);
    return "OK";
}
