importScripts("script/model_utils.js", "script/display.js", "script/models/KingdomData.js", "script/models/GameStateData.js", "script/models/ArmyData.js", "script/models/PlayerInfoModel.js", "script/models/CastleUserData.js", "script/models/CastleLordData.js", "script/models/CastleData.js", "script/dialogs/AttackDialog.js","script/dialogs/CastleAttackSetupDialog.js");


function internalDataUpdate() {
    var json = {};

    var kingdomDataJSON = {};
    kingdomDataJSON.activeKingdomID = getActiveKingdomID();


    json.currentKingdomData = kingdomDataJSON;
    json.gameState = getGameState();


    var armyData = getArmyData();
    json.armyData = armyData.toJSON();
    var playerInfo = getPlayerInfo();
    json.player = playerInfo.toJSON();
    var castleUserData = getCastleUserData();
    json.castleData = castleUserData.toJSON();
    var castleData = getCastleData();
    if (castleData != null) {
        json.currentCastleData = castleData.toJSON();
    }
    var lordData = getCastleLordData();
    json.lordData = lordData.toJSON();
    return json;
}

function main() {
    var json = {};

    var layoutManager = getLayoutManager();
    var layoutManagerJSON = layoutManager.toJSON();
    //world map things
    var worldMapScreen = getWorldMapScreen(layoutManager);
    if (worldMapScreen != null) {
        layoutManagerJSON.worldMap = worldMapScreen.toJSON();
    }
    var attackDialog = getAttackDialog();
    if (attackDialog != null) {
        layoutManagerJSON.attack = attackDialog.toJSON();

        var unitsSetupDialog = getCastleAttackSetupDialog();
        if(unitsSetupDialog != null) {
            layoutManagerJSON.attackSetup = unitsSetupDialog;
        }
    }

    json.display = layoutManagerJSON;
    json.internal = internalDataUpdate();
    return stringify(json);
}