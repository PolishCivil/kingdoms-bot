function getClass(name) {
    //noinspection JSUnresolvedVariable
    var controller = script_ctx.class_controller;
    return controller.getDefinition(name);
}

var DICT_UTILS_CLS = getClass("org.as3commons.lang.DictionaryUtils");
function getDictionaryKeys(dictionary) {
    return DICT_UTILS_CLS.getKeys(dictionary);
}

function stringify(object) {
    return script_ctx.json.stringify(object);
}
var DISPLAY_CONTROLLER = getClass("controller.impl.DisplayController");
function getRectangle(sprite) {
    return DISPLAY_CONTROLLER.getRectangle(sprite);
}

var kingdomNames = [];
kingdomNames[0] = "GREAT_EMPIRE";
kingdomNames[2] = "ETERNAL_ICE_LAND";
kingdomNames[1] = "BURNING_SANDS";
kingdomNames[3] = "FIERY_HEIGTHS";
kingdomNames[4] = "STORM_ARCHIPELAGO";
kingdomNames[10] = "BERIMOND";
kingdomNames[23] = "UNDERGROUND";
kingdomNames[21] = "THORNY_LAND";
kingdomNames[22] = "SWORD_COAST";

function worldMapObject(internalObject) {
    var object = {};

    // var coolongDown = internalObject["isCoolingDown"];
    var x = internalObject["absAreaPosX"];
    var y = internalObject["absAreaPosY"];
    var kingdomId = internalObject["kingdomId"];
    object.kingdom = kingdomIDToName(kingdomId);
    object.x = x;
    object.y = y;
    return object;

}

function kingdomIDToName(kingdomID) {
    return kingdomNames[kingdomID];
}

function parseInt(text) {
    var integer = script_ctx.parseInt(text);
    if (text == "") {
        return -1;
    }
    return integer;
}