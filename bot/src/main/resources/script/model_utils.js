importScripts("script/script_std.js");

var APP_CORE_CLS_NAME = "com.goodgamestudios.mobileBasic.application.AppCore";
var APP_CORE_CLS = getClass(APP_CORE_CLS_NAME);
var APP_CORE_INSTANCE = APP_CORE_CLS.getInstance();

function getAppCoreInstance() {
    return APP_CORE_INSTANCE;
}

function getModelInstance(name) {
    var modelClass = getClass(name);
    var appCoreInstance = getAppCoreInstance();
    return appCoreInstance.getModel(modelClass);
}
