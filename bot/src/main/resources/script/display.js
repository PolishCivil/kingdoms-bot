importScripts("script/model_utils.js", "script/script_std.js", "script/world_map/world_map.js");

var LAYOUT_MANAGER = getModelInstance("com.goodgamestudios.castle.basicView.layout.layoutManager.BasicLayoutManager");
var dialogs = LAYOUT_MANAGER["_dialogs"];
var ringMenuComponents = LAYOUT_MANAGER["ringMenuComponents"];
var ringMenuComponentsDict = ringMenuComponents["componentDict"];
var panelsDict = LAYOUT_MANAGER["panels"]["componentDict"];
var layers = LAYOUT_MANAGER["_layers"];
var screens = LAYOUT_MANAGER["screens"]["componentDict"];
var shownDialogNames = dialogs.getShownDialogNames();

function getLayoutManager() {

    var struct = {};
    struct.instance = LAYOUT_MANAGER;
    struct.screens = screens;
    struct.panels = panelsDict;
    struct.ringMenu = ringMenuComponentsDict["RingMenu"];

    struct.toJSON = function () {
        var json = {};
        json.shownDialogNames = shownDialogNames;
        json.shownScreens = getDictionaryKeys(struct.screens);
        json.shownPanels = getDictionaryKeys(struct.panels);
        json.layers = getDictionaryKeys(layers);

        if (struct.ringMenu != null) {
            var buttons = struct.ringMenu["ringMenuButtons"];
            json.ringMenu = {};
            json.ringMenu.buttons = [];

            for (var i = 0; i < buttons.length; i++) {
                var obj = buttons[i];
                var params = obj["params"];
                var button = {};
                json.ringMenu.buttons[i] = button;
                button.rectangle = getRectangle(obj["iconHolder"]);
                button.headingText = params["headingText"];
            }
        }
        return json;
    };


    return struct;
}


function getDialogInstance(name) {
    var shown = dialogs.getShownDialogsInCollectionForName(name);
    if (shown.length == 0) {
        return null;
    }
    return shown[0];
}