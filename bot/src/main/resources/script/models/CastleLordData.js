importScripts("script/script_std.js", "script/model_utils.js");

var CASTLE_LORD_DATA = getModelInstance("com.goodgamestudios.castle.gameplay.lord.models.CastleLordData");


function lordObject(internalObject) {
    var object = {};

    object.consecutiveLordID = internalObject["consecutiveLordID"];
    object.id = internalObject["id"];
    object.isAvailableForMovement = internalObject["isAvailableForMovement"];
    object.type = internalObject["type"];

    return object;
}

function getCastleLordData() {
    var object = {};
    var lordsDictionary = CASTLE_LORD_DATA["lords"];
    var generals = lordsDictionary["general"];
    var barons = lordsDictionary["baron"];
    var leaders = [];
    for (var i = 0; i < generals.length; i++) {
        leaders[i] = lordObject(generals[i]);
    }
    //
    // for (var i = 0; i < barons.length; i++) {
    //     var baron = barons[i];
    //     leaders[allIndex] = lordObject(baron);
    //     allIndex++;
    // }

    object.leaders = leaders;
    object.toJSON = function () {
        return object;
    };
    return object;
}