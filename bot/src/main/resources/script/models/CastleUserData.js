importScripts("script/script_std.js", "script/model_utils.js");

var CASTLE_USER_DATA_MODEL = getModelInstance("com.goodgamestudios.castle.gameplay.player.models.CastleUserData");


function getCastleUserData() {
    var object = {};
    object.gold = 0;
    object.rubies = 0;

    var internalCastleList = CASTLE_USER_DATA_MODEL["castleListVO"];
    var internalList = internalCastleList.getList();

    var ownedLocations = [];
    for (var i = 0; i < internalList.length; i++) {
        var obj = internalList[i];
        ownedLocations[i] = worldMapObject(obj);
    }

    object.ownedLocations = ownedLocations;
    object.toJSON = function () {
        return object;
    };
    return object;
}