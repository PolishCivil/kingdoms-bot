importScripts("script/model_utils.js");


var GAME_STATE_DATA = getModelInstance("com.goodgamestudios.castle.application.models.GameStateData");

function getGameState() {
    return GAME_STATE_DATA["state"];
}