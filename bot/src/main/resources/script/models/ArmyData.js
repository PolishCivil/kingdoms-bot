importScripts("script/script_std.js", "script/model_utils.js");

var ARMY_DATA = getModelInstance("com.goodgamestudios.castle.gameplay.worldmap.models.ArmyData");


function mapMovementObject(internalObject) {
    var object = {};
    object.myMovement = internalObject["isMyMovement"];
    object.source = worldMapObject(internalObject["sourceArea"]);
    object.target = worldMapObject(internalObject["targetArea"]);
    object.timeReachTargetMillis = internalObject.getTimeUnitMovementReachTargetInMilliSeconds();
    object.type = internalObject["movementType"];

    return object;
}

function getArmyData() {
    var armyData = {};

    var movementsInternal = ARMY_DATA["mapMovements"];
    var movements = [];
    for (var i = 0; i < movementsInternal.length; i++) {
        var movementInternalObject = movementsInternal[i];
        var object = mapMovementObject(movementInternalObject);
        movements[i] = object;
    }
    armyData.toJSON = function () {
        return armyData;
    };
    armyData.moves = movements;
    return armyData;
}


