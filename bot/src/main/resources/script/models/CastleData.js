importScripts("script/script_std.js", "script/model_utils.js");

var CASTLE_DATA = getModelInstance("com.goodgamestudios.castle.gameplay.castle.models.CastleData");

function getCastleData() {
    var object = {};
    var area = CASTLE_DATA["areaInfo"];
    if (area == null) {
        return null;
    }
    object.location = worldMapObject(area);
    object.toJSON = function () {
        return object;
    };
    return object;
}