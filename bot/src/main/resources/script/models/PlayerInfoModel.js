importScripts("script/model_utils.js");

var PLAYER_INFO_MODEL = getModelInstance("com.goodgamestudios.mobileBasic.application.model.PlayerInfoModel");


function getPlayerInfo() {
    var object = {};
    object.userName = PLAYER_INFO_MODEL["userName"];
    object.email = PLAYER_INFO_MODEL["email"];
    object.toJSON = function () {
        return object;
    };
    return object;
}