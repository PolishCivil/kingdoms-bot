importScripts("script/script_std.js");

var PERFORMING_ATTACK_DATA = getModelInstance("com.goodgamestudios.castle.gameplay.attack.models.PerformingAttackData");


function unitInventory() {
    var attackInfo = PERFORMING_ATTACK_DATA["attackInfo"];
    var unitInventory = attackInfo["unitInventory"];
    var all = unitInventory["getAll"];
    return all;
}