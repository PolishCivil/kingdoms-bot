importScripts("script/model_utils.js");


var KINGDOM_DATA_MODEL = getModelInstance("com.goodgamestudios.castle.gameplay.kingdom.models.KingdomData");

function getActiveKingdomID(){
    return KINGDOM_DATA_MODEL["activeKingdomID"];
}