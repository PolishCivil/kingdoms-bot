importScripts("script/model_utils.js", "script/display.js", "script/models/KingdomData.js", "script/models/GameStateData.js", "script/models/ArmyData.js", "script/models/PlayerInfoModel.js", "script/models/CastleUserData.js", "script/models/CastleLordData.js", "script/models/CastleData.js", "script/dialogs/AttackDialog.js", "script/dialogs/CastleAttackSetupDialog.js");


function getActionPanel() {
    var layoutManager = getLayoutManager();
    if (layoutManager.panels["CastleActionPanel"] != null) {
        return layoutManager.panels["CastleActionPanel"];
    }
    if (layoutManager.panels["CastleActionPanelWorldmap"] != null) {
        return layoutManager.panels["CastleActionPanelWorldmap"];
    }
    if (layoutManager.panels["CastleActionPanelOwnCastle"] != null) {
        return layoutManager.panels["CastleActionPanelOwnCastle"];
    }
    if (layoutManager.panels["ActionPanelFactionMap"] != null) {
        return layoutManager.panels["ActionPanelFactionMap"];
    }
    if (layoutManager.panels["CastleActionPanelKingdomMap"] != null) {
        return layoutManager.panels["CastleActionPanelKingdomMap"];
    }
    if (layoutManager.panels["CastleActionPanelFactionCamp"] != null) {
        return layoutManager.panels["CastleActionPanelFactionCamp"];
    }
    return null;
}

function getLocationsButton(actionPanel) {
    var items = actionPanel["menuButtonTravel"].getListItems();
    var button = null;
    for (var i = 0; i < items.length; i++) {
        var className = script_ctx.getQualifiedClassName(items[i]);
        if (className.search("ButtonOpenLocationsMenu") != -1) {
            button = items[i];
        }
    }
    return button;
}

function main() {
    var actionPanel = getActionPanel();
    if (actionPanel == null) {
        return "ActionPanel NULL!";
    }
    var locationsDialog = getDialogInstance("LocationsDialog");
    if (locationsDialog != null) {
        return "Dialog visible!";
    }
    var button = getLocationsButton(actionPanel);
    if (button == null) {
        return "BUTTON NULL!";
    }
    script_ctx.touch(button["disp"]);
    return "OK";
}
