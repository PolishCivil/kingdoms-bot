importScripts("script/model_utils.js", "script/display.js", "script/models/KingdomData.js", "script/models/GameStateData.js", "script/models/ArmyData.js", "script/models/PlayerInfoModel.js", "script/models/CastleUserData.js", "script/models/CastleLordData.js", "script/models/CastleData.js", "script/dialogs/AttackDialog.js", "script/dialogs/CastleAttackSetupDialog.js");


function main() {
    var x = 1297;
    var y = 63;
    //GENERATED_CODE
    var layoutManager = getLayoutManager();
    var navigationPanel = navigationPanel(layoutManager);
    navigationPanel.inputCoordXField["text"] = x;
    navigationPanel.inputCoordYField["text"] = y;
    var gotoPushButton = navigationPanel.searchListContainer["buttonList"][1];
    gotoPushButton["onClickCallbackFunction"]();
    return gotoPushButton;
}
