importScripts("script/dialogs/LocationsDialog.js");


function main() {
    var kingdomID = -1;
    //GENERATED_CODE

    var dialog = getLocationsDialogInstance();
    if (dialog !== null) {
        var castleList = dialog["listCastlesPlayer"]["listCastles"];
        var castleListDataProvider = castleList["dataProvider"];
        var castleListItems = castleList["listItems"];
        var dataArray = castleListDataProvider["data"];

        var mapObjectIndex = -1;
        for (var index = 0; index < dataArray["length"]; index++) {
            var obj = dataArray[index];
            if (obj["kingdomId"] === kingdomID) {
                mapObjectIndex = index;
                break;
            }
        }
        if (mapObjectIndex !== -1) {
            var button = castleListItems[mapObjectIndex]["btnGoToCoordinates"];
            button["onClickCallbackFunction"]();
            return "OK";
        } else {
            return "Object with kingdomID not found";
        }
    } else {
        //dialog not visible
    }
    return "Dialog not visible";
}
