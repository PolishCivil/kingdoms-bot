importScripts("script/mediator_utils.js", "script/display.js", "script/models/PerformingAttackData.js");



function getCastleAttackSetupDialog() {
    var instance = getDialogInstance("CastleAttackSetupDialog");

    if (instance == null) {
        return null;
    }

    var mediatorMappings = getMediatorMappings("com.goodgamestudios.castle.gameplay.attack.views.attackDialog.unitSelection.UnitSelectionDialog");
    var mediatorMapping = mediatorMappings[2];
    var args = [];
    args[0] = instance;
    args[1] = mediatorMapping;
    var mediator = getMediator(args);

    var object = {};
    var attackWaveSlotInfoVO = instance["properties"]["attackWaveSlotInfoVO"];
    object.slotInfo = attackWaveSlotInfoVO;
    object.toJSON = function () {
        return object;
    };
    return object;
}