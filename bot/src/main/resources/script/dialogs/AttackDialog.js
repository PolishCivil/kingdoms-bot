importScripts("script/mediator_utils.js", "script/display.js", "script/models/PerformingAttackData.js");


function getAttackDialog() {
    var instance = getDialogInstance("AttackDialog");
    if (instance == null) {
        return null;
    }
    var mediatorMappings = getMediatorMappings("com.goodgamestudios.castle.gameplay.attack.views.attackDialog.AttackDialog");
    var mediatorMapping = mediatorMappings[2];


    var args = [];
    args[0] = instance;
    args[1] = mediatorMapping;

    var mediator = getMediator(args);
    var object = {};
    object.mediator = mediator;
    object.selectedFlank = mediator["_currentFlankId"];
    object.availableWaves = instance["waveList"]["_items"].length;
    object.toJSON = function () {
        var json = {};
        json.selectedFlank = this.selectedFlank;
        json.availableWaves = this.availableWaves;
        return json;
    };
    return object;
}