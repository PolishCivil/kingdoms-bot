importScripts("script/script_std.js", "script/model_utils.js");


var MEDIATOR_MAP = getModelInstance("robotlegs.bender.extensions.mediatorMap.api.IMediatorMap");
var MEDIATOR_FACTORY = MEDIATOR_MAP["_ggsFactory"];
var MEDIATOR_MAPPINGS = MEDIATOR_FACTORY["_mediators"];
var VIEW_HANDLER = MEDIATOR_MAP["_viewHandler"];
var VIEW_HANDLER_FACTORY = VIEW_HANDLER["_factory"];
var VIEW_HANDLER_MAPPINGS = VIEW_HANDLER["_knownMappings"];


function getMediatorMappings(name) {
    var classDefinition = getClass(name);
    var mapping = VIEW_HANDLER_MAPPINGS[classDefinition];
    return mapping;
}

function getMediator(args) {
    var instance = args[0];
    var mapping = args[1];
    var mediator = MEDIATOR_MAPPINGS[instance];
    if(mediator == null) {
        return null;
    }
    var test = mediator[mapping];
    return test;
}
