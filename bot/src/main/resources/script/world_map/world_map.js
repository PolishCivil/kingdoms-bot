importScripts("script/script_std.js", "script/model_utils.js");

function getWorldMapScreen(layoutObject) {
    var screen = layoutObject.screens["CastleWorldMapScreen"];
    if (screen == null) {
        screen = layoutObject.screens["FactionMapScreen"];
        if (screen == null) {
            return null;
        }
    }
    var worldMapScreen = {};
    var navigationPanel = navigationPanel(layoutObject);
    worldMapScreen.navigationPanel = navigationPanel;
    worldMapScreen.toJSON = function () {
        var jsonObject = {};

        var ringMenu = layoutObject.ringMenu;
        var sprite = screen["_worldMap"];
        var objectCollection = sprite["objectCollection"];
        var clickAbleObjects = objectCollection["clickableObjects"];

        if (ringMenu != null) {
            var worldMapObjectVO = ringMenu["properties"]["ringMenuVO"]["ringMenuObject"];
            var test = worldMapObjectVO["worldmapObjectVO"];
            jsonObject.selectedMapLocation = worldMapObject(test);
            jsonObject.ringMenuVisible = true;
        } else {
            jsonObject.ringMenuVisible = false;
        }

        if (navigationPanel != null) {
            jsonObject.navigationPanel = navigationPanel.toJSON();
        }
        return jsonObject;
    };

    return worldMapScreen;
}

function navigationPanel(layoutManagerObject) {
    var panelObject = {};

    var panel = layoutManagerObject.panels["CastleWorldMapNavigationPanel"];
    if (panel == null) {
        panel = layoutManagerObject.panels["TopNaviFactionMapPanel"];
        if (panel == null) {
            return null;
        }
    }

    var searchList = panel["searchListContainer"];
    if (searchList == null) {
        return null;
    }
    var inputCoordXField = searchList["dispClip"]["input_coordX"];
    var inputCoordYField = searchList["dispClip"]["input_coordY"];
    var goButton = searchList["dispClip"]["btn_goto"];
    var searchButton = searchList["dispClip"]["iconSearch"];
    panelObject.searchListContainer = searchList;
    panelObject.inputCoordXField = inputCoordXField;
    panelObject.inputCoordYField = inputCoordYField;
    panelObject.goButton = goButton;
    panelObject.searchButton = searchButton;
    panelObject.toJSON = function () {
        var jsonObject = {};
        var target = {};
        target.x = parseInt(inputCoordXField["text"]);
        target.y = parseInt(inputCoordYField["text"]);
        jsonObject.searchContainerVisible = !searchList["_isHidden"] && !searchList["isLocked"];
        jsonObject.targetLocation = target;
        return jsonObject;
    };

    return panelObject;
}

