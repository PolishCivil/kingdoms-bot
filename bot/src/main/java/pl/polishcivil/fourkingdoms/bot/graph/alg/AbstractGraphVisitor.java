package pl.polishcivil.fourkingdoms.bot.graph.alg;


import pl.polishcivil.fourkingdoms.bot.graph.impl.DirectedGraph;

/**
 * Created by polish on 17.05.15.
 */
public abstract class AbstractGraphVisitor<N> {
    /**
     * @param directedGraph
     */
    @SuppressWarnings("unchecked")
    public <T extends AbstractGraphVisitor<N>> T start(DirectedGraph<N> directedGraph) {
        return (T) this;
    }

    /**
     * @param node
     */
    public abstract void visitNode(N node, DirectedGraph<N> graph);

    /**
     * @param edge
     */
    public abstract void visitEdge(DirectedGraph.Edge<N> edge, DirectedGraph<N> graph);

    /**
     *
     */
    protected abstract void onFinish(DirectedGraph<N> directedGraph);


}
