
package pl.polishcivil.fourkingdoms.bot.graph;

import java.util.HashMap;

/**
 * Created by polish on 06.05.15.
 */
public interface IPropertyHolder {
    /**
     * @param <T>
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    <T> Object getProperty(String key);

    /**
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
    <T> T setProperty(String key, Object value);

    /**
     * @return
     */
    HashMap<String, Object> getProperties();
}
