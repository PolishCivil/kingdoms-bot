package pl.polishcivil.fourkingdoms.bot.graph.alg.dfs.impl;


import pl.polishcivil.fourkingdoms.bot.graph.alg.dfs.AbstractDFS;
import pl.polishcivil.fourkingdoms.bot.graph.impl.DirectedGraph;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by polish on 07.05.15.
 */
public abstract class DFSForward<N> extends AbstractDFS<N> {

    @Override
    protected void dfs(N node, DirectedGraph<N> graph, List<N> include) {
        if (graph.getNodeProperty(node, DirectedGraph.VISITED_PROPERTY, false)) {
            return;
        }
        graph.setNodeProperty(node, DirectedGraph.VISITED_PROPERTY, true);

        //notify overrides
        visitNode(node, graph);

        final LinkedList<DirectedGraph.Edge<N>> outgoingEdges = graph.getOutgoingEdges(node);
        for (DirectedGraph.Edge<N> edge : outgoingEdges) {
            if (include == null || include.contains(edge.getTarget())) {
                edge.setProperty(DirectedGraph.VISITED_PROPERTY, true);

                //notify overrides
                visitEdge(edge, graph);

                dfs(edge.getTarget(), graph, include);
            }
        }

        //notify overrides
        postVisitNode(node, graph);
    }
}
