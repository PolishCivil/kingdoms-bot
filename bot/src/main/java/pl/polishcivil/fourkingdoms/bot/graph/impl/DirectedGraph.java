package pl.polishcivil.fourkingdoms.bot.graph.impl;

import pl.polishcivil.fourkingdoms.bot.graph.IPropertyHolder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Simple directed graph implementation.
 *
 * @param <N> - node type.
 */
public class DirectedGraph<N> implements Iterable<N> {
    //used in most algorithms, both for node and edge
    public static final String VISITED_PROPERTY = "VISITED";

    private final LinkedList<N> nodes = new LinkedList<>();
    private final LinkedList<Edge<N>> edges = new LinkedList<>();

    private final HashMap<N, HashMap<String, Object>> nodeProperties = new HashMap<>();

    /**
     * @param node
     * @return
     */
    public boolean addNode(N node) {
        if (this.nodes.contains(node)) {
            return false;
        }
        this.nodes.add(node);
        final HashMap<String, Object> defaultProperties = new HashMap<>();
        defaultProperties.put(VISITED_PROPERTY, false);
        this.nodeProperties.put(node, defaultProperties);
        return true;
    }

    /**
     * @param src
     * @param target
     */
    public Edge<N> addEdge(N src, N target) {
        validateSourceAndDestinationNodes(src, target);
        // Add the edge by adding the target node into the outgoing aind incoming edges
        final Edge<N> edge = new Edge<>(src, target);
        this.edges.add(edge);
        return edge;
    }

    /**
     * @param node
     * @param key
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> T getNodeProperty(N node, String key, T def) {
        Object val = this.nodeProperties.get(node).get(key);
        if (val != null) return (T) val;
        else {
            this.nodeProperties.get(node).put(key, def);
            return def;
        }
    }

    /**
     * @param node
     * @param key
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> T getNodeProperty(N node, String key) {
        Object val = this.nodeProperties.get(node).get(key);
        return (T) val;
    }

    /**
     * @param node
     */
    public void clearNodeProperties(N node) {
        this.nodeProperties.get(node).clear();
    }

    /**
     */
    public void clearNodesProperties() {
        this.nodes.forEach(this::clearNodeProperties);
    }

    /**
     * @param node
     * @param key
     * @param value
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> T setNodeProperty(N node, String key, Object value) {
        return (T) this.nodeProperties.get(node).put(key, value);
    }

    /**
     * @param node
     * @return
     */
    public LinkedList<Edge<N>> getOutgoingEdges(N node) {
        return edges.stream().filter(edge -> edge.source.equals(node)).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * @param node
     * @return
     */
    public LinkedList<Edge<N>> getIncomingEdges(N node) {
        return edges.stream().filter(edge -> edge.target.equals(node)).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * @param node
     * @return
     */
    public LinkedList<N> getDirectSuccessors(N node) {
        // Check that the node exists
        if (!nodes.contains(node))
            throw new NoSuchElementException("Source node does not exist. node=" + node);
        return getOutgoingEdges(node).stream().map(edge -> edge.target).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * @param node
     * @return
     */
    public LinkedList<N> getChildren(N node) {
        LinkedList<N> set = new LinkedList<>();
        set.addAll(getDirectPredecessors(node));
        set.addAll(getDirectSuccessors(node));
        return set;
    }

    /**
     * @param node
     * @return
     */
    public LinkedList<N> getDirectPredecessors(N node) {
        // Check that the node exists
        if (!nodes.contains(node))
            throw new NoSuchElementException("Source node does not exist. node=" + node);
        return getIncomingEdges(node).stream().map(edge -> edge.source).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * @return
     */
    public LinkedList<N> getNodes() {
        return this.nodes;
    }

    /**
     * @return
     */
    public LinkedList<Edge<N>> getEdges() {
        return edges;
    }

    /**
     * @return
     */
    public Iterator<N> iterator() {
        return this.nodes.iterator();
    }

    /**
     * @return
     */
    public int size() {
        return this.nodes.size();
    }

    /**
     * @return
     */
    public boolean isEmpty() {
        return this.nodes.isEmpty();
    }

    /**
     * @param src
     * @param dest
     */
    private void validateSourceAndDestinationNodes(N src, N dest) {
        if (!nodes.contains(src) || !this.nodes.contains(dest) || src == null || dest == null)
            throw new NoSuchElementException("Both nodes must be in the graph.");
    }

    /**
     * @param outputFile
     * @param name
     * @throws IOException
     */
    public void exportDOT(File outputFile, String name) throws IOException {
        try (FileOutputStream out = new FileOutputStream(outputFile)) {
            out.write("digraph g {\n".getBytes());
            out.write(("label = \"" + name + "\"\n").getBytes());

            for (int index = 0; index < nodes.size(); index++) {
                N node = nodes.get(index);
                out.write((index + "[shape = box, label = \"" + node.toString() + "\"]\n").getBytes());
            }

            for (Edge<N> edge : edges) {
                int targetIndex = this.nodes.indexOf(edge.target);
                int sourceIndex = this.nodes.indexOf(edge.source);
                out.write(("" + (sourceIndex == -1 ? "SOME_CODE" : sourceIndex) + " -> " + (targetIndex == -1 ? "SOME_CODE" : targetIndex)).getBytes());
                Object label = edge.getProperty(Edge.LABEL_PROPERTY);
                if (label != null) {
                    out.write((" [label = \"" + label + "\"]").getBytes());
                }
                out.write("\n".getBytes());
            }
            out.write("}\n".getBytes());
        }
    }

    /**
     * Basic edge struct.
     *
     * @param <N>
     */
    @SuppressWarnings("unchecked")
    public static class Edge<N> implements IPropertyHolder {
        public static final String LABEL_PROPERTY = "LABEL";
        private N source;
        private N target;
        final HashMap<String, Object> properties = new HashMap<>();

        public Edge(N source, N target) {
            this.source = source;
            this.target = target;
        }

        /**
         * @return
         */
        public N getSource() {
            return source;
        }

        /**
         * @return
         */
        public N getTarget() {
            return target;
        }

        @Override
        public <T> T getProperty(String key) {
            return (T) properties.get(key);
        }

        @Override
        public <T> T setProperty(String key, Object value) {
            return (T) this.properties.put(key, value);
        }

        @Override
        public HashMap<String, Object> getProperties() {
            return properties;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Edge<?> edge = (Edge<?>) o;

            if (!source.equals(edge.source)) return false;
            return target.equals(edge.target);

        }

        @Override
        public int hashCode() {
            int result = source.hashCode();
            result = 31 * result + target.hashCode();
            return result;
        }

        /**
         * @param source
         */
        public void setSource(N source) {
            this.source = source;
        }

        /**
         * @param target
         */
        public void setTarget(N target) {
            this.target = target;
        }

        /**
         * @param from
         */
        public void copyProperties(IPropertyHolder from) {
            this.properties.putAll(from.getProperties());
        }


    }
}