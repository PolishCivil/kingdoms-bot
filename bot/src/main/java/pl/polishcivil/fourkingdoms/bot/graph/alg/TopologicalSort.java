package pl.polishcivil.fourkingdoms.bot.graph.alg;


import pl.polishcivil.fourkingdoms.bot.graph.impl.DirectedGraph;

import java.util.ArrayList;
import java.util.List;

public class TopologicalSort {
    static final String NODE_EXPANDED_PROPERTY = "EXPANDED";

    /**
     * @param graph
     * @return
     */
    public static <N> List<N> sortDependency(DirectedGraph<N> graph) {
        return sortDependency(graph, true);
    }

    /**
     * @param graph
     * @param reverse
     * @param <N>
     * @return
     */
    public static <N> List<N> sortDependency(DirectedGraph<N> graph, boolean reverse) {


        DirectedGraph<N> reversedGraph = reverse ? reverseGraph(graph) : graph;

        List<N> result = new ArrayList<N>();

        /* We'll also maintain a third set consisting of all nodes that have
         * been fully expanded.  If the graph contains a cycle, then we can
         * detect this by noting that a node has been explored but not fully
         * expanded.
         */
        for (N node : reversedGraph) {
            reversedGraph.setNodeProperty(node, NODE_EXPANDED_PROPERTY, false);
        }

        // Fire off a Depth-First Search from each node in the graph
        for (N node : reversedGraph)
            explore(node, reversedGraph, result);

        return result;
    }


    /**
     * Recursively performs a Depth-First Search from the specified node, marking all nodes
     * encountered by the search.
     *
     * @param node   The node to begin the search from.
     * @param graph  The graph in which to perform the search.
     * @param result A list holding the topological sortDependency of the graph.
     */
    private static <T> void explore(T node, DirectedGraph<T> graph, List<T> result) {
        if (graph.getNodeProperty(node, DirectedGraph.VISITED_PROPERTY, false)) {
            // if this node has already been expanded, then it's already been assigned a
            // position in the final topological sortDependency and we don't need to explore it again.
            if (graph.getNodeProperty(node, NODE_EXPANDED_PROPERTY, false)) return;

            // if it hasn't been expanded, it means that we've just found a node that is currently being explored,
            // and therefore is part of a cycle.  In that case, we should report an error.
            throw new IllegalArgumentException("A cycle was detected within the Graph when exploring node " + node.toString());
        }

        graph.setNodeProperty(node, DirectedGraph.VISITED_PROPERTY, true);

        // recursively explore all predecessors of this node
        for (T predecessor : graph.getDirectPredecessors(node))
            explore(predecessor, graph, result);

        result.add(node);
        graph.setNodeProperty(node, NODE_EXPANDED_PROPERTY, true);
    }

    private static <T> DirectedGraph<T> reverseGraph(DirectedGraph<T> graph) {
        DirectedGraph<T> result = new DirectedGraph<>();

        // Add all the nodes from the original graph
        for (T node : graph) {
            result.addNode(node);
        }

        // Scan over all the edges in the graph, adding their reverse to the reverse graph.
        for (T node : graph) {
            for (T endpoint : graph.getDirectSuccessors(node)) {
                result.addEdge(endpoint, node);
            }
        }

        return result;
    }


}