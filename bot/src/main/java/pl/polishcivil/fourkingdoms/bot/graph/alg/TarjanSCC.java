package pl.polishcivil.fourkingdoms.bot.graph.alg;


import pl.polishcivil.fourkingdoms.bot.graph.impl.DirectedGraph;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by polish on 20.05.15.
 */
public class TarjanSCC<N> extends AbstractGraphVisitor<N> {
    static final String LOW = "low";
    static final String INDEX = "index";
    static final int UNDEFINED = -1;
    private final List<N> includeList;
    private final LinkedList<LinkedList<N>> sccS = new LinkedList<>();
    private final Stack<N> stack = new Stack<>();
    boolean addSingle = false;
    int index = 0;

    /**
     * @param includeList
     */
    public TarjanSCC(List<N> includeList) {
        this.includeList = includeList;
    }

    /**
     *
     */
    public TarjanSCC() {
        this.includeList = null;
    }

    @Override
    public <T extends AbstractGraphVisitor<N>> T start(DirectedGraph<N> g) {
        g.clearNodesProperties();

        if (includeList != null) {
            for (N n : includeList) {
                visitNode(n, g);
            }
        } else {
            for (N n : g) {
                visitNode(n, g);
            }
        }


        return (T) this;
    }

    @Override
    public void visitNode(N node, DirectedGraph<N> g) {
        setIndex(node, g, index);
        setLowLink(node, g, index);
        index++;

        stack.push(node);

        for (N w : g.getDirectSuccessors(node)) {
            if (includeList == null || includeList.contains(w)) {
                if (getIndex(w, g) == UNDEFINED) {
                    visitNode(w, g);
                    setLowLink(node, g, Math.min(getLowLink(node, g), getLowLink(w, g)));
                } else if (stack.contains(w)) {
                    setLowLink(node, g, Math.min(getLowLink(node, g), getIndex(w, g)));
                }
            }
        }

        if (getLowLink(node, g) == getIndex(node, g)) {
            //scc found
            final LinkedList<N> scc = new LinkedList<>();
            N v;
            do {
                v = stack.pop();
                scc.addFirst(v);
            } while (v != node);
            //add
            if (scc.size() > 1 || addSingle) {
                sccS.add(scc);
            }
        }
    }

    @Override
    public void visitEdge(DirectedGraph.Edge<N> edge, DirectedGraph<N> graph) {
    }

    @Override
    protected void onFinish(DirectedGraph<N> directedGraph) {
    }

    /**
     * @param addSingle
     */
    public void setAddSingle(boolean addSingle) {
        this.addSingle = addSingle;
    }

    /**
     * @return - the strong connected components set.
     */
    public LinkedList<LinkedList<N>> get() {
        return sccS;
    }

    /**
     * @param node
     * @param graph
     * @return
     */
    int getLowLink(N node, DirectedGraph<N> graph) {
        return graph.getNodeProperty(node, LOW, UNDEFINED);
    }

    /**
     * @param node
     * @param graph
     * @return
     */
    int getIndex(N node, DirectedGraph<N> graph) {
        return graph.getNodeProperty(node, INDEX, UNDEFINED);
    }

    /**
     * @param node
     * @param graph
     * @param val
     * @return
     */
    void setLowLink(N node, DirectedGraph<N> graph, int val) {
        graph.setNodeProperty(node, LOW, val);
    }

    /**
     * @param node
     * @param graph
     * @param val
     * @return
     */
    void setIndex(N node, DirectedGraph<N> graph, int val) {
        graph.setNodeProperty(node, INDEX, val);
    }
}
