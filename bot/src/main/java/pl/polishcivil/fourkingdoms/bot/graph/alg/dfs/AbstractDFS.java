package pl.polishcivil.fourkingdoms.bot.graph.alg.dfs;


import pl.polishcivil.fourkingdoms.bot.graph.alg.AbstractGraphVisitor;
import pl.polishcivil.fourkingdoms.bot.graph.impl.DirectedGraph;

import java.util.List;

/**
 * Created by polish on 07.05.15.
 */
public abstract class AbstractDFS<N> extends AbstractGraphVisitor<N> {
    @Override
    public <T extends AbstractGraphVisitor<N>> T start(DirectedGraph<N> directedGraph) {
        final T start = super.start(directedGraph);
        for (N node : directedGraph) {
            dfs(node, directedGraph, null);
        }
        onFinish(directedGraph);
        return start;
    }

    public <T extends AbstractGraphVisitor<N>> T start(DirectedGraph<N> directedGraph, List<N> include) {
        final T start = super.start(directedGraph);
        for (N node : include) {
            dfs(node, directedGraph, include);
        }
        onFinish(directedGraph);
        return start;
    }

    /**
     * @param node
     * @param graph
     * @param include - if null, then include whole graph
     */
    protected abstract void dfs(N node, DirectedGraph<N> graph, List<N> include);

    /**
     */
    public abstract void postVisitNode(N node, DirectedGraph<N> graph);
}
