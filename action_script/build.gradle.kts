import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradlefx.conventions.FlexType
import org.gradlefx.conventions.FrameworkLinkage
import org.gradlefx.conventions.GradleFxConvention
import org.gradlefx.plugins.AbstractGradleFxPlugin
import org.gradlefx.plugins.GradleFxPlugin
import org.gradlefx.tasks.BuildFx
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension

val apkToolFile = file("resources/apktool.jar")
val gameApkFile = file("resources/game/FourKingdoms.apk")
val apkToolBuildDir = file("build/apk_tool").apply { mkdirs() }
val apkToolPreloaderFile = file("build/apk_tool/assets/Preloader.swf")
val modifiedApkFile = file("build/FourKingdoms-modified.apk")
val modifiedSignedApkFile = file("build/FourKingdoms-modified-signed.apk")
val modifiedApkKeystoreFile = file("resources/android/debug.keystore")

val actionScriptOutputFile
    get() = withConvention(GradleFxConvention::class) {
        file("build/$name.${if (type.isLib) "swc" else "swf"}")
    }

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath("org.gradlefx:gradlefx:1.5.0")
    }
}

plugins {
    idea
    kotlin("jvm") version "1.2.51"
}

apply<GradleFxPlugin>().also {
    project.extensions.create("gradlefx", GradleFxPlugin::class.java)

    repositories {
        ivy {
            name = "Apache Flex"
            artifactPattern("http://archive.apache.org/dist/flex/[revision]/binaries/[module]-[revision]-bin.[ext]")
        }
        ivy {
            name = "Adobe Air SDK"
            artifactPattern("http://download.macromedia.com/air/win/download/[revision]/[module].[ext]")
        }
    }

    dependencies {
        add("airSDK", create("com.adobe", "AdobeAIRSDK", "30.0", ext = "zip"))
        add("flexSDK", create("org.apache", "apache-flex-sdk", "4.14.1", ext = "zip"))
        add("external", files(file("deps/as3commons-lang-0.3.4.swc")))
    }
}


configure<GradleFxConvention> {
    sdkAutoInstall.showPrompts(false)
    type = FlexType.air
    air.apply {
        applicationDescriptor("src/airdescriptor.xml")
        storepass("gradlefx")
    }
    playerVersion = "32"
    mainClass = "Preloader.as"
    additionalCompilerOptions = listOf("-swf-version=32", "-compiler.strict", "-compiler.optimize", "-warnings=false")
}

configure<KotlinJvmProjectExtension> {
    repositories {
        mavenCentral()
    }
    dependencies {
        compile(files("lib/ffdec_lib.jar", "lib/LZMA.jar"))
        compile(kotlin("stdlib"))
    }
}

tasks {
    "decode apk"(JavaExec::class) {
        group = "fourKingdomsUpdater"
        main = "-jar"
        args = listOf("-jar", apkToolFile.absolutePath, "-f", "d", "-s", gameApkFile.absolutePath, "-o", apkToolBuildDir.absolutePath)
    }

    "replace apk preloader" {
        group = "fourKingdomsUpdater"
        dependsOn("buildFx", "decode apk")
        doLast {
            file("build/action_script.swf").copyTo(apkToolPreloaderFile, true)
        }
    }

    "edit four kingdoms swf file"(JavaExec::class) {
        dependsOn("compileKotlin", "decode apk")

        val runtimeFile = file("build/apk_tool/assets/EmpireFourKingdoms.swf")
        val editedFile = file("build/apk_tool/assets/EmpireFourKingdoms-edited.swf")

        group = "fourKingdomsUpdater"
        classpath = java.sourceSets["main"].runtimeClasspath
        args = listOf(runtimeFile.absolutePath, editedFile.absolutePath)
        main = "MainKt"
        doLast {
            editedFile.copyTo(runtimeFile, true)
            editedFile.delete()
        }
    }

    "build modified apk"(JavaExec::class) {
        group = "fourKingdomsUpdater"
        dependsOn("replace apk preloader", "edit four kingdoms swf file")
        main = "-jar"
        args = listOf("-jar", apkToolFile.absolutePath, "b", apkToolBuildDir.absolutePath, "-o", modifiedApkFile.absolutePath)
    }

    "sign modified apk"(Exec::class) {
        group = "fourKingdomsUpdater"
        dependsOn("build modified apk")
        commandLine = listOf("jarsigner", "-verbose",
                "-keystore", modifiedApkKeystoreFile.absolutePath,
                "-storepass", "android",
                "-digestalg", "SHA1", "-sigalg", "MD5withRSA",
                "-signedjar", modifiedSignedApkFile.absolutePath,
                modifiedApkFile.absolutePath, "androiddebugkey")
    }

    "create bot apk"{
        group = "fourKingdomsUpdater"
        dependsOn("sign modified apk")
    }

    "adb uninstall modified apk"(Exec::class) {
        group = "fourKingdomsUpdaterTests"
        commandLine = listOf("adb", "shell", "am", "start", "-a", "android.intent.action.DELETE", "-d", "package:air.com.goodgamestudios.empirefourkingdoms")
    }

    "adb push modified apk"(Exec::class) {
        dependsOn("adb uninstall modified apk")
        group = "fourKingdomsUpdaterTests"
        commandLine = listOf("adb", "push", modifiedSignedApkFile.absolutePath, "/data/local/tmp/air.com.goodgamestudios.empirefourkingdoms")
    }

    "adb install modified apk"(Exec::class) {
        dependsOn("adb push modified apk")
        group = "fourKingdomsUpdaterTests"
        commandLine = listOf("adb", "shell", "pm", "install", "-t", "-r", "/data/local/tmp/air.com.goodgamestudios.empirefourkingdoms")
    }

    "adb run modified apk"(Exec::class) {
        dependsOn("adb install modified apk")
        group = "fourKingdomsUpdaterTests"
        commandLine = listOf("adb", "shell", "am", "start", "-n", "air.com.goodgamestudios.empirefourkingdoms/air.com.goodgamestudios.empirefourkingdoms.AppEntry", "-a", "android.intent.action.MAIN", "-c", "android.intent.category.LAUNCHER")
    }
}

