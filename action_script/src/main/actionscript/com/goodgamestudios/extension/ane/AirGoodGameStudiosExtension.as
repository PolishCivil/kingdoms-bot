package com.goodgamestudios.extension.ane
{
import flash.events.EventDispatcher;
import flash.events.StatusEvent;
import flash.external.ExtensionContext;
import flash.system.Capabilities;

public class AirGoodGameStudiosExtension extends EventDispatcher
{

    private static var _instance:AirGoodGameStudiosExtension;


    private var extCtx:ExtensionContext = null;

    public function AirGoodGameStudiosExtension()
    {
        super();
        if(!_instance)
        {
            extCtx = ExtensionContext.createExtensionContext("com.goodgamestudios.extension",null);
            if(extCtx != null)
            {
                extCtx.addEventListener("status",onStatus);
            }
            else
            {
                trace("[AirGoodGameStudiosExtension] Error - Extension Context is null.");
            }
            _instance = this;
            return;
        }
        throw Error("This is a singleton, use getInstance(), do not call the constructor directly.");
    }

    public static function getInstance() : AirGoodGameStudiosExtension
    {
        return !!_instance?_instance:new AirGoodGameStudiosExtension();
    }

    public function get isGoodGameExtensionSupported() : Boolean
    {
        return Capabilities.manufacturer.search("iOS") > -1 || Capabilities.manufacturer.search("Android") > -1;
    }

    public function getAndroidSDK() : int
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getAndroidSDK") as int;
        }
        return -1;
    }

    public function getAndroidVersion() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getAndroidVersion") as String;
        }
        return "";
    }

    public function getDeviceManufacturer() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDeviceManufacturer") as String;
        }
        return "";
    }

    public function getDeviceModel() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDeviceModel") as String;
        }
        return "";
    }

    public function getDeviceName() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDeviceName") as String;
        }
        return "";
    }

    public function getDeviceBrand() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDeviceBrand") as String;
        }
        return "";
    }

    public function getDeviceProduct() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDeviceProduct") as String;
        }
        return "";
    }

    public function getDeviceHeapSize() : int
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDeviceHeapSize") as int;
        }
        return -1;
    }

    public function showRatingDialog() : Boolean
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("showRatingDialog") as Boolean;
        }
        return false;
    }

    public function isRatingDialogSupported() : Boolean
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("isRatingDialogSupported") as Boolean;
        }
        return false;
    }

    public function immersiveMode() : void
    {
        if(isGoodGameExtensionSupported)
        {
            extCtx.call("immersiveMode");
        }
    }

    public function hideSplashScreen() : void
    {
        if(isGoodGameExtensionSupported)
        {
            extCtx.call("hideSplashScreen");
        }
    }

    public function showSplashScreen() : void
    {
        if(isGoodGameExtensionSupported)
        {
            extCtx.call("showSplashScreen");
        }
    }

    public function getReferrer() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getReferrer") as String;
        }
        return "";
    }

    public function getCarrierInfo() : MobileCarrierInfo
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getCarrierInfo") as MobileCarrierInfo;
        }
        return null;
    }

    public function getAndroidID() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getAndroidID") as String;
        }
        return "";
    }

    public function getInstallerPackageName() : String
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getInstallerPackageName") as String;
        }
        return "";
    }

    public function getKeyboardY() : int
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getKeyboardY") as int;
        }
        return -1;
    }

    public function getKeyboardHeight() : int
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getKeyboardHeight") as int;
        }
        return -1;
    }

    public function getDisplayTopCutoutSize() : int
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDisplayTopCutoutSize") as int;
        }
        return 0;
    }

    public function getDisplayBottomCutoutSize() : int
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("getDisplayBottomCutoutSize") as int;
        }
        return 0;
    }

    public function displayStatusBar() : Boolean
    {
        if(isGoodGameExtensionSupported)
        {
            return extCtx.call("displayStatusBar") as Boolean;
        }
        return false;
    }

    private function onStatus(param1:StatusEvent) : void
    {
    }
}
}
