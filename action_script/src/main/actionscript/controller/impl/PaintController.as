/**
 * Created by polish on 3/22/2017.
 */
package controller.impl {
import avmplus.accessorXml;

import com.senocular.ui.VirtualMouse;

import flash.display.Graphics;
import flash.display.Sprite;
import flash.geom.Rectangle;

public class PaintController {
    private static var rectangle:Rectangle = null;
    private static var rectangleColor = 0xFF0000;
    private static var rectangleColorAlpha = 0.5;

    public static function redraw(mouse:VirtualMouse) {
        var graphics:Graphics = getGraphics();
        graphics.clear();
        mouse.draw(graphics);
        if (rectangle) {
            drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height, rectangleColor, rectangleColorAlpha);
        }
    }

    public static function setRectangle(x:*, y:*, w:*, h:*, c:*, a:*) {
        trace(x, y, w, h, c, a);
        rectangle = new Rectangle(x, y, w, h);
        rectangleColor = int(c);
        rectangleColorAlpha = a * 1.0;
        redraw(Preloader.instance.virtualMouse);
    }

    public static function fillRect(x:*, y:*, w:*, h:*, c:*, a:*) {
        var graphics:Graphics = getGraphics();
        graphics.beginFill(c, a * 1.0);
        graphics.drawRect(x, y, w, h);
        graphics.endFill();
    }

    public static function drawRect(x:*, y:*, w:*, h:*, c:*, a:*) {
        var graphics:Graphics = getGraphics();
        graphics.lineStyle(1, c, a * 1.0);
        graphics.drawRect(x, y, w, h);
    }

    public static function fillOval(x:*, y:*, r:*, c:*, a:*) {
        var graphics:Graphics = getGraphics();
        graphics.beginFill(c, a * 1.0);
        graphics.drawCircle(x, y, r);
        graphics.endFill();
    }

    public static function drawOval(x:*, y:*, r:*, c:*, a:*) {
        var graphics:Graphics = getGraphics();
        graphics.lineStyle(1, c, a * 1.0);
        graphics.drawCircle(x, y, r);
    }

    private static function getGraphics():* {
        return Preloader.overlay.graphics;
    }

}
}
