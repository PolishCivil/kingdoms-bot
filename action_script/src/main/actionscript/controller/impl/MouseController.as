/**
 * Created by polish on 3/22/2017.
 */
package controller.impl {
public class MouseController {
    public static function mouseX():String {
        return String(Preloader.instance.virtualMouse.getLocation().x);
    }

    public static function mouseY():String {
        return String(Preloader.instance.virtualMouse.getLocation().y);
    }

    public static function setLocation(x:*, y:*) {
        Preloader.requestFocus();
        Preloader.instance.virtualMouse.setLocation(x, y);
    }

    public static function touchDown() {
        Preloader.requestFocus();
        Preloader.instance.virtualMouse.press();
    }

    public static function touchUp() {
        Preloader.requestFocus();
        Preloader.instance.virtualMouse.release();
    }
}
}
