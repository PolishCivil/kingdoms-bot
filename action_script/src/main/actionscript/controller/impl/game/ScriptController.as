/**
 * Created by polish on 4/9/2017.
 */
package controller.impl.game {

import controller.impl.ClassController;
import controller.impl.game.model.DictionaryEntry;

import flash.display.Sprite;
import flash.events.Event;

import flash.utils.Dictionary;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;

import scripting.Parser;

import scripting.Scanner;

import scripting.VirtualMachine;

public class ScriptController {
    private static var vm:VirtualMachine;

    private static var script_context = {};
    {
        script_context.result = "";
        script_context.class_controller = ClassController;
        script_context.json = JSON;
        script_context.parseInt = parseInt;
        script_context.getQualifiedClassName = getQualifiedClassName;

        script_context.sendEvent = function (object:Sprite, name:String):Boolean {
            return object.dispatchEvent(new Event(name));
        };
        script_context.touch = function (object:Sprite):Boolean {
            object.dispatchEvent(new Event("touchBegin"));
            object.dispatchEvent(new Event("touchEnd"));
            return object.dispatchEvent(new Event("touchOver"));
        };
        script_context.new_instance = function (cls:*):* {
            return new cls()
        };
        script_context.new_instance_1 = function (cls:*, arg_1:*):* {
            return new cls(arg_1);
        };
        script_context.new_instance_2 = function (cls:*, arg_1:*, arg_2:*):* {
            return new cls(arg_1, arg_2);
        };
        script_context.new_instance_3 = function (cls:*, arg_1:*, arg_2:*, arg_3:*):* {
            return new cls(arg_1, arg_2, arg_3);
        };
        script_context.new_instance_4 = function (cls:*, arg_1:*, arg_2:*, arg_3:*, arg_4:*):* {
            return new cls(arg_1, arg_2, arg_3, arg_4);
        };
        script_context.new_instance_5 = function (cls:*, arg_1:*, arg_2:*, arg_3:*, arg_4:*, arg_5:*):* {
            return new cls(arg_1, arg_2, arg_3, arg_4, arg_5);
        };
        script_context.new_instance_6 = function (cls:*, arg_1:*, arg_2:*, arg_3:*, arg_4:*, arg_5:*, arg_6:*):* {
            return new cls(arg_1, arg_2, arg_3, arg_4, arg_5, arg_6);
        };
        script_context.new_instance_7 = function (cls:*, arg_1:*, arg_2:*, arg_3:*, arg_4:*, arg_5:*, arg_6:*, arg_7:*):* {
            return new cls(arg_1, arg_2, arg_3, arg_4, arg_5, arg_6, arg_7);
        };
    }

    public static function executeScript(code:String):String {
        try {
            var scanner:Scanner = new Scanner(code);
            var parser:Parser = new Parser(scanner);
            vm = new VirtualMachine();
            vm.getGlobalObject().script_ctx = script_context;
            vm.setByteCode(parser.parse());
            vm.execute();
            return String(script_context.result);
        } catch (e:Error) {
            return String("FATAL " + e);
        }
        return String("NONE ");
    }

    public static function execute(code:String):String {
        try {
            var variables = new Dictionary();
            var returnSet = [];
            var lines:Array = code.split("\n");
            var currentLine = "";
            for each (var line:String in lines) {
                currentLine = line;
                var arguments:Array = line.split("\t");
                var opcode:String = arguments[0];
                var invokeStatic = false;
                switch (opcode) {
                    case "INVOKE_STATIC":
                        invokeStatic = true;
                    case "INVOKE":
                        var varName = arguments[1];
                        var outVarName = arguments[2];
                        var methodName = arguments[3];
                        try {
                            var paramObjects:Array = [];
                            for (var i:int = 4; i < arguments.length; i++) {
                                var paramVarName:Object = arguments[i];
                                paramObjects[i - 4] = variables[paramVarName];
                            }
                            var paramCnt = arguments.length - 4;
                            var object = invokeStatic ? ClassController.getDefinition(varName) : variables[varName];
                            switch (paramCnt) {
                                case 0:
                                    variables[outVarName] = call_0_function(object, methodName);
                                    break;
                                case 1:
                                    variables[outVarName] = call_1_function(object, methodName, paramObjects[0]);
                                    break;
                                case 2:
                                    variables[outVarName] = call_2_function(object, methodName, paramObjects[0], paramObjects[1]);
                                    break;
                                case 3:
                                    variables[outVarName] = call_3_function(object, methodName, paramObjects[0], paramObjects[1], paramObjects[2]);
                                    break;
                                case 4:
                                    variables[outVarName] = call_4_function(object, methodName, paramObjects[0], paramObjects[1], paramObjects[2], paramObjects[3]);
                                    break;
                                case 5:
                                    variables[outVarName] = call_5_function(object, methodName, paramObjects[0], paramObjects[1], paramObjects[2], paramObjects[3], paramObjects[4]);
                                    break;
                                case 6:
                                    variables[outVarName] = call_6_function(object, methodName, paramObjects[0], paramObjects[1], paramObjects[2], paramObjects[3], paramObjects[4], paramObjects[5]);
                                    break;
                            }
                        } catch (e:Error) {
                            variables[outVarName] = "ERROR: " + e;
                        }
                        break;
                    case "PUT_INT":
                        var varName = arguments[1];
                        var value = arguments[2];
                        variables[varName] = int(value);
                        break;
                    case "PUT_STR":
                        var varName = arguments[1];
                        var value = arguments[2];
                        variables[varName] = String(value);
                        break;
                    case "GET":
                        try {
                            var from = arguments[1];
                            var to = arguments[2];
                            var fieldName = arguments[3];
                            variables[to] = variables[from][fieldName];
                        } catch (e:Error) {
                            variables[outVarName] = "ERROR: " + e;
                        }
                        break;
                    case "SET":
                        try {
                            var from = arguments[1];
                            var valueVarName = arguments[2];
                            var to = arguments[3];
                            var fieldName = arguments[4];
                            variables[from][fieldName] = variables[valueVarName];
                            variables[to] = variables[from][fieldName];
                        } catch (e:Error) {
                            variables[outVarName] = "ERROR: " + e;
                        }
                        break;
                    case "GET_DICT":
                        try {
                            var from = arguments[1];
                            var to = arguments[2];
                            var keyVarName = arguments[3];
                            var v:* = variables[keyVarName];
                            var dict:* = variables[from];
                            variables[to] = dict[v];
                        } catch (e:Error) {
                            variables[outVarName] = "ERROR: " + e;
                        }
                        break;
                    case "PUSH_JSON":
                        try {
                            var fromAndTo:* = arguments[1];
                            if (variables[fromAndTo] is Dictionary) {
                                var entries:Vector.<*> = new Vector.<*>();
                                var dictionary:Dictionary = Dictionary(variables[fromAndTo]);
                                for (var k:Object in dictionary) {
                                    var d_value:* = dictionary[k];
                                    var d_key:* = k;
                                    entries.push(new DictionaryEntry(d_key, d_value))
                                }
                                returnSet.push(fromAndTo + "--->" + JSON.stringify(entries));
                            } else {
                                returnSet.push(fromAndTo + "--->" + JSON.stringify(variables[fromAndTo]));
                            }
                        } catch (e:Error) {
                            returnSet.push(fromAndTo + "--->" + "ERROR: " + e);
                        }
                        break;
                    case "PUSH":
                        var varName = arguments[1];
                        returnSet.push(varName + "--->" + variables[varName]);
                        break;
                }
            }
            var returnValue = "";
            for each (var string:String in returnSet) {
                returnValue += ":::";
                returnValue += string;
            }
        } catch (e:Error) {
            return String("FATAL " + e);
        }
        return String(returnValue);
    }

    private static function getMediatorInstance(viewObj:*, mediatorCls:Class):* {
        var mediatorMapCls:Class = ClassController.getDefinition("robotlegs.bender.extensions.mediatorMap.api.IMediatorMap");
        var appCoreCls:Class = ClassController.getDefinition("com.goodgamestudios.mobileBasic.application.AppCore");
        var mediatorMapInstance:* = appCoreCls["getModel"](mediatorMapCls);

        var viewHandlerInstance:* = mediatorMapInstance["_viewHandler"];
        var knownMappingsDict:* = viewHandlerInstance["_knownMappings"];
        var knowMappingsForMediatorCls:* = knownMappingsDict[mediatorCls];
        if (knowMappingsForMediatorCls) {
            for each (var key:* in knowMappingsForMediatorCls) {
                var mapping:* = knowMappingsForMediatorCls[key];
                if (mapping["mediatorClass"] == mediatorCls) {
                }
            }
        }
        return null
    }

    private static function call_0_function(owner:*, f_name:String):* {
        return owner[f_name]();
    }

    private static function call_1_function(owner:*, f_name:String, param1:*):* {
        return owner[f_name](param1);
    }

    private static function call_2_function(owner:*, f_name:String, param1:*, param2:*):* {
        return owner[f_name](param1, param2);
    }

    private static function call_3_function(owner:*, f_name:String, param1:*, param2:*, param3:*):* {
        return owner[f_name](param1, param2, param3);
    }

    private static function call_4_function(owner:*, f_name:String, param1:*, param2:*, param3:*, param4:*):* {
        return owner[f_name](param1, param2, param3, param4);
    }

    private static function call_5_function(owner:*, f_name:String, param1:*, param2:*, param3:*, param4:*, param5:*):* {
        return owner[f_name](param1, param2, param3, param4, param5);
    }

    private static function call_6_function(owner:*, f_name:String, param1:*, param2:*, param3:*, param4:*, param5:*, param6:*):* {
        return owner[f_name](param1, param2, param3, param4, param5, param6);
    }
}
}
