package controller.impl.game.model {
public class DictionaryEntry {
    public var key;
    public var value;


    public function DictionaryEntry(key:*, value:*) {
        this.key = key;
        this.value = value;
    }
}
}