/**
 * Created by polish on 4/8/2017.
 */
package controller.impl {
import flash.events.KeyboardEvent;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.ui.KeyLocation;

public class KeyboardController {

    public static function pressKey(keyCode:*, keyChar:*) {

        if (Preloader.loader.stage.focus) {
            Preloader.loader.stage.focus.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, keyChar, keyCode, KeyLocation.STANDARD));
            Preloader.loader.stage.focus.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_UP, true, false, keyChar, keyCode, KeyLocation.STANDARD));
        }
        trace("pressed ", keyCode, " ", keyChar, " focus = ", Preloader.loader.stage.focus);
    }

    public static function setText(text:*, x:*, y:*) {
        if (Preloader.loader.stage.focus) {
            if (Preloader.loader.stage.focus is TextField) {
                var textField:TextField = TextField(Preloader.loader.stage.focus);
                var rectangle:Rectangle = DisplayController.getRectangle(textField);
                trace("rect = ", int(rectangle.x), " ", int(rectangle.y), " vs ", x, ",", y);
                if (int(rectangle.x) == int(x) && int(rectangle.y) == int(y)) {
                    textField.text = text;
                    trace("Settled text for ", textField, " text = ", text, " after = ", textField);
                }
            }
        }
    }

}
}
