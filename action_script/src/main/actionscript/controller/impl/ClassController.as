/**
 * Created by polish on 3/26/2017.
 */
package controller.impl {
import flash.system.ApplicationDomain;

public class ClassController {
    public static function getDefinition(name:String):Class {
        if (!Preloader.loader) {
            return null;
        }
        var domain:ApplicationDomain = getDomain();
        return domain.getDefinition(name) as Class;
    }

    public static function getDomain():ApplicationDomain {
        if (!Preloader.loader) {
            return null;
        }
        return Preloader.loader.contentLoaderInfo.applicationDomain;
    }
}
}
