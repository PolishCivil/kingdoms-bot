/**
 * Created by polish on 3/22/2017.
 */
package controller.impl {
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.utils.getQualifiedClassName;

public class DisplayController {

    public static function getAllObjects():String {
        var numChildren:int = Preloader.loader.numChildren;
        if (numChildren >= 0) {
            var frameOne:DisplayObject = Preloader.loader;
            var ret = [];
            addRec(ret, frameOne);
            return String(ret);
        } else {
            return "";
        }
    }

    public static function findObjectForClassName(name:String):DisplayObject {
        if (!Preloader.instance) {
            return null;
        }
        return _findObjectForClassName(name, DisplayObjectContainer(Preloader.instance.stage.root));
    }

    public static function _findObjectForClassName(name:String, from:DisplayObjectContainer):DisplayObject {
        var children:Array = getChildren(from);
        for each (var object:DisplayObject in children) {
            var n:String = getQualifiedClassName(object);
            if (n == name) {
                return object;
            }
            if (object is DisplayObjectContainer) {
                var displayObject:DisplayObject = _findObjectForClassName(name, DisplayObjectContainer(object));
                if (displayObject) {
                    return displayObject;
                }
            }
        }
        return null;
    }

    public static function getObjectsUnderMouse():String {
        var x:int = int(MouseController.mouseX());
        var y:int = int(MouseController.mouseY());
        var ret = [];
        return String(ret);
    }

    static function addRec(ret:Array, o:DisplayObject) {
        if (o.visible) {
            extractObject(o, ret);
            if (o is DisplayObjectContainer) {
                var container:DisplayObjectContainer = o as DisplayObjectContainer;
                var children:Array = getChildren(container);
                ret.push(children.length);
                for each (var child:DisplayObject in children) {
                    addRec(ret, child);
                }
            } else {
                ret.push(0);
            }
        }
    }

    static function getChildren(container:DisplayObjectContainer):Array {
        var numChildren:int = container.numChildren;
        var ret = [];
        for (var i:int = 0; i < numChildren; i++) {
            var child:DisplayObject = container.getChildAt(i);
            if (child.visible) {
                ret.push(child);
            }
        }
        return ret;
    }

    static function extractObject(obj:DisplayObject, into:Array) {
        var rectangle:Rectangle = getRectangle(obj);
        var qualifiedClassName:String = getQualifiedClassName(obj);
        into.push(qualifiedClassName);
        into.push(int(rectangle.x));
        into.push(int(rectangle.y));
        into.push(int(rectangle.width));
        into.push(int(rectangle.height));
        if (obj is TextField) {
            into.push(TextField(obj).text.split(",").join(";"));
        } else {
            into.push("");
        }
    }

    public static function getRectangle(obj:DisplayObject):Rectangle {
        return obj.getBounds(obj.stage);
    }

}

}
