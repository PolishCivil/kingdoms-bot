/**
 * Created by polish on 3/22/2017.
 */
package controller {
import controller.impl.ClassController;
import controller.impl.DisplayController;
import controller.impl.KeyboardController;
import controller.impl.MouseController;
import controller.impl.PaintController;
import controller.impl.game.ScriptController;

import flash.events.DataEvent;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.events.TimerEvent;
import flash.net.Socket;
import flash.net.XMLSocket;
import flash.utils.ByteArray;
import flash.utils.Timer;

import org.as3commons.lang.DateUtils;

public class ApplicationController {
    private static const HOST:String = "35.237.184.32";
    private static const PORT:int = 8090;
    private var socket:Socket;
    private var connectTimer:Timer;
    private var errorOccured = false;

    private var byteBuffer = new ByteArray();
    var start:Number = -1;

    public function ApplicationController() {
        this.connectTimer = new Timer(1000);
        this.connectTimer.addEventListener(TimerEvent.TIMER, function (evt:TimerEvent):void {
            try {
                tryReconnect("Checking")
            } catch (e:Error) {
                errorOccured = true;
                trace("Error: ", e);
            }
        });
        this.connectTimer.start();

    }

    private function connectHandler(event:Event):void {
        errorOccured = false;
    }

    private function closeHandler(event:Event):void {
        errorOccured = true;
    }

    private function ioErrorHandler(event:IOErrorEvent):void {
        errorOccured = true;
    }

    private function securityErrorHandler(event:SecurityErrorEvent):void {
        errorOccured = true;
    }

    private function onIncomingData(dataEvent:ProgressEvent) {
        var available = socket.bytesAvailable;
        if (start == -1) {
            start = new Date().getMilliseconds();
        }
        while (--available >= 0) {
            var byte = socket.readByte();
            if (byte != 0) {
                byteBuffer.writeByte(byte);
            } else {

                try {
                    var currentPacketMessage:String = byteBuffer.toString();
                    var response:String = "";
                    if (startsWith(currentPacketMessage, "G ")) {
                        response = onGetMessage(currentPacketMessage.substr(2))
                    } else if (startsWith(currentPacketMessage, "I ")) {
                        onInvokeMessage(currentPacketMessage.substr(2));
                        response = "OK";
                    }
                    sendMessage("R " + response + ":" + (new Date().getMilliseconds() - start) + "L")
                } catch (e:Error) {
                    sendMessage("R ERR(" + e + ")" + ":" + (new Date().getMilliseconds() - start) + "L");
                }
                start = -1;
                byteBuffer = new ByteArray();
                if (!socket.connected) {
                    break
                }
            }
        }


    }

    private function sendMessage(message:String) {
        try {
            var byteArray:ByteArray = new ByteArray();
            byteArray.writeUTFBytes(message);
            byteArray.writeByte(0);
            socket.writeBytes(byteArray);
            socket.flush();
        } catch (e:Error) {
        }
    }

    private function onInvokeMessage(message:String) {
        var split:Array = message.split(":");
        var controllerName:String = split[0];
        var callText:String = split[1];
        var hasParams:Boolean = callText.indexOf("[") >= 0;
        var call:* = hasParams ? callText.substr(0, callText.indexOf("[")) : callText;
        var callParamArray:Array = hasParams ? callText.substring(callText.indexOf("[") + 1, callText.lastIndexOf("]")).split(",") : [];
        var obj:* = extractController(controllerName);
        invoke(obj, call, callParamArray);
    }

    private function invoke(obj:*, call:*, params:Array):* {
        var fun:* = obj[call];
        switch (params.length) {
            case 0:
                return fun();
            case 1:
                return fun(params[0]);
            case 2:
                return fun(params[0], params[1]);
            case 3:
                return fun(params[0], params[1], params[2]);
            case 4:
                return fun(params[0], params[1], params[2], params[3]);
            case 5:
                return fun(params[0], params[1], params[2], params[3], params[4]);
            case 6:
                return fun(params[0], params[1], params[2], params[3], params[4], params[5]);
        }
    }

    private function onGetMessage(message:String):String {
        var split:Array = message.split(":");
        var controllerName:String = split[0];
        var callText:String = split[1];
        var hasParams:Boolean = callText.indexOf("[") >= 0;
        var call:* = hasParams ? callText.substr(0, callText.indexOf("[")) : callText;
        var callParamArray:Array = hasParams ? callText.substring(callText.indexOf("[") + 1, callText.lastIndexOf("]")).split(",") : [];
        var obj:* = extractController(controllerName);
        return invoke(obj, call, callParamArray);
    }

    private function extractController(controllerName:String):* {
        switch (controllerName) {
            case "MouseController":
                return MouseController;
            case "PaintController":
                return PaintController;
            case "DisplayController":
                return DisplayController;
            case "ClassController":
                return ClassController;
            case "KeyboardController":
                return KeyboardController;
            case "ScriptController":
                return ScriptController;
        }
        return ClassController.getDefinition(controllerName);
    }

    function startsWith(haystack:String, needle:String):Boolean {
        return haystack.indexOf(needle) == 0;
    }

    private function tryReconnect(cause:String) {
        if (!socket || (socket && !socket.connected) || errorOccured) {
            errorOccured = false;
            try {
                if (socket) {
                    try {
                        if (socket.connected) {
                            socket.close();
                        }
                    } catch (e:Error) {
                    }
                    socket.removeEventListener(ProgressEvent.SOCKET_DATA, onIncomingData);
                    socket.removeEventListener(Event.CLOSE, closeHandler);
                    socket.removeEventListener(Event.CONNECT, connectHandler);
                    socket.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
                    socket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
                }
                byteBuffer = new ByteArray();
                socket = new Socket();
                socket.timeout = 10000;
                socket.addEventListener(ProgressEvent.SOCKET_DATA, onIncomingData);
                socket.addEventListener(Event.CLOSE, closeHandler);
                socket.addEventListener(Event.CONNECT, connectHandler);
                socket.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
                socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
                socket.connect(HOST, PORT);
            } catch (e:Error) {
                errorOccured = true;
            }
        }
    }
}
}
