package {
import com.goodgamestudios.extension.ane.AirGoodGameStudiosExtension;
import com.senocular.ui.VirtualMouse;
import com.senocular.ui.VirtualMouseMouseEvent;

import controller.ApplicationController;

import flash.desktop.NativeApplication;
import flash.desktop.SystemIdleMode;
import flash.display.Loader;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.display.Stage;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.net.URLRequest;
import flash.text.TextField;
import flash.utils.Timer;

public class Preloader extends Sprite {
    public static var loader:Loader;
    public static var overlay:Sprite = new Sprite();
    public static var instance:Preloader;
    public var virtualMouse:VirtualMouse;

    private var _appLoader:Loader;
    private var _splashTimer:Timer;
    private var _loaded:Boolean = false;

    public function Preloader() {
        this.virtualMouse = new VirtualMouse(stage, 0, 0);


        //replace jsons
        Sprite.prototype.toJSON = function (k):* {
            return "sprite";
        };

        MovieClip.prototype.toJSON = function (k):* {
            return "movieclip";
        };

        Stage.prototype.toJSON = function (k):* {
            return "stage";
        };

        instance = this;
        _appLoader = new Loader();
        loader = _appLoader;
        _splashTimer = new Timer(2000);
        super();
        AirGoodGameStudiosExtension.getInstance().showSplashScreen();
        this.addEventListener("addedToStage", init);
    }


    private function init(_arg_1:Event):void {
        this.removeEventListener("addedToStage", init);
        _splashTimer.addEventListener("timer", checkRemoveSplash);
        _splashTimer.start();
        var urlRequest:URLRequest = new URLRequest("EmpireFourKingdoms.swf");
        loader.load(urlRequest);
        loader.contentLoaderInfo.addEventListener("complete", canShowApp);
    }

    private function checkRemoveSplash(_arg_1:Event):void {
        _splashTimer.stop();
        if (_loaded) {
            _splashTimer.removeEventListener("timer", checkRemoveSplash);
            removeSplash(_arg_1);
        }
        else {
            _splashTimer.delay = 500;
            _splashTimer.start();
        }
    }

    private function canShowApp(_arg_1:Event):void {
        _splashTimer.stop();
        _appLoader.contentLoaderInfo.removeEventListener("complete", canShowApp);
        _loaded = true;

        addEventListener(MouseEvent.MOUSE_DOWN, function (evt:MouseEvent) {
            var touchEvent:TouchEvent = new TouchEvent(TouchEvent.TOUCH_BEGIN);
            touchEvent.localX = evt.localX;
            touchEvent.localY = evt.localY;
            if (!(evt is VirtualMouseMouseEvent)) {
                virtualMouse.setLocation(evt.stageX, evt.stageY);
                virtualMouse.press();
            }
            evt.target.dispatchEvent(touchEvent);
        });
        addEventListener(MouseEvent.MOUSE_UP, function (evt:MouseEvent) {
            var touchEvent:TouchEvent = new TouchEvent(TouchEvent.TOUCH_END);
            touchEvent.localX = evt.localX;
            touchEvent.localY = evt.localY;
            if (!(evt is VirtualMouseMouseEvent)) {
                virtualMouse.setLocation(evt.stageX, evt.stageY);
                virtualMouse.release();
            }
            evt.target.dispatchEvent(touchEvent);
        });
        addEventListener(MouseEvent.MOUSE_MOVE, function (evt:MouseEvent) {
            var touchEvent:TouchEvent = new TouchEvent(TouchEvent.TOUCH_MOVE);
            touchEvent.localX = evt.localX;
            touchEvent.localY = evt.localY;
            if (!(evt is VirtualMouseMouseEvent)) {
                virtualMouse.setLocation(evt.stageX, evt.stageY);
                virtualMouse.release();
            }
            evt.target.dispatchEvent(touchEvent);
        });
        addChild(_appLoader);
        addChild(overlay);
        try {
            new ApplicationController();
        } catch (e:Error) {
            removeChild(_appLoader);
            var textField:TextField = new TextField();
            textField.textColor = 0xFF0000;
            textField.text = "ERROR: " + e;
            addChild(textField)
        }
    }

    private function removeSplash(_arg_1:Event):void {
        _splashTimer = null;
        _appLoader = null;
    }

    public static function requestFocus():void {

    }

}
}//package 