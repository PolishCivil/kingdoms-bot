import com.jpexs.decompiler.flash.SWF
import com.jpexs.decompiler.flash.abc.ABC
import com.jpexs.decompiler.flash.abc.types.Namespace
import com.jpexs.decompiler.flash.abc.types.traits.Trait
import java.io.File
import java.io.FileOutputStream

/**
 * Created by polish on 4/20/2017.
 */
fun main(args: Array<String>) {
    val input = File(args[0])
    val swf = SWF(input.inputStream(), false)
    println("swf.gfx = ${swf.gfx}")
    val abc = swf.abcList[0].abc

    for (instanceInfo in abc.instance_info) {
        val traits = instanceInfo.instance_traits
        val name = instanceInfo.getName(abc.constants).getNameWithNamespace(abc.constants, false).toString()
        val endIndex = name.lastIndexOf('.')
        if (endIndex != -1) {
            if (checkPackage(name)) {

                for (trait in traits.traits.filter { it.kindType == Trait.TRAIT_SLOT || it.kindType == Trait.TRAIT_CONST }) {

                    val multiname = trait.getName(abc)
                    val varName = multiname.getName(abc.constants, emptyList(), true, false)

                    val getterFound = findGetter(abc, varName, traits.traits)

                    val namespace = multiname.getNamespace(abc.constants)
                    if (!getterFound) {
                        if (namespace.kind == Namespace.KIND_PRIVATE || namespace.kind == Namespace.KIND_PROTECTED) {


                            if (trait.kindType == Trait.TRAIT_CONST) {
                                if (varName == "_mediators") {
                                    trait.kindType = Trait.TRAIT_SLOT//needed
                                    val newID = abc.constants.getNamespaceId(Namespace.KIND_PACKAGE, "", 0, true)
                                    multiname.namespace_index = newID
                                    println("$name : $varName (${multiname.kindStr}) made public kind = ${namespace.kind}")
                                }
                            } else {
                                val newID = abc.constants.getNamespaceId(Namespace.KIND_PACKAGE, "", 0, true)
                                multiname.namespace_index = newID
                                println("$name : $varName (${multiname.kindStr}) made public")
                            }
                        }
                    }
                }
            }
        }

    }
    swf.isModified = true
    swf.fixAS3Code()
    swf.saveTo(FileOutputStream(args[1]))
}

val usedPackagesOnGameplay = setOf("worldmap", "attack", "equipment", "castle", "lord", "actionPanel", "minuteSkip")

//maybe we should generate custom getters?
private fun checkPackage(name: String): Boolean {
    if (name.startsWith("robotlegs.bender.extensions.ggsMediatorMap.impl")) {
        return true
    }
    if (name.startsWith("robotlegs.bender.extensions.mediatorMap.impl")) {
        return true
    }
    if (name.startsWith("com.goodgamestudios.castle.application")) {
        return true
    }
    if (name.startsWith("com.goodgamestudios.castle.gameplay.actionpanel")) {
        return true
    }
    if (name.startsWith("com.goodgamestudios.castle.gameplay.attack")) {
        return true
    }
    if (name.startsWith("com.goodgamestudios.castle.gameplay.castle")) {
        return true
    }
    if (name.startsWith("com.goodgamestudios.castle.gameplay.worldmap")) {
        return true
    }
    if (name.startsWith("com.goodgamestudios.castle.gameplay")) {
        for (used in usedPackagesOnGameplay) {
            if (name.replace("com.goodgamestudios.castle.gameplay.", "").startsWith(used)) {
                return true
            }
        }
    }
    return !name.startsWith("com.goodgamestudios.castle.gameplay")
}

fun findGetter(abc: ABC, fieldName: String, traits: List<Trait>): Boolean {
    return traits.filter { it.kindType == Trait.TRAIT_GETTER }
            .map { it.getName(abc) }
            .map { it.getName(abc.constants, emptyList(), true, false) }
            .contains(fieldName.replaceFirst("_", ""))
}

