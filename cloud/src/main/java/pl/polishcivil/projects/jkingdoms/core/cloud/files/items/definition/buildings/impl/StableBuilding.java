package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings.impl;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemGroupType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings.ILevelBuilding;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings.ISimpleBuilding;
import pl.polishcivil.projects.jkingdoms.core.data.types.horse.MoveHorseSpeed;

import java.util.List;

/**
 * Created by polish on 1/17/2017.
 */
@Data
public class StableBuilding implements ILevelBuilding, ISimpleBuilding {
    private String type;
    private ItemGroupType group;
    private String name;
    private int ID;
    private int level;
    private List<MoveHorseSpeed> unlockHorses;
}
