package pl.polishcivil.projects.jkingdoms.core.data.types;

import java.util.concurrent.TimeUnit;

public enum TaxCollectTime {
    TCT_NOT_SET(0, -1),
    TCT_00_10(10, 0),
    TCT_00_30(30, 1),
    TCT_01_30(90, 2),
    TCT_03_00(180, 3),
    TCT_06_00(360, 4),
    TCT_12_00(720, 5),
    TCT_24_00(1440, 6);

    private final int minutes;
    private final int optionNr;

    TaxCollectTime(final int minutes, final int optionNr) {
        this.minutes = minutes;
        this.optionNr = optionNr;
    }

    public int getMinutes() {
        return this.minutes;
    }

    public TimeUnit getTimeUnit() {
        return TimeUnit.MINUTES;
    }

    public int getOptionNr() {
        return this.optionNr;
    }

    public int calculateCostAmount(final int populationSize, final boolean isTaxCollectorCorrupted, final TaxBonusLevel bonusLevel, final TaxFeesLevel feesLevel) {
        switch (this) {
            case TCT_00_10: {
                return 0;
            }
            case TCT_00_30:
            case TCT_01_30:
            case TCT_03_00:
            case TCT_06_00: {
                return this.calculateFinalTaxInquiry(this.calculateBasicTaxInquiry(populationSize), isTaxCollectorCorrupted, bonusLevel, feesLevel) / 10;
            }
            case TCT_12_00: {
                return 0;
            }
            case TCT_24_00: {
                return 0;
            }
            default: {
                return 0;
            }
        }
    }

    public int calculateBasicTaxInquiry(final int populationSize) {
        switch (this) {
            case TCT_00_10: {
                return populationSize / 3;
            }
            case TCT_00_30: {
                return (int) (populationSize * 0.66);
            }
            case TCT_01_30: {
                return (int) (populationSize * 1.32);
            }
            case TCT_03_00: {
                return (int) (populationSize * 1.98);
            }
            case TCT_06_00: {
                return populationSize * 3;
            }
            case TCT_12_00: {
                return (int) (populationSize * 7.2);
            }
            case TCT_24_00: {
                return (int) (populationSize * 19.2);
            }
            default: {
                return 0;
            }
        }
    }

    public int calculateBonusTaxInquiry(final int basicTaxAmount, final boolean isTaxCollectorCorrupted, final TaxBonusLevel bonusLevel) {
        int finalTax = basicTaxAmount;
        if (basicTaxAmount == 0) {
            return 0;
        }
        if (bonusLevel != null && bonusLevel != TaxBonusLevel.NO_BONUS) {
            finalTax += (int) Math.round(finalTax * (0.01 * bonusLevel.getBonusInPercent()));
        }
        if (isTaxCollectorCorrupted) {
            finalTax += (int) Math.round(finalTax * 0.2);
        }
        return finalTax - basicTaxAmount;
    }

    public int calculateFeesTaxInquiry(final int basicTaxAmount, final boolean isTaxCollectorCorrupted, final TaxFeesLevel feesLevel) {
        int finalTax = basicTaxAmount;
        if (basicTaxAmount == 0) {
            return 0;
        }
        if (feesLevel != null && feesLevel != TaxFeesLevel.NO_BONUS) {
            finalTax += (int) Math.round(finalTax * (0.01 * feesLevel.getBonusInPercent()));
        }
        if (isTaxCollectorCorrupted) {
            finalTax += (int) Math.round(finalTax * 0.2);
        }
        return finalTax - basicTaxAmount;
    }

    public int calculateResearchTaxInquiry(final int basicTaxAmount, final boolean isTaxCollectorCorrupted, final TaxBonusLevel bonusLevel, final TaxFeesLevel feesLevel) {
        int finalTax = basicTaxAmount;
        if (basicTaxAmount == 0) {
            return 0;
        }
        int percent = 0;
        if (bonusLevel != null && bonusLevel != TaxBonusLevel.NO_BONUS) {
            percent += bonusLevel.getBonusInPercent();
        }
        if (feesLevel != null && feesLevel != TaxFeesLevel.NO_BONUS) {
            percent += feesLevel.getBonusInPercent();
        }
        if (percent > 0) {
            finalTax += (int) Math.round(finalTax * (0.01 * percent));
        }
        if (isTaxCollectorCorrupted) {
            finalTax += (int) Math.round(finalTax * 0.2);
        }
        return finalTax - basicTaxAmount;
    }

    public int calculateFinalTaxInquiry(final int populationSize, final boolean isTaxCollectorCorrupted, final TaxBonusLevel bonusLevel, final TaxFeesLevel feesLevel) {
        final int basicTax = this.calculateBasicTaxInquiry(populationSize);
        return basicTax + this.calculateBonusTaxInquiry(basicTax, isTaxCollectorCorrupted, bonusLevel);
    }

    public static TaxCollectTime getByOptionNr(final int optionNr) {
        TaxCollectTime[] values;
        for (int length = (values = values()).length, i = 0; i < length; ++i) {
            final TaxCollectTime taxCT = values[i];
            if (taxCT.getOptionNr() == optionNr) {
                return taxCT;
            }
        }
        return null;
    }

    public static TaxCollectTime getByMinutes(final int minutes) {
        TaxCollectTime[] values;
        for (int length = (values = values()).length, i = 0; i < length; ++i) {
            final TaxCollectTime taxCT = values[i];
            if (taxCT.getMinutes() == minutes) {
                return taxCT;
            }
        }
        return null;
    }
}
