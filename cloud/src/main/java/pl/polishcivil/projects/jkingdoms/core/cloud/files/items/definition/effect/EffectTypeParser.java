package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;

/**
 * Created by polish on 1/19/17.
 */
public class EffectTypeParser implements ItemDefinitionParser<EffectTypeDefinition> {
    @Override
    public EffectTypeDefinition parse(Node node) throws Exception {
        int id = getAttribute(node, "effectTypeID").map(Integer::parseInt).orElseThrow(() -> new RuntimeException
                ("Couldn't find id attribute"));
        String name = getAttribute(node, "name").orElseThrow(() -> new RuntimeException("Couldn't find name " +
                "attribute"));
        int maxTotalBonus = Integer.parseInt(getAttributeDefault(node, "maxTotalBonus", "-1"));
        EffectTypeDefinition definition = new EffectTypeDefinition();
        definition.setID(id);
        definition.setName(name);
        definition.setMaxTotalBonus(maxTotalBonus);
        return definition;
    }
}
