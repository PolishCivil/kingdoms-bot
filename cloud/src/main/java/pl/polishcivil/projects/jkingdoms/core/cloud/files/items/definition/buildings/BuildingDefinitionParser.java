package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemGroupType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings.impl.StableBuilding;
import pl.polishcivil.projects.jkingdoms.core.data.types.horse.MoveHorseSpeed;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by polish on 1/17/2017.
 */
public class BuildingDefinitionParser implements ItemDefinitionParser<ISimpleBuilding> {

    public ISimpleBuilding parse(Node node) throws Exception {
        NamedNodeMap attributes = node.getAttributes();

        String name = Optional.ofNullable(attributes.getNamedItem("name"))
                .map(Node::getNodeValue)
                .orElseThrow(() -> new RuntimeException("Couldn't find name"));
        String type = Optional.ofNullable(attributes.getNamedItem("type"))
                .map(Node::getNodeValue)
                .orElseThrow(() -> new RuntimeException("Couldn't find type"));

        ItemGroupType group = Optional.ofNullable(attributes.getNamedItem("group"))
                .map(Node::getNodeValue)
                .flatMap(ItemGroupType::forRaw)
                .orElseThrow(() -> new RuntimeException(String.format("Couldn't find group for name %s", name)));

        int id = Optional.ofNullable(attributes.getNamedItem("wodID"))
                .map(Node::getNodeValue)
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't find id"));

        ISimpleBuilding definition;
        switch (name) {
            case "FactionStable":
            case "Stable":
            case "Harbor":
                StableBuilding stableBuilding = new StableBuilding();
                List<MoveHorseSpeed> unlockHorses = Optional.ofNullable(attributes.getNamedItem("unlockHorses"))
                        .map(Node::getNodeValue)
                        .map(v -> Stream.of(v.split(",")))
                        .orElse(Stream.empty())
                        .map(Integer::parseInt)
                        .map(MoveHorseSpeed::forID).collect(Collectors.toList());

                if (unlockHorses.isEmpty()) {
                    throw new RuntimeException("No horses found for definition!");
                }

                stableBuilding.setUnlockHorses(unlockHorses);

                setLevelBuildingData(stableBuilding, attributes);
                definition = stableBuilding;
                break;
            default:
                throw new RuntimeException(String.format("Unknown building name %s", name));
        }
        definition.setID(id);
        definition.setName(name);
        definition.setType(type);
        definition.setGroup(group);
        return definition;
    }


    private void setLevelBuildingData(ILevelBuilding building, NamedNodeMap attributes) {
        int level = Optional.ofNullable(attributes.getNamedItem("level"))
                .map(Node::getNodeValue)
                .map(Integer::parseInt)
                .orElse(-1);
        building.setLevel(level);
    }
}
