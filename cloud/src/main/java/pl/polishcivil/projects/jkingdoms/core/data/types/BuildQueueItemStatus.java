package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum BuildQueueItemStatus {
    BUSY,
    FREE,
    BLOCKED;

    public static BuildQueueItemStatus getStatusFromValue(int wid) {
        switch (wid) {
            case -2:
                return BLOCKED;
            case -1:
                return FREE;
            default:
                return BUSY;
        }
    }
}
