package pl.polishcivil.projects.jkingdoms.core.data.merchants;

import org.apache.logging.log4j.LogManager;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.packages.PackageDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.PackageEventSourceType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by polish on 1/15/2017.
 */
public class Merchants {
    private static Set<Merchant> registeredMerchants = new HashSet<>();

    public static void registerMerchant(Merchant merchant) {
        LogManager.getLogger().debug("Registered merchant {}", merchant);
        registeredMerchants.add(merchant);
    }


    static {
        registerMerchant(new Merchant(58, "UnitDealer Seaqueen", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(14, "UnitDealer Storm Island", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(76, "UnitDealer Berimond", PackageEventSourceType.EVENT));

        registerMerchant(new Merchant(22, "Merchant", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(27, "Armorer", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(101, "Equipment", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(66, "GiftMerchant", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(21, "Shadow Units", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(1, "Slum Desert", PackageEventSourceType.SLUM));
        registerMerchant(new Merchant(2, "Slum Ice", PackageEventSourceType.SLUM));
        registerMerchant(new Merchant(3, "Slum Volcano", PackageEventSourceType.SLUM));
        registerMerchant(new Merchant(72, "NomadInvasion", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(49, "NomadHunter", PackageEventSourceType.EVENT));
        registerMerchant(new Merchant(4, "Seaqueen", PackageEventSourceType.EVENT));
    }

    public static List<Merchant> findMerchantsForPackage(EmpireCloud cloud, PackageDefinition definition) {
        return registeredMerchants.stream().filter(it -> {
            List<PackageDefinition> available = it.getAvailablePackages(cloud);
            return available.contains(definition);
        }).collect(Collectors.toList());
    }

    public static Set<Merchant> getRegisteredMerchants() {
        return registeredMerchants;
    }
}
