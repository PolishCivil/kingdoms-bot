package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemGroupType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.ILocalizedDefinition;

/**
 * Created by polish on 12/5/16.
 */
@Data
public class UnitDefinition implements ILocalizedDefinition, IDefinition {
    private int ID;
    private ItemGroupType group;
    private String typeName;
    private UnitRecruitType recruit;
    private int rubyCost;
    private int goldCost;
    private int woodCost;
    private int stoneCost;
    private int[] cleavageOfCellsCost;
    private double healCostRuby;
    private double healCostGold;

    @Override
    public String getLanguageKey() {
        return String.format("%s_name", typeName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UnitDefinition that = (UnitDefinition) o;

        return ID == that.ID;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ID;
        return result;
    }
}
