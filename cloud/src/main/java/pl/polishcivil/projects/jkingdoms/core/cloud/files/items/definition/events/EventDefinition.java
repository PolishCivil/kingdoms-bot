package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.events;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom;

import java.util.Set;

/**
 * Created by polish on 1/15/2017.
 */
@Data
public class EventDefinition implements IDefinition {
    private Set<Kingdom> kingdoms;
    private String type;
    private int ID;
    private int[] packageIDS;
}
