package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 1/15/2017.
 */
public enum PackageEventSourceType {
    EVENT("events", 0),
    SLUM("slums", 3),
    EVENT_DISGUISED_SLUM("slums", 3),
    VIP("vip", 2),;

    private final String name;
    private final int buyType;

    PackageEventSourceType(String name, int buyType) {
        this.buyType = buyType;
        this.name = name;
    }
}
