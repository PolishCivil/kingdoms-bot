package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;

/**
 * Created by polish on 3/3/2017.
 */
@Data
public class EquipmentSetDefinition implements IDefinition {
    private int ID;
    private int setID;
    private StaticEffectData effect;
    private int neededItems;
}
