package pl.polishcivil.projects.jkingdoms.core.cloud;

/**
 * Created by polish on 12/5/16.
 */
public class EmpireCloudDataUpdateException extends Exception {
    public EmpireCloudDataUpdateException(String message) {
        super(message);
    }
}
