package pl.polishcivil.projects.jkingdoms.core.data.types;

import java.util.Arrays;

/**
 * Created by polish on 1/17/2017.
 */
public enum EquipmentSlotType {
    HERO(6),
    ARMOR(1),
    ARTIFACT(4),
    HELMET(3),
    SKIN(5),
    WEAPON(2),
    UNKNOWN(-1);
    private int rawType;

    EquipmentSlotType(int rawType) {
        this.rawType = rawType;
    }

    public static EquipmentSlotType getForRaw(int rawType) {
        return Arrays.stream(EquipmentSlotType.values()).filter(it -> it.rawType == rawType).findFirst().orElse(UNKNOWN);
    }

    public int getRawType() {
        return rawType;
    }
}
