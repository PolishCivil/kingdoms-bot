package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 1/17/2017.
 */
public enum LordType {
    BARON,
    GENERAL,
    UNKNOWN;

    public static LordType getForRaw(int raw) {
        switch (raw) {
            case 1:
                return BARON;
            case 2:
                return GENERAL;
        }
        return UNKNOWN;
    }
}
