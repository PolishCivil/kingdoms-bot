package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.gem;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem.GemDefinition;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 1/19/17.
 */
public class GemsDB extends AbstractDefinitionDB<GemDefinition> {
    public List<GemDefinition> getGemStaticVOsForSetID(int setID) {
        return getAll().stream().filter(it -> it.getSetID() == setID).collect(Collectors.toList());
    }
}
