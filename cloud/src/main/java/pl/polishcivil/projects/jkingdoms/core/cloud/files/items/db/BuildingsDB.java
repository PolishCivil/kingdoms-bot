package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings.ISimpleBuilding;

/**
 * Created by polish on 1/19/17.
 */
public class BuildingsDB extends AbstractDefinitionDB<ISimpleBuilding> {

}
