package pl.polishcivil.projects.jkingdoms.core.data;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.CollectableItemType;

import java.util.HashSet;

/**
 * Created by polish on 1/14/2017.
 */
@Data
public class CollectableItem {
    public static final HashSet<CollectableItemType> GROUP_SET_BASIC_CURRENCY = new HashSet<CollectableItemType>() {{
        add(CollectableItemType.C1);
        add(CollectableItemType.C2);
    }};
    public static final HashSet<CollectableItemType> GROUP_SET_SPECIAL_CURRENCY = new HashSet<CollectableItemType>() {{

    }};
    public static final HashSet<CollectableItemType> GROUP_SET_CURRENCY = new HashSet<CollectableItemType>() {{
        addAll(GROUP_SET_BASIC_CURRENCY);
        addAll(GROUP_SET_SPECIAL_CURRENCY);
    }};
    public static final HashSet<CollectableItemType> GROUP_SET_RESOURCES = new HashSet<CollectableItemType>() {{
        add(CollectableItemType.FOOD);
        add(CollectableItemType.WOOD);
    }};

    public static final HashSet<CollectableItemType> GROUP_SET_GOODS = new HashSet<CollectableItemType>() {{
        addAll(GROUP_SET_RESOURCES);
        addAll(GROUP_SET_CURRENCY);
    }};


    private final CollectableItemType type;

    //only for units tbh
    private final int id;
    private final int amount;


    public static String getCollectableIteInfoText(CollectableItem item, EmpireCloud cloud) {
        StringBuilder builder = new StringBuilder();
        CollectableItemType type = item.getType();
        switch (type) {
            case C1:
            case C2:
            case WOOD:
            case STONE:
            case FOOD:
                String tooltip = cloud.getLanguageFile(EmpireLanguageTag.PL).getMessage(type.getDefaultTooltipId());
                builder.append(item.getAmount()).append(" * ").append(tooltip);
                break;
            case UNITS:
                UnitDefinition def = cloud.getItemsFile().getUnits().findByID(item.getId());
                String unitName = def.getLocalizedName(cloud, EmpireLanguageTag.PL);
                builder.append(item.getAmount()).append(" * ").append(unitName);
                break;
        }
        return builder.toString();
    }
}
