package pl.polishcivil.projects.jkingdoms.core.cloud;

import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileLocation;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileStorage;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileType;

/**
 * Created by polish on 12/5/16.
 */
public enum EmpireCloudType {

    EMPIRE_WEB(new EmpireFileStorage(
            new EmpireFileLocation(EmpireFileType.GF_LANGUAGES, "http://media.goodgamestudios.com/games-languages/12/{0}.ggs"),
            new EmpireFileLocation(EmpireFileType.GF_ITEMS, "http://media.goodgamestudios.com/branches/castle/default/config/items_v{0}.ggs"),
            new EmpireFileLocation(EmpireFileType.GF_ITEMS_VERSION, "http://media.goodgamestudios.com/branches/castle/default/ItemsVersion.properties"))
    ),
    EMPIRE_CUSTOM(new EmpireFileStorage(
            new EmpireFileLocation(EmpireFileType.GF_LANGUAGES, EmpireCloudType.class.getResource("/languages/en.zip").toString().replace("en.zip","{0}.zip")),
            new EmpireFileLocation(EmpireFileType.GF_ITEMS, EmpireCloudType.class.getResource("/items.zip").toString()),
            new EmpireFileLocation(EmpireFileType.GF_ITEMS_VERSION, EmpireCloudType.class.getResource("/ItemsVersion.properties").toString()))
    );

    private final EmpireFileStorage storage;

    EmpireCloudType(final EmpireFileStorage storage) {
        this.storage = storage;
    }

    public EmpireFileStorage getStorage() {
        return this.storage;
    }
}
