package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.packages.PackageDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 1/19/17.
 */
public class PackagesDB extends AbstractDefinitionDB<PackageDefinition> {

    public List<PackageDefinition> filter(int... ids) {
        List<Integer> values = new ArrayList<>();
        for (int id : ids) {
            values.add(id);
        }
        return items.stream().filter(v -> values.contains(v.getID())).collect(Collectors.toList());
    }
}
