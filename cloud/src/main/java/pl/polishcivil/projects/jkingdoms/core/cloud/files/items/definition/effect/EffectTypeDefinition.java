package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

/**
 * Created by polish on 1/19/17.
 */
@Data
public class EffectTypeDefinition implements IDefinition {
    private int ID;
    private String name;
    private int maxTotalBonus;
}
