package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum CastleStatus {
    FREE,
    CREATING,
    MOVING,
    COOLDOWN,
    OCCUPIED,
    OWNED,
    RUINS
}
