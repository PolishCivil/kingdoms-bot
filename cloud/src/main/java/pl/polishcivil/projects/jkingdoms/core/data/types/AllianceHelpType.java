package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum AllianceHelpType {
    RECRUITMENT(1, 3, 0),
    HEAL(2, 5, 0),
    REPAIR(3, 10, 86400),
    BUILD(4, 3, 0),
    IDK(6, 3, 0),
    UNKNOWN(-1, -1, -1);

    private final int rawType;
    private final int minAlianceHelpCount;
    private final int waitToNextTime;

    AllianceHelpType(int rawType, int minAlianceHelpCount, int waitToNextTime) {
        this.rawType = rawType;
        this.minAlianceHelpCount = minAlianceHelpCount;
        this.waitToNextTime = waitToNextTime;
    }

    public int getRawType() {
        return this.rawType;
    }

    public int getMinAlianceHelpCount() {
        return this.minAlianceHelpCount;
    }

    public int getWaitToNextTime() {
        return this.waitToNextTime;
    }

    public static AllianceHelpType getByRawType(int rawType) {
        AllianceHelpType[] var4;
        int var3 = (var4 = values()).length;

        for (int var2 = 0; var2 < var3; ++var2) {
            AllianceHelpType bt = var4[var2];
            if (bt.getRawType() == rawType) {
                return bt;
            }
        }
        return UNKNOWN;
    }
}