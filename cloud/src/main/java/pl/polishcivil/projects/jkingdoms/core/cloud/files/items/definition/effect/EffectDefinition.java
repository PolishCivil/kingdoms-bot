package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect.EffectTypesDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.LocationType;

import java.util.List;

/**
 * Created by polish on 1/19/17.
 */
@Data
public class EffectDefinition implements IDefinition {
    private int ID;
    private String name;
    private int effectTypeID;
    private List<LocationType> areaTypeIds;

    private EffectTypeDefinition _effectType;

    public EffectTypeDefinition getEffectType(EmpireCloud cloud) {
        if (_effectType != null) {
            return _effectType;
        }
        EffectTypesDB db = cloud.getItemsFile().getEffectTypes();
        return _effectType = db.findByID(effectTypeID);
    }
}
