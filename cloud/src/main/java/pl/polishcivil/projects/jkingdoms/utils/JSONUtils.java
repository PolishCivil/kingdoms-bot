package pl.polishcivil.projects.jkingdoms.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by polish on 9/25/16.
 */
public class JSONUtils {
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Serializes the object to json.
     *
     * @param object - the object to serialize
     * @return - the text of json
     */
    public static String serialize(Object object) {
        return gson.toJson(object);
    }

    /**
     * Serializes the object to json.
     *
     * @param object - the object to serialize
     * @param output - the output file
     */
    public static void serialize(Object object, File output) {
        try {
            @Cleanup
            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(output), "UTF-8"));
            gson.toJson(object, out);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deserialize specific json to specific object of specific type
     *
     * @param jsonText - the text of json
     * @param cls      - object class
     * @param <T>      - the type of object
     * @return
     */
    public static <T> T deserialize(@NotNull String jsonText, Class<T> cls) {
        return gson.fromJson(jsonText, cls);
    }

    /**
     * Deserialize specific json to specific object of specific type
     *
     * @param jsonText - the text of json
     * @param <T>      - the type of object
     * @return
     */
    public static <T> List<T> deserializeList(String jsonText, Class<T> elementType) {
        return gson.fromJson(jsonText, new ListOfJson<T>(elementType));
    }

    public static class ListOfJson<T> implements ParameterizedType {

        private Class<?> wrapped;

        public ListOfJson(Class<T> wrapper) {
            this.wrapped = wrapper;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[]{wrapped};
        }

        @Override
        public Type getRawType() {
            return List.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }

    }

    /**
     * Deserialize specific json to specific object of specific type
     *
     * @param jsonFile - the file that contains text of json
     * @param cls      - object class
     * @param <T>      - the type of object
     * @return
     */
    public static <T> T deserialize(File jsonFile, Class<T> cls) {
        try {
            @Cleanup
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(jsonFile), "UTF-8"));
            return deserialize(reader, cls);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deserialize specific json to specific object of specific type
     *
     * @param reader - the reader that contains text of json
     * @param cls    - object class
     * @param <T>    - the type of object
     * @return
     */
    public static <T> T deserialize(Reader reader, Class<T> cls) {
        return gson.fromJson(reader, cls);
    }

    /**
     * Deserialize specific json to specific object of specific type
     *
     * @param jsonURL - the url that contains text of json
     * @param cls     - object class
     * @param <T>     - the type of object
     * @return
     */
    public static <T> T deserialize(URL jsonURL, Class<T> cls) {
        try {
            URLConnection connection = jsonURL.openConnection();
            @Cleanup InputStream inputStream = connection.getInputStream();
            @Cleanup InputStreamReader reader = new InputStreamReader(inputStream);
            return gson.fromJson(reader, cls);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param jsonText
     * @return
     */
    public static boolean isTextValidJSONFast(String jsonText) {
        return jsonText.startsWith("{") || jsonText.startsWith("[");
    }

    /**
     * @param jsonText
     * @return
     */
    public static boolean isTextValidJSON(String jsonText) {
        try {
            gson.fromJson(jsonText, Object.class);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean isVarEmpty(JsonObject object, String varName) {
        JsonElement jsonElement = object.get(varName);
        if (jsonElement == null) {
            return true;
        }
        return jsonElement.getAsString().length() == 0;
    }

    public static boolean isVarEmpty(@Nullable JsonElement jsonElement) {
        if (jsonElement == null) {
            return true;
        }
        if (jsonElement.isJsonObject()) {
            return jsonElement.toString().equals("{}");
        }
        return jsonElement.getAsString().length() == 0;
    }
}
