package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 1/14/2017.
 */
public enum CollectableItemType {
    C1("C1", ResourceType.GOLD, "cash", "cash_short_info"),
    C2("C2", ResourceType.RUBY, "gold", "gold_short_info"),
    UNITS("units", null),
    WOOD("wood", ResourceType.WOOD, "wood", "wood_short_info"),
    STONE("stone", ResourceType.STONE, "stone", "stone_short_info"),
    FOOD("food", ResourceType.FOOD, "food", "food_short_info"),;

    private String name;
    private ResourceType type;
    private String defaultTooltipId;
    private String defaultDescriptionID;

    CollectableItemType(String name, ResourceType type, String defaultTooltipId, String defaultDescriptionID) {
        this.name = name;
        this.type = type;
        this.defaultTooltipId = defaultTooltipId;
        this.defaultDescriptionID = defaultDescriptionID;
    }

    CollectableItemType(String name, ResourceType type) {
        this.name = name;
        this.type = type;
        this.defaultTooltipId = "";
        this.defaultDescriptionID = "";
    }


    public String getName() {
        return name;
    }

    public ResourceType getType() {
        return type;
    }

    public String getDefaultTooltipId() {
        return defaultTooltipId;
    }

    public String getDefaultDescriptionID() {
        return defaultDescriptionID;
    }
}
