package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum MoveType {
    ATTACK(0),
    DEFENCE(1),
    TRAVEL(2),
    SPY(3),
    MARKET(4),
    SIEGE(5),
    TREASUREHUNT(6),
    SHADOWATTACK(7),
    SHADOWTRAVEL(8),
    NPC_ATTACK(11),
    PLAGUEMONK(14),
    OCCUPY_FACTION(15),
    FOREIGN_CASTLE_ATTACK(17),
    BERIMOND_ATTACK(18);

    final int rawType;

    MoveType(final int rawType) {
        this.rawType = rawType;
    }

    public int getRawType() {
        return this.rawType;
    }

    public static MoveType getByRawType(final int rawType) {
        MoveType[] values;
        for (int length = (values = values()).length, i = 0; i < length; ++i) {
            final MoveType mt = values[i];
            if (mt.getRawType() == rawType) {
                return mt;
            }
        }
        return null;
    }
}