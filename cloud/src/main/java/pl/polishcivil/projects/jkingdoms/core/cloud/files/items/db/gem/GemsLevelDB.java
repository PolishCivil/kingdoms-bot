package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.gem;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem.GemLevelDefinition;

/**
 * Created by polish on 3/3/2017.
 */
public class GemsLevelDB extends AbstractDefinitionDB<GemLevelDefinition> {
}
