package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.types.LocationType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 3/17/2017.
 */
public class EquipmentGemEffectDefinitionParser implements ItemDefinitionParser<EquipmentGemEffectDefinition> {
    @Override
    public EquipmentGemEffectDefinition parse(Node node) throws Exception {
        int effectID = Integer.parseInt(getAttribute(node, "effectID").orElseThrow(() -> new RuntimeException("Couldn't find gem effect id")));
        String name = getAttribute(node, "name").orElseThrow(() -> new RuntimeException("Couldn't find gem effect name"));
        int effectTypeID = Integer.parseInt(getAttribute(node, "effectTypeID").orElseThrow(() -> new RuntimeException("Couldn't find gem effect type id")));
        String areaTypeID = getAttributeDefault(node, "areaTypeID", "");
        List<LocationType> locationTypes = areaTypeID.equals("") ? Collections.emptyList() : Arrays.stream(areaTypeID.split(",")).mapToInt(Integer::parseInt).mapToObj(LocationType::getByRawType).collect(Collectors.toList());
        return new EquipmentGemEffectDefinition(effectID, locationTypes, effectTypeID, name);
    }
}
