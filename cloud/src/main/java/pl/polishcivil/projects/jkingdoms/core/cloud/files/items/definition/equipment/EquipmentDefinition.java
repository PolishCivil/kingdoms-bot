package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.ILocalizedDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;
import pl.polishcivil.projects.jkingdoms.core.data.types.LordType;

import java.util.List;

/**
 * Created by polish on 3/3/2017.
 */
@Data
public class EquipmentDefinition implements IDefinition, ILocalizedDefinition {
    private int ID;//UNIQUE ID
    private int setID;
    private int rarity;
    private int category;
    private int pictureID;
    private LordType wearer;
    private List<StaticEffectData> bonuses;

    @Override
    public String getLanguageKey() {
        return String.format("equipment_unique_%d", ID);
    }
}
