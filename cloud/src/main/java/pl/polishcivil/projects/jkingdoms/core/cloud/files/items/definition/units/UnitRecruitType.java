package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by polish on 12/5/16.
 */
public enum UnitRecruitType {
    UNKNOWN("Unknown"),
    Unknowntool("Unknowntool"),
    KINGDOM_UNIT("Kingdomunit"),
    KEEP("Keep"),
    WORKSHOP("Workshop"),
    BARRACKS("Barracks"),
    D_WORKSHOP("Dworkshop"),
    SHADOW_UNIT("Shadowunit"),
    EVENT_UNIT("Eventunit"),
    QUICK_ATTACK("QuickAttack"),
    ELITE_TOOL("Elitetool"),
    SHADOW_TOOL("Shadowtool"),
    EVENT_TOOL("Eventtool");

    private final String raw;

    UnitRecruitType(String raw) {
        this.raw = raw;
    }

    public static Optional<UnitRecruitType> forRaw(String rawType) {
        return Arrays.stream(UnitRecruitType.values()).filter(it -> it.raw.equals(rawType)).findFirst();
    }
}
