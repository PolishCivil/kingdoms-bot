package pl.polishcivil.projects.jkingdoms.core.data.types;

import java.util.Arrays;

public enum QueueType {
    SOLDIERS(0),
    TOOLS(1),
    HOSPITAL(2),
    VOLUNTEERS(3);

    private final int rawType;

    QueueType(int rawType) {
        this.rawType = rawType;
    }

    public int getRawType() {
        return this.rawType;
    }

    public static QueueType getByRawType(int rawType) {
        return Arrays.stream(QueueType.values()).filter(it -> it.rawType == rawType).findAny()
                .orElseThrow(() -> new RuntimeException(String.format("Couldn't find queue type for id %d", rawType)));
    }
}
