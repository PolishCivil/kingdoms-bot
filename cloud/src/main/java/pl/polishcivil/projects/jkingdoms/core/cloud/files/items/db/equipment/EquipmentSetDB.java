package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.equipment;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment.EquipmentSetDefinition;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 3/3/2017.
 */
public class EquipmentSetDB extends AbstractDefinitionDB<EquipmentSetDefinition> {
    public List<EquipmentSetDefinition> getAllForSetID(int setID) {
        return getAll().stream().filter(it -> it.getSetID() == setID).collect(Collectors.toList());
    }
}
