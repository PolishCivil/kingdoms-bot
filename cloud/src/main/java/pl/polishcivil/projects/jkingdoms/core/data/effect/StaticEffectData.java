package pl.polishcivil.projects.jkingdoms.core.data.effect;

import lombok.Data;

/**
 * Created by polish on 3/3/2017.
 */
@Data
public class StaticEffectData {
    private final int effectID;
    private final int effectValue;
}
