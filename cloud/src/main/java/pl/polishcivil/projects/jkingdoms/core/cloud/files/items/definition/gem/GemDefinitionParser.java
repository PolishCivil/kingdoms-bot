package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;
import pl.polishcivil.projects.jkingdoms.core.data.types.LordType;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

/**
 * Created by polish on 1/18/2017.
 */
public class GemDefinitionParser implements ItemDefinitionParser<GemDefinition> {

    public GemDefinition parse(Node node) throws Exception {
        int gemID = getAttribute(node, "gemID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse gem, missing id item"));

        int setID = getAttribute(node, "setID")
                .map(Integer::parseInt)
                .orElse(-1);

        int gemLevel = getAttribute(node, "gemLevelID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException(format("Couldn't parse gem %d, missing level item", gemID)));

        int followingGemID = getAttribute(node, "followingGemID")
                .map(Integer::parseInt)
                .orElse(-1);

        int wearerID = getAttribute(node, "wearerID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException(format("Couldn't parse gem %d, missing wearer item", gemID)));

        List<StaticEffectData> effects = parseGemEffectTypes(getAttributeDefault(node, "effects", ""));

        GemDefinition gemDefinition = new GemDefinition();
        gemDefinition.setID(gemID);
        gemDefinition.setLevel(gemLevel);
        gemDefinition.setFollowingGemID(followingGemID);
        gemDefinition.setLordType(LordType.getForRaw(wearerID));
        gemDefinition.setSetID(setID);
        gemDefinition.setEffects(effects);
        return gemDefinition;
    }

    private List<StaticEffectData> parseGemEffectTypes(String param1) {
        String[] _loc5_ = param1.split(",");
        List<StaticEffectData> _loc2_ = new ArrayList<>();

        for (int i = 0; i < _loc5_.length; i++) {
            String[] _loc4_ = _loc5_[i].split("&");
            int effectID = Integer.parseInt(_loc4_[0]);
            int effectValue = Integer.parseInt(_loc4_[1]);
            _loc2_.add(new StaticEffectData(effectID, effectValue));

        }
        return _loc2_;
    }
}
