package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.feasts;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by polish on 12/17/16.
 */
@Data
public class FeastDefinition implements IDefinition {
    public enum FeastType {
        SMALL("small"), MEDIUM("medium"), BIG("big"), FOURTH("fourth"), RUBY("ruby");

        private final String rawType;

        FeastType(String rawType) {
            this.rawType = rawType;
        }

        public String getRawType() {
            return rawType;
        }

        public static Optional<FeastType> getByRawType(String rawType) {
            return Arrays.stream(FeastType.values()).filter(it -> it.getRawType().equals(rawType)).findAny();
        }
    }

    private int ID;
    private int productionBoostPercentage;
    private int durationInSeconds;
    private int foodCost;
    private int rubyCost;
    private int minLevel;
    private FeastType type;
}
