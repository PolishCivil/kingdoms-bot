package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 1/17/2017.
 */
public enum RarityType {
    COMMON,
    RARE,
    EPIC,
    LEGENDARY,
    UNIQUE,
    UNKNOWN,;

    public static RarityType getForRaw(int rawType) {
        switch (rawType) {
            case 0:
                return UNIQUE;
            case 1:
                return COMMON;
            case 2:
                return RARE;
            case 3:
                return EPIC;
            case 4:
                return LEGENDARY;
            case 10:
                return UNIQUE;//hero
            case 11:
                return COMMON;//hero
            case 12:
                return RARE;//hero
            case 13:
                return EPIC;//hero
            case 14:
                return LEGENDARY;//hero
            default:
                return UNKNOWN;
        }
    }
}
