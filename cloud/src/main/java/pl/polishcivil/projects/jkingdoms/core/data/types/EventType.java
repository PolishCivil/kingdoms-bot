package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum EventType {
    SEASON(false, 0),
    NOMAD_INVASION(false, 0),
    ALCHEMIST(false, 0),
    ALLIANCE_TOURNEMENT(false, 0),
    ARMORER(false, 0),
    ARTIFACT(false, 0),
    BEGGING_KNIGHTS(false, 0),
    BOUNTY_HUNTER(false, 0),
    COLOSSUS(false, 0),
    ENCHANTER(false, 0),
    EQUIPMENT_MERCHANT(false, 0),
    ARCHITEKT(false, 0),
    FACTION(false, 0),
    MERCHANT(false, 0),
    PLAGUE(false, 0),
    RANDOM_DUNGEON(false, 0),
    RENEGADE(false, 0),
    RESSOURCE_EXCHANGER(false, 0),
    SHADOW_UNIT(false, 0),
    TOURNEMENT(false, 0),
    TREASURE(false, 0),
    UNIT_DEALER(false, 0),
    RESEARCHER(false, 0),
    NOMAD_HUNTER(false, 0),
    ALIANCE_PRIME_TIME(true, 0),
    PAYMENT_REWARD(true, 0),
    INDIVIDUAL_SPECIAL_OFFER(true, 0),
    NOBILITY_CONTEST(false, 0),
    SWORD_COAST(false, 0),
    WEAPONS_MERCHANT(false, 0),
    ARCHITEKT_OFFER(true, 0),
    WHEEL_OF_FORTUNE(false, 0),
    PREDICTION_GAME(false, 0),
    GLORY_FLAG(false, 0),
    UNDERWORLD(false, 0),
    ALIENINVASION(false, 0);

    private final boolean isRubySpecialOffer;
    private final int eventCastleId;

    EventType(boolean isRubySpecialOffer, int eventCastleId) {
        this.isRubySpecialOffer = isRubySpecialOffer;
        this.eventCastleId = eventCastleId;
    }

    public boolean isRubySpecialOffer() {
        return this.isRubySpecialOffer;
    }

    public boolean hasEventCastleId() {
        return this.eventCastleId != 0;
    }

    public int getEventCastleId() {
        return this.eventCastleId;
    }
}
