package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.packages;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.CollectableItem;
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by polish on 1/14/2017.
 */
@Data
public class PackageDefinition implements IDefinition {
    private int ID;
    private Kingdom[] validKingdoms;
    private PackageType type;
    private CollectableItem cost;
    private List<CollectableItem> rewards;
    private int stock;
    private boolean notRebuyable;

    public enum PackageType {
        GEM("gem"),
        ITEM("item"),
        SOLDIER("soldier"),
        DECORATION("deco"),
        RESOURCES("resources"),
        TOOL("tool"),
        REWARD_BAG("rewardBag"),
        PLAGUE_MONK("plagueMonk"),
        HERO("hero"),
        XP("xp"),
        VIP("VIP"),;

        private final String rawType;

        PackageType(String rawType) {
            this.rawType = rawType;
        }

        public String getRawType() {
            return rawType;
        }

        public static Optional<PackageType> getByRawType(String rawType) {
            return Arrays.stream(PackageType.values()).filter(it -> it.getRawType().equals(rawType)).findAny();
        }
    }

    public int getRemainingStock(int boughtCount) {
        if (stock > 0) {
            return Math.max(stock - boughtCount, 0);
        }
        if (notRebuyable) {
            return Math.max(1 - boughtCount, 0);
        }
        return 1000;
    }
}
