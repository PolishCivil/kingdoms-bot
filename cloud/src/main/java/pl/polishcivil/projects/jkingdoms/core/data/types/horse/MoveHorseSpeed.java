package pl.polishcivil.projects.jkingdoms.core.data.types.horse;

import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom;

import java.util.Arrays;

public enum MoveHorseSpeed {
    NO_HORSE(-1, 0, 0, 0, false, Kingdom.UNKNOWN.getKID(), HorseType.NO_HORSE),


    FIRST_HORSE(1001, 1, 10, 10, false, Kingdom.UNKNOWN.getKID(), HorseType.HORSE),
    SECOND_HORSE(1002, 1, 17, 17, true, Kingdom.UNKNOWN.getKID(), HorseType.BATTLE_HORSE),
    THIRD_HORSE(1003, 1, 27, 27, true, Kingdom.UNKNOWN.getKID(), HorseType.FAST_STEED),

    FIRST_HORSE_STABLE_2(1004, 2, 16, 16, false, Kingdom.UNKNOWN.getKID(), HorseType.HORSE),
    SECOND_HORSE_STABLE_2(1005, 2, 27, 27, true, Kingdom.UNKNOWN.getKID(), HorseType.BATTLE_HORSE),
    THIRD_HORSE_STABLE_2(1006, 2, 43, 43, true, Kingdom.UNKNOWN.getKID(), HorseType.FAST_STEED),

    FIRST_HORSE_STABLE_3(1007, 3, 22, 22, false, Kingdom.UNKNOWN.getKID(), HorseType.HORSE),
    SECOND_HORSE_STABLE_3(1008, 3, 37, 37, true, Kingdom.UNKNOWN.getKID(), HorseType.BATTLE_HORSE),
    THIRD_HORSE_STABLE_3(1009, 3, 59, 59, true, Kingdom.UNKNOWN.getKID(), HorseType.FAST_STEED),

    CODA_1(1030, 1, 12, 16, false, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.HORSE),
    CARAVEL_1(1031, 1, 20, 27, true, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.BATTLE_HORSE),
    GALLEON_1(1032, 1, 32, 43, true, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.FAST_STEED),

    CODA_2(1033, 2, 20, 27, false, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.HORSE),
    CARAVEL_2(1034, 2, 32, 43, true, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.BATTLE_HORSE),
    GALLEON_2(1035, 2, 54, 72, true, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.FAST_STEED),

    CODA_3(1036, 3, 26, 35, false, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.HORSE),
    CARAVEL_3(1037, 3, 44, 59, true, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.BATTLE_HORSE),
    GALLEON_3(1038, 3, 70, 94, true, Kingdom.STORM_ARCHIPELAGO.getKID(), HorseType.FAST_STEED),

    BERIMOND_FIRST_HORSE(1021, 1, 0, 0, false, Kingdom.BERIMOND.getKID(), HorseType.HORSE),
    BERIMOND_SECOND_HORSE(1022, 1, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.BATTLE_HORSE),
    BERIMOND_THIRD_HORSE(1023, 1, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.FAST_STEED),

    BERIMOND_FIRST_HORSE_2(1039, 2, 0, 0, false, Kingdom.BERIMOND.getKID(), HorseType.HORSE),
    BERIMOND_SECOND_HORSE_2(1040, 2, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.BATTLE_HORSE),
    BERIMOND_THIRD_HORSE_2(1041, 2, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.FAST_STEED),

    BERIMOND_FIRST_HORSE_3(1024, 3, 0, 0, false, Kingdom.BERIMOND.getKID(), HorseType.HORSE),
    BERIMOND_SECOND_HORSE_3(1025, 3, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.BATTLE_HORSE),
    BERIMOND_THIRD_HORSE_3(1026, 3, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.FAST_STEED),

    BERIMOND_FIRST_HORSE_4(1042, 4, 0, 0, false, Kingdom.BERIMOND.getKID(), HorseType.HORSE),
    BERIMOND_SECOND_HORSE_4(1043, 4, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.BATTLE_HORSE),
    BERIMOND_THIRD_HORSE_4(1044, 4, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.FAST_STEED),

    BERIMOND_FIRST_HORSE_5(1027, 5, 0, 0, false, Kingdom.BERIMOND.getKID(), HorseType.HORSE),
    BERIMOND_SECOND_HORSE_5(1028, 5, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.BATTLE_HORSE),
    BERIMOND_THIRD_HORSE_5(1029, 5, 0, 0, true, Kingdom.BERIMOND.getKID(), HorseType.FAST_STEED),;


    private final int speedOption;
    private final int requiredStableLevel;
    private final int speedUnitBonusInPercent;
    private final int speedMarketBonusInPercent;
    private final boolean ruby;
    private final HorseType horseType;
    private final int kid;

    MoveHorseSpeed(int speedOption, int requiredStableLevel, int speedUnitBonusInPercent, int speedMarketBonusInPercent, boolean ruby, int kid, HorseType horseType) {
        this.speedOption = speedOption;
        this.requiredStableLevel = requiredStableLevel;
        this.speedUnitBonusInPercent = speedUnitBonusInPercent;
        this.speedMarketBonusInPercent = speedMarketBonusInPercent;
        this.horseType = horseType;
        this.kid = kid;
        this.ruby = ruby;
    }

    public int getSpeedOption() {
        return this.speedOption;
    }

    public int getRequiredStableLevel() {
        return this.requiredStableLevel;
    }

    public int getSpeedUnitBonusInPercent() {
        return this.speedUnitBonusInPercent;
    }

    public int getSpeedMarketBonusInPercent() {
        return this.speedMarketBonusInPercent;
    }

    public HorseType getHorseType() {
        return this.horseType;
    }

    public int getKid() {
        return this.kid;
    }

    public static MoveHorseSpeed forID(int id) {
        return Arrays.stream(MoveHorseSpeed.values()).filter(it -> it.getSpeedOption() == id).findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Couldn't find horse with id %d", id)));
    }
}