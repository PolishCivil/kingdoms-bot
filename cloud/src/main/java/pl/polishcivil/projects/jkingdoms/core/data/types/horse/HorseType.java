package pl.polishcivil.projects.jkingdoms.core.data.types.horse;

public enum HorseType {
    NO_HORSE,
    HORSE,
    BATTLE_HORSE,
    FAST_STEED,
}