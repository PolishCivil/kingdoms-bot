package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.packages;

import org.apache.logging.log4j.LogManager;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.CollectableItem;
import pl.polishcivil.projects.jkingdoms.core.data.types.CollectableItemType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static pl.polishcivil.projects.jkingdoms.utils.StringUtils.capitalizeFirstLetter;

/**
 * Created by polish on 1/14/2017.
 */
public class PackageDefinitionParser implements ItemDefinitionParser<PackageDefinition> {

    public PackageDefinition parse(Node definitionNode) {
        NamedNodeMap attributes = definitionNode.getAttributes();

        return parseType(attributes).map(it -> {
            Optional<List<CollectableItem>> optRewards = parseRewards(it, attributes);
            Optional<CollectableItem> optPrice = parsePrice(attributes);
            Optional<Integer> optID = parseID(attributes);

            PackageDefinition definition = new PackageDefinition();
            definition.setType(it);

            if (!optRewards.isPresent()) {
                throw new RuntimeException(String.format("Couldn't parse rewards for type %s", it));
            } else {
                definition.setRewards(optRewards.get());
            }

            if (!optPrice.isPresent()) {
                throw new RuntimeException(String.format("Couldn't parse price for type %s", it));
            } else {
                definition.setCost(optPrice.get());
            }

            if (!optID.isPresent()) {
                throw new RuntimeException(String.format("Couldn't parse id for type %s", it));
            } else {
                definition.setID(optID.get());
            }
            return definition;
        }).orElseThrow(() -> new RuntimeException("Couldn't parse type"));
    }

    private Optional<List<CollectableItem>> parseRewards(PackageDefinition.PackageType type, NamedNodeMap attributes) {
        ArrayList<CollectableItem> rewards = new ArrayList<>();
        switch (type) {
            case GEM:
                break;
            case ITEM:
                break;
            case SOLDIER:
            case TOOL:
                int unitID = Optional.ofNullable(attributes.getNamedItem("unitID"))
                        .map(Node::getNodeValue)
                        .map(Integer::parseInt)
                        .orElse(-1);
                int unitAmount = Optional.ofNullable(attributes.getNamedItem("unitAmount"))
                        .map(Node::getNodeValue)
                        .map(Integer::parseInt)
                        .orElse(-1);

                if (unitID != -1 && unitAmount != -1) {
                    rewards.add(new CollectableItem(CollectableItemType.UNITS, unitID, unitAmount));
                }
                break;
            case DECORATION:
                break;
            case RESOURCES:
                int length = attributes.getLength();
                for (int i = 0; i < length; i++) {
                    Node item = attributes.item(i);
                    String name = item.getNodeName();

                    if (name.startsWith("amount")) {
                        int amount = Integer.parseInt(item.getNodeValue());
                        String resourceName = name.replace("amount", "");
                        switch (resourceName) {
                            case "C1":
                                rewards.add(new CollectableItem(CollectableItemType.C1, -1, amount));
                                break;
                            case "Wood":
                                rewards.add(new CollectableItem(CollectableItemType.WOOD, -1, amount));
                                break;
                            case "Food":
                                rewards.add(new CollectableItem(CollectableItemType.FOOD, -1, amount));
                                break;
                            case "Stone":
                                rewards.add(new CollectableItem(CollectableItemType.STONE, -1, amount));
                                break;
                            default:
                                LogManager.getLogger().warn("Unknown reward resource {}", resourceName);
                        }
                    }
                }

                break;
            case REWARD_BAG:
                break;
            case PLAGUE_MONK:
                break;
            case HERO:
                break;
            case XP:
                break;
            case VIP:
                break;
        }

        if (rewards.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(rewards);
    }

    private Optional<CollectableItem> parsePrice(NamedNodeMap attributes) {
        for (CollectableItemType type : CollectableItem.GROUP_SET_GOODS) {
            int amount = Optional.ofNullable(attributes.getNamedItem(String.format("packagePrice%s", capitalizeFirstLetter(type.getName()))))
                    .map(Node::getNodeValue)
                    .map(Integer::parseInt)
                    .orElse(0);

            if (amount > 0) {
                return Optional.of(new CollectableItem(type, -1, amount));
            }
        }
        return Optional.empty();
    }

    private Optional<PackageDefinition.PackageType> parseType(NamedNodeMap attributes) {
        return PackageDefinition.PackageType.getByRawType(attributes.getNamedItem("packageType").getNodeValue());
    }

    private Optional<Integer> parseID(NamedNodeMap attributes) {
        return Optional.ofNullable(attributes.getNamedItem("packageID"))
                .map(Node::getNodeValue)
                .map(Integer::parseInt);
    }
}
