package pl.polishcivil.projects.jkingdoms.core.cloud;

import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileLocation;

import java.io.File;

/**
 * Created by polish on 12/5/16.
 */
public abstract class AbstractEmpireFile {
    private final File localFile;
    private final EmpireFileLocation remoteFileLocation;
    protected final EmpireCloud cloud;

    protected AbstractEmpireFile(EmpireCloud cloud, File localFile, EmpireFileLocation remoteFileLocation) {
        this.cloud = cloud;
        this.localFile = localFile;
        this.remoteFileLocation = remoteFileLocation;
    }

    /**
     * Updates the specific file
     *
     * @throws EmpireCloudDataUpdateException
     */
    public abstract void update() throws EmpireCloudDataUpdateException;

    /**
     * Loads the data from the local file
     */
    public abstract void load() throws Exception;


    public File getLocalFile() {
        return localFile;
    }

    public EmpireFileLocation getRemoteFileLocation() {
        return remoteFileLocation;
    }
}
