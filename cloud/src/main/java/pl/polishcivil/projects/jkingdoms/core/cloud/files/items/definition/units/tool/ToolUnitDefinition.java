package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;

/**
 * Created by polish on 12/5/16.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public class ToolUnitDefinition extends UnitDefinition {
    private ToolUnitType type;
}
