package pl.polishcivil.projects.jkingdoms.core.data.merchants;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.PackagesDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.packages.PackageDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.PackageEventSourceType;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 1/15/2017.
 */
@Data
public class Merchant {
    private final int ID;
    private final String displayName;
    private final PackageEventSourceType packageSource;


    public List<PackageDefinition> getAvailablePackages(EmpireCloud cloud) {
        PackagesDB packagesDB = cloud.getItemsFile().getPackages();
        if (packageSource == PackageEventSourceType.EVENT) {
            return cloud.getItemsFile().getEvents()._findByID(ID)//getting event definition
                    .map(it -> packagesDB.filter(it.getPackageIDS()).stream()
                            .collect(Collectors.toList())).orElse(Collections.emptyList());

        }
        return Collections.emptyList();
    }
}
