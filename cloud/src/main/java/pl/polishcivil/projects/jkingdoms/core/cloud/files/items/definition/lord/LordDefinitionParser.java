package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.lord;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;

/**
 * Created by polish on 3/3/2017.
 */
public class LordDefinitionParser implements ItemDefinitionParser<LordDefinition> {
    @Override
    public LordDefinition parse(Node node) throws Exception {
        int ID = getAttribute(node, "lordID").map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse lord definition, missing id"));
        LordDefinition lordDefinition = new LordDefinition();
        lordDefinition.setID(ID);
        return lordDefinition;
    }
}
