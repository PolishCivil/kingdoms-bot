package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.LocationType;

import java.util.List;

/**
 * Created by polish on 3/17/2017.
 */
@Data
public class EquipmentGemEffectDefinition implements IDefinition {
    private final int ID;
    private final List<LocationType> locationTypes;
    private final int effectTypeID;
    private final String name;
}
