package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.types.LordType;

import static java.lang.String.format;

/**
 * Created by polish on 1/19/17.
 */
public class EquipmentEffectParser implements ItemDefinitionParser<EquipmentEffectDefinition> {
    @Override
    public EquipmentEffectDefinition parse(Node node) throws Exception {
        int id = getAttribute(node, "equipmentEffectID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't get id"));
        int effectID = getAttribute(node, "effectID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException(format("Couldn't get effect id for id %d", id)));
        int bonus = getAttribute(node, "bonus")
                .map(Integer::parseInt)
                .orElse(0);
        boolean ignoreCap = getAttribute(node, "ignoreCap")
                .map(Integer::parseInt)
                .map(v -> v == 1)
                .orElse(false);
        double enchantmentPrimaryBonus = getAttribute(node, "enchantmentPrimaryBonus")
                .map(Double::parseDouble)
                .orElse(0.0);
        double enchantmentSecondaryBonus = getAttribute(node, "enchantmentSecondaryBonus")
                .map(Double::parseDouble)
                .orElse(0.0);
        LordType wearer = getAttribute(node, "wearerID")
                .map(Integer::parseInt)
                .map(LordType::getForRaw)
                .orElseThrow(() -> new RuntimeException(format("Couldn't get wearer for id %d", id)));
        EquipmentEffectDefinition definition = new EquipmentEffectDefinition();
        definition.setID(id);
        definition.setEffectID(effectID);
        definition.setWearer(wearer);
        definition.setEnchantmentPrimaryBonus(enchantmentPrimaryBonus);
        definition.setEnchantmentSecondaryBonus(enchantmentSecondaryBonus);
        definition.setIgnoreCap(ignoreCap);
        definition.setBonus(bonus);
        return definition;
    }
}
