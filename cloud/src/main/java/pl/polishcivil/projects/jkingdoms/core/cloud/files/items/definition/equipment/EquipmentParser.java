package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.EffectsParserUtils;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;
import pl.polishcivil.projects.jkingdoms.core.data.types.LordType;

import java.util.List;

/**
 * Created by polish on 3/3/2017.
 */
public class EquipmentParser implements ItemDefinitionParser<EquipmentDefinition> {
    @Override
    public EquipmentDefinition parse(Node node) throws Exception {
        int ID = getAttribute(node, "equipmentID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't get ID"));
        int slotID = getAttribute(node, "slotID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't get slotID"));
        int setID = getAttribute(node, "setID")
                .map(Integer::parseInt)
                .orElse(-1);
        int rarity = getAttribute(node, "rarenessID")
                .map(Integer::parseInt)
                .orElse(0);
        LordType wearer = getAttribute(node, "wearerID")
                .map(Integer::parseInt)
                .map(LordType::getForRaw)
                .orElseThrow(() -> new RuntimeException("Couldn't get wearerID"));
        List<StaticEffectData> effectsData = EffectsParserUtils.parse(getAttributeDefault(node, "effects", ""));
        EquipmentDefinition definition = new EquipmentDefinition();
        definition.setID(ID);
        definition.setRarity(rarity);
        definition.setBonuses(effectsData);
        definition.setSetID(setID);
        definition.setCategory(slotID);
        definition.setWearer(wearer);
        return definition;
    }
}
