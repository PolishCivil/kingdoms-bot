package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 11/26/2016.
 */

public enum ResourceType {
    WOOD(true, true, null, "W"),
    STONE(true, true, null, "S"),
    FOOD(true, true, null, "F"),
    IRON(true, true, Kingdom.GREAT_EMPIRE, "I"),
    CHARCOAL(true, true, Kingdom.ETERNAL_ICE_LAND, "C"),
    OIL(true, true, Kingdom.BURNING_SANDS, "O"),
    GLASS(true, true, Kingdom.FIERY_HEIGTHS, "G"),
    AQUAMARINE(false, true, Kingdom.STORM_ARCHIPELAGO, "A"),
    GOLD(false, false, null, "C1"),
    RUBY(false, false, null, "C2");

    private boolean canSend;
    private boolean storedInCastle;
    private Kingdom kingdom;
    private String rawValue;

    ResourceType(boolean canSend, boolean storedInCastle, Kingdom kingdom, String rawValue) {
        this.canSend = canSend;
        this.storedInCastle = storedInCastle;
        this.kingdom = kingdom;
        this.rawValue = rawValue;
    }

    public boolean isStoredInCastle() {
        return this.storedInCastle;
    }

    public void setStoredInCastle(boolean storedInCastle) {
        this.storedInCastle = storedInCastle;
    }

    public boolean canSend() {
        return this.canSend;
    }

    public Kingdom getKingdom() {
        return this.kingdom;
    }

    public String getRawValue() {
        return this.rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public static ResourceType getByRawValue(String value) {
        ResourceType[] var4;
        int var3 = (var4 = values()).length;

        for (int var2 = 0; var2 < var3; ++var2) {
            ResourceType item = var4[var2];
            if (item.rawValue.equalsIgnoreCase(value)) {
                return item;
            }
        }

        return null;
    }
}
