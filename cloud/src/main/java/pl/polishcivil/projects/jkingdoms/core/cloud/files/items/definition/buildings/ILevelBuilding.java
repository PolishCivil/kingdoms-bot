package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings;

/**
 * Created by polish on 1/17/2017.
 */
public interface ILevelBuilding {
    int getLevel();

    void setLevel(int level);
}
