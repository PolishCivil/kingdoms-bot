package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.title;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.EffectsParserUtils;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.title.TitleDefinition.TitleType;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;

import java.util.List;

/**
 * Created by polish on 3/17/2017.
 */
public class TitleDefinitionParser implements ItemDefinitionParser<TitleDefinition> {

    @Override
    public TitleDefinition parse(Node node) throws Exception {
        int titleID = Integer.parseInt(getAttributeDefault(node, "titleID", "-1"));
        TitleType type = TitleType.valueOf(getAttributeDefault(node, "type", "UNKNOWN"));
        List<StaticEffectData> effects = EffectsParserUtils.parse(getAttributeDefault(node, "effects", ""));
        return new TitleDefinition(titleID, type, effects);
    }
}
