package pl.polishcivil.projects.jkingdoms.core;

/**
 * Created by polish on 12/5/16.
 */
public enum EmpireLanguageTag {
    PL("pl"), EN("en"), NL("nl");

    private String tag;

    EmpireLanguageTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
