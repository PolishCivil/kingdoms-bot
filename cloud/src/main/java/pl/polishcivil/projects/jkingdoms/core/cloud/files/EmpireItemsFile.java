package pl.polishcivil.projects.jkingdoms.core.cloud.files;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.*;
import pl.polishcivil.projects.jkingdoms.core.BotConstants;
import pl.polishcivil.projects.jkingdoms.core.cloud.AbstractEmpireFile;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloudDataUpdateException;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemGroupType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.*;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect.EffectTypesDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect.EffectsDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect.EquipmentEffectsDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect.GemEffectsDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.equipment.EquipmentDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.equipment.EquipmentSetDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.gem.GemsDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.gem.GemsLevelDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.paragon.ParagonSkillDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.title.TitlesDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings.BuildingDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect.EffectParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect.EffectTypeParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect.EquipmentEffectParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect.EquipmentGemEffectDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment.EquipmentParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment.EquipmentSetParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.events.EventDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.feasts.FeastDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem.GemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem.GemLevelParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.isles.IsleDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.lord.LordDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.packages.PackageDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.paragon.ParagonSkillDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.title.TitleDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitRecruitType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitRoleType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.soldier.SoldierUnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitType;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileLocation;
import pl.polishcivil.projects.jkingdoms.utils.XMLUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by polish on 12/5/16.
 */
@Data
public class EmpireItemsFile extends AbstractEmpireFile {
    private double version;
    private String date;

    private final UnitsDB units = new UnitsDB();
    private final FeastsDB feastDefinitions = new FeastsDB();
    private final IslesDB isles = new IslesDB();
    private final PackagesDB packages = new PackagesDB();
    private final EventsDB events = new EventsDB();
    private final BuildingsDB buildingDefinitions = new BuildingsDB();
    private final GemsDB gems = new GemsDB();
    private final GemsLevelDB gemsLevel = new GemsLevelDB();
    private final EffectsDB effects = new EffectsDB();
    private final EffectTypesDB effectTypes = new EffectTypesDB();
    private final EquipmentEffectsDB equipmentEffects = new EquipmentEffectsDB();
    private final EquipmentSetDB equipmentSets = new EquipmentSetDB();
    private final EquipmentDB equipments = new EquipmentDB();
    private final LordsDB lords = new LordsDB();
    private final ParagonSkillDB paragonSkills = new ParagonSkillDB();
    private final GemEffectsDB gemEffects = new GemEffectsDB();
    private final TitlesDB titles = new TitlesDB();

    @Override
    public String toString() {
        return String.format("%d units, " +
                        "%d buildings, " +
                        "%d isles, " +
                        "%d feasts, " +
                        "%d gems, " +
                        "%d effect types, " +
                        "%d effects, " +
                        "%d equipment effects ",
                units.size(),
                buildingDefinitions.size(),
                isles.size(),
                feastDefinitions.size(),
                gems.size(),
                effectTypes.size(),
                effects.size(),
                equipmentEffects.size()
        );
    }

    public EmpireItemsFile(EmpireCloud cloud, EmpireFileLocation remoteFileLocation) {
        super(cloud, new File(BotConstants.GAME_CLOUD_DIRECTORY, "items"), remoteFileLocation);
    }

    @Override
    public void update() throws EmpireCloudDataUpdateException {
        int version = cloud.getVersionFile().getVersion();
        LogManager.getLogger().info("Updating items file for version {}", version);
        cloud.updateGameFile(getLocalFile(), getRemoteFileLocation(), true, version);
    }

    @Override
    public void load() throws Exception {
        LogManager.getLogger().info("Loading items file for version {}", cloud.getVersionFile().getVersion());
        Document document = XMLUtils.getBuilder().parse(getLocalFile());

        Element root = document.getDocumentElement();
        loadUnits(root);
        loadFeasts(root);

        gems.addAll(parse(root, "gems", "gem", new GemDefinitionParser()));
        buildingDefinitions.addAll(parse(root, "buildings", "building", new BuildingDefinitionParser()));
        isles.addAll(parse(root, "isles", "isle", new IsleDefinitionParser()));
        events.addAll(parse(root, "events", "event", new EventDefinitionParser()));
        packages.addAll(parse(root, "packages", "package", new PackageDefinitionParser()));
        effectTypes.addAll(parse(root, "effecttypes", "effecttype", new EffectTypeParser()));
        effects.addAll(parse(root, "effects", "effect", new EffectParser()));
        equipmentEffects.addAll(parse(root, "equipment_effects", "equipment_effect", new EquipmentEffectParser()));
        gemsLevel.addAll(parse(root, "gemlevels", "gemlevel", new GemLevelParser()));
        equipmentSets.addAll(parse(root, "equipment_sets", "equipment_set", new EquipmentSetParser()));
        equipments.addAll(parse(root, "equipments", "equipment", new EquipmentParser()));
        lords.addAll(parse(root, "lords", "lord", new LordDefinitionParser()));
        paragonSkills.addAll(parse(root, "legendskills", "legendskill", new ParagonSkillDefinitionParser()));
        try {
            gemEffects.addAll(parse(root, "equipment_gem_effects", "equipment_gem_effect", new EquipmentGemEffectDefinitionParser()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        titles.addAll(parse(root, "titles", "title", new TitleDefinitionParser()));

        LogManager.getLogger().debug("Loaded items file {}", this);
    }


    private <T> List<T> parse(Element root, String listName, String elementName, ItemDefinitionParser<T> parser) {
        NodeList nodeList = ((Element) root.getElementsByTagName(listName).item(0))
                .getElementsByTagName(elementName);
        return IntStream.range(0, nodeList.getLength())
                .mapToObj(nodeList::item)
                .flatMap(v -> {
                    try {
                        return Stream.of(parser.parse(v));
                    } catch (Exception e) {
                        LogManager.getLogger().error(e);
                        return Stream.empty();
                    }
                }).collect(Collectors.toList());
    }

    private void loadUnits(Element root) {
        NodeList unitsNodeList = ((Element) root.getElementsByTagName("units").item(0)).getElementsByTagName("unit");
        int unitsSize = unitsNodeList.getLength();
        for (int i = 0; i < unitsSize; i++) {
            Node unitNode = unitsNodeList.item(i);
            UnitDefinition unitDefinition = readUnitDefinition(unitNode);
            if (unitDefinition != null) {
                units.add(unitDefinition);
            }
        }
    }

    @Nullable
    private UnitDefinition readUnitDefinition(Node unitNode) {
        NamedNodeMap attributes = unitNode.getAttributes();
        ItemGroupType groupType = getGroupType(attributes);
        String rawRecruitType = getItem(attributes, "name");
        UnitRecruitType unitRecruitType = UnitRecruitType.forRaw(rawRecruitType)
                .orElseThrow(() -> new RuntimeException(String.format("Couldn't find recruit type for %s", rawRecruitType)));

        UnitDefinition definition = null;
        switch (unitRecruitType) {
            case SHADOW_UNIT:
            case EVENT_UNIT:
            case BARRACKS:
            case QUICK_ATTACK:
            case KINGDOM_UNIT:
            case KEEP:
                SoldierUnitDefinition soldierDefinition = (SoldierUnitDefinition) (definition = new SoldierUnitDefinition());
                //grabbing raws
                String rawRole = getItem(attributes, "role");

                //mapping raws
                UnitRoleType role = UnitRoleType.forRaw(rawRole)
                        .orElseThrow(() -> new RuntimeException(String.format("Could't find role %s", rawRole)));
                int meleeDefence = Integer.parseInt(getItemDefault(attributes, "meleeDefence", "0"));
                int meleeAttack = Integer.parseInt(getItemDefault(attributes, "meleeAttack", "0"));
                int rangeDefence = Integer.parseInt(getItemDefault(attributes, "rangeDefence", "0"));
                int rangeAttack = Integer.parseInt(getItemDefault(attributes, "rangeAttack", "0"));
                int speed = Integer.parseInt(getItemDefault(attributes, "speed", "0"));
                double fameAsDef = Double.parseDouble(getItemDefault(attributes, "fameAsDef", "0"));
                double fameAsOff = Double.parseDouble(getItemDefault(attributes, "fameAsOff", "0"));
                double healGold = Double.parseDouble(getItemDefault(attributes, "healingCostC1", "0"));
                double healRuby = Double.parseDouble(getItemDefault(attributes, "healingCostC2", "0"));
                boolean hybrid = Boolean.parseBoolean(getItemDefault(attributes, "hybrid", "false"));

                //setting
                soldierDefinition.setRole(role);
                soldierDefinition.setMeleeDefence(meleeDefence);
                soldierDefinition.setMeleeAttack(meleeAttack);
                soldierDefinition.setRangeDefence(rangeDefence);
                soldierDefinition.setRangeAttack(rangeAttack);
                soldierDefinition.setSpeed(speed);
                soldierDefinition.setFameAsDef(fameAsDef);
                soldierDefinition.setFameAsOff(fameAsOff);
                soldierDefinition.setHybrid(hybrid);
                soldierDefinition.setHealCostGold(healGold);
                soldierDefinition.setHealCostRuby(healRuby);
                break;
            case WORKSHOP:
            case D_WORKSHOP:
            case ELITE_TOOL:
            case SHADOW_TOOL:
            case EVENT_TOOL:
                ToolUnitDefinition toolUnitDefinition = (ToolUnitDefinition) (definition = new ToolUnitDefinition());
                //grabbing raws
                String rawToolType = getItem(attributes, "typ");
                //mapping raws
                ToolUnitType toolType = ToolUnitType.forRaw(rawToolType).orElseThrow(() -> new RuntimeException(String.format("Could't find type %s", rawToolType)));
                //setting
                toolUnitDefinition.setType(toolType);
                break;
            case UNKNOWN:
            case Unknowntool:
                return null;
        }
        String typeName = getItem(attributes, "type");
        int woodCost = Integer.parseInt(getItemDefault(attributes, "costWood", "0"));
        int stoneCost = Integer.parseInt(getItemDefault(attributes, "costStone", "0"));
        int goldCost = Integer.parseInt(getItemDefault(attributes, "costC1", "0"));
        int rubyCost = Integer.parseInt(getItemDefault(attributes, "costC2", "0"));
        int[] cleavageOfCellsCost = Arrays.stream(getItemDefault(attributes, "cleavageOfCellsCost", "")
                .split(","))
                .filter(it -> !it.isEmpty())
                .mapToInt(Integer::parseInt)
                .toArray();

        definition.setGoldCost(goldCost);
        definition.setRubyCost(rubyCost);
        definition.setWoodCost(woodCost);
        definition.setStoneCost(stoneCost);
        definition.setID(Integer.parseInt(getItem(attributes, "wodID")));
        definition.setGroup(groupType);
        definition.setTypeName(typeName);
        definition.setRecruit(unitRecruitType);
        definition.setCleavageOfCellsCost(cleavageOfCellsCost);
        return definition;
    }

    private void loadFeasts(Element root) {
        NodeList feastsNodeList = ((Element) root.getElementsByTagName("feasts").item(0)).getElementsByTagName("feast");
        int feastsSize = feastsNodeList.getLength();
        for (int i = 0; i < feastsSize; i++) {
            Node feastNode = feastsNodeList.item(i);
            FeastDefinition definition = readFeastDefinition(feastNode);
            feastDefinitions.add(definition);
        }
    }

    @NotNull
    private FeastDefinition readFeastDefinition(Node feastNode) {
        NamedNodeMap attributes = feastNode.getAttributes();
        int feastID = Integer.parseInt(getItemDefault(attributes, "feastID", "0"));
        int productionBoost = Integer.parseInt(getItemDefault(attributes, "productionBoost", "0"));
        int duration = Integer.parseInt(getItemDefault(attributes, "duration", "0"));
        int costFood = Integer.parseInt(getItemDefault(attributes, "costFood", "0"));
        int costRuby = Integer.parseInt(getItemDefault(attributes, "costC2", "0"));
        int minLevel = Integer.parseInt(getItemDefault(attributes, "minLevel", "0"));
        String type = getItemDefault(attributes, "type", "small");
        FeastDefinition definition = new FeastDefinition();
        definition.setID(feastID);
        definition.setProductionBoostPercentage(productionBoost);
        definition.setDurationInSeconds(duration);
        definition.setFoodCost(costFood);
        definition.setRubyCost(costRuby);
        definition.setMinLevel(minLevel);
        FeastDefinition.FeastType feastType = FeastDefinition.FeastType.getByRawType(type)
                .orElseThrow(() -> new RuntimeException(String.format("Unknown feast type %s", type)));
        definition.setType(feastType);
        return definition;
    }


    private String getItem(NamedNodeMap attributes, String param) {
        Node namedItem = attributes.getNamedItem(param);
        if (namedItem == null) {
            throw new RuntimeException("Couldn't find item " + param);
        }
        return namedItem.getNodeValue();
    }

    private String getItemDefault(NamedNodeMap attributes, String param, String _default) {
        Node namedItem = attributes.getNamedItem(param);
        if (namedItem == null) {
            return _default;
        }
        return namedItem.getNodeValue();
    }

    private ItemGroupType getGroupType(NamedNodeMap attributes) {
        String rawGroupType = getItem(attributes, "group");
        return ItemGroupType.forRaw(rawGroupType)
                .orElseThrow(() -> new RuntimeException("Couldn't find group type for " + rawGroupType));
    }
}
