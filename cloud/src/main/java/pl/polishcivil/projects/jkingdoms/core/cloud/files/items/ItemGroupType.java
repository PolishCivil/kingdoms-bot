package pl.polishcivil.projects.jkingdoms.core.cloud.files.items;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by polish on 12/5/16.
 */
public enum ItemGroupType {
    MAP_OBJECT("Mapobject"),
    UNIT("Unit"),
    BUILDING("Building"),
    GATE("Gate"),
    TOWER("Tower"),
    MOAT("Moat"),
    DEFENCE("Defence"),
    GROUND("Ground"),
    FIXED_POSITION_BUILDING("FixedPositionBuilding");

    private final String raw;

    ItemGroupType(final String raw) {
        this.raw = raw;
    }

    public static Optional<ItemGroupType> forRaw(String rawType) {
        return Arrays.stream(ItemGroupType.values()).filter(it -> it.raw.equals(rawType)).findFirst();
    }
}
