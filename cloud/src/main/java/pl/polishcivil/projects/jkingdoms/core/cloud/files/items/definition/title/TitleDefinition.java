package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.title;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;

import java.util.List;

/**
 * Created by polish on 3/17/2017.
 */
@Data
public class TitleDefinition implements IDefinition {
    public enum TitleType {
        UNKNOWN, FAME, ISLE, FACTION
    }

    private final int ID;
    private final TitleType type;
    private final List<StaticEffectData> effects;
}
