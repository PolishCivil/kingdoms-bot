package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect.EffectDefinition;

/**
 * Created by polish on 1/19/17.
 */
public class EffectsDB extends AbstractDefinitionDB<EffectDefinition> {
}
