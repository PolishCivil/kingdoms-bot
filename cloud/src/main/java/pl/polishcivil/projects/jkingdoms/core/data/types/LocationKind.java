package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum LocationKind {
    NPC,
    CONQUERED,
    PLAYER_ETERNAL,
    OTHER
}