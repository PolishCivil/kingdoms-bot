package pl.polishcivil.projects.jkingdoms.core.cloud.storage;

/**
 * Created by polish on 12/5/16.
 */
public class EmpireFileStorage {
    private EmpireFileLocation[] files;

    public EmpireFileStorage() {
    }

    public EmpireFileStorage(final EmpireFileLocation... files) {
        this();
        this.files = files;
    }

    public EmpireFileLocation[] getFiles() {
        return this.files;
    }

    public void setFiles(final EmpireFileLocation[] files) {
        this.files = files;
    }


    public EmpireFileLocation getFileByType(final EmpireFileType fileType) {
        EmpireFileLocation[] files;
        for (int length = (files = this.files).length, i = 0; i < length; ++i) {
            final EmpireFileLocation gfl = files[i];
            if (gfl.getFileType() == fileType) {
                return gfl;
            }
        }
        return null;
    }
}
