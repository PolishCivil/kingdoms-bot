package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.soldier;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitRoleType;

/**
 * Created by polish on 12/5/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SoldierUnitDefinition extends UnitDefinition {
    private UnitRoleType role;
    private int rangeDefence;
    private int meleeDefence;
    private int meleeAttack;
    private int rangeAttack;
    private int speed;
    private double fameAsDef;
    private double fameAsOff;
    private boolean hybrid;

    public boolean isDefenceUnit() {
        return fameAsDef >= fameAsOff;
    }


    public boolean isOffensiveUnit() {
        return fameAsOff >= fameAsDef;
    }

    public boolean isHybrid() {
        if (hybrid) {
            return true;
        }
        return isDefenceUnit() && isOffensiveUnit();
    }
}
