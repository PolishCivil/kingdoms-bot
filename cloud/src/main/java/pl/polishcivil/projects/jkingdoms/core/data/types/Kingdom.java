package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum Kingdom {
    GREAT_EMPIRE(0, null),
    ETERNAL_ICE_LAND(2, null),
    BURNING_SANDS(1, null),
    FIERY_HEIGTHS(3, null),
    STORM_ARCHIPELAGO(4, null),
    BERIMOND(10, null),
    UNKNOWN(-1, null),
    UNDERGROUND(23, EventType.UNDERWORLD),
    THORNY_LAND(21, EventType.SEASON),
    SWORD_COAST(22, EventType.SWORD_COAST);

    final int KID;
    final EventType eventType;

    Kingdom(int KID, EventType eventType) {
        this.KID = KID;
        this.eventType = eventType;
    }

    public int getKID() {
        return this.KID;
    }

    public static Kingdom getByEventType(EventType et) {
        if (et != null) {
            Kingdom[] var4;
            int var3 = (var4 = values()).length;

            for (int var2 = 0; var2 < var3; ++var2) {
                Kingdom k = var4[var2];
                if (k.eventType == et) {
                    return k;
                }
            }
        }

        return UNKNOWN;
    }

    public static Kingdom getByRawType(int KID) {
        Kingdom[] var4;
        int var3 = (var4 = values()).length;

        for (int var2 = 0; var2 < var3; ++var2) {
            Kingdom k = var4[var2];
            if (k.KID == KID) {
                return k;
            }
        }

        return UNKNOWN;
    }

    public static boolean hasValue(int KID) {
        Kingdom[] var4;
        int var3 = (var4 = values()).length;

        for (int var2 = 0; var2 < var3; ++var2) {
            Kingdom k = var4[var2];
            if (k.KID == KID) {
                return true;
            }
        }

        return false;
    }

    public EventType getEventType() {
        return this.eventType;
    }
}
