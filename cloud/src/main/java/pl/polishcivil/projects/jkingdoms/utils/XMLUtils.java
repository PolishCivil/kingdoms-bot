package pl.polishcivil.projects.jkingdoms.utils;

import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

/**
 * Created by polish on 9/22/16.
 */
public class XMLUtils {
    private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();
    private static final Transformer transformer;
    private static final DocumentBuilder builder;

    static {
        try {
            transformer = transformerFactory.newTransformer();
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Converts document to string
     *
     * @param document - the document
     * @return text of the xml document
     */
    public static String documentToString(Document document) {
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(document), new StreamResult(writer));
        } catch (TransformerException e) {
            LogManager.getLogger().catching(e);
        }
        return writer.getBuffer().toString().replaceAll("\n|\r", "");
    }

    public static Transformer getTransformer() {
        return transformer;
    }

    public static TransformerFactory getTransformerFactory() {
        return transformerFactory;
    }

    public static DocumentBuilder getBuilder() {
        return builder;
    }

    public static Document newDocument() {
        return builder.newDocument();
    }
}
