package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.title;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.title.TitleDefinition;

/**
 * Created by polish on 3/17/2017.
 */
public class TitlesDB extends AbstractDefinitionDB<TitleDefinition> {
}
