package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.buildings;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemGroupType;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

/**
 * Created by polish on 1/17/2017.
 */
public interface ISimpleBuilding extends IDefinition {
    String getType();

    String getName();

    ItemGroupType getGroup();

    void setType(String type);

    void setName(String name);

    void setID(int id);

    void setGroup(ItemGroupType group);
}
