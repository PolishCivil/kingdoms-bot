package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.paragon;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.paragon.ParagonSkillDefinition.ParagonSkillTreeEnum;

import static pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.paragon.ParagonSkillDefinition.ParagonSkillType;

/**
 * Created by polish on 3/16/2017.
 */
public class ParagonSkillDefinitionParser implements ItemDefinitionParser<ParagonSkillDefinition> {

    @Override
    public ParagonSkillDefinition parse(Node node) throws Exception {
        int legendSkillID = Integer.parseInt(getAttributeDefault(node, "legendSkillID", "0"));
        int requiredSkillID = Integer.parseInt(getAttributeDefault(node, "requiredLegendSkillID", "0"));
        int followingSkillID = Integer.parseInt(getAttributeDefault(node, "followingLegendSkillID", "0"));
        int level = Integer.parseInt(getAttributeDefault(node, "level", "0"));
        int tier = Integer.parseInt(getAttributeDefault(node, "tier", "0"));
        int group = Integer.parseInt(getAttributeDefault(node, "costSkillPoints", "0"));
        int costSkillPoints = Integer.parseInt(getAttributeDefault(node, "skillGroupID", "0"));
        String _effectType = getAttributeDefault(node, "effectType", "-");
        String effectTypeName = _effectType.substring(0, 1).toLowerCase() + _effectType.substring(1);
        ParagonSkillTreeEnum skillTree = ParagonSkillTreeEnum.getByRawID(Integer.parseInt(getAttributeDefault(node, "skillTreeID", "0")));
        ParagonSkillType skillType = ParagonSkillType.getByRawValue(getAttributeDefault(node, "specialType", "ordinary"));
        double effectValue = Double.parseDouble(getAttributeDefault(node, "effectValue", "0"));
        double totalEffectValue = Double.parseDouble(getAttributeDefault(node, "totalEffectValue", "0"));
        ParagonSkillDefinition definition = new ParagonSkillDefinition(legendSkillID);
        definition.setRequiredSkillID(requiredSkillID);
        definition.setFollowingSkillID(followingSkillID);
        definition.setLevel(level);
        definition.setTier(tier);
        definition.setGroupID(group);
        definition.setCostSkillPoints(costSkillPoints);
        definition.setEffectTypeName(effectTypeName);
        definition.setSkillTree(skillTree);
        definition.setSkillType(skillType);
        definition.setEffectValue(effectValue);
        definition.setTotalEffectValue(totalEffectValue);
        return definition;
    }
}
