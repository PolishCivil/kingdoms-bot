package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;

/**
 * Created by polish on 3/3/2017.
 */
public class EquipmentSetParser implements ItemDefinitionParser<EquipmentSetDefinition> {
    @Override
    public EquipmentSetDefinition parse(Node node) throws Exception {
        int ID = getAttribute(node, "ID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't get ID"));
        int setID = getAttribute(node, "setID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't get setID"));
        int neededItems = getAttribute(node, "neededItems")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't get neededItems"));
        String effects = getAttribute(node, "effects")
                .map(v -> v.replace("&amp", "&"))
                .orElseThrow(() -> new RuntimeException("Couldn't get effects"));
        EquipmentSetDefinition definition = new EquipmentSetDefinition();
        definition.setID(ID);
        definition.setSetID(setID);
        definition.setNeededItems(neededItems);
        String[] split = effects.split("&");
        StaticEffectData effectData = new StaticEffectData(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
        definition.setEffect(effectData);
        return definition;
    }
}
