package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum TaxBonusLevel {
    NO_BONUS(0, 0),
    LEVEL_1(1, 4),
    LEVEL_2(2, 8),
    LEVEL_3(3, 12),
    LEVEL_4(4, 16),
    LEVEL_5(5, 20);

    private final int level;
    private final int bonusInPercent;

    TaxBonusLevel(final int level, final int bonusInPercent) {
        this.level = level;
        this.bonusInPercent = bonusInPercent;
    }

    public int getLevel() {
        return this.level;
    }

    public int getBonusInPercent() {
        return this.bonusInPercent;
    }

    public static TaxBonusLevel getByRawValue(final int value) {
        TaxBonusLevel[] values;
        for (int length = (values = values()).length, i = 0; i < length; ++i) {
            final TaxBonusLevel tbl = values[i];
            if (tbl.getLevel() == value) {
                return tbl;
            }
        }
        return null;
    }
}
