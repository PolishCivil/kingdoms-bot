package pl.polishcivil.projects.jkingdoms.core.cloud.files.items;

import org.w3c.dom.Node;

import java.util.Optional;

/**
 * Created by polish on 1/19/17.
 */
public interface ItemDefinitionParser<T> {

    T parse(Node node) throws Exception;


    default Optional<String> getAttribute(Node node, String name) {
        return Optional.ofNullable(node.getAttributes().getNamedItem(name))
                .map(Node::getNodeValue);
    }

    default String getAttributeDefault(Node node, String name, String _default) {
        return Optional.ofNullable(node.getAttributes().getNamedItem(name))
                .map(Node::getNodeValue)
                .orElse(_default);
    }
}
