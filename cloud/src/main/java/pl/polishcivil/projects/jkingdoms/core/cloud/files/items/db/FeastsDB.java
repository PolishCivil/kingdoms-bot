package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.feasts.FeastDefinition;

/**
 * Created by polish on 1/19/17.
 */
public class FeastsDB extends AbstractDefinitionDB<FeastDefinition> {
}
