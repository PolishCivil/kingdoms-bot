package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.lord;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

/**
 * Created by polish on 3/3/2017.
 */
@Data
public class LordDefinition implements IDefinition {
    private int ID;
}
