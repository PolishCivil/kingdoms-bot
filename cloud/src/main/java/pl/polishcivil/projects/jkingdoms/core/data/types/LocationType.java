package pl.polishcivil.projects.jkingdoms.core.data.types;

import java.util.Arrays;

public enum LocationType {
    //static great empire locations
    MAIN_CASTLE(1, Kingdom.GREAT_EMPIRE, LocationKind.PLAYER_ETERNAL),
    ALIEN_CAMP(21, Kingdom.GREAT_EMPIRE, LocationKind.PLAYER_ETERNAL),
    BIG_MONUMENT(26, Kingdom.GREAT_EMPIRE, LocationKind.CONQUERED),
    RESOURCE_FIELD(4, Kingdom.GREAT_EMPIRE, LocationKind.CONQUERED),
    SAMURAI_CAMP(33, Kingdom.GREAT_EMPIRE, LocationKind.NPC),
    NOMADS_CAMP(27, Kingdom.GREAT_EMPIRE, LocationKind.NPC),

    //static burning sands locations
    DESERT_FORTRESS(11, Kingdom.BURNING_SANDS, LocationKind.NPC),

    //static storm archipelago locations
    RESOURCE_ISLAND(24, Kingdom.STORM_ARCHIPELAGO, LocationKind.CONQUERED),
    STORM_FORT(25, Kingdom.STORM_ARCHIPELAGO, LocationKind.NPC),

    //static berimond locations
    BERIMOND_OBSERWATION_TOWER(17, Kingdom.BERIMOND, LocationKind.CONQUERED),
    BERIMOND_CASTLE(15, Kingdom.BERIMOND, LocationKind.OTHER),

    //dynamic npc locations
    ROBBER_BARON(2, null, LocationKind.NPC),

    //dynamic conquered locations
    VILLAGE(16, null, LocationKind.CONQUERED),
    CAPITAL2(22, null, LocationKind.CONQUERED),
    LABORATORY(28, null, LocationKind.CONQUERED),
    CAPITAL(3, null, LocationKind.CONQUERED),
    PRODUCTION_VILLAGE(10, null, LocationKind.CONQUERED),

    //dynamic eternal locations
    EVENT_START_CAMP(8, null, LocationKind.PLAYER_ETERNAL),
    OTHER_MAIN_CASTLE(12, null, LocationKind.PLAYER_ETERNAL),

    //dynamic other locations
    ALLIANCE_CITY(32, null, LocationKind.OTHER),
    PLACE_HOLDER(31, null, LocationKind.OTHER),

    KINGS_TOWER(23, null, LocationKind.OTHER),
    EVENT_DUNGEON(13, null, LocationKind.OTHER),
    FACTION_CAPITAL(18, null, LocationKind.OTHER),
    UNKNOWN_0(30, null, LocationKind.OTHER),
    UNKNOWN_1(34, null, LocationKind.OTHER),

    UNKNOWN(-1, null, LocationKind.OTHER);

    private final int rawType;
    private final Kingdom kindom;
    private final LocationKind locationKind;

    LocationType(int rawType, Kingdom predefinedKingdom, LocationKind locationKind) {
        this.rawType = rawType;
        this.kindom = predefinedKingdom;
        this.locationKind = locationKind;
    }

    public static LocationType getByRawType(int rawType) {
        return Arrays.stream(LocationType.values()).filter(it -> it.rawType == rawType).findFirst()
                .orElse(UNKNOWN);
    }

    public int getRawType() {
        return this.rawType;
    }

    public Kingdom getKindom() {
        return this.kindom;
    }


    public LocationKind getLocationKind() {
        return this.locationKind;
    }
}
