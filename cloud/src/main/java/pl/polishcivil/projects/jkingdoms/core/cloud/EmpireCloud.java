package pl.polishcivil.projects.jkingdoms.core.cloud;

import lombok.Cleanup;
import org.apache.logging.log4j.LogManager;
import pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.EmpireItemsFile;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.EmpireLanguageFile;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.EmpireVersionFile;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.title.TitlesDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.title.TitleDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileLocation;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileStorage;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileType;
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static pl.polishcivil.projects.jkingdoms.core.BotConstants.BOT_CLOUD_TYPE_FILE;

/**
 * Created by polish on 12/5/16.
 */
public class EmpireCloud {
    private static final EmpireCloud INSTANCE = new EmpireCloud(EmpireCloudType.EMPIRE_CUSTOM, EmpireLanguageTag.PL, EmpireLanguageTag.EN);

    static {
        EmpireCloudType type = EmpireCloudType.EMPIRE_CUSTOM;
        try {
            INSTANCE.load();
            markCloudType(type);
        } catch (Exception e) {
            LogManager.getLogger().catching(e);
        }
    }

    public static void markCloudType(EmpireCloudType type) {
        try {
            JSONUtils.serialize(type, BOT_CLOUD_TYPE_FILE);
        } catch (Exception e) {
            LogManager.getLogger().catching(e);
        }
    }

    private final EmpireCloudType type;
    private final EmpireLanguageTag[] tags;
    private final EmpireVersionFile versionFile;
    private final HashMap<EmpireLanguageTag, EmpireLanguageFile> languageFiles = new HashMap<>();
    private final EmpireItemsFile itemsFile;
    private boolean loaded = false;

    public EmpireCloud(EmpireCloudType type, EmpireLanguageTag... tags) {
        this.type = type;
        this.tags = tags;
        EmpireFileStorage storage = type.getStorage();
        this.versionFile = new EmpireVersionFile(this, storage.getFileByType(EmpireFileType.GF_ITEMS_VERSION));
        for (EmpireLanguageTag tag : tags) {
            this.languageFiles.put(tag, new EmpireLanguageFile(this, tag.getTag(), storage.getFileByType(EmpireFileType.GF_LANGUAGES)));
        }
        this.itemsFile = new EmpireItemsFile(this, storage.getFileByType(EmpireFileType.GF_ITEMS));
    }

    public static void main(String[] args) throws Exception {
        EmpireCloud empireCloud = INSTANCE;
        empireCloud.load();
        EmpireItemsFile itemsFile = empireCloud.getItemsFile();

        for (UnitDefinition unitDefinition : itemsFile.getUnits()) {
            String localizedName = unitDefinition.getLocalizedName(empireCloud, EmpireLanguageTag.PL);
            System.out.println("localizedName = " + localizedName);
        }
    }

    /**
     * Loads all data for this cloud
     *
     * @throws Exception
     */
    public void load() throws Exception {
        boolean versionChanged = false;
        if (versionFile.getLocalFile().exists()) {
            versionFile.load();
            int version = versionFile.getVersion();
            versionFile.update();
            versionFile.load();
            versionChanged = version != versionFile.getVersion();
        } else {
            versionFile.update();
            versionFile.load();
        }
        for (Map.Entry<EmpireLanguageTag, EmpireLanguageFile> entry : languageFiles.entrySet()) {
            if (!entry.getValue().getLocalFile().exists() || versionChanged) {
                entry.getValue().update();
            }
            entry.getValue().load();
        }
        if (!itemsFile.getLocalFile().exists() || versionChanged) {
            itemsFile.update();
        }
        itemsFile.load();
        loaded = true;
    }


    /**
     * @param location
     * @param unzip
     * @param params
     * @throws EmpireCloudDataUpdateException
     */
    public void updateGameFile(File localFile, EmpireFileLocation location, boolean unzip, Object... params) throws EmpireCloudDataUpdateException {
        try {
            String parametrizedURL = location.getUrl();
            for (int i = 0; i < params.length; i++) {
                String param = params[i].toString();
                parametrizedURL = parametrizedURL.replace("{" + i + "}", param);
            }
            URLConnection connection = new URL(parametrizedURL).openConnection();

            if (localFile.exists()) {
                if (!localFile.delete()) {
                    throw new EmpireCloudDataUpdateException(String.format("Couldn't delete local file %s", localFile));
                }
            } else {
                try {
                    localFile.createNewFile();
                } catch (IOException e) {
                    throw new EmpireCloudDataUpdateException(e.getMessage());
                }
            }

            byte[] buffer = new byte[1024];
            @Cleanup
            FileOutputStream fileOutputStream = new FileOutputStream(localFile);
            @Cleanup
            InputStream inputStream = connection.getInputStream();

            if (unzip) {
                @Cleanup
                ZipInputStream zipInputStream = new ZipInputStream(inputStream);
                ZipEntry entry = zipInputStream.getNextEntry();
                int read = -1;
                while ((read = zipInputStream.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, read);
                }
                if (zipInputStream.getNextEntry() != null) {
                    throw new EmpireCloudDataUpdateException(String.format("More than one zip entries found in zipped file %s", parametrizedURL));
                }
            } else {
                int read = -1;
                while ((read = inputStream.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, read);
                }
            }
        } catch (IOException e) {
            throw new EmpireCloudDataUpdateException(e.getMessage());
        }
    }

    public EmpireLanguageFile getLanguageFile(EmpireLanguageTag tag) {
        return languageFiles.get(tag);
    }

    public EmpireLanguageTag[] getTags() {
        return tags;
    }

    public EmpireCloudType getType() {
        return type;
    }

    public EmpireItemsFile getItemsFile() {
        return itemsFile;
    }

    public EmpireVersionFile getVersionFile() {
        return versionFile;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public static EmpireCloud getINSTANCE() {
        return INSTANCE;
    }
}
