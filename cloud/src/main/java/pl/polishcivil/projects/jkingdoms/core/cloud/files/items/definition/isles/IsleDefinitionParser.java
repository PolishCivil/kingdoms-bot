package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.isles;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;

import static java.lang.String.format;

/**
 * Created by polish on 1/19/17.
 */
public class IsleDefinitionParser implements ItemDefinitionParser<IsleDefinition> {
    @Override
    public IsleDefinition parse(Node node) {
        int isleID = getAttribute(node, "IsleID").map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse isle definition, missing id"));
        String rawType = getAttribute(node, "type")
                .orElseThrow(() -> new RuntimeException(format("Couldn't parse isle definition %d missing type id", isleID)));
        IsleDefinition.Type type = IsleDefinition.Type.getByRawType(rawType)
                .orElseThrow(() -> new RuntimeException(format("Unknown isle type %s", rawType)));

        int dungeonLevel = Integer.parseInt(getAttributeDefault(node, "dungeonlevel", "-1"));
        int guards = Integer.parseInt(getAttributeDefault(node, "guards", "-1"));
        int globalCoolDown = Integer.parseInt(getAttributeDefault(node, "globalCooldown", "-1"));
        int localCoolDown = Integer.parseInt(getAttributeDefault(node, "localCooldown", "-1"));

        IsleDefinition isleDefinition = new IsleDefinition();
        isleDefinition.setID(isleID);
        isleDefinition.setType(type);
        isleDefinition.setDungeonLevel(dungeonLevel);
        isleDefinition.setGuards(guards);
        isleDefinition.setGlobalCoolDown(globalCoolDown);
        isleDefinition.setLocalCoolDown(localCoolDown);
        return isleDefinition;
    }
}
