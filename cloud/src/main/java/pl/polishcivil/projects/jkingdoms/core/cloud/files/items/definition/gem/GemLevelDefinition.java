package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

/**
 * Created by polish on 3/3/2017.
 */
@Data
public class GemLevelDefinition implements IDefinition {
    private int levelID;
    private int saleValue;
    private int insertCostGold;
    private int craftSuccessChance;
    private int craftCostGold;
    private int craftCostRuby;
    private int removalCostRuby;

    @Override
    public int getID() {
        return levelID;
    }
}
