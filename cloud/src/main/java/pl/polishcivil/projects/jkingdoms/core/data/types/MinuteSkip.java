package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 12/19/16.
 */
public enum MinuteSkip {
    MINUTE_SKIP_1(1),
    MINUTE_SKIP_2(5),
    MINUTE_SKIP_3(10),
    MINUTE_SKIP_4(30),
    MINUTE_SKIP_5(60),
    MINUTE_SKIP_6(300),
    MINUTE_SKIP_7(1440);

    private final int minutes;

    MinuteSkip(int minutes) {
        this.minutes = minutes;
    }

    public String getKey() {
        return String.format("MS%d", ordinal() + 1);
    }

    public int getMinutes() {
        return minutes;
    }

    @Override
    public String toString() {
        return String.format("Minute skip minutes = %d", minutes);
    }
}
