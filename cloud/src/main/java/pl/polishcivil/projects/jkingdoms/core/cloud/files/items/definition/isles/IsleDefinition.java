package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.isles;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by polish on 22.12.16.
 */
@Data
public class IsleDefinition implements IDefinition {
    public enum Type {
        DUNGEON("DUNGEON"),
        VILLAGEWOOD("VILLAGEWOOD"),
        VILLAGEAQUAMARINE("VILLAGEAQUAMARINE"),
        VILLAGESTONE("VILLAGESTONE"),;

        private final String rawType;

        Type(String rawType) {
            this.rawType = rawType;
        }

        public String getRawType() {
            return rawType;
        }

        public static Optional<Type> getByRawType(String rawType) {
            return Arrays.stream(IsleDefinition.Type.values()).filter(it -> it.getRawType().equals(rawType)).findAny();
        }
    }

    private Type type;
    private int ID;
    private int dungeonLevel;
    private int guards;
    private int localCoolDown;
    private int globalCoolDown;
}
