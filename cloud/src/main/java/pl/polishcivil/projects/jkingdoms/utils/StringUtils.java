package pl.polishcivil.projects.jkingdoms.utils;

/**
 * Created by polish on 1/15/2017.
 */
public class StringUtils {
    public static String capitalizeFirstLetter(String text) {
        return String.format("%s%s", Character.toUpperCase(text.charAt(0)), text.substring(1));
    }
}
