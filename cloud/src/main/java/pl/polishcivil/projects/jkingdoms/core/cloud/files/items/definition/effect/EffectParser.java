package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.types.LocationType;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by polish on 1/19/17.
 */
public class EffectParser implements ItemDefinitionParser<EffectDefinition> {
    @Override
    public EffectDefinition parse(Node node) throws Exception {
        int id = getAttribute(node, "effectID").map(Integer::parseInt).orElseThrow(() -> new RuntimeException("Couldn't" +
                " get id attribute"));
        String name = getAttribute(node, "name").orElseThrow(() -> new RuntimeException(String.format("Couldn't get " +
                "name attribute for id %d", id)));
        int effectTypeID = getAttribute(node, "effectTypeID").map(Integer::parseInt).orElseThrow(() -> new
                RuntimeException(String.format("Couldn't find effect type attribute for id %d name %s", id, name)));
        List<LocationType> areaTypeID = getAttribute(node, "areaTypeID")
                .map(v -> v.split(","))
                .map(Stream::of)
                .map(v -> v.map(Integer::parseInt))
                .map(v -> v.map(LocationType::getByRawType))
                .map(v -> v.collect(Collectors.toList())).orElse(Collections.emptyList());
        EffectDefinition definition = new EffectDefinition();
        definition.setAreaTypeIds(areaTypeID);
        definition.setID(id);
        definition.setName(name);
        definition.setEffectTypeID(effectTypeID);
        return definition;
    }
}
