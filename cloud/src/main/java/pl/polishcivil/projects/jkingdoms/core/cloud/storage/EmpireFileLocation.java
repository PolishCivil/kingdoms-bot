package pl.polishcivil.projects.jkingdoms.core.cloud.storage;

public class EmpireFileLocation {
    private final EmpireFileType fileType;
    private final String url;

    public EmpireFileLocation(final EmpireFileType fileType, final String url) {
        this.fileType = fileType;
        this.url = url;
    }

    public EmpireFileType getFileType() {
        return this.fileType;
    }

    public String getUrl() {
        return this.url;
    }
}
