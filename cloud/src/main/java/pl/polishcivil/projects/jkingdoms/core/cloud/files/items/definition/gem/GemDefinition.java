package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;
import pl.polishcivil.projects.jkingdoms.core.data.types.LordType;

import java.util.List;

/**
 * Created by polish on 1/18/2017.
 */
@Data
public class GemDefinition implements IDefinition {
    private int ID;
    private LordType lordType;
    private int followingGemID;
    private int level;
    private int setID;
    private List<StaticEffectData> effects;

    public boolean canBeUpgraded() {
        return followingGemID > 0;
    }

    public boolean isSetMember() {
        return setID != -1;
    }
}
