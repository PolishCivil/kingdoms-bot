package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by polish on 12/5/16.
 */
public enum ToolUnitType {
    ATTACK("Attack"),
    DEFENCE("Defence");
    private final String raw;

    ToolUnitType(String raw) {
        this.raw = raw;
    }

    public static Optional<ToolUnitType> forRaw(String rawType) {
        return Arrays.stream(ToolUnitType.values()).filter(it -> it.raw.equals(rawType)).findFirst();
    }
}
