package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.LordType;

/**
 * Created by polish on 1/19/17.
 */
@Data
public class EquipmentEffectDefinition implements IDefinition {
    private int ID;
    private int effectID;
    private LordType wearer;
    private double enchantmentPrimaryBonus;
    private double enchantmentSecondaryBonus;
    private int bonus;
    private boolean ignoreCap;
    private String _shortDesc;
    private EffectDefinition _effect;

    public String getShortDescription(EmpireCloud cloud, EmpireLanguageTag tag) {
        if (_shortDesc != null) {
            return _shortDesc;
        }
        EffectDefinition effect = getEffect(cloud);
        return _shortDesc = cloud.getLanguageFile(tag).getMessage(String.format("equip_effect_description_short_%s", effect.getName()));
    }

    public EffectDefinition getEffect(EmpireCloud cloud) {
        if (_effect != null) {
            return _effect;
        }
        return _effect = cloud.getItemsFile().getEffects().findByID(effectID);
    }
}
