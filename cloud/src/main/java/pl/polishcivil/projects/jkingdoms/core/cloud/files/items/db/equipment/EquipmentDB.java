package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.equipment;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.equipment.EquipmentDefinition;

import java.util.ArrayList;

/**
 * Created by polish on 3/3/2017.
 */
public class EquipmentDB extends AbstractDefinitionDB<EquipmentDefinition> {
    public ArrayList<Integer> getEquipmentIDsForSet(int setID) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (EquipmentDefinition definition : getAll()) {
            if (definition.getSetID() == setID) {
                integers.add(definition.getID());
            }
        }
        return integers;
    }
}
