package pl.polishcivil.projects.jkingdoms.core.data.types;

public enum TaxFeesLevel {
    NO_BONUS(0, 0, 0),
    LEVEL_1(1, 1, 377),
    LEVEL_2(2, 2, 378),
    LEVEL_3(3, 3, 379);

    private final int level;
    private final int bonusInPercent;
    private final int researchId;

    TaxFeesLevel(final int level, final int bonusInPercent, final int researchId) {
        this.level = level;
        this.bonusInPercent = bonusInPercent;
        this.researchId = researchId;
    }

    public int getLevel() {
        return this.level;
    }

    public int getBonusInPercent() {
        return this.bonusInPercent;
    }

    public int getResearchId() {
        return this.researchId;
    }

    public static TaxFeesLevel getByRawValue(final int value) {
        TaxFeesLevel[] values;
        for (int length = (values = values()).length, i = 0; i < length; ++i) {
            final TaxFeesLevel tfl = values[i];
            if (tfl.getLevel() == value) {
                return tfl;
            }
        }
        return null;
    }
}
