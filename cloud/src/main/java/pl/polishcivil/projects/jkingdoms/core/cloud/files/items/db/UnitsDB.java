package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.soldier.SoldierUnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitType;
import pl.polishcivil.projects.jkingdoms.core.data.types.UnitType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 1/19/17.
 */
public class UnitsDB extends AbstractDefinitionDB<UnitDefinition> {


    public List<ToolUnitDefinition> getToolUnits() {
        return items.stream()
                .filter(it -> it instanceof ToolUnitDefinition)
                .map(it -> (ToolUnitDefinition) it).collect(Collectors.toList());
    }

    public List<SoldierUnitDefinition> getSoldierUnits() {
        return items.stream()
                .filter(it -> it instanceof SoldierUnitDefinition)
                .map(it -> (SoldierUnitDefinition) it).collect(Collectors.toList());
    }

    public UnitType getTypeOf(int id) {
        UnitDefinition byID = findByID(id);
        if (byID instanceof ToolUnitDefinition) {
            ToolUnitType type = ((ToolUnitDefinition) byID).getType();
            if (type == ToolUnitType.ATTACK) {
                return UnitType.SIEGE_TOOL;
            } else {
                return UnitType.DEFENCE_TOOL;
            }
        } else {
            return UnitType.SOLDIER;
        }
    }
}
