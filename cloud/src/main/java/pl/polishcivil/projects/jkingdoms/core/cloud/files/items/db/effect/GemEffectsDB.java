package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db.effect;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.effect.EquipmentGemEffectDefinition;

/**
 * Created by polish on 3/17/2017.
 */
public class GemEffectsDB extends AbstractDefinitionDB<EquipmentGemEffectDefinition> {
}
