package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition;

/**
 * Created by polish on 1/19/17.
 */
public interface IDefinition {
    int getID();
}
