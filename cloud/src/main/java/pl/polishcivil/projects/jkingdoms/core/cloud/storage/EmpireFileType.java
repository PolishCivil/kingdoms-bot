package pl.polishcivil.projects.jkingdoms.core.cloud.storage;

public enum EmpireFileType {
    GF_ITEMS,
    GF_ITEMS_VERSION,
    GF_LANGUAGES,
}
