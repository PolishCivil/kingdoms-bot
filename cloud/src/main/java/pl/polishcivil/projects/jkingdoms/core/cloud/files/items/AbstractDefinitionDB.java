package pl.polishcivil.projects.jkingdoms.core.cloud.files.items;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

import java.util.*;
import java.util.function.Consumer;

/**
 * Created by polish on 1/19/17.
 */
public class AbstractDefinitionDB<T extends IDefinition> implements Iterable<T> {
    protected final ArrayList<T> items = new ArrayList<T>();

    public T findByID(int ID) {
        return items.stream().filter(it -> it.getID() == ID).findFirst().orElseThrow(() -> new RuntimeException(String.format("Couldn't find definition for id %d " + getClass().getSimpleName(), ID)));
    }

    public Optional<T> _findByID(int ID) {
        return items.stream().filter(it -> it.getID() == ID).findFirst();
    }

    public void add(T definition) {
        items.add(definition);
    }

    public int size() {
        return items.size();
    }

    public void addAll(List<T> list) {
        items.addAll(list);
    }

    public List<T> getAll() {
        return Collections.unmodifiableList(items);
    }

    @Override
    public Iterator<T> iterator() {
        return items.iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        items.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return items.spliterator();
    }
}
