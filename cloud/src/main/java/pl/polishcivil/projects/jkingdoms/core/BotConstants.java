package pl.polishcivil.projects.jkingdoms.core;

import java.io.File;

/**
 * Created by polish on 11/29/2016.
 */
public class BotConstants {
    public static final File BOT_HOME_DIR = new File(System.getProperty("user.home"), "jkingdoms");
    public static final File BOT_LICENSE_FILE = new File(BOT_HOME_DIR, "license.lic");
    public static final File BOT_LOGS_DIR = new File(BOT_HOME_DIR, "logs");
    public static final File BOT_CONFIGURATIONS_DIR = new File(BOT_HOME_DIR, "configurations");
    public static final File BOT_ATTACK_CONFIGS_DIR = new File(BOT_HOME_DIR, "attack_configs");
    public static final File GAME_CLOUD_DIRECTORY = new File(BOT_HOME_DIR, "game_cloud");
    public static final File BOT_CLOUD_TYPE_FILE = new File(GAME_CLOUD_DIRECTORY, "type.json");

    static {
        createDirectory(BOT_HOME_DIR);
        createDirectory(BOT_ATTACK_CONFIGS_DIR);
        createDirectory(GAME_CLOUD_DIRECTORY);
        createDirectory(BOT_CONFIGURATIONS_DIR);
        createDirectory(BOT_LOGS_DIR);
    }

    private static void createDirectory(File dir) {
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                throw new RuntimeException(String.format("Couldn't create directory %s", dir));
            }
        }
    }


    public static File getBotConfigurationFile(String username) {
        return new File(BOT_CONFIGURATIONS_DIR, username.toLowerCase() + ".json");
    }
}
