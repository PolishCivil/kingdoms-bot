package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.db;

import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.AbstractDefinitionDB;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.lord.LordDefinition;

/**
 * Created by polish on 3/3/2017.
 */
public class LordsDB extends AbstractDefinitionDB<LordDefinition> {

}
