package pl.polishcivil.projects.jkingdoms.core.cloud.files;

import lombok.Cleanup;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Document;
import pl.polishcivil.projects.jkingdoms.core.BotConstants;
import pl.polishcivil.projects.jkingdoms.core.cloud.AbstractEmpireFile;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloudDataUpdateException;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileLocation;
import pl.polishcivil.projects.jkingdoms.utils.XMLUtils;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * Created by polish on 12/5/16.
 */
public class EmpireVersionFile extends AbstractEmpireFile {
    @Getter
    private int version = -1;

    public EmpireVersionFile(EmpireCloud cloud, EmpireFileLocation remoteFileLocation) {
        super(cloud, new File(BotConstants.GAME_CLOUD_DIRECTORY, "version"), remoteFileLocation);
    }

    @Override
    public void update() throws EmpireCloudDataUpdateException {
        LogManager.getLogger().info("Updating version file...");
        cloud.updateGameFile(getLocalFile(), getRemoteFileLocation(), false);
    }

    @Override
    public void load() throws Exception {
        LogManager.getLogger().info("Loading version file...");
        Properties properties = new Properties();
        Document document = XMLUtils.newDocument();
        @Cleanup
        FileReader reader = new FileReader(getLocalFile());
        properties.load(reader);
        try {
            this.version = Integer.parseInt(properties.getProperty("CastleItemXMLVersion"));
        } catch (Exception e) {
            LogManager.getLogger().catching(e);
            this.version = -1;
        }
        LogManager.getLogger().info("Loaded version file version = {}", getVersion());
    }
}
