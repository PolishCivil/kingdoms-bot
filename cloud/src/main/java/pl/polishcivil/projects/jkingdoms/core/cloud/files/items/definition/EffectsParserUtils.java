package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition;

import org.apache.logging.log4j.LogManager;
import pl.polishcivil.projects.jkingdoms.core.data.effect.StaticEffectData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by polish on 3/17/2017.
 */
public class EffectsParserUtils {
    public static List<StaticEffectData> parse(String value) {
        if (value.isEmpty()) {
            return Collections.emptyList();
        }

        String[] effects = value.replace("&amp;", "&").split(",");
        ArrayList<StaticEffectData> effectsData = new ArrayList<>();
        for (String effect : effects) {
            if (effect.length() > 0) {
                String[] split = effect.split("&");
                if (split.length > 0) {
                    if (split.length == 2) {
                        StaticEffectData data = new StaticEffectData(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
                        effectsData.add(data);
                    } else {
                        LogManager.getLogger().error("Couldn't parse effect data {}", Arrays.toString(effects));
                    }
                }
            }
        }
        return effectsData;
    }
}
