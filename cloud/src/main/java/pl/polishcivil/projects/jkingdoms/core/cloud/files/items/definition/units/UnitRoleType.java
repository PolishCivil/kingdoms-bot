package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by polish on 12/5/16.
 */
public enum UnitRoleType {
    MELEE("melee"),
    RANGED("ranged");
    private final String raw;

    UnitRoleType(String raw) {
        this.raw = raw;
    }


    public static Optional<UnitRoleType> forRaw(String rawType) {
        return Arrays.stream(UnitRoleType.values()).filter(it -> it.raw.equals(rawType)).findFirst();
    }
}
