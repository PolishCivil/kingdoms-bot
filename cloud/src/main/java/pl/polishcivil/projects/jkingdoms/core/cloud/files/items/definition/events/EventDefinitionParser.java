package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.events;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by polish on 1/15/2017.
 */
public class EventDefinitionParser implements ItemDefinitionParser<EventDefinition> {
    public EventDefinition parse(Node definitionNode) throws Exception {

        NamedNodeMap attributes = definitionNode.getAttributes();
        int eventID = Optional.ofNullable(attributes.getNamedItem("eventID"))
                .map(Node::getNodeValue)
                .map(Integer::parseInt).orElseThrow(() -> new RuntimeException("Couldn't parse event definition's id"));

        Set<Kingdom> kIDs = Optional.ofNullable(attributes.getNamedItem("kIDs"))
                .map(Node::getNodeValue)
                .map(v -> v.split(","))
                .map(Stream::of)
                .map(v -> v.map(Integer::parseInt))
                .map(v -> v.map(Kingdom::getByRawType))
                .map(v -> v.collect(Collectors.toSet())).orElseThrow(() -> new RuntimeException("Couldn't parse event definition's kids"));

        String eventType = Optional.ofNullable(attributes.getNamedItem("eventType"))
                .map(Node::getNodeValue).orElseThrow(() -> new RuntimeException("Couldn't parse event definition's type"));

        int[] packageIDs = Stream.of(getAttribute(definitionNode, "packageIDs")
                .map(v -> v.split("\\+")).orElse(new String[0]))
                .map(Integer::parseInt).mapToInt(v -> v).toArray();


        EventDefinition eventDefinition = new EventDefinition();
        eventDefinition.setID(eventID);
        eventDefinition.setKingdoms(kIDs);
        eventDefinition.setType(eventType);
        eventDefinition.setPackageIDS(packageIDs);

        return eventDefinition;
    }
}
