package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.gem;

import org.w3c.dom.Node;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.ItemDefinitionParser;

/**
 * Created by polish on 3/3/2017.
 */
public class GemLevelParser implements ItemDefinitionParser<GemLevelDefinition> {
    @Override
    public GemLevelDefinition parse(Node node) throws Exception {
        int level = getAttribute(node, "gemLevelID")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse gem, missing gemLevelID"));

        int saleValue = getAttribute(node, "saleValue")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse gem, missing saleValue"));

        int insertCostGold = getAttribute(node, "insertCostC1")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse gem, missing insertCostC1"));

        int removalCostRuby = getAttribute(node, "removalCostC2")
                .map(Integer::parseInt)
                .orElseThrow(() -> new RuntimeException("Couldn't parse gem, missing removalCostC2"));

        int craftSuccessChance = getAttribute(node, "craftSuccessChance")
                .map(Integer::parseInt)
                .orElse(-1);
        int craftCostGold = getAttribute(node, "craftCostC1")
                .map(Integer::parseInt)
                .orElse(-1);
        int craftCostRuby = getAttribute(node, "craftCostC2")
                .map(Integer::parseInt)
                .orElse(-1);

        GemLevelDefinition gemLevelDefinition = new GemLevelDefinition();
        gemLevelDefinition.setLevelID(level);
        gemLevelDefinition.setSaleValue(saleValue);
        gemLevelDefinition.setInsertCostGold(insertCostGold);
        gemLevelDefinition.setRemovalCostRuby(removalCostRuby);
        gemLevelDefinition.setCraftSuccessChance(craftSuccessChance);
        gemLevelDefinition.setCraftCostGold(craftCostGold);
        gemLevelDefinition.setCraftCostRuby(craftCostRuby);
        return gemLevelDefinition;
    }
}
