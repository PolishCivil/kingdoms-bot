package pl.polishcivil.projects.jkingdoms.core.data.types;

/**
 * Created by polish on 11/24/2016.
 */
public enum UnitType {
    SOLDIER, DEFENCE_TOOL, SIEGE_TOOL
}
