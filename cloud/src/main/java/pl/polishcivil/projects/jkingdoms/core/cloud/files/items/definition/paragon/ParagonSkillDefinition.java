package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.paragon;

import lombok.Data;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.IDefinition;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by polish on 3/16/2017.
 */
@Data
public class ParagonSkillDefinition implements IDefinition {
    public enum ParagonSkillTreeEnum {
        UNKNOWN(-1),
        ATTACK(0),
        DEFENSE(1);
        private final int id;

        ParagonSkillTreeEnum(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static ParagonSkillTreeEnum getByRawID(int id) {
            return Arrays.stream(values()).filter(it -> it.getId() == id).findFirst().orElse(UNKNOWN);
        }
    }

    public enum ParagonSkillType {
        UNKNOWN("unknown"),
        ORDINARY("ordinary"),
        SPECIAL("special"),
        ULTIMATE("ultimate");
        private final String rawValue;

        ParagonSkillType(String rawValue) {
            this.rawValue = rawValue;
        }

        public String getRawValue() {
            return rawValue;
        }

        public static ParagonSkillType getByRawValue(String raw) {
            return Arrays.stream(values()).filter(it -> Objects.equals(it.getRawValue(), raw)).findFirst().orElse(UNKNOWN);
        }
    }

    private final int ID;

    private int baseSkillID;
    private int requiredSkillID;
    private int followingSkillID;
    private int level;
    private int tier;
    private int groupID;
    private int costSkillPoints;
    private int totalCostSkillPoints;
    private double effectValue;
    private double totalEffectValue;
    private int maxLevel;
    private int requiredParagonSkillID;
    private int followingParagonSkillID;

    private ParagonSkillTreeEnum skillTree;
    private ParagonSkillType skillType;
    private String effectTypeName;
}
