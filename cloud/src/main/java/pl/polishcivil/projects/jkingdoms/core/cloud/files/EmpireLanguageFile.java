package pl.polishcivil.projects.jkingdoms.core.cloud.files;

import lombok.Getter;
import lombok.ToString;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import pl.polishcivil.projects.jkingdoms.core.BotConstants;
import pl.polishcivil.projects.jkingdoms.core.cloud.AbstractEmpireFile;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloudDataUpdateException;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.ILocalizedDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.storage.EmpireFileLocation;
import pl.polishcivil.projects.jkingdoms.utils.XMLUtils;

import java.io.File;
import java.text.MessageFormat;
import java.util.HashMap;

/**
 * Created by polish on 12/5/16.
 */
@ToString
public class EmpireLanguageFile extends AbstractEmpireFile {
    private final HashMap<String, String> entryHashMap = new HashMap<>();
    @Getter
    private String languageTag;
    @Getter
    private int version;
    @Getter
    private String branch;


    public EmpireLanguageFile(EmpireCloud cloud, String languageTag, EmpireFileLocation remoteFileLocation) {
        super(cloud, new File(BotConstants.GAME_CLOUD_DIRECTORY, String.format("language_%s", languageTag)), remoteFileLocation);
        System.out.println("remoteFileLocation = " + remoteFileLocation.getUrl());
        this.languageTag = languageTag;
    }

    @Override
    public void update() throws EmpireCloudDataUpdateException {
        LogManager.getLogger().info("Updating language file for tag {}", languageTag);
        cloud.updateGameFile(getLocalFile(), getRemoteFileLocation(), true, languageTag);
    }

    @Override
    public void load() throws Exception {
        LogManager.getLogger().info("Loading language file for tag {}", languageTag);
        Document parse = XMLUtils.getBuilder().parse(getLocalFile());
        Element root = parse.getDocumentElement();
        this.entryHashMap.clear();
        this.version = Integer.parseInt(root.getAttribute("versionNo"));
        this.branch = root.getAttribute("branch");
        NodeList keys = root.getElementsByTagName("text");
        int length = keys.getLength();
        for (int i = 0; i < length; i++) {
            Node item = keys.item(i);
            String name = item.getAttributes().getNamedItem("name").getNodeValue();
            String id = item.getAttributes().getNamedItem("id").getNodeValue();
            entryHashMap.put(id.toLowerCase(), name);
        }
        LogManager.getLogger().info("Loaded {} messages for {} language", entryHashMap.size(), languageTag);
    }

    public String getMessage(String id) {
        String message = entryHashMap.get(id.toLowerCase());
        if (message == null) {
            if (id.equals("QuickAttack_name")) {
                return "QUICK_ATTACK";
            }
            LogManager.getLogger().error("Couldn't find message for id {}", id);
            return ""+id;
        }
        return message;
    }

    public String getFormattedMessage(String id, Object... args) {
        String pattern = entryHashMap.get(id.toLowerCase());
        return MessageFormat.format(pattern, args);
    }

    public String getName(@Nullable ILocalizedDefinition localizedItem) {
        if (localizedItem == null) {
            return "n/a";
        }
        return getMessage(localizedItem.getLanguageKey());
    }
}
