package pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition;

import pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;

/**
 * Created by polish on 12/5/16.
 */
public interface ILocalizedDefinition {
    String getLanguageKey();

    default String getLocalizedName(EmpireCloud cloud, EmpireLanguageTag tag) {
        return cloud.getLanguageFile(tag).getName(this);
    }
}
