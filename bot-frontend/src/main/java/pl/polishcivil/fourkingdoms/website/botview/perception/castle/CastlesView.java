package pl.polishcivil.fourkingdoms.website.botview.perception.castle;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig;
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleData;
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleUserData;
import pl.polishcivil.fourkingdoms.website.utils.WebStringUtils;

/**
 * Created by Polish Civil on 14.05.2017.
 */
public class CastlesView extends VerticalLayout {
    private final Bot bot;
    private final Grid<LocationConfig> ownedLocations = new Grid<>();
    private final Label currentCastleLabel = new Label();

    public CastlesView(Bot bot) {
        this.bot = bot;
        ownedLocations.addColumn(LocationConfig::getKingdom).setCaption("Kingdom");
        ownedLocations.addColumn(LocationConfig::getX).setCaption("X");
        ownedLocations.addColumn(LocationConfig::getY).setCaption("Y");
        setCaption("Castles");
        currentCastleLabel.setCaption("Current castle:");
        addComponent(currentCastleLabel);
        ownedLocations.setCaption("Owned locations");
        ownedLocations.setSizeFull();
        addComponent(ownedLocations);
    }

    public void update() {
        CastleUserData castleData = bot.getInternalDataPerception().getCastleData();
        if (castleData != null) {
            ownedLocations.setItems(castleData.getOwnedLocations());
        } else {
            ownedLocations.setItems();
        }

        CastleData currentCastleData = bot.getInternalDataPerception().getCurrentCastleData();
        if (currentCastleData != null) {
            currentCastleLabel.setValue(WebStringUtils.locationToString(currentCastleData.getLocation()));
        } else {
            currentCastleLabel.setValue("none");
        }
    }
}
