package pl.polishcivil.fourkingdoms.website.components;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.Setter;
import com.vaadin.ui.ComboBox;
import pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;

import java.util.ArrayList;

/**
 * Created by polish on 6/3/17.
 */
public class EditableUnitListComponent<T> extends ComboBox<UnitDefinition> {
    public EditableUnitListComponent(T bean, ValueProvider<T, String> getter, Setter<T, String> setter, EmpireCloud cloud) {
        setItemCaptionGenerator(v -> v.getLocalizedName(cloud, EmpireLanguageTag.PL));
        String selectedUnit = getter.apply(bean);
        ArrayList<UnitDefinition> units = new ArrayList<>(cloud.getItemsFile().getUnits().getAll());
        if (selectedUnit != null) {
            units.stream().filter(it -> it.getLocalizedName(cloud, EmpireLanguageTag.EN).equals(selectedUnit)).findFirst()
                    .ifPresent(this::setSelectedItem);
        }
        setEmptySelectionAllowed(false);
        setDataProvider(new ListDataProvider<UnitDefinition>(units));
        addSelectionListener((SingleSelectionListener<UnitDefinition>) singleSelectionEvent -> singleSelectionEvent.getSelectedItem().ifPresent(v -> {
            setter.accept(bean, v.getLocalizedName(cloud, EmpireLanguageTag.EN));
        }));
        setSizeFull();
    }
}
