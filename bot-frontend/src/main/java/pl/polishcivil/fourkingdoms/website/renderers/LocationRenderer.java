package pl.polishcivil.fourkingdoms.website.renderers;

import com.vaadin.ui.renderers.AbstractRenderer;
import elemental.json.Json;
import elemental.json.JsonValue;
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig;

/**
 * Created by Polish Civil on 30.05.2017.
 */
public class LocationRenderer extends AbstractRenderer<Object, LocationConfig> {
    public LocationRenderer() {
        this("NULL");
    }

    public LocationRenderer(String nullRepresentation) {
        super(LocationConfig.class, nullRepresentation);
    }

    public JsonValue encode(LocationConfig value) {
        if (value == null) {
            return Json.create("NULL");
        }
        return Json.create(value.getKingdom().name() + " " + value.getX() + " " + value.getY());
    }
}
