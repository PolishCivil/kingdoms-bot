package pl.polishcivil.fourkingdoms.website.components;

import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.TextField;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig;


/**
 * Created by polish on 6/3/17.
 */
public class EditableTextRenderer<T> extends TextField {
    public EditableTextRenderer(T bean, ValueProvider<T, String> getter, Setter<T, String> setter) {
        String current = getter.apply(bean);
        setValue(current);
        addValueChangeListener(change -> {
            setter.accept(bean, change.getValue());
        });
        setSizeFull();
    }
}
