package pl.polishcivil.fourkingdoms.website.converters;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import org.apache.logging.log4j.LogManager;
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig;
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Polish Civil on 30.05.2017.
 */
public class LocationTextConverter implements Converter<String, LocationConfig> {
    @Override
    public Result<LocationConfig> convertToModel(String s, ValueContext valueContext) {
        try {
            String[] split = s.split(" ");
            Kingdom kingdom;
            try {
                kingdom = Kingdom.valueOf(split[0]);
            } catch (Exception e) {
                return Result.error("Couldn't find kingdom with name " + split[0] + " available: " + Arrays.stream(Kingdom.values()).map(Enum::name).collect(Collectors.toList()));
            }
            int x = Integer.parseInt(split[1]);
            int y = Integer.parseInt(split[2]);
            return Result.ok(new LocationConfig(kingdom, x, y));
        } catch (Exception e) {
            LogManager.getLogger().catching(e);
            return Result.error(e.getMessage());
        }
    }

    @Override
    public String convertToPresentation(LocationConfig location, ValueContext valueContext) {
        return location.getKingdom().name() + " " + location.getX() + " " + location.getY();
    }
}
