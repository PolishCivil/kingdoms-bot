package pl.polishcivil.fourkingdoms.website.botview.windows.armyconfig;

import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.ui.*;
import org.jetbrains.annotations.Nullable;
import org.jsoup.helper.StringUtil;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackWaveConfig;
import pl.polishcivil.projects.jkingdoms.core.EmpireLanguageTag;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.UnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.cloud.files.items.definition.units.tool.ToolUnitDefinition;
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by polish on 5/6/2017.
 */
public class ArmyConfigWindow extends Window {
    private final VerticalLayout soldiers = new VerticalLayout();
    private final VerticalLayout tools = new VerticalLayout();
    private final HashMap<AttackWall, List<AttackWaveConfig>> configuration = new HashMap<>();

    static final int MAX_SLOTS = 3;
    static final int MAX_WAVES = 6;

    public ArmyConfigWindow() {
        super("Army configuration");
        setModal(true);

        AttackWall[] walls = AttackWall.values();
        ComboBox<AttackWall> attackWallListSelect = new ComboBox<>("Wall", Arrays.stream(walls).filter(it -> it != AttackWall.UNKNOWN).collect(Collectors.toList()));
        attackWallListSelect.setSelectedItem(AttackWall.MIDDLE);
        attackWallListSelect.setHeight(100, Unit.PERCENTAGE);
        HorizontalLayout content = new HorizontalLayout(attackWallListSelect);


        for (int waveNr = 0; waveNr < MAX_WAVES; waveNr++) {
            HorizontalLayout waveLayout = new HorizontalLayout(new Label("Wave " + waveNr));
            for (int slot = 0; slot < MAX_SLOTS; slot++) {
                waveLayout.addComponent(new TextField("SLOT " + slot));
            }
            soldiers.addComponent(waveLayout);
        }


        for (int waveNr = 0; waveNr < MAX_WAVES; waveNr++) {
            HorizontalLayout waveLayout = new HorizontalLayout(new Label("Wave " + waveNr));
            for (int slot = 0; slot < MAX_SLOTS; slot++) {
                waveLayout.addComponent(new TextField("SLOT " + slot));
            }
            tools.addComponent(waveLayout);
        }

        Panel panel = new Panel("Troops");
        panel.setContent(new VerticalLayout(new HorizontalLayout(new Label("Soldiers"), soldiers), new HorizontalLayout(new Label("Tools"), tools)));
        panel.setSizeFull();
        content.addComponent(panel);

        content.setSizeFull();
        content.setComponentAlignment(attackWallListSelect, Alignment.TOP_LEFT);
        content.setComponentAlignment(panel, Alignment.TOP_RIGHT);
        content.setExpandRatio(panel, 1.0F);
        setContent(content);
        setSizeUndefined();

        attackWallListSelect.addSelectionListener((SingleSelectionListener<AttackWall>) singleSelectionEvent -> {
            AttackWall oldValue = singleSelectionEvent.getOldValue();
            configuration.put(oldValue, getCurrentWallConfig());
            AttackWall newValue = singleSelectionEvent.getValue();
            configuration.putIfAbsent(newValue, new ArrayList<>());
            setCurrentWallConfig(configuration.get(newValue));
        });

        addCloseListener(new CloseListener() {
            @Override
            public void windowClose(CloseEvent closeEvent) {
                getCurrentWallConfig();
            }
        });
    }

    private void setCurrentWallConfig(List<AttackWaveConfig> configs) {
        //clear the data
        for (int waveNr = 0; waveNr < MAX_WAVES; waveNr++) {
            for (int slotNr = 0; slotNr < MAX_SLOTS; slotNr++) {
                getSlotComponent(tools, waveNr, slotNr).setValue("");
                getSlotComponent(soldiers, waveNr, slotNr).setValue("");
            }
        }
        //populate the data
        for (int waveNr = 0; waveNr < MAX_WAVES; waveNr++) {
            if (configs.size() > waveNr) {
                AttackWaveConfig attackWaveConfig = configs.get(waveNr);
                List<AttackUnitConfig> units = attackWaveConfig.getUnits();

                for (AttackUnitConfig unit : units) {
                    if (unit.isBestMeleeUnit() || unit.isBestRangeUnit()) {
                        TextField emptySoldierSlot = getEmptySlotComponent(soldiers, waveNr);
                        if (emptySoldierSlot != null) {
                            emptySoldierSlot.setValue(unit.getIdentifier() + "," + unit.getUnitAmount() + (unit.getPercentage() ? "%" : ""));
                        }
                    } else {
                        UnitDefinition unitDefinition = EmpireCloud.getINSTANCE().getItemsFile().getUnits().findByID(unit.id());
                        boolean tool = unitDefinition instanceof ToolUnitDefinition;
                        TextField emptySlot = getEmptySlotComponent(tool ? tools : soldiers, waveNr);
                        if (emptySlot != null) {
                            emptySlot.setValue(unitDefinition.getLocalizedName(EmpireCloud.getINSTANCE(), EmpireLanguageTag.EN) + "," + unit.getUnitAmount() + (unit.getPercentage() ? "%" : ""));
                        }
                    }
                }
            }
        }
    }

    private TextField getSlotComponent(VerticalLayout unitsLayout, int waveNr, int slotNr) {
        HorizontalLayout component = (HorizontalLayout) unitsLayout.getComponent(waveNr);
        return (TextField) component.getComponent(1 + slotNr);
    }

    @Nullable
    private TextField getEmptySlotComponent(VerticalLayout unitsLayout, int waveNr) {
        HorizontalLayout component = (HorizontalLayout) unitsLayout.getComponent(waveNr);

        for (int slotNr = 0; slotNr < MAX_SLOTS; slotNr++) {
            TextField textField = (TextField) component.getComponent(1 + slotNr);
            if (textField.isEmpty()) {
                return textField;
            }
        }
        return null;
    }

    private ArrayList<AttackWaveConfig> getCurrentWallConfig() {
        ArrayList<AttackWaveConfig> configs = new ArrayList<>();
        addUnits(configs, soldiers, false);
        addUnits(configs, tools, true);
        return configs;
    }

    private void addUnits(ArrayList<AttackWaveConfig> into, VerticalLayout unitsLayout, boolean tool) {
        for (int waveNr = 0; waveNr < MAX_WAVES; waveNr++) {
            if (into.size() <= waveNr) {
                into.add(new AttackWaveConfig(new ArrayList<>()));
            }
            HorizontalLayout component = (HorizontalLayout) unitsLayout.getComponent(waveNr);
            for (int slot = 1; slot < MAX_SLOTS + 1; slot++) {
                TextField slotTextField = (TextField) component.getComponent(slot);
                addUnit(into.get(waveNr).getUnits(), slotTextField, tool);
            }
        }
    }

    private void addUnit(List<AttackUnitConfig> into, TextField slotTextField, boolean tool) {
        String text = slotTextField.getValue();
        if (!text.isEmpty() && text.contains(",")) {
            String[] split = text.split(",");
            String unitIdentifier = split[0];
            int unitAmount = Integer.parseInt(split[1].replace("%", ""));
            boolean percentageAmount = split[1].contains("%");

            AttackUnitConfig config;

            if (StringUtil.isNumeric(unitIdentifier)) {
                config = AttackUnitConfig.Companion.id(Integer.parseInt(unitIdentifier), unitAmount, percentageAmount, tool);
            } else {
                config = AttackUnitConfig.Companion.forName(unitIdentifier, unitAmount, percentageAmount);
            }

            into.add(config);
        }
    }
}
