package pl.polishcivil.fourkingdoms.website.components;

import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.TextField;
import org.apache.logging.log4j.LogManager;

/**
 * Created by polish on 6/3/17.
 */
public class EditableNumberComponent<T> extends TextField {
    public EditableNumberComponent(T bean, ValueProvider<T, Integer> getter, Setter<T, Integer> setter) {
        Integer current = getter.apply(bean);
        setValue(Integer.toString(current));
        addValueChangeListener(change -> {
            try {
                int v = Integer.parseInt(change.getValue());
                setter.accept(bean, v);
            } catch (NumberFormatException ignore) {
            }
        });
        setSizeFull();
    }
}
