package pl.polishcivil.fourkingdoms.website.components;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.CheckBox;

/**
 * Created by polish on 6/1/17.
 */
public class EditableBooleanComponent<T> extends CheckBox {
    private final T bean;

    public EditableBooleanComponent(T bean, ValueProvider<T, Boolean> getter, Setter<T, Boolean> setter) {
        this.bean = bean;
        setValue(getter.apply(bean));
        addValueChangeListener((ValueChangeListener<Boolean>) valueChangeEvent -> {
            setter.accept(bean, valueChangeEvent.getValue());
        });
        setSizeFull();
    }
}
