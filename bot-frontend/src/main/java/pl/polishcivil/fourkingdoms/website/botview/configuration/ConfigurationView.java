package pl.polishcivil.fourkingdoms.website.botview.configuration;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.schedule.config.AttackScheduleConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration;
import pl.polishcivil.fourkingdoms.website.botview.configuration.auth.AuthConfigurationView;
import pl.polishcivil.fourkingdoms.website.botview.configuration.misc.MiscConfigurationView;
import pl.polishcivil.fourkingdoms.website.botview.configuration.schedule.SchedulesConfigurationView;

import java.util.List;

/**
 * Created by Polish Civil on 14.05.2017.
 */
public class ConfigurationView extends VerticalLayout implements IConfigurationView {
    private final Bot bot;
    private final SchedulesConfigurationView scheduleListConfigurationView;
    private final AuthConfigurationView authConfigurationView;
    private final MiscConfigurationView miscConfigurationView;
    private BotConfiguration currentBuffer;

    public ConfigurationView(Bot bot) {
        this.bot = bot;
        setCaption("Configuration");
        TabSheet tabSheet = new TabSheet();
        this.scheduleListConfigurationView = new SchedulesConfigurationView(bot);
        this.authConfigurationView = new AuthConfigurationView(bot);
        this.miscConfigurationView = new MiscConfigurationView(bot);

        tabSheet.addComponent(authConfigurationView);
        tabSheet.addComponent(scheduleListConfigurationView);
        tabSheet.addComponent(miscConfigurationView);

        Button saveButton = new Button("Save & apply");
        saveButton.addClickListener((Button.ClickListener) clickEvent -> {
            BotConfiguration configuration = buildConfiguration();
            bot.getTaskService().reconfigure(configuration);
            Notification.show("Settings applied!", Notification.Type.TRAY_NOTIFICATION);
        });
        saveButton.setWidth("100%");
        addComponent(new VerticalLayout(tabSheet, saveButton));
        setSizeFull();
    }

    private BotConfiguration buildConfiguration() {
        List<AttackScheduleConfig> configs = scheduleListConfigurationView.build();
        if (currentBuffer == null) {
            throw new RuntimeException("Buffer is null!");
        }
        return new BotConfiguration(configs, authConfigurationView.getSelectedAuthCode(), currentBuffer.getUsername(), miscConfigurationView.getSelectedReconnectMinutes());
    }

    @Override
    public void update(BotConfiguration configuration) {
        this.currentBuffer = configuration;
        scheduleListConfigurationView.update(configuration);
        authConfigurationView.update(configuration);
        miscConfigurationView.update(configuration);
    }
}
