package pl.polishcivil.fourkingdoms.website.botview.configuration.misc;

import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration;

import java.util.Arrays;

public class MiscConfigurationView extends VerticalLayout {
    private final Bot bot;
    private final ComboBox<Integer> reconnectComboBox = new ComboBox<>("Reconnect after x minute", Arrays.asList(0, 2, 5, 10, 15));

    public MiscConfigurationView(Bot bot) {
        setCaption("Misc");
        this.bot = bot;
        this.reconnectComboBox.setWidth("100%");
        this.addComponent(reconnectComboBox);


        this.reconnectComboBox.addSelectionListener(new SingleSelectionListener<Integer>() {
            @Override
            public void selectionChange(SingleSelectionEvent<Integer> event) {

            }
        });
    }

    public void update(BotConfiguration configuration) {
        int value = 0;
        if (configuration.getReconnectTimeoutMinutes() > 0) {
            value = configuration.getReconnectTimeoutMinutes() / 60000;
        }
        this.reconnectComboBox.setValue(value);
    }

    public int getSelectedReconnectMinutes() {
        return this.reconnectComboBox.getSelectedItem().orElse(0);
    }
}
