package pl.polishcivil.fourkingdoms.website.botview.configuration.auth;

import com.vaadin.data.HasValue;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.auth.AuthData;
import pl.polishcivil.fourkingdoms.auth.Authentication;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration;
import pl.polishcivil.fourkingdoms.website.utils.WebStringUtils;

public class AuthConfigurationView extends VerticalLayout {
    private final TextField authField = new TextField();

    public AuthConfigurationView(Bot bot) {
        setCaption("Auth");
        authField.setWidth("100%");
        addComponentsAndExpand(authField);

        authField.addValueChangeListener((HasValue.ValueChangeListener<String>) event -> {
            AuthData authData = Authentication.INSTANCE.encodeHash(event.getValue());
            if (authData.isValid() && !authData.getUsernames().isEmpty()) {
                authField.setCaption("Valid util " + authData.validUntilDate().toString());
            } else {
                authField.setCaption("Not valid!");
            }
        });
    }


    public String getSelectedAuthCode() {
        return authField.getValue();
    }

    public void update(BotConfiguration configuration) {
        authField.setValue(configuration.getAuthKey());
    }
}
