package pl.polishcivil.fourkingdoms.website.utils;

import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig;

/**
 * Created by Polish Civil on 14.05.2017.
 */
public class WebStringUtils {
    public static String locationToString(LocationConfig location) {
        return String.format("%s (%d, %d)", location.getKingdom(), location.getX(), location.getY());
    }

    public static String formatTimeMillis(long millis) {
        long hours = millis / 3600000;
        long mins = millis / 60000 % 60;
        long seconds = millis / 1000 % 60;
        return String.format("%02d:%02d:%02d", hours, mins, seconds);
    }

    public static String formatTimeMillisLong(long millis) {
        long hours = millis / 3600000;
        long mins = millis / 60000 % 60;
        long seconds = millis / 1000 % 60;
        if (millis < 1000) {
            return String.format("%02d:%02d:%02d:%03d", hours, mins, seconds, millis);
        }
        return String.format("%02d:%02d:%02d", hours, mins, seconds);
    }
}
