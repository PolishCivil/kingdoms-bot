package pl.polishcivil.fourkingdoms.website.botview;

import com.vaadin.ui.*;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.universe.event.IEventHandler;
import pl.polishcivil.fourkingdoms.universe.event.impl.BotConfigurationChangedEvent;
import pl.polishcivil.fourkingdoms.universe.event.impl.BotPerceptionUpdateEvent;
import pl.polishcivil.fourkingdoms.universe.event.impl.BotTaskUpdateEvent;
import pl.polishcivil.fourkingdoms.website.BotServlet;
import pl.polishcivil.fourkingdoms.website.botview.configuration.ConfigurationView;
import pl.polishcivil.fourkingdoms.website.botview.development.DevInfoView;
import pl.polishcivil.fourkingdoms.website.botview.perception.PerceptionView;
import pl.polishcivil.fourkingdoms.website.botview.task.TasksView;

/**
 * Created by polish on 5/6/2017.
 */
public class BotView extends Panel {
    private final Bot bot;
    private final PerceptionView perceptionView;
    private final TasksView tasksView;
    private final ConfigurationView configurationView;
    private final DevInfoView devInfoView;

    public BotView(Bot bot) {
        this.bot = bot;
        this.perceptionView = new PerceptionView(bot);
        this.tasksView = new TasksView(bot);
        this.configurationView = new ConfigurationView(bot);
        this.devInfoView = new DevInfoView(bot);
        if (bot.getTaskService().getResolvedConfiguration() != null) {
            this.configurationView.update(bot.getTaskService().getResolvedConfiguration());
        }
        BotServlet.getInstance().getUniverse().getEventBus().registerHandler(BotPerceptionUpdateEvent.class, this::handlePerceptionUpdate);
        BotServlet.getInstance().getUniverse().getEventBus().registerHandler(BotTaskUpdateEvent.class, this::handleTaskUpdate);
        BotServlet.getInstance().getUniverse().getEventBus().registerHandler(BotConfigurationChangedEvent.class, this::handleConfigurationChange);

        VerticalLayout components = new VerticalLayout();

        TabSheet tabsheet = new TabSheet();
        tabsheet.addComponent(perceptionView);

        tabsheet.addComponent(tasksView);
        tabsheet.addComponent(configurationView);
        tabsheet.addComponent(devInfoView);
        components.addComponent(tabsheet);
        setContent(components);
        setSizeFull();
    }

    private void handleConfigurationChange(@NotNull BotConfigurationChangedEvent event) {
        if (event.getBot() == bot) {
            getUI().access(() -> configurationView.update(event.getBot().getTaskService().getResolvedConfiguration()));
        }

    }

    public void handlePerceptionUpdate(@NotNull BotPerceptionUpdateEvent event) {
        if (event.getBot() == bot) {
            getUI().access(() -> {
                perceptionView.update();
                devInfoView.update();
            });
        }
    }

    public void handleTaskUpdate(@NotNull BotTaskUpdateEvent event) {
        if (event.getBot() == bot) {
            getUI().access(() -> {
                tasksView.update();
                devInfoView.update();
            });
        }
    }

    @Override
    public void detach() {
        LogManager.getLogger().debug("Bot view detached...");
        BotServlet.getInstance().getUniverse().getEventBus().deregister((IEventHandler<BotPerceptionUpdateEvent>) this::handlePerceptionUpdate);
        BotServlet.getInstance().getUniverse().getEventBus().deregister((IEventHandler<BotTaskUpdateEvent>) this::handleTaskUpdate);
        BotServlet.getInstance().getUniverse().getEventBus().registerHandler(BotConfigurationChangedEvent.class, this::handleConfigurationChange);
        super.detach();
    }

}
