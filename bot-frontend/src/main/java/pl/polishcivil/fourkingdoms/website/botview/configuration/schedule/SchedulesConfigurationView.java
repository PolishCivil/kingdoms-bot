package pl.polishcivil.fourkingdoms.website.botview.configuration.schedule;

import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.schedule.config.AttackScheduleConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration;

import java.util.List;

/**
 * Created by polish on 6/3/17.
 */
public class SchedulesConfigurationView extends VerticalLayout {
    public AttackScheduleConfig currentSelectedSchedule = null;
    public AttackConfig currentSelectedAttack = null;

    public final ScheduleListConfigurationView schedulesView;
    public final AttackListConfigurationView attacksView;
    public final AttackArmyConfigurationView armyView;

    public SchedulesConfigurationView(Bot bot) {
        setCaption("Schedules");
        this.schedulesView = new ScheduleListConfigurationView(bot, this);
        this.attacksView = new AttackListConfigurationView(bot, this);
        this.armyView = new AttackArmyConfigurationView(bot, this);
        this.attacksView.setEnabled(false);
        this.armyView.setEnabled(false);
        addComponentsAndExpand(schedulesView, attacksView, armyView);
    }


    public List<AttackScheduleConfig> build() {
        return schedulesView.build();
    }

    public void update(BotConfiguration configuration) {
        schedulesView.update(configuration);
    }
}
