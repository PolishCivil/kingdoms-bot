package pl.polishcivil.fourkingdoms.website.botview.perception.map;

import com.vaadin.ui.*;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.game.KingdomData;
import pl.polishcivil.fourkingdoms.bot.model.game.army.ArmyData;
import pl.polishcivil.fourkingdoms.bot.model.game.map.MapMovementVO;
import pl.polishcivil.fourkingdoms.website.utils.WebStringUtils;

import java.util.List;

/**
 * Created by Polish Civil on 13.05.2017.
 */
public class MapView extends VerticalLayout {
    private final Bot bot;
    private final Grid<MapMovementVO> movesGrid = new Grid<>();
    private final Label currentKingdomLabel = new Label();

    public MapView(Bot bot) {
        this.bot = bot;
        setCaption("Map");
        currentKingdomLabel.setCaption("Current kingdom");
        movesGrid.addColumn((v) -> WebStringUtils.locationToString(v.getSource())).setCaption("Source");
        movesGrid.addColumn((v) -> WebStringUtils.locationToString(v.getTarget())).setCaption("Target");
        movesGrid.addColumn(MapMovementVO::getType).setCaption("Type");
        movesGrid.addColumn((v) -> WebStringUtils.formatTimeMillis(v.getTimeReachTargetMillis())).setCaption("Reach target in hh:mm:ss");
        movesGrid.addColumn((v) -> v.getMyMovement() ? "Yes" : "No").setCaption("My movement?");


        movesGrid.setCaption("Current moves");
        movesGrid.setSizeFull();
        addComponent(currentKingdomLabel);
        addComponent(movesGrid);
    }


    public void update() {
        ArmyData armyData = bot.getInternalDataPerception().getArmyData();
        KingdomData currentKingdomData = bot.getInternalDataPerception().getCurrentKingdomData();
        if (armyData != null && currentKingdomData != null) {
            List<MapMovementVO> moves = armyData.getMoves();
            movesGrid.setItems(moves);
            currentKingdomLabel.setValue(currentKingdomData.getActiveKingdom().name());
        }
    }


}
