package pl.polishcivil.fourkingdoms.website.botview.configuration.schedule;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.EditorCancelEvent;
import com.vaadin.ui.components.grid.EditorCancelListener;
import com.vaadin.ui.components.grid.EditorSaveEvent;
import com.vaadin.ui.components.grid.EditorSaveListener;
import com.vaadin.ui.renderers.ComponentRenderer;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.schedule.config.AttackScheduleConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration;
import pl.polishcivil.fourkingdoms.website.botview.configuration.IConfigurationView;
import pl.polishcivil.fourkingdoms.website.components.EditableBooleanComponent;
import pl.polishcivil.fourkingdoms.website.components.EditableTextRenderer;
import pl.polishcivil.fourkingdoms.website.utils.VaadinUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Polish Civil on 29.05.2017.
 */
public class ScheduleListConfigurationView extends VerticalLayout implements IConfigurationView, EditorSaveListener<AttackScheduleConfig>, EditorCancelListener<AttackScheduleConfig>, SelectionListener<AttackScheduleConfig> {
    private final Bot bot;
    private final Grid<AttackScheduleConfig> configGrid = new Grid<>();

    private final ListDataProvider<AttackScheduleConfig> scheduleConfigListDataProvider = new ListDataProvider<>(new ArrayList<>());
    private final SchedulesConfigurationView root;

    public ScheduleListConfigurationView(Bot bot, SchedulesConfigurationView root) {
        this.bot = bot;
        this.root = root;
        setScheduleGridColumns();
        configGrid.setDataProvider(scheduleConfigListDataProvider);
        configGrid.getEditor().addSaveListener(this);
        configGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        configGrid.addSelectionListener(this);
        configGrid.setSizeFull();

        addComponent(configGrid);
        setSizeFull();
    }

    private void setScheduleGridColumns() {
        VaadinUtils.makeEditable(configGrid, AttackScheduleConfig.class);
        configGrid.addColumn(v -> new EditableTextRenderer<>(v, AttackScheduleConfig::getName, AttackScheduleConfig::setName)).setCaption("Name").setRenderer(new ComponentRenderer()).setStyleGenerator((v) -> "v-align-center");
        configGrid.addColumn(v -> new EditableBooleanComponent<>(v, AttackScheduleConfig::getEnabled, AttackScheduleConfig::setEnabled)).setCaption("Enabled").setRenderer(new ComponentRenderer()).setStyleGenerator((v) -> "v-align-center");
    }

    @Override
    public void update(BotConfiguration configuration) {
        update(configuration.getScheduleConfigs());
    }

    void update(List<AttackScheduleConfig> list) {
        scheduleConfigListDataProvider.getItems().clear();
        scheduleConfigListDataProvider.refreshAll();
        scheduleConfigListDataProvider.getItems().addAll(list);
        scheduleConfigListDataProvider.refreshAll();
        configGrid.deselectAll();
        root.currentSelectedSchedule = null;
    }

    public List<AttackScheduleConfig> build() {
        if (root.currentSelectedSchedule != null) {
            List<AttackConfig> build = root.attacksView.build();
            root.currentSelectedSchedule.setAttacks(build);
            configGrid.getDataProvider().refreshAll();
        }
        return new ArrayList<>(scheduleConfigListDataProvider.getItems());
    }

    @Override
    public void onEditorSave(EditorSaveEvent<AttackScheduleConfig> editorSaveEvent) {
        Grid<AttackScheduleConfig> grid = editorSaveEvent.getGrid();
        grid.getDataProvider().refreshAll();
        grid.setDataProvider(grid.getDataProvider());
    }

    @Override
    public void onEditorCancel(EditorCancelEvent<AttackScheduleConfig> editorCancelEvent) {

    }

    @Override
    public void selectionChange(SelectionEvent<AttackScheduleConfig> selectionEvent) {
        if (root.currentSelectedSchedule != null) {
            List<AttackConfig> build = root.attacksView.build();
            root.currentSelectedSchedule.setAttacks(build);
        }
        root.currentSelectedSchedule = selectionEvent.getFirstSelectedItem().orElse(null);
        root.attacksView.setEnabled(root.currentSelectedSchedule != null);
        root.attacksView.update(selectionEvent.getFirstSelectedItem().map(AttackScheduleConfig::getAttacks).orElse(Collections.emptyList()));
    }
}
