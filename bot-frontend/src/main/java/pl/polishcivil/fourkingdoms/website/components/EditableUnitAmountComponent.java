package pl.polishcivil.fourkingdoms.website.components;

import com.vaadin.ui.TextField;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig;

/**
 * Created by polish on 6/3/17.
 */
public class EditableUnitAmountComponent extends TextField {
    public EditableUnitAmountComponent(AttackUnitConfig config) {
        addValueChangeListener(change -> {
            String value = change.getValue();
            if (matchesPattern(value)) {
                boolean percentage = value.endsWith("%");
                int amount = Integer.parseInt(value.replace("%", ""));
                config.setPercentage(percentage);
                config.setUnitAmount(amount);
            } else {
                setValue(change.getOldValue());
            }
        });
        setValue(config.getUnitAmount() + (config.getPercentage() ? "%" : ""));
        setSizeFull();
    }

    private boolean matchesPattern(String value) {
        try {
            Integer.parseInt(value.replace("%", ""));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
