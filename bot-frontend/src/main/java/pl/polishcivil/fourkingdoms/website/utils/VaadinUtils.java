package pl.polishcivil.fourkingdoms.website.utils;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.shared.ui.dnd.DropEffect;
import com.vaadin.shared.ui.dnd.EffectAllowed;
import com.vaadin.shared.ui.grid.DropLocation;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.EditorSaveEvent;
import com.vaadin.ui.components.grid.GridDragSource;
import com.vaadin.ui.components.grid.GridDropTarget;
import com.vaadin.ui.renderers.ComponentRenderer;
import org.apache.logging.log4j.LogManager;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig;
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Polish Civil on 30.05.2017.
 */
@SuppressWarnings("ALL")
public class VaadinUtils {
    @SuppressWarnings("unchecked")
    public static void gridRefresh(EditorSaveEvent editorSaveEvent) {
        DataProvider dataProvider = editorSaveEvent.getGrid().getDataProvider();
        editorSaveEvent.getGrid().setDataProvider(dataProvider);
    }

    public static void makeDragable(Grid grid) {
        GridDragSource dragSource = new GridDragSource<>(grid);
        final Set[] draggedItems = {new HashSet()};

        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        dragSource.setEffectAllowed(EffectAllowed.MOVE);
        dragSource.addGridDragStartListener(event ->
                // Keep reference to the dragged items
                draggedItems[0] = new HashSet(event.getDraggedItems())
        );
        GridDropTarget<AttackConfig> dropTarget = new GridDropTarget<>(grid, DropMode.ON_TOP_OR_BETWEEN);
        dropTarget.setDropEffect(DropEffect.MOVE);
        dropTarget.addGridDropListener(event -> {
            // Accepting dragged items from another Grid in the same UI
            event.getDragSourceExtension().ifPresent(source -> {
                if (source instanceof GridDragSource) {

                    // If drop was successful, remove dragged items from source Grid
                    if (event.getDropEffect() == DropEffect.MOVE) {
                        ((ListDataProvider<AttackConfig>) grid.getDataProvider()).getItems()
                                .removeAll(draggedItems[0]);
                        grid.getDataProvider().refreshAll();

                        // Remove reference to dragged items
                    }

                    // Get the target Grid's items
                    ListDataProvider<AttackConfig> dataProvider = (ListDataProvider<AttackConfig>)
                            event.getComponent().getDataProvider();
                    List items = (List) dataProvider.getItems();

                    // Calculate the target row's index
                    int index = items.size();
                    if (event.getDropTargetRow().isPresent()) {
                        index = items.indexOf(event.getDropTargetRow().get()) + (
                                event.getDropLocation() == DropLocation.BELOW ? 1 : 0);
                    }
                    if (index == -1) {
                        index = 0;
                    }
                    // Add dragged items to the target Grid
                    items.addAll(index, draggedItems[0]);
                    dataProvider.refreshAll();
                    draggedItems[0] = null;
                }
            });
        });
    }

    @SuppressWarnings("unchecked")
    public static <T extends Cloneable> void makeEditable(Grid<T> grid, Class<T> beanCls) {
        makeEditable(grid, beanCls, null, null);
    }

    public static <T extends Cloneable> void makeEditable(Grid<T> grid, Class<T> beanCls, Runnable beforeChange, Runnable onChange) {
        //todo change to button renderer
        Grid.Column<T, Button> removeColumn = grid.addColumn(v -> {
            Button button = new Button("-");
            button.setSizeFull();
            button.addClickListener((Button.ClickListener) clickEvent -> {
                if (beforeChange != null) {
                    beforeChange.run();
                }
                List<T> collect = grid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
                ((ListDataProvider<T>) grid.getDataProvider()).getItems().remove(v);
                ((ListDataProvider<T>) grid.getDataProvider()).refreshAll();
                if (onChange != null) {
                    onChange.run();
                }
            });
            return button;
        }).setRenderer(new ComponentRenderer()).setCaption("R").setStyleGenerator((item) -> "v-align-center").setMaximumWidth(96);
        Grid.Column<T, Button> cloneColumn = grid.addColumn(v -> {
            Button button = new Button("+");
            button.setSizeFull();
            button.addClickListener((Button.ClickListener) clickEvent -> {
                if (beforeChange != null) {
                    beforeChange.run();
                }
                List<T> collect = grid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
                try {
                    String serialize = JSONUtils.serialize(v);
                    T deserialize = JSONUtils.deserialize(serialize, (Class<T>) v.getClass());
                    collect.add(deserialize);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((ListDataProvider<T>) grid.getDataProvider()).getItems().clear();
                ((ListDataProvider<T>) grid.getDataProvider()).getItems().addAll(collect);
                grid.getDataProvider().refreshAll();
                if (onChange != null) {
                    onChange.run();
                }
            });
            return button;
        }).setRenderer(new ComponentRenderer()).setCaption("C").setStyleGenerator((item) -> "v-align-center").setMaximumWidth(96);
        Button addNewButton = new Button("+");
        addNewButton.addClickListener((Button.ClickListener) clickEvent -> {
            if (beforeChange != null) {
                beforeChange.run();
            }
            List<T> collect = grid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
            try {
                T t = beanCls.newInstance();
                collect.add(t);
            } catch (Exception e) {
                LogManager.getLogger().catching(e);
            }
            ((ListDataProvider<T>) grid.getDataProvider()).getItems().clear();
            ((ListDataProvider<T>) grid.getDataProvider()).getItems().addAll(collect);
            grid.getDataProvider().refreshAll();
            if (onChange != null) {
                onChange.run();
            }
        });
        addNewButton.setSizeFull();
        grid.prependHeaderRow().join(removeColumn, cloneColumn).setComponent(addNewButton);
    }
}
