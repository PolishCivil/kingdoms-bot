package pl.polishcivil.fourkingdoms.website;

import com.vaadin.annotations.VaadinServletConfiguration;
import pl.polishcivil.fourkingdoms.universe.BotServer;
import pl.polishcivil.fourkingdoms.universe.BotUniverse;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;

import javax.servlet.annotation.WebServlet;

/**
 * Created by polish on 5/6/2017.
 */

public class BotServlet {
    private static BotServlet ourInstance = new BotServlet();

    private final BotUniverse universe = new BotUniverse();

    public static BotServlet getInstance() {
        return ourInstance;
    }

    private BotServlet() {
        EmpireCloud.getINSTANCE();
        new BotServer(universe).start(8090);
    }


    public BotUniverse getUniverse() {
        return universe;
    }
}
