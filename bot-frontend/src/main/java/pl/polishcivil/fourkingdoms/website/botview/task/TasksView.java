package pl.polishcivil.fourkingdoms.website.botview.task;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig;
import pl.polishcivil.fourkingdoms.bot.perception.impl.bot.BotPerception;
import pl.polishcivil.fourkingdoms.bot.task.impl.attack.AttackTask;
import pl.polishcivil.fourkingdoms.bot.task.impl.attack.schedule.ScheduleTask;
import pl.polishcivil.fourkingdoms.website.utils.WebStringUtils;

import java.util.List;

/**
 * Created by Polish Civil on 14.05.2017.
 */
public class TasksView extends VerticalLayout {
    private final Bot bot;

    private final Button serviceStatusButton = new Button();
    private final Grid<AttackTask> pendingAttacksGrid = new Grid<>();
    private final Label currentAttackLabel = new Label();

    public TasksView(Bot bot) {
        this.bot = bot;
        setCaption("Tasks");
        serviceStatusButton.setCaption("Service status: unknown");
        pendingAttacksGrid.setCaption("Pending attacks:");
        currentAttackLabel.setCaption("Current attack:");
        serviceStatusButton.addClickListener((e) -> {
            BotPerception perception = bot.getPerception();
            switch (perception.getTaskServiceStatus()) {
                case RESOLVING_BOT:
                case RUNNING:
                    bot.getTaskService().pause();
                    break;
                case PAUSED:
                    bot.getTaskService().resume();
                    break;
            }
        });
        addComponent(serviceStatusButton);
        addComponent(pendingAttacksGrid);
        addComponent(currentAttackLabel);

        pendingAttacksGrid.addColumn(v -> WebStringUtils.locationToString(v.getConfiguration().getSource())).setCaption("Source");
        pendingAttacksGrid.addColumn(v -> WebStringUtils.locationToString(v.getConfiguration().getTarget())).setCaption("Target");
        pendingAttacksGrid.addColumn(v -> (v.resolvedState())).setCaption("State");
        pendingAttacksGrid.addColumn(v -> (v.getErrorOccurred() ? "Yes" : "No")).setCaption("Error occured");
        pendingAttacksGrid.setWidth("100%");
    }

    public void update() {
        BotPerception perception = bot.getPerception();
        serviceStatusButton.setCaption("Service status: " + perception.getTaskServiceStatus().name());

        ScheduleTask currentSchedule = perception.getCurrentSchedule();
        if (currentSchedule != null) {
            List<AttackTask> pendingAttacks = currentSchedule.getAttackTasks();
            pendingAttacksGrid.setItems(pendingAttacks);
            pendingAttacksGrid.setCaption("Pending attacks for schedule (" + currentSchedule.getConfiguration().getName() + ")");
            AttackTask currentAttack = currentSchedule.currentAttack();

            LocationConfig source = currentAttack.getConfiguration().getSource();
            LocationConfig target = currentAttack.getConfiguration().getTarget();
            currentAttackLabel.setValue(WebStringUtils.locationToString(source) + " -> " + WebStringUtils.locationToString(target) +
                    " (status = " + currentAttack.resolvedState() + " error occurred = " + (currentAttack.getErrorOccurred() ? "yes" : "no") + ")");
            pendingAttacksGrid.select(currentAttack);
        } else {
            pendingAttacksGrid.setCaption("No schedule active");
            pendingAttacksGrid.setItems();
            currentAttackLabel.setValue("none");
            pendingAttacksGrid.deselectAll();
        }
        pendingAttacksGrid.markAsDirty();
    }
}
