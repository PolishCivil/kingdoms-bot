package pl.polishcivil.fourkingdoms.website.botview.perception;

import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.website.botview.perception.castle.CastlesView;
import pl.polishcivil.fourkingdoms.website.botview.perception.map.MapView;
import pl.polishcivil.fourkingdoms.website.botview.perception.player.PlayerInfoView;

/**
 * Created by Polish Civil on 14.05.2017.
 */
public class PerceptionView extends VerticalLayout {
    private final MapView mapView;
    private final PlayerInfoView playerInfoView;
    private final CastlesView castlesView;

    public PerceptionView(Bot bot) {
        this.mapView = new MapView(bot);
        this.playerInfoView = new PlayerInfoView(bot);
        this.castlesView = new CastlesView(bot);
        TabSheet components = new TabSheet();

        components.addComponent(mapView);
        components.addComponent(playerInfoView);
        components.addComponent(castlesView);
        addComponent(components);
        setCaption("Perception");
    }

    public void update() {
        this.mapView.update();
        this.playerInfoView.update();
        this.castlesView.update();
    }
}
