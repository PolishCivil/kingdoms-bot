package pl.polishcivil.fourkingdoms.website.botview.configuration.schedule;

import com.vaadin.data.provider.Query;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.Setter;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ComponentRenderer;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig;
import pl.polishcivil.fourkingdoms.bot.model.game.HorseOption;
import pl.polishcivil.fourkingdoms.bot.model.game.LocationConfig;
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleUserData;
import pl.polishcivil.fourkingdoms.website.components.EditableBooleanComponent;
import pl.polishcivil.fourkingdoms.website.components.EditableNumberComponent;
import pl.polishcivil.fourkingdoms.website.components.EditableSelectionComponent;
import pl.polishcivil.fourkingdoms.website.utils.VaadinUtils;
import pl.polishcivil.projects.jkingdoms.core.data.types.Kingdom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Polish Civil on 30.05.2017.
 */
public class AttackListConfigurationView extends VerticalLayout implements SelectionListener<AttackConfig> {
    private final Bot bot;
    private final SchedulesConfigurationView root;
    private final Grid<AttackConfig> grid = new Grid<>();

    public AttackListConfigurationView(Bot bot, SchedulesConfigurationView root) {
        this.bot = bot;
        this.root = root;
        setMargin(new MarginInfo(false, false, false, false));

        VaadinUtils.makeEditable(grid, AttackConfig.class, () -> {
        }, () -> {
            if (root.currentSelectedAttack != null) {
                root.currentSelectedAttack.setWaveConfigs(root.armyView.build());
            }
            if (root.currentSelectedSchedule != null) {
                root.currentSelectedSchedule.setAttacks(build());
            }
        });

        grid.addColumn(v -> {
            List<LocationConfig> ownedLocations = Collections.emptyList();
            CastleUserData castleData = bot.getInternalDataPerception().getCastleData();
            if (castleData != null) {
                ownedLocations = castleData.getOwnedLocations();
            }
            return new EditableSelectionComponent<>(v, AttackConfig::getSource, (Setter<AttackConfig, LocationConfig>) (attackConfig, location) -> {
                attackConfig.setSource(location);
                LocationConfig target = attackConfig.getTarget();
                attackConfig.setTarget(new LocationConfig(location.getKingdom(), target.getX(), target.getY()));
            }, ownedLocations)
                    .setGenerator(location -> String.format("%s (%d, %d)", location.getKingdom(), location.getX(), location.getY()));
        }).setRenderer(new ComponentRenderer()).setMinimumWidth(320).setStyleGenerator((c) -> "v-align-center");

        grid.addColumn(v -> new EditableNumberComponent<>(v, (b) -> b.getTarget().getX(), (b, val) -> {
            Kingdom kingdom = b.getSource().getKingdom();
            int y = b.getTarget().getY();
            b.setTarget(new LocationConfig(kingdom, val, y));
        })).setCaption("Target X").setRenderer(new ComponentRenderer()).setStyleGenerator((item) -> "v-align-center");

        grid.addColumn(v -> new EditableNumberComponent<>(v, (b) -> b.getTarget().getY(), (b, val) -> {
            Kingdom kingdom = b.getSource().getKingdom();
            int x = b.getTarget().getX();
            b.setTarget(new LocationConfig(kingdom, x, val));
        })).setCaption("Target Y").setRenderer(new ComponentRenderer()).setStyleGenerator((item) -> "v-align-center");

        grid.addColumn(v -> new EditableSelectionComponent<>(v, AttackConfig::getHorse, AttackConfig::setHorse, HorseOption.values())).setCaption("Horse").setRenderer(new ComponentRenderer()).setStyleGenerator((item) -> "v-align-center");
        grid.addColumn(v -> new EditableNumberComponent<>(v, AttackConfig::getLeaderOptionNr, AttackConfig::setLeaderOptionNr)).setCaption("Leader nr").setRenderer(new ComponentRenderer()).setStyleGenerator((item) -> "v-align-center");
        grid.addColumn((v) -> new EditableBooleanComponent<>(v, AttackConfig::getSkipCoolDown, AttackConfig::setSkipCoolDown), new ComponentRenderer()).setCaption("Skip cool-down").setStyleGenerator((item) -> "v-align-center");
        VaadinUtils.makeDragable(grid);
        grid.setSizeFull();
        addComponentsAndExpand(grid);
        setSizeFull();
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.getSelectionModel().addSelectionListener(this);
    }


    void update(List<AttackConfig> list) {
        grid.setItems(new ArrayList<>(list));//make editable
        grid.getDataProvider().refreshAll();
    }

    List<AttackConfig> build() {
        if (root.currentSelectedAttack != null) {
            root.currentSelectedAttack.setWaveConfigs(root.armyView.build());
        }
        return grid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
    }

    @Override
    public void selectionChange(SelectionEvent<AttackConfig> selectionEvent) {
        if (root.currentSelectedAttack != null) {
            root.currentSelectedAttack.setWaveConfigs(root.armyView.build());
        }
        root.currentSelectedAttack = selectionEvent.getFirstSelectedItem().orElse(null);
        root.armyView.update(selectionEvent.getFirstSelectedItem().orElse(null));
    }
}
