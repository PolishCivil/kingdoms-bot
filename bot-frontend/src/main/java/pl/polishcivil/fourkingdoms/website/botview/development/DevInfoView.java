package pl.polishcivil.fourkingdoms.website.botview.development;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.website.utils.WebStringUtils;

/**
 * Created by Polish Civil on 15.05.2017.
 */
public class DevInfoView extends VerticalLayout {
    private final Bot bot;
    private final Label perceptionInfoLabel = new Label();
    private final Label dialogsLabel = new Label();
    private final Label panelsLabel = new Label();
    private final Label screensLabel = new Label();

    public DevInfoView(Bot bot) {
        this.bot = bot;
        setCaption("Development");
        dialogsLabel.setCaption("Visible dialogs:");
        panelsLabel.setCaption("Visible panels:");
        perceptionInfoLabel.setCaption("Perception info:");
        screensLabel.setCaption("Screens:");
        addComponent(perceptionInfoLabel);
        addComponent(dialogsLabel);
        addComponent(panelsLabel);
        addComponent(screensLabel);
    }

    public void update() {
        dialogsLabel.setValue(bot.getDisplayPerception().getShownDialogNames().toString());
        panelsLabel.setValue(bot.getDisplayPerception().getShownPanels().toString());
        perceptionInfoLabel.setValue(String.format("Age: %s", WebStringUtils.formatTimeMillisLong(bot.getPerceptionAge())));
        screensLabel.setValue(bot.getDisplayPerception().getShownScreens().toString());
    }
}
