package pl.polishcivil.fourkingdoms.website.botview.configuration.schedule;

import com.google.gson.annotations.JsonAdapter;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.ComponentRenderer;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.Nullable;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackUnitConfig;
import pl.polishcivil.fourkingdoms.bot.model.bot.attack.config.AttackWaveConfig;
import pl.polishcivil.fourkingdoms.bot.model.json.WaveConfigsAdapter;
import pl.polishcivil.fourkingdoms.website.components.EditableBooleanComponent;
import pl.polishcivil.fourkingdoms.website.components.EditableUnitAmountComponent;
import pl.polishcivil.fourkingdoms.website.components.EditableUnitListComponent;
import pl.polishcivil.fourkingdoms.website.utils.VaadinUtils;
import pl.polishcivil.projects.jkingdoms.core.cloud.EmpireCloud;
import pl.polishcivil.projects.jkingdoms.core.data.types.AttackWall;
import pl.polishcivil.projects.jkingdoms.utils.JSONUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Polish Civil on 30.05.2017.
 */
public class AttackArmyConfigurationView extends TabSheet {
    private static final int WAVES = 6;
    private final Bot bot;
    private final SchedulesConfigurationView root;

    private final List<Grid<AttackUnitConfig>> leftSideUnits = new ArrayList<>();
    private final List<Grid<AttackUnitConfig>> rightSideUnits = new ArrayList<>();
    private final List<Grid<AttackUnitConfig>> middleSideUnits = new ArrayList<>();
    private final TextArea currentConfigurationJSON = new TextArea("Current data");
    private final TextArea newConfigurationJSON = new TextArea("New data");

    public AttackArmyConfigurationView(Bot bot, SchedulesConfigurationView root) {
        this.bot = bot;
        this.root = root;
        for (int waveNr = 0; waveNr < WAVES; waveNr++) {
            Grid<AttackUnitConfig> leftGrid = new Grid<>();
            Grid<AttackUnitConfig> rightGrid = new Grid<>();
            Grid<AttackUnitConfig> middleGrid = new Grid<>();
            initUnitGrid(leftGrid);
            initUnitGrid(rightGrid);
            initUnitGrid(middleGrid);
            leftSideUnits.add(leftGrid);
            rightSideUnits.add(rightGrid);
            middleSideUnits.add(middleGrid);
        }
        for (AttackWall wall : AttackWall.values()) {
            if (wall != AttackWall.UNKNOWN) {
                VerticalLayout units = new VerticalLayout();
                units.setCaption(wall.name());
                TabSheet wavesSheet = new TabSheet();
                for (int waveNr = 0; waveNr < WAVES; waveNr++) {
                    VerticalLayout waveLayout = new VerticalLayout();
                    waveLayout.setCaption("Wave " + (waveNr + 1));
                    Grid<AttackUnitConfig> grid = getUnitsGrid(wall, waveNr);

                    Button addUnitButton = new Button("Add");
                    addUnitButton.setSizeFull();
                    addUnitButton.addClickListener((Button.ClickListener) clickEvent -> {
                        List<AttackUnitConfig> collect = grid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
                        collect.add(new AttackUnitConfig("Scaling ladder", 1, true, true));
                        grid.setItems(new ArrayList<>(collect));
                        updateJSON();
                    });
                    VerticalLayout layout = new VerticalLayout(grid, addUnitButton);
                    layout.setSizeFull();
                    waveLayout.addComponent(layout);
                    waveLayout.setSizeFull();
                    wavesSheet.addComponent(waveLayout);
                }
                units.addComponent(wavesSheet);
                addComponent(units);
            }
        }
        Button refreshButton = new Button("Update current");
        refreshButton.addClickListener((e) -> updateJSON());
        Button setButton = new Button("Apply raw");
        setButton.addClickListener((e) -> {
            String value = newConfigurationJSON.getValue();
            try {
                ArmyConfigWrapper deserialize = JSONUtils.deserialize(value, ArmyConfigWrapper.class);
                update(deserialize.waveConfigs);
                Notification.show("Updated army configuration!");
            } catch (Exception error) {
                Notification.show("Error on deserialization", Notification.Type.ERROR_MESSAGE);
                LogManager.getLogger().catching(error);
            }
        });
        currentConfigurationJSON.setSizeFull();
        newConfigurationJSON.setSizeFull();
        VerticalLayout loadSave = new VerticalLayout(refreshButton, currentConfigurationJSON, setButton, newConfigurationJSON);
        loadSave.setSizeFull();
        loadSave.setCaption("Raw edit");
        addComponent(loadSave);
        setSizeFull();
        updateJSON();
    }

    private void updateJSON() {
        ArmyConfigWrapper wrapper = new ArmyConfigWrapper();
        wrapper.waveConfigs = build();
        String json = JSONUtils.serialize(wrapper);
        currentConfigurationJSON.setValue(json);
    }

    private void initUnitGrid(Grid<AttackUnitConfig> grid) {
        VaadinUtils.makeEditable(grid, AttackUnitConfig.class, () -> {

        }, () -> {
            if (root.currentSelectedAttack != null) {
                root.currentSelectedAttack.setWaveConfigs(build());
            }
            updateJSON();
        });

        grid.setSizeFull();
        grid.addColumn(v -> new EditableUnitListComponent<>(v, AttackUnitConfig::getIdentifier, AttackUnitConfig::setIdentifier, EmpireCloud.getINSTANCE())).setCaption("Identifier").setRenderer(new ComponentRenderer()).setStyleGenerator((v) -> "v-align-center");
        grid.addColumn(v -> new EditableBooleanComponent<>(v, AttackUnitConfig::getTool, AttackUnitConfig::setTool)).setCaption("Tool").setRenderer(new ComponentRenderer()).setStyleGenerator((v) -> "v-align-center");
        grid.addColumn(EditableUnitAmountComponent::new).setCaption("Amount").setRenderer(new ComponentRenderer()).setStyleGenerator((v) -> "v-align-center");
        grid.setDataProvider(new ListDataProvider<>(new ArrayList<>()));
    }

    private Grid<AttackUnitConfig> getUnitsGrid(AttackWall wall, int waveNr) {
        switch (wall) {
            case LEFT:
                return leftSideUnits.get(waveNr);
            case MIDDLE:
                return middleSideUnits.get(waveNr);
            case RIGHT:
                return rightSideUnits.get(waveNr);
        }
        throw new RuntimeException(":( " + wall + " " + waveNr);
    }

    Map<AttackWall, List<AttackWaveConfig>> build() {
        HashMap<AttackWall, List<AttackWaveConfig>> out = new HashMap<>();
        for (AttackWall attackWall : AttackWall.values()) {
            if (attackWall != AttackWall.UNKNOWN) {
                ArrayList<AttackWaveConfig> waves = new ArrayList<>();
                for (int waveNr = 0; waveNr < WAVES; waveNr++) {
                    List<AttackUnitConfig> units = getUnitsGrid(attackWall, waveNr).getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
                    AttackWaveConfig waveConfig = new AttackWaveConfig(units);
                    waves.add(waveConfig);
                }
                out.put(attackWall, waves);
            }
        }
        return out;
    }

    void update(Map<AttackWall, List<AttackWaveConfig>> config) {
        for (AttackWall wall : AttackWall.values()) {
            if (wall == AttackWall.UNKNOWN) {
                continue;
            }
            List<AttackWaveConfig> waveConfigs = config == null ? Collections.emptyList() : config.get(wall);
            if (waveConfigs == null) {
                waveConfigs = Collections.emptyList();
            }
            for (int waveNr = 0; waveNr < WAVES; waveNr++) {
                Grid<AttackUnitConfig> grid = getUnitsGrid(wall, waveNr);
                if (waveConfigs.size() <= waveNr) {
                    grid.setItems(new ArrayList<>());
                    grid.getDataProvider().refreshAll();
                } else {
                    AttackWaveConfig waveConfig = waveConfigs.get(waveNr);
                    grid.setItems(new ArrayList<>(waveConfig.getUnits()));
                    grid.getDataProvider().refreshAll();
                }
            }
        }
        updateJSON();
    }

    void update(@Nullable AttackConfig config) {
        setEnabled(config != null);
        update(config == null ? new HashMap<>() : config.getWaveConfigs());
    }


    public static class ArmyConfigWrapper {
        @JsonAdapter(WaveConfigsAdapter.class)
        public Map<AttackWall, List<AttackWaveConfig>> waveConfigs;
    }
}
