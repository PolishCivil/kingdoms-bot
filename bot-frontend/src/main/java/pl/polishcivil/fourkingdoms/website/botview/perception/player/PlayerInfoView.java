package pl.polishcivil.fourkingdoms.website.botview.perception.player;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.game.player.CastleUserData;
import pl.polishcivil.fourkingdoms.bot.model.game.player.PlayerInfoModel;

/**
 * Created by Polish Civil on 14.05.2017.
 */
public class PlayerInfoView extends VerticalLayout {
    private final Bot bot;
    private final Label usernameLabel = new Label();
    private final Label emailLabel = new Label();

    public PlayerInfoView(Bot bot) {
        this.bot = bot;
        setCaption("Player");
        usernameLabel.setCaption("Username");
        emailLabel.setCaption("Email");
        addComponent(usernameLabel);
        addComponent(emailLabel);
    }

    public void update() {
        PlayerInfoModel player = bot.getInternalDataPerception().getPlayer();
        if (player != null) {
            usernameLabel.setValue(player.getUserName());
            emailLabel.setValue(player.getEmail());

        }
    }
}
