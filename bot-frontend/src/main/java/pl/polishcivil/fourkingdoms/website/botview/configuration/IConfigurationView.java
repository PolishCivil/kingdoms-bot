package pl.polishcivil.fourkingdoms.website.botview.configuration;

import pl.polishcivil.fourkingdoms.bot.model.bot.config.BotConfiguration;

/**
 * Created by Polish Civil on 29.05.2017.
 */
public interface IConfigurationView {
    public void update(BotConfiguration configuration);
}
