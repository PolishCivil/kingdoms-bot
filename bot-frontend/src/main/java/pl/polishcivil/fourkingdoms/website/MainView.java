package pl.polishcivil.fourkingdoms.website;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.data.HasValue;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import pl.polishcivil.fourkingdoms.auth.AuthData;
import pl.polishcivil.fourkingdoms.auth.Authentication;
import pl.polishcivil.fourkingdoms.bot.Bot;
import pl.polishcivil.fourkingdoms.bot.model.game.player.PlayerInfoModel;
import pl.polishcivil.fourkingdoms.universe.event.BotEvent;
import pl.polishcivil.fourkingdoms.universe.event.IEventHandler;
import pl.polishcivil.fourkingdoms.universe.event.impl.BotConnectedEvent;
import pl.polishcivil.fourkingdoms.universe.event.impl.BotDisconnectedEvent;
import pl.polishcivil.fourkingdoms.website.botview.BotView;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by polish on 5/6/2017.
 */

@PreserveOnRefresh
@Theme("valo-dark")
public class MainView extends UI implements IEventHandler<BotEvent> {
    final Grid<Bot> botGrid = new Grid<>();
    final TextField authTextField = new TextField();

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setPollInterval(1000);
        HorizontalSplitPanel layout = new HorizontalSplitPanel();
        layout.setSplitPosition(20, Unit.PERCENTAGE);
        authTextField.addValueChangeListener((HasValue.ValueChangeListener<String>) event -> {
            AuthData encoded = Authentication.INSTANCE.encodeHash(event.getValue());
            List<String> usernames = encoded.getUsernames();
            if (!usernames.isEmpty()) {
                boolean plural = usernames.size() > 1;
                authTextField.setCaption("Valid for " + usernames.size() + " account" + (plural ? "s" : ""));
            } else {
                authTextField.setCaption("not valid");
            }
            setEditableBotsDataProvider(encoded);
        });
        botGrid.addColumn((v) -> {
            PlayerInfoModel info = v.getInternalDataPerception().getPlayer();
            return info == null ? "n/a" : info.getUserName();
        }).setCaption("Username").setId("Username");
        botGrid.addColumn(v -> v.getConnection().getConnection().getRemoteAddress().getHostAddress()).setCaption("Device").setId("Device");

        botGrid.addSelectionListener((SelectionListener<Bot>) multiSelectionEvent -> {
            Optional<Bot> firstSelectedItem = multiSelectionEvent.getFirstSelectedItem();
            if (firstSelectedItem.isPresent()) {
                LogManager.getLogger().debug("Selected bot {}", firstSelectedItem.get());
                layout.setSecondComponent(new BotView(firstSelectedItem.get()));
            } else {
                layout.setSecondComponent(null);
            }
        });
        VerticalLayout components = new VerticalLayout();
        authTextField.setWidth("100%");
        components.addComponent(authTextField);
        components.addComponentsAndExpand(botGrid);

        layout.setFirstComponent(components);
        botGrid.prependHeaderRow().join(botGrid.getColumns().stream().map(Grid.Column::getId).toArray(String[]::new)).setText("Connected bots");
        botGrid.setSizeFull();

        layout.setSizeFull();

        setContent(layout);

        BotServlet.getInstance().getUniverse().getEventBus().registerHandler(BotDisconnectedEvent.class, this);
        BotServlet.getInstance().getUniverse().getEventBus().registerHandler(BotConnectedEvent.class, this);
    }

    private void setEditableBotsDataProvider(AuthData data) {
        botGrid.setDataProvider(DataProvider.fromCallbacks
                ((CallbackDataProvider.FetchCallback<Bot, Void>) query -> filteredBots(data),
                        (CallbackDataProvider.CountCallback<Bot, Void>) query -> (int) filteredBots(data).count()));
    }

    private Stream<Bot> filteredBots(AuthData data) {
        return BotServlet.getInstance().getUniverse().getConnectedBots().stream().filter(it -> {
            PlayerInfoModel player = it.getGamePerception().getInternal().getPlayer();
            if (player != null) {
                String userName = player.getUserName();
                return data.getUsernames().stream().anyMatch(userName::equalsIgnoreCase);
            }
            return false;
        });
    }

    @Override
    public void handle(@NotNull BotEvent event) {
        botGrid.getDataProvider().refreshAll();
    }


    @Override
    public void detach() {
        BotServlet.getInstance().getUniverse().getEventBus().deregister(this);
        BotServlet.getInstance().getUniverse().getEventBus().deregister(this);
        super.detach();
    }
}
