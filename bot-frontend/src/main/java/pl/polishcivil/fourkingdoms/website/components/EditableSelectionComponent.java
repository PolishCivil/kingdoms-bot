package pl.polishcivil.fourkingdoms.website.components;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.Setter;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by polish on 6/1/17.
 */
public class EditableSelectionComponent<T, V> extends ComboBox<V> {
    private final T bean;

    public EditableSelectionComponent(T bean, ValueProvider<T, V> getter, Setter<T, V> setter, V... available) {
        this(bean, getter, setter, Arrays.asList(available));
    }

    public EditableSelectionComponent(T bean, ValueProvider<T, V> getter, Setter<T, V> setter, List<V> available) {
        this.bean = bean;
        V selectedLocation = getter.apply(bean);
        ArrayList<V> locations = new ArrayList<>(available);
        if (selectedLocation != null) {
            if (!locations.contains(selectedLocation)) {
                locations.add(selectedLocation);
            }
        }
        setEmptySelectionAllowed(false);
        setDataProvider(new ListDataProvider<V>(locations));
        setSelectedItem(selectedLocation);

        addSelectionListener((SingleSelectionListener<V>) singleSelectionEvent -> singleSelectionEvent.getSelectedItem().ifPresent(v -> {
            setter.accept(bean, v);
        }));
        setSizeFull();
    }

    @SuppressWarnings("unchecked")
    public EditableSelectionComponent<V, T> setGenerator(ItemCaptionGenerator<V> itemCaptionGenerator) {
        setItemCaptionGenerator(itemCaptionGenerator);
        return (EditableSelectionComponent<V, T>) this;
    }
}
